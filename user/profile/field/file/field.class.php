<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * File profile field.
 *
 * @package    profilefield_file
 * @copyright  2014 onwards Shamim Rezaie {@link http://foodle.org}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Class profile_field_file
 *
 * @copyright  2014 onwards Shamim Rezaie {@link http://foodle.org}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once('qrlib.php');
        
class profile_field_file extends profile_field_base {
    
    /**
     * Add fields for editing a file profile field.
     * @param moodleform $mform
     */
    public function edit_field_add($mform) {
        $mform->addElement('filemanager', $this->inputname, format_string($this->field->name), null, $this->get_filemanageroptions());
    }

    /**
     * Overwrite the base class to display the data for this field
     */
    public function display_data() {
        global $CFG,$DB,$OUTPUT;
        // Default formatting.
        $data = parent::display_data();

        $context = context_user::instance($this->userid, MUST_EXIST);
        
        $files = create_qr_code($this->userid,$this->fieldid);
        
        $data = array();
        if($files){
            foreach ($files as $file) {
                $path = '/' . $context->id . '/profilefield_file/files_' . $this->fieldid . '/' .
                        $file->get_itemid() .
                        $file->get_filepath() .
                        $file->get_filename();
                $url = file_encode_url("$CFG->wwwroot/pluginfile.php", $path, true);
                $filename = $file->get_filename();
                //$data[] = html_writer::link($url, $filename);
                $class='';
                $size='100px';
                $attributes = array('src'=>$url, 'alt'=>$filename, 'title'=>$filename, 'class'=>$class, 'width'=>$size, 'height'=>$size);

                // get the image html output 
                $img = html_writer::empty_tag('img', $attributes);
                $link = new moodle_url($CFG->wwwroot . '/user/profile/field/file/qr_popup.php',array('userid'=>$this->userid,'path'=>$path));
                $action = new popup_action('click', $link, 'qr-popup', array('height' => 460, 'width' => 400));
                $data[] = $OUTPUT->action_link($link, $img, $action);
        
            }
        }
        else{
            $data[] = get_string('noposorgassignment','profilefield_file');
        }
        $data = implode('<br />', $data);

        return $data;
    }

    /**
     * Saves the data coming from form
     * @param stdClass $usernew data coming from the form
     * @return mixed returns data id if success of db insert/update, false on fail, 0 if not permitted
     */
    public function edit_save_data($usernew) {
        if (!isset($usernew->{$this->inputname})) {
            // Field not present in form, probably locked and invisible - skip it.
            return;
        }

        $usercontext = context_user::instance($this->userid, MUST_EXIST);
        file_save_draft_area_files($usernew->{$this->inputname}, $usercontext->id, 'profilefield_file', "files_{$this->fieldid}", 0, $this->get_filemanageroptions());
        parent::edit_save_data($usernew);
    }

    /**
     * Sets the default data for the field in the form object
     * @param  moodleform $mform instance of the moodleform class
     */
    public function edit_field_set_default($mform) {
        if ($this->userid !== -1 and $this->userid != 0) {
            $filemanagercontext = context_user::instance($this->userid);
        } else {
            $filemanagercontext = context_system::instance();
        }

        $draftitemid = file_get_submitted_draft_itemid($this->inputname);
        file_prepare_draft_area($draftitemid, $filemanagercontext->id, 'profilefield_file', "files_{$this->fieldid}", 0, $this->get_filemanageroptions());
        $mform->setDefault($this->inputname, $draftitemid);
        $this->data = $draftitemid;
        //we don't want anyone editing this one
        $mform->removeElement($this->inputname);
        parent::edit_field_set_default($mform);
    }

    /**
     * Just remove the field element if locked.
     * @param moodleform $mform instance of the moodleform class
     * @todo improve this
     */
    public function edit_field_set_locked($mform) {
        if (!$mform->elementExists($this->inputname)) {
            return;
        }
        if ($this->is_locked() and !has_capability('moodle/user:update', context_system::instance())) {
            $mform->removeElement($this->inputname);
        }
    }

    /**
     * Hook for child classess to process the data before it gets saved in database
     * @param stdClass $data
     * @param stdClass $datarecord The object that will be used to save the record
     * @return  mixed
     */
    public function edit_save_data_preprocess($data, $datarecord) {
        return 0;  // we set it to zero because this value is actually redaundant
                    // it cannot be set to null or an empty string either because the field's
                    // value will not be shown on user's profile.
    }

    /**
     * Loads a user object with data for this field ready for the edit profile
     * form
     * @param stdClass $user a user object
     */
    public function edit_load_user_data($user) {
        $user->{$this->inputname} = null;   // it should be set to null, otherwise the loaded files will
                                            // get manipulated when $userform->set_data($user) is called
                                            // later in user/edit.php or user/editadvanced.php
    }

    private function get_filemanageroptions() {
        return array(
            'maxfiles' => $this->field->param1,
            'maxbytes' => $this->field->param2,
            'subdirs' => 0,
            'accepted_types' => '*'
        );
    }
    // override the load data method - here we want to create it automatically if empty
    function load_data() {
        global $DB,$USER;

        /// Load the field object
        if (($this->fieldid == 0) or (!($field = $DB->get_record('user_info_field', array('id'=>$this->fieldid))))) {
            $this->field = NULL;
            $this->inputname = '';
        } else {
            $this->field = $field;
            $this->inputname = 'profile_field_'.$field->shortname;
        }

        if (!empty($this->field)) {
            if ($data = $DB->get_record('user_info_data', array('userid'=>$this->userid, 'fieldid'=>$this->fieldid), 'data, dataformat')) {
                $this->data = $data->data;
                $this->dataformat = $data->dataformat;
            } else {
                if(isloggedin() and isset($this->userid) and $this->userid > 0){ // some of the subfunctions only work if user has already logged in, and this gets called during the login
                    //create it
                    $files=create_qr_code($this->userid,$this->fieldid);
                    $usernew = new stdclass();
                    $usernew->id = $this->userid;
                    $usernew->{$this->inputname} = $files[0]->get_id();
                    $this->edit_save_data($usernew);
                    //now try the get data again
                    if ($data = $DB->get_record('user_info_data', array('userid'=>$this->userid, 'fieldid'=>$this->fieldid), 'data, dataformat')) {
                        $this->data = $data->data;
                        $this->dataformat = $data->dataformat;
                    } else {
                        $this->data = $this->field->defaultdata;
                        $this->dataformat = FORMAT_HTML;
                    }
                }
            }
        } else {
            $this->data = NULL;
        }
    }
}
