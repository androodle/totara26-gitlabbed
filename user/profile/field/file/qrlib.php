<?php

require_once($CFG->dirroot . '/user/profile/field/file/phpqrcode/qrlib.php');

function create_qr_code($userid,$fieldid){
    global $CFG,$DB;
    
    // make one if we can: ingredients are id, firstname, lastname, pos, org
    $user = $DB->get_record('user',array('id'=>$userid));
    $sql = "select pa.*,p.fullname as position,o.fullname as organisation from mdl_pos_assignment pa
        inner join mdl_pos p on pa.positionid = p.id
        inner join mdl_org o on pa.organisationid = o.id
        where userid = $userid";
    $user_pos = $DB->get_record_sql($sql);
    if(!$user_pos){
        //dummy one up
        $user_pos = new stdClass();
        $user_pos->organisation = 'Not set';
        $user_pos->position = 'Not set';
    } 
    $context = context_user::instance($userid, MUST_EXIST);
    $component = 'profilefield_file';
    $filearea = "files_{$fieldid}";
    $itemid = 0;
    $filepath = '/';
    
    $PNG_TEMP_DIR = $CFG->dataroot ."/temp/";
    
    $data = "id:$user->id,firstname:$user->firstname,lastname:$user->lastname,organisation:$user_pos->organisation,position:$user_pos->position";
    $qr_physical_filepath = $PNG_TEMP_DIR.'qrcode-'.$user->id.'.png';
    $errorCorrectionLevel = 2;
    $matrixPointSize = 3;
    $margin = 2;
    
    QRcode::png($data, $qr_physical_filepath, $errorCorrectionLevel, $matrixPointSize, $margin);
    
    // the file should now be there, so let's make a moodle file out of it
    $fs = get_file_storage();
    
    $filerecord = array('component' => $component, 'filearea' => $filearea,
                    'contextid' => $context->id, 'itemid' => $itemid, 'filepath' => $filepath,
                    'filename' => basename($qr_physical_filepath));
    $stored_file = $fs->get_file($context->id, $component, $filearea, $itemid, $filepath, basename($qr_physical_filepath));
    if(!$stored_file){
        try{
            $stored_file = $fs->create_file_from_pathname($filerecord, $qr_physical_filepath);
        }
        catch(Exception $e){
            $message = $e->getMessage();
            $message .= "contextid: $context->id component: $component filearea: $filearea itemid: $itemid filepath: $filepath filename:" .basename($qr_physical_filepath) ;
            error_log($message);
        }
    }
    
    //it has to go back as an array to match the get_area_files call return value
    return array($stored_file);
}