<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'profilefield_file', language 'fa'.
 *
 * @package   profilefield_file
 * @copyright 2014 onwards Shamim Rezaie  {@link http://foodle.org}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['forceunique'] = 'آیا مقدار این مشخصه باید غیر تکراری باشد؟';
$string['forceunique_error'] = 'مقدار این گزینه باید روی «خیر» قرار داده شود.';
$string['forceunique_help'] = 'این گزینه برای مشخصهٔ از نوع فایل کاربردی ندارد. بر روی گزینهٔ «خیر» قفل شده است.';
$string['pluginname'] = 'ارسال فایل';
$string['signup'] = 'در صفحهٔ عضویت نمایش داده شود؟';
$string['signup_error'] = 'بنا به دلایل امنیتی این گزینه باید بر روی «خیر» قرار داده شود.';
$string['signup_help'] = 'به دلیل مسائل امنیتی، مشخصه‌های از نوع فایل نمی‌توانند در صفحهٔ عضویت نمایش داده شوند. مودل به هیچ عنوان اجازهٔ ارسال فایل توسط کاربر مهمان یا کاربرانی که با استفاده از نام کاربری خود به‌طور کامل وارد سایت نشده‌اند را نمی‌دهد.';
