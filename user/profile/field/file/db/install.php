<?php

/**
 * Androgogic QR code: Install
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     14/10/2014
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 * */
function xmldb_profilefield_file_install() {
    global $CFG, $DB;
    $result = true;

    $sql = "SELECT * FROM mdl_user_info_field WHERE shortname = 'QRcode'";
    $existing = $DB->get_record_sql($sql);
    if (!$existing) {
        $sql = "INSERT INTO `mdl_user_info_field`
        (`shortname`,`name`,`datatype`,
        `descriptionformat`,`categoryid`,`sortorder`,
        `required`,`locked`,`visible`,
        `forceunique`,`signup`,`defaultdata`,
        `defaultdataformat`,`param1`,`param2`
        )
        VALUES
        ('QRcode','QR code','file'
        '',1,1,1,
        0,0,2,0,0,
        '',0,1,234881024
        );";
        $DB->execute($sql);
    }
    
    return $result;
}


    