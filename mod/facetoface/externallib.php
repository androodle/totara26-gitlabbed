<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * External facetoface API
 *
 * @package    mod_facetoface
 * @copyright  2012 Mark Nelson <markn@moodle.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

require_once("$CFG->libdir/externallib.php");

class mod_facetoface_external extends external_api {

    /**
     * Describes the parameters for record_attendance.
     *
     * @return external_external_function_parameters
     * @since Moodle 2.5
     */
    public static function record_attendance_parameters() {
        return new external_function_parameters(
                array('attendance_details' => new external_single_structure(
                    array(
                        'userid' => new external_value(PARAM_INT, 'User id'),
                        'sessionid' => new external_value(PARAM_INT, 'Session id'),
                        'attendance_status' => new external_value(PARAM_INT, 'Did they attend? Must be one of the following: 100(full),90(partial) or 80(noshow)'),
                    )
                )
            )
        );
    }

    /**
     * Records attendance at a physical (face to face) event
     *
     * @param array $courseids the course ids
     * @return array the facetoface details
     * @since Moodle 2.5
     */
    public static function record_attendance($attendance_details = array()) {
        global $CFG, $DB;

        require_once($CFG->dirroot . "/mod/facetoface/lib.php");

        $params = self::validate_parameters(self::record_attendance_parameters(), array('attendance_details' => $attendance_details));
        
        $result = array();
        $result['result'] = 'Fail';
        
        if (empty($params['attendance_details'])) {
            $result['errormessage'] = 'Attendance record could not be created: empty packet';
            return $result;
        } else {
            $attendance_details = $params['attendance_details'];
        }
        
        //log the call
        $attendance_details_obj = new stdClass();
        $attendance_details_obj->user_id = $attendance_details['userid'];
        $attendance_details_obj->session_id = $attendance_details['sessionid'];
        $attendance_details_obj->attendance_status = $attendance_details['attendance_status'];
        $attendance_details_obj->date_created = time();
        
        $attendance_details_obj->id=$DB->insert_record('facetoface_webservice_log',$attendance_details_obj);
        
        
        $good_attendance_statuses = array(MDL_F2F_STATUS_NO_SHOW,MDL_F2F_STATUS_PARTIALLY_ATTENDED,MDL_F2F_STATUS_FULLY_ATTENDED); 
        if(!in_array($attendance_details['attendance_status'], $good_attendance_statuses)){
            $result['errormessage'] = 'Unknown attendance status: accepted status codes are: ' . implode(',', $good_attendance_statuses);
            $attendance_details_obj->result = $result['result'];
            $attendance_details_obj->error_message = $result['errormessage'];
            $DB->update_record('facetoface_webservice_log',$attendance_details_obj);
            return $result;
        }
        
        //let's see if we can identify the user,session and signup
        $user = $DB->get_record('user',array('id'=>$attendance_details['userid']));
        if(!$user){
            $result['errormessage'] = 'Attendance record could not be created: user not found by supplied userid';
            $attendance_details_obj->result = $result['result'];
            $attendance_details_obj->error_message = $result['errormessage'];
            $DB->update_record('facetoface_webservice_log',$attendance_details_obj);
            return $result;
        }
        $session = $DB->get_record('facetoface_sessions',array('id'=>$attendance_details['sessionid']));
        if(!$session){
            $result['errormessage'] = 'Attendance record could not be created: session not found by supplied sessionid';
            $attendance_details_obj->result = $result['result'];
            $attendance_details_obj->error_message = $result['errormessage'];
            $DB->update_record('facetoface_webservice_log',$attendance_details_obj);
            return $result;
        }
        // get the course id of the course that holds the f2f
        $sql = "select f.course from mdl_facetoface f
        inner join mdl_facetoface_sessions fs on f.id = fs.facetoface
        where fs.id = $session->id";

        $course_id = $DB->get_field_sql($sql);

        // is the user enrolled in the course?
        $sql = "select ue.* from mdl_user_enrolments ue
        inner join mdl_enrol e on ue.enrolid = e.id
        where courseid = $course_id and userid = $user->id";

        $results = $DB->get_records_sql($sql);
        $user_enrolled_in_course = false;
        if($results){
            $user_enrolled_in_course = true;
        }
        if(!$user_enrolled_in_course){
            // is the user enrolled in a program that holds the course that holds the f2f?
            $sql = "select pua.* from mdl_prog_user_assignment pua
            inner join mdl_prog_courseset cs on pua.programid = cs.programid
            inner join mdl_prog_courseset_course csc on cs.id = csc.coursesetid
            where csc.courseid = $course_id and pua.userid = $user->id";

            $results = $DB->get_records_sql($sql);

            if(!$results){
                $result['errormessage'] = 'Attendance record could not be created: user was not enrolled in course nor a program that held the course';
                $attendance_details_obj->result = $result['result'];
                $attendance_details_obj->error_message = $result['errormessage'];
                $DB->update_record('facetoface_webservice_log',$attendance_details_obj);
                return $result;
            }
            else{
                //enrol the user in the course
                $manual_enrol = enrol_get_plugin('manual');
                $instances = enrol_get_instances($course_id, false);
                foreach ($instances as $instance) {
                    if ($instance->enrol === 'manual') {
                        $manual_enrol->enrol_user($instance, $user->id);
                        break;
                    }
                }
                
            }
        }
        // if we got here then the user should be enrolled in the course
        $signup = $DB->get_record('facetoface_signups',array('sessionid'=>$attendance_details['sessionid'],'userid'=>$attendance_details['userid']));
        // if they are not signed up then sign them up
        if(!$signup){
            $signup = new stdClass();
            $signup->userid = $user->id;
            $signup->sessionid = $session->id;
            $signup->notificationtype = 3;
            $signup->id = $DB->insert_record('facetoface_signups',$signup);
        }
        //mark their attendance. Have we got a booking for them? if not put one in
        $signup_status_booking = $DB->get_record('facetoface_signups_status',array('signupid'=>$signup->id,'statuscode'=>MDL_F2F_STATUS_BOOKED));
        if(!$signup_status_booking){
            $signup_status_booking = new stdClass();
            $signup_status_booking->signupid = $signup->id;
            $signup_status_booking->statuscode = MDL_F2F_STATUS_BOOKED;
            $signup_status_booking->superceded = 0;
            $signup_status_booking->createdby = 0;
            $signup_status_booking->timecreated = time();
            $signup_status_booking->id = $DB->insert_record('facetoface_signups_status',$signup_status_booking);
        }
        else{
            //if so then supercede it, if it's not that way already
            if($signup_status_booking->superceded != 1){
                $signup_status_booking->superceded = 1;
                $DB->update_record('facetoface_signups_status',$signup_status_booking);
            }
        }
        $signup_status = $DB->get_record('facetoface_signups_status',array('signupid'=>$signup->id,'statuscode'=>$attendance_details['attendance_status']));
        if(!$signup_status){
            $signup_status = new stdClass();
            $signup_status->signupid = $signup->id;
            $signup_status->statuscode = $attendance_details['attendance_status'];
            $signup_status->superceded = 0;
            $signup_status->createdby = 0;
            $signup_status->timecreated = time();
            $signup_status->id = $DB->insert_record('facetoface_signups_status',$signup_status);
        }
        else{
            //make sure it's not superceded
            if($signup_status->superceded != 0){
                $signup_status->superceded = 0;
                $DB->update_record('facetoface_signups_status',$signup_status);
            }
        }
        $result['result'] = 'Success';
        $attendance_details_obj->result = $result['result'];
        $DB->update_record('facetoface_webservice_log',$attendance_details_obj);
        return $result;
    }

    /**
     * Describes the get_facetoface return value.
     *
     * @return external_single_structure
     * @since Moodle 2.5
     */
     public static function record_attendance_returns() {
        new external_single_structure(
            array(
                'result' => new external_value(PARAM_TEXT, 'Success or failure'),
                'errormessage' => new external_value(PARAM_TEXT, 'errormessage if any')
            )
        );
    }

    
}
