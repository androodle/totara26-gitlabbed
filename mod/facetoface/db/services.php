<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Face to face external functions and service definitions.
 *
 * @package    mod_facetoface
 * @copyright  2012 Mark Nelson <markn@moodle.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$functions = array(

    'mod_facetoface_record_attendance' => array(
        'classname' => 'mod_facetoface_external',
        'methodname' => 'record_attendance',
        'classpath' => 'mod/facetoface/externallib.php',
        'description' => 'Allows admin at "face to face" event to record attendance via mobile app.',
        'type' => 'write',
        'capabilities' => 'mod/facetoface:takeattendance'
    )

    
);
$services = array(
        'Moodle mobile web service' => array(
                'functions' => array ('mod_facetoface_record_attendance'), 
                'restrictedusers' => 1, // if 1, the administrator must manually select which user can use this service. 
                                                   // (Administration > Plugins > Web services > Manage services > Authorised users)
                'enabled'=>1, // if 0, then token linked to this service won't work
        )
);