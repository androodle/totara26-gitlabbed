<?php
require('../../../config.php');
global $DB, $USER, $CFG;
require_once($CFG->dirroot . '/repository/lib.php');
require_login();
$even = '';
// BEGIN: Andresco       
$PAGE->requires->js('/totara/core/js/lib/jquery-2.1.0.min.js');
$PAGE->requires->js('/repository/andresco_cmis/js/custom_property.js');
$PAGE->requires->js('/repository/andresco_cmis/js/esprima.js');
$PAGE->requires->js('/repository/andresco_cmis/js/common.js');
$PAGE->requires->js('/repository/andresco_cmis/js/andresco_scafold.js');
$PAGE->requires->js('/totara/core/js/lib/jquery-ui-1.10.4.custom.min.js');
$PAGE->requires->js('/repository/andresco_cmis/js/jquery.loader-0.3.js');
$PAGE->requires->css('/repository/andresco_cmis/css/loader.css');
// END: Andresco

$systemcontext = get_context_instance(CONTEXT_SYSTEM);
$PAGE->set_context($systemcontext);
$PAGE->set_url('/blocks/ui_scafold/add_scafold.php');
//$viewurl = new moodle_url('/blocks/ui_scafold/add_scafold.php');
//$returnurl = new moodle_url('/blocks/ui_scafold/add_scafold.php');
$PAGE->navbar->add('Admin', '/admin');
$PAGE->navbar->add(get_string('customp_property_configuration', 'repository_andresco_cmis'));
$PAGE->set_heading($SITE->fullname);
$PAGE->set_pagelayout('mydashboard');
$PAGE->set_button("");
$PAGE->set_title('Dynamic Metadata Mapping Configuration');
//$objform = new scafold_form('save_scafold.php');
echo $OUTPUT->header();
//$objform->display();    
// GET the Scafold Data
$rid = required_param('rid', PARAM_INT);
$repo = repository::get_repository_by_id($rid, $systemcontext->id);

$listby = '';
//ANDRESCO:: Fetch Repository instance Data
$where3 = " WHERE ( sfui.repository_instance_id = '" . $rid . "' ) ";
$scafold_array = $DB->get_records_sql("SELECT sfui.* FROM {scafold_ui} sfui $where3 $listby");
$total_scafold_data = count($scafold_array);
// ENDS

$htm = '';
$htm .= "<div align='center'><h2>" . get_string('customp_property_configuration', 'repository_andresco_cmis') . "</h2><br/><span id='global_error' style='color: red;display:none;'>contact Androgogic</span></div>";
// ANDRESCO:: Display Custom Form
$htm .= "<form name='Row_form' id='Row_form' method='post' action='save_scafold.php?rid=" . $rid . "'  >";
$htm .= "<div align='right'><input type='button' name='addrow' id='addrow' value='Add Row +' onclick='addrRow(" . $rid . ");' ></div></br>";


$htm .= "<div id='show_error' style='display:none;color:red;background-color:#DDDDDD;' align='left'>Error Found!!</br>
         <div>Below listed Property-Names have conflicts. Please correct them.</div>   
         <div id='err2' ></div> 
         </div>";



$htm .= '<table id="add" style="font-size: 78%" cellspacing="0" cellpadding="2" width="100%" > 
            <th style="background-color: grey" >' . get_string('ui_scafold_property_name', 'repository_andresco_cmis') . '</th>
            <th style="background-color: grey">' . get_string('ui_scafold_display_label', 'repository_andresco_cmis') . '</th>
            <th style="background-color: grey">' . get_string('ui_scafold_is_constraint', 'repository_andresco_cmis') . '</th>
            <th style="background-color: grey">' . get_string('ui_scafold_default_value_expression', 'repository_andresco_cmis') . '</th>
            <th style="background-color: grey">' . get_string('ui_scafold_rendering_type', 'repository_andresco_cmis') . '</th>
            <th style="background-color: grey">' . get_string('ui_scafold_is_mandatory', 'repository_andresco_cmis') . '</th>
            <th style="background-color: grey">' . get_string('ui_scafold_delete', 'repository_andresco_cmis') . '</th>';

$htm .= '<tbody >';
if ($total_scafold_data == '0') {
    $htm .= '
            <tr style="background-color:#E0E0E0;vertical-align:top;height:50px" id="new_0">
                <td><input type="hidden" name="weightage[]" id="weightage_0" value="0" >
                <input type="text" class="prop_name" name="property_name_0" id="property_name_0" ></td>
                <td><input  class="label_name" type="text" name="display_label_0" id="display_label_0"></td>
                <td><input onclick="checkAlfresco(' . $rid . ',0)" type="checkbox" name="is_constraint_0" id="is_constraint_0">
                    </br><span style="color:red;" id="constraint_error_0"></span></td>
                <td><span id = "default_span_0"><input type="text" onblur="getvalue(this)" class = "default_value" name="default_value_expression_0" id="default_value_expression_0">
                    <br/><span id="error_span_0" style="color:red;"></span><span id="span_0" style="color:green;"></span></span></td>
                <td>
                <select class="render_type" name="render_type_0" id="render_type_0">
                <option value="Single Line Textfield">Single Line Textfield</option>
                <option value="Do Not Render">Do Not Render</option>
                <option value="Display as Read Only">Display as Read Only</option>
                <option value="Multiline Textarea">Multiline Textarea</option>
                <option value="Checkbox">Checkbox</option>
                <option value="datetime">Datetime</option>
                </select>
                </td>
                <td><input type="checkbox" name="is_mandatory_0" id="is_mandatory_0"></td>
                <td><img style="cursor:pointer"  width="14" height="14" src="../pix/delete.png" ></td>
                </tr>';
} else {

    // ANDRESCO:: Loop the Repository instance Data obtained
    foreach ($scafold_array as $values) {

        $chkval = $chkval2 = $chkval3 = '';
        if ($values->is_constraint == '1') {
            $chkval = 'checked=checked';
        }
        if ($values->is_mandatory == '1') {
            $chkval2 = 'checked=checked';
        }
        $dropDownSelected1 = $dropDownSelected2 = $dropDownSelected3 = $dropDownSelected4 = $dropDownSelected5 = $dropDownSelected6 = $dropDownSelected7 = $dropDownSelected8 = '';
        if ($values->render_type == 'Single Line Textfield') {
            $dropDownSelected1 = 'selected=selected';
        } if ($values->render_type == 'Do Not Render') {
            $dropDownSelected2 = 'selected=selected';
        } if ($values->render_type == 'Display as Read Only') {
            $dropDownSelected3 = 'selected=selected';
        } if ($values->render_type == 'Multiline Textarea') {
            $dropDownSelected4 = 'selected=selected';
        } if ($values->render_type == 'Checkbox') {
            $dropDownSelected5 = 'selected=selected';
        } if ($values->render_type == 'Multiple selection box') {
            $dropDownSelected6 = 'selected=selected';
        } if ($values->render_type == 'Drop Down List') {
            $dropDownSelected7 = 'selected=selected';
        }if ($values->render_type == 'datetime') {
            $dropDownSelected8 = 'selected=selected';
        }

        if ($values->is_constraint == '1') {
            $constraint_prop = array();
            $aspectprop = str_replace(']', '', $values->property_name);
            $asparr = explode('[', $aspectprop);
            $prop = $asparr[1];
            $prop_array = explode(':', $prop);
            $constraint_prop = $repo->fetch_search_values($prop_array[0]. ':' .$prop_array[1]);
            $chkval = 'checked=checked';
            $htm .= '
                <tr ' . $even . ' id="new_' . $values->weightage . '" style="background-color:#E0E0E0;vertical-align:top;height:50px">
                <td><input type="hidden" name="weightage[]" id="weightage_' . $values->weightage . '" value="' . $values->weightage . '" >
                <input "type="text" class="prop_name" name="property_name_' . $values->weightage . '" id="property_name_' . $values->weightage . '" value="' . $values->property_name . '"></td>
                <td><input  class="label_name" type="text" name="display_label_' . $values->weightage . '" id="display_label_' . $values->weightage . '" value="' . $values->display_label . '"></td>
                <td><input onclick="checkAlfresco(' . $values->repository_instance_id . ' , ' . $values->weightage . ' )" type="checkbox" ' . $chkval . '  name="is_constraint_' . $values->weightage . '" id="is_constraint_' . $values->weightage . '"  >
                    </br><span style="color:red;" id="constraint_error_' . $values->weightage . '"></span></td>
                <td><span id = "default_span_' . $values->weightage . '">
                    <select onblur="getvalue(this)" name="default_value_expression_' . $values->weightage . '" id="default_value_expression_' . $values->weightage . '" style = "width:155px">';
            foreach ($constraint_prop as $key => $value) {
                if ($values->default_value_expression == $value) {
                    $htm .= '<option value = "' . $key . '" selected>' . $value . '</option>';
                } else {
                    $htm .= '<option value = "' . $key . '">' . $value . '</option>';
                }
            }
            $htm .= '</select>
                    <br/>
                        <span id="error_span_' . $values->weightage . '" style="color:red;"></span>
                        <span id="span_' . $values->weightage . '" style="color:green;"></span>
                    </span>
                    </td>
                <td>
                <select class="render_type" name="render_type_' . $values->weightage . '" id="render_type_' . $values->weightage . '">
                <option  ' . $dropDownSelected6 . ' value="Multiple selection box">Multiple selection box</option>
                <option  ' . $dropDownSelected7 . ' value="Drop Down List">Drop Down List</option>
                </select>
                </td>';
            if (count($constraint_prop) == 0) {
                $htm .= '<td><input type="checkbox" ' . $chkval2 . '  name="is_mandatory_' . $values->weightage . '" id="is_mandatory_' . $values->weightage . '" disabled="disabled"></td>
                <td><img style="cursor:pointer" onclick="fncDelRow(' . $values->weightage . ')" width="14" height="14" src="../pix/delete.png" ></td>
            </tr>';
            } else {
                $htm .= '<td><input type="checkbox" ' . $chkval2 . '  name="is_mandatory_' . $values->weightage . '" id="is_mandatory_' . $values->weightage . '" ></td>
                <td><img style="cursor:pointer" onclick="fncDelRow(' . $values->weightage . ')" width="14" height="14" src="../pix/delete.png" ></td>
            </tr>';
            }
        } else {
            $htm .= '
                <tr ' . $even . ' id="new_' . $values->weightage . '" style="background-color:#E0E0E0;vertical-align:top;height:50px">
                <td><input type="hidden" name="weightage[]" id="weightage_' . $values->weightage . '" value="' . $values->weightage . '" >
                <input "type="text" class="prop_name" name="property_name_' . $values->weightage . '" id="property_name_' . $values->weightage . '" value="' . $values->property_name . '"></td>
                <td><input  class="label_name" type="text" name="display_label_' . $values->weightage . '" id="display_label_' . $values->weightage . '" value="' . $values->display_label . '"></td>
                <td><input onclick="checkAlfresco(' . $values->repository_instance_id . ' , ' . $values->weightage . ' )" type="checkbox" ' . $chkval . '  name="is_constraint_' . $values->weightage . '" id="is_constraint_' . $values->weightage . '"  >
                        </br><span style="color:red;" id="constraint_error_' . $values->weightage . '"></span></td>
                <td>
                    <span id = "default_span_' . $values->weightage . '">
                        <input type="text" onblur="getvalue(this)" name="default_value_expression_' . $values->weightage . '" id="default_value_expression_' . $values->weightage . '" value = "' . htmlentities($values->default_value_expression) . '" >
                        <br/>
                        <span id="error_span_' . $values->weightage . '" style="color:red;"></span>
                        <span id="span_' . $values->weightage . '" style="color:green;"></span>
                    </span>
                </td>
                <td>
                <select class="render_type" name="render_type_' . $values->weightage . '" id="render_type_' . $values->weightage . '">
                <option  ' . $dropDownSelected1 . ' value="Single Line Textfield">Single Line Textfield</option>
                <option  ' . $dropDownSelected2 . ' value="Do Not Render">Do Not Render</option>
                <option  ' . $dropDownSelected3 . ' value="Display as Read Only">Display as Read Only</option>
                <option  ' . $dropDownSelected4 . ' value="Multiline Textarea">Multiline Textarea</option>
                <option  ' . $dropDownSelected5 . ' value="Checkbox">Checkbox</option>                
                 <option  ' . $dropDownSelected8 . ' value="datetime">Datetime</option>
                </select>
                </td>
                <td><input type="checkbox" ' . $chkval2 . '  name="is_mandatory_' . $values->weightage . '" id="is_mandatory_' . $values->weightage . '" ></td>
                <td><img style="cursor:pointer" onclick="fncDelRow(' . $values->weightage . ')" width="14" height="14" src="../pix/delete.png" ></td>
            </tr>';
        }
    }
}
$sess_key = $USER->sesskey;
$returnurl = new moodle_url('/admin/repository.php?sesskey=' . $sess_key . '&action=edit&repos=andresco_cmis');
$htm .= '</tbody>';
$htm .= '</table>';
$htm .= "<div align='right'><input onclick='return validateProp(1);' type='button' name='validaterow' id='validaterow' value='Validate'  > <input type='submit' name='saverow' id='saverow' value='Save'> <input type='button' name='cancel' id='cancel' onclick='cancelFunction();' value='Cancel'></div>";
$htm .= "</form>";

echo $htm;
echo $OUTPUT->footer();
?>  

<script type="text/javascript">

    $(document).ready(function() {
        var key = 0;
        var formData = $("#Row_form").serializeArray();
        for (var i = 0; i < formData.length; i++) {
            if (formData[i].name == "default_value_expression_" + key) {
                formData[i].id = "default_value_expression_" + key;
                getvalueOnLoad(formData[i]);
                key++;
            }
        }
        $("#add tbody").sortable({helper: fixHelper});

    });

    function cancelFunction() {
        var url = "<?php echo $returnurl; ?>";
        var div = document.createElement('div');
        div.innerHTML = url;
        url = div.firstChild.nodeValue;
        document.location.href = url;
    }


    function checkAlfresco(rid, rowCount) {
        $("#global_error").hide();
        var name = $('#property_name_' + rowCount).val();
        var htmlData = '<option value="Multiple selection box">Multiple selection box</option><option value="Drop Down List">Drop Down List</option>';
        var htmlDataRepo = "<option value='Single Line Textfield'>Single Line Textfield</option>\n\
                           <option value='Do Not Render'>Do Not Render</option>\n\
                           <option value='Display as Read Only'>Display as Read Only</option>\n\
                           <option value='Multiline Textarea'>Multiline Textarea</option>\n\
                           <option value='Checkbox'>Checkbox</option>\n\<option value='datetime'>Datetime</option>";
        if ($('#is_constraint_' + rowCount).is(':checked')) {
            $.loader({className: "blue-with-image-2", content: ''});

            $.post("<?php echo new moodle_url('/repository/andresco_cmis/util/validatecontraints.php'); ?>", {rid: rid, name: name, id: rowCount})
                    .done(function(data) {
                $.loader('close');
                var obj = $.parseJSON(data);
                var errMSg = obj.msg;
                if (obj.errcode == 'NA' || obj.errcode == 'INVALID')
                {
                    $('#constraint_error_' + rowCount).css('color', 'red');
                    $('#is_constraint_' + rowCount).click();
                    $('#default_value_expression_' + rowCount).remove();
                    var htm = "<input type = 'text' id = 'default_value_expression_ " + rowCount + "' name = 'default_value_expression_ " + rowCount + "' onblur='getvalue(this)' class = 'default_value' style='width: 151px;'><br/><span id='error_span_" + rowCount + "' style='color:red;'></span><span id='span_" + rowCount + "' style='color:green;'></span>";
                    $('#default_span_' + rowCount).html(htm);
                }
                else if (obj.errcode == "ERROR") {
                    $("#global_error").show();
                }
                else {
                    $('#constraint_error_' + rowCount).css('color', 'green');
                    var dropdown = $('#render_type_' + rowCount).attr('id');
                    // Clear drop down list
                    $('#render_type_' + rowCount).empty();
                    //set new values
                    $('#render_type_' + rowCount).html(htmlData);
                    $('#render_type_' + rowCount).html(htmlData);
                    if (obj.list) {
                        $('#default_value_expression_' + rowCount).remove();
                        $('#default_span_' + rowCount).html(obj.list);
                    } else {
                        var htm = "<select id = 'default_value_expression_" + rowCount + "' name = 'default_value_expression_" + rowCount + "' onblur='getvalue(this)' class = 'default_value' style='width: 155px;'></select><br/><span id='error_span_" + rowCount + "' style='color:red;'></span><span id='span_" + rowCount + "' style='color:green;'></span>";
                        $('#default_value_expression_' + rowCount).remove();
                        $('#default_span_' + rowCount).html(htm);
                        $('#is_mandatory_' + rowCount).attr('disabled', 'disabled');
                    }
                }
                $('#constraint_error_' + rowCount).text(errMSg);
            });
        } else {
            $('#constraint_error_' + rowCount).text('');
            // Clear drop down list
            $('#render_type_' + rowCount).empty();
            //set new values
            $('#render_type_' + rowCount).html(htmlDataRepo);
            $('#default_value_expression_' + rowCount).remove();
            var htm = "<input type = 'text' id = 'default_value_expression_" + rowCount + "' name = 'default_value_expression_" + rowCount + "' onblur='getvalue(this)' class = 'default_value' style='width: 151px;'><br/><span id='error_span_" + rowCount + "' style='color:red;'></span><span id='span_" + rowCount + "' style='color:green;'></span>";
            $('#default_span_' + rowCount).html(htm);
            $('#is_mandatory_' + rowCount).attr('disabled', false);
            $('#is_mandatory_' + rowCount).attr('checked', false);
        }
    }

    function request(config) {
        var data = {};

        if (config.form) {
            if (typeof(config.form) == 'string') {
                config.form = $("#" + config.form)[0];
            }
            if (!config.url && config.form.action) {
                config.url = config.form.action;
            }

            if (config.form.method && !config.method) {
                config.method = config.form.method;
            }

            for (var i = 0; i < config.form.elements.length; i++) {
                var element = config.form.elements[i];
                if (!element.name) {
                    continue;
                }

                //Select type multiple
                if (element.multiple) {
                    var opt = element.options;
                    var values = [];
                    for (var j = 0; j < opt.length; j++) {
                        if (opt[j].selected)
                            values.push(opt[j].value);
                    }
                    var optval = values.join('|');
                    data[element.name] = optval;
                }
                else if (element.type == 'checkbox') {
                    if (element.checked) {
                        data[element.name] = element.value;
                    }
                    else {
                        data[element.name] = false;
                    }
                }
                else if (element.type == 'radio') {
                    if (element.checked) {
                        data[element.name] = element.value;
                    }
                }
                else if (element.name) {
                    data[element.name] = element.value;
                }
            }
        }

        for (var key in config.data) {
            data[key] = config.data[key];
        }

        if (!config.method) {
            config.method = "POST";
        }

        $.ajax({
            async: (typeof(config.async) != 'undefined') ? config.async : true,
            type: config.method,
            url: config.url,
            data: data,
            success: function(response, code) {
                if (config.container) {
                    config.container.innerHTML = response;
                }

                if (config.success) {
                    config.success(response);
                }

                //Parsing response javascript
                var jscode = "";
                var parts = response.match(/<script[^>]*>(.|\n|\t|\r)*?<\/script>/gi);
                if (parts) {
                    for (i = 0; i < parts.length; i++) {
                        jscode += parts[i].replace(/<script[^>]*>|<\/script>/gi, "");
                        response = response.replace(parts[i], "");
                    }
                }


                if (jscode != "") {
                    var incomingScript = document.createElement('SCRIPT');
                    incomingScript.type = 'text/javascript';
                    incomingScript.lang = 'javascript';
                    incomingScript.defer = true;
                    incomingScript.text = jscode;
                    document.getElementsByTagName('head')[0].appendChild(incomingScript);
                }

            },
            error: function() {
            }
        });
    }
    // $(document).ready(function() {
    //        $('#Row_form').submit(function() {
    //            var val = $("input[type=submit][clicked=true]").val()
    //            validateProp(val);
    //            return false;
    //        });

    function validateProp(val) {
        $("#global_error").hide();
        var flag = true;
        if (val == "2") {
            document.Row_form.submit();
        }
        $('#add > tbody  > tr .prop_name').each(function() {
            var prop_name_id = '#' + $(this).attr('id');
            var get_prop_name_data = $(prop_name_id).val();
            if ($.trim(get_prop_name_data) == '') {
                $(prop_name_id).focus();
                alert("Property name can not be left blank");
                flag = false;
                return flag;
            }
        });

        //check if property name is blank then dont apply further validation
        if (flag == false) {
            return flag;
        }

        //Check for duplicate property values
        var values = [];
        $('#add > tbody  > tr .prop_name').each(function() {
            var prop_name_id = '#' + $(this).attr('id');
            var prop_name_data = $(prop_name_id).val();
            if ($.inArray(prop_name_data, values) > '-1') {
                $(prop_name_id).focus();
                alert("'" + prop_name_data + "' value must be unique.");
                flag = false; // <-- stops the loop
                return flag;
            } else {
                values.push(prop_name_data);
            }
        });
        if (flag == false) {
            return flag;
        }

        $('#add > tbody  > tr .label_name').each(function() {
            var label_name_id = '#' + $(this).attr('id');
            var get_label_name_data = $(label_name_id).val();
            if ($.trim(get_label_name_data) == '') {
                $(label_name_id).focus();
                alert("Display name can not be left blank");
                flag = false;
                return flag;
            }
        });
        if (flag == false) {
            return flag;
        }

        $('#add > tbody  > tr .render_type').each(function() {
            var render_type_value = $(this).val();
            var render_type_id_array = ($(this).attr('id')).split('_');
            var default_value_id = "#default_value_expression_" + render_type_id_array[2];
            var default_value = $("#default_value_expression_" + render_type_id_array[2]).val();
            var mandatory = $("#is_mandatory_" + render_type_id_array[2]).is(":checked");
            if ($.trim(default_value) == '' && (render_type_value == "Do Not Render" || render_type_value == "Display as Read Only")) {
                $(default_value_id).focus();
                alert("For '" + render_type_value + "' default value can not be left blank");
                flag = false;
                return flag;
            }
        });
        if (flag == false) {
            return flag;
        }
        if (flag) {
            $.loader({className: "blue-with-image-2", content: ''});
            request({
                url: "<?php echo new moodle_url('/repository/andresco_cmis/util/validateprop.php'); ?>",
                form: jQuery("#Row_form")[0],
                data: {rid: "<?php echo $rid; ?>"
                },
                method: "POST",
                success: function(response) {
                    $.loader('close');
                    var data = JSON.parse(response);
                    var total_data = data.length;
                    if (total_data > 0) {
                        if (data[0] == "ERROR") {
                            $('#global_error').show();
                        } else {
                            $('#err2').html('');
                            var html = '<ul>';
                            for (var i = 0; i < total_data; i++) {
                                html += '<li style="color:red;">' + data[i] + '</li>';
                            }
                            html += '</ul>';
                            $('#show_error').show();
                            $('#err2').append(html);
                        }
                    } else {
                        $('#show_error').hide();
                        $('#err2').html('');
//                        if (val == "2") {
//                            document.Row_form.submit();
//                        }
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    $.loader('close');
                }

            }
            );
        }
    }
    //Evaluate value of expression
    function getvalueOnLoad(obj) {
        var default_value = obj.value;
        var parsed_str = '';
        var final_str = '';
        var str = '';
        var flag = true;
        var error_obj = '';
        var error_msg = '';
        var split_id = obj.id.split("_");
        var js_array = default_value.split("REF");
        if (default_value != '' && js_array.length > 1) {
            var code = js_array[1].replace("[", "");
            var code = code.replace("]", "");
            $('.prop_name').each(function() {
                if ($(this).val() == code) {
                    var id_array = $(this).attr('id').split("_");
                    default_value = $("#default_value_expression_" + id_array[2]).val();
                }
                if (default_value == '') {
                    error_obj = "Unsuccessful";
                    error_msg = "Not a proper reference";
                }
            });
        }
        if (default_value != '') {
            $.loader({className: "blue-with-image-2", content: ''});
            $.ajax({
                type: "POST",
                url: "../util/parse_variables.php",
                data: {default_value: default_value},
                async: false
            }).done(function(msg) {
                $.loader('close');
                var data = $.parseJSON(msg);
                if (data.result == "Successful") {
                    parsed_str = data.value;
                } else {
                    error_obj = data.result;
                    error_msg = data.value;
                }
            });
        } else {
            parsed_str = default_value;
        }
        if (error_obj == '') {
            var js_array = parsed_str.split("JS");
            for (var i = 0; i < js_array.length; i++) {
                var isMatch = js_array[i].substr(0, 1) == "[";
                if (isMatch) {
                    var code = js_array[i].replace("[", "");
                    var remain_string = code.split("]");
                    code = remain_string[0];
                    options = {};
                    try {
                        result = esprima.parse(code, options);
                        var exVal = eval(code);
                        str = JSON.stringify(result, adjustRegexLiteral, 4);
                        options.tokens = true;
                        if (window.updateTree) {
                            window.updateTree(result);
                        }
                        var err = 'No error';
                    } catch (e) {
                        if (window.updateTree) {
                            window.updateTree();
                        }
                        str = e.name + ': ' + e.message;
                        err = str;
                        //TODO return and save the value
                    }
                    if (err == "No error") {
                        //TODO: How to obtain the code result
                        exVal = exVal + remain_string[1];
                        var parsed_str = parsed_str.replace("JS" + js_array[i], exVal);
//                    var parsed_str = parsed_str.replace("JS|js", "");
                    } else {
                        //Set error for Javascript expression
                        $("#error_span_" + split_id[3]).text(err);
                        $("#span_" + split_id[3]).text("");
                        return;
                    }
                }
            }
        } else {
            //Set error for context variable
            $("#error_span_" + split_id[3]).text("Error: " + error_msg);
            $("#span_" + split_id[3]).text("");
            return;
        }
        $("#span_" + split_id[3]).text(parsed_str);
        $("#error_span_" + split_id[3]).text("");
        return;
    }


// Special handling for regular expression literal since we need to
// convert it to a string literal, otherwise it will be decoded
// as object "{}" and the regular expression would be lost.
    function adjustRegexLiteralOnLoad(key, value) {
        if (key === 'value' && value instanceof RegExp) {
            value = value.toString();
        }
        return value;
    }
    //});
</script>
