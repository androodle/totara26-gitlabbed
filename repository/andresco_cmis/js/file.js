function putVals(obj) {
    var myFileName = obj.files[0].name.substr(0, obj.files[0].name.lastIndexOf("."));
    var myFileType = obj.files[0].type;
    var myFileSize = obj.files[0].size;
    var i=0;
    var j=0;
    $("input.not_required_upload").each(function() {
        if ($(this).val().trim().indexOf("Sample File") >= 0)
        {
            $(this).addClass('filename_'+i);
            putVals.titlestr = $(this).val().replace("Sample File", '$');
        }
        else if ($(this).val().trim().indexOf(putVals.fname) >= 0) {
            putVals.titlestr = $(this).val().replace(putVals.fname, '$');
        }
        if ($(this).val().trim().indexOf("image/png") >= 0)
        {
            $(this).addClass('filetype_'+i);
            putVals.typestr = $(this).val().replace("image/png", '$');
        }
        else if ($(this).val().trim().indexOf(putVals.ftype) >= 0) {
            putVals.typestr = $(this).val().replace(putVals.ftype, '$');
        }
        if ($(this).val().trim().indexOf("15726") >= 0)
        {
            $(this).addClass('filesize_'+i);
            putVals.sizestr = $(this).val().replace("15726", '$');

        }
        else if ($(this).val().trim().indexOf(putVals.fsize) >= 0) {
            putVals.sizestr = $(this).val().replace(putVals.fsize, '$');
        }
        if (putVals.titlestr !== undefined)
        {
            // alert(putVals.titlestr);
            var newTitle = putVals.titlestr.replace("$", myFileName);
            $(".filename_"+i).val(newTitle);
        }
        if (putVals.typestr !== undefined) {
            var newType = putVals.typestr.replace("$", myFileType);
            $(".filetype_"+i).val(newType);
        }
        if (putVals.sizestr !== undefined) {
            var newSize = putVals.sizestr.replace("$", myFileSize);
            $(".filesize_"+i).val(newSize);
        }
   i++;
    });
    $("input.required_upload").each(function() {
        if ($(this).val().trim().indexOf("Sample File") >= 0)
        {
            $(this).addClass('filename1_'+j);
            putVals.titlestr1 = $(this).val().replace("Sample File", '#');
        }
        else if ($(this).val().trim().indexOf(putVals.fname) >= 0) {
            putVals.titlestr1 = $(this).val().replace(putVals.fname, '#');
        }
        if ($(this).val().trim().indexOf("image/png") >= 0)
        {
            $(this).addClass('filetype1_'+j);
            putVals.typestr1 = $(this).val().replace("image/png", '#');
        }
        else if ($(this).val().trim().indexOf(putVals.ftype) >= 0) {
            putVals.typestr1 = $(this).val().replace(putVals.ftype, '#');
        }
        if ($(this).val().trim().indexOf("15726") >= 0)
        {
            $(this).addClass('filesize1_'+j);
            putVals.sizestr1 = $(this).val().replace("15726", '#');

        }
        else if ($(this).val().trim().indexOf(putVals.fsize) >= 0) {
            putVals.sizestr1 = $(this).val().replace(putVals.fsize, '#');
        }
        if (putVals.titlestr1 !== undefined)
        {
            var newTitle = putVals.titlestr1.replace("#", myFileName);
            $(".filename1_"+j).val(newTitle);
        }
        if (putVals.typestr1 !== undefined) {
            var newType = putVals.typestr1.replace("#", myFileType);
            $(".filetype1_"+j).val(newType);
        }
        if (putVals.sizestr1 !== undefined) {
            var newSize = putVals.sizestr1.replace("#", myFileSize);
            $(".filesize1_"+j).val(newSize);
        }
    j++;
    });
    putVals.fname = myFileName;
    putVals.ftype = myFileType;
    putVals.fsize = myFileSize;
}

function datepick(obj){
$(function() {
    var iid = $(obj).attr('id').trim();
    $('#'+iid).datepicker();
  });
}