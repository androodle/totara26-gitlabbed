<?php

/**
 * -----------------------------------------------------------------------------*
 * Andresco Repository Plugin: Library
 * -----------------------------------------------------------------------------*
 *
 * Andresco is copyright 2014+ by Androgogic Pty Ltd
 * http://www.androgogic.com.au/andresco
 *
 * This code originated from the Moodle delivered repository/alfresco/lib.php
 * and has been significantly enhanced and improved.
 *
 * ----------------------------------------------------------------------------- 
 */

require_once($CFG->dirroot . '/repository/andresco_cmis/lib/cmis_repository_wrapper.php');
require_once($CFG->dirroot . '/repository/andresco_cmis/lib/userlib.php');

class repository_andresco_cmis extends repository {

    private $ticket = null;
    private $user_session = null;
    private $store = null;
    private $alfresco;
    private $current_node = null;
    private $client;
    private $auth_options;
    private $repositoryid;

    public function __construct($repositoryid, $context = SYSCONTEXTID, $options = array()) {

        global $SESSION, $CFG, $USER;
        
        if (empty($CFG->passwordsaltmain)) {
            $CFG->passwordsaltmain = 'NUYCgfI2^vsJ?wbkpdN{5},q<t:';
        }
        parent::__construct($repositoryid, $context, $options);
        $this->sessname = 'alfresco_ticket'; // HACK
        //set people url for user services
        if (!empty($this->options['alfresco_cmis_url'])) {
            $this->base_url = substr($this->options['alfresco_cmis_url'], 0, strpos($this->options['alfresco_cmis_url'], '/alfresco/s/cmis'));
        }
        $this->server_url = '';
        if (!empty($this->options['alfresco_cmis_url'])) {
            $this->server_url = $this->options['alfresco_cmis_url'];
        } else {
            return;
        }
        
        if (!empty($this->options['connection_username'])) {
            $this->username = $this->options['connection_username'];
        } else {
            return;
        }
        if (!empty($this->options['connection_password'])) {
            $connection_password = $this->options['connection_password'];
            $this->password = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $CFG->passwordsaltmain, base64_decode($connection_password), MCRYPT_MODE_ECB);
        } else {
            return;
        }
        $this->repositoryid = $repositoryid;

        // Set up a new user service
        $this->userlib = new user_service($repositoryid, $this->base_url, $this->username, $this->password, $this->options);

        // Set up th session
        try {            
            $this->disabled = FALSE;
            
            //$this->ticket = $this->userlib->get_user_ticket($this->options, $repositoryid);            
            
            //$this->auth_options = array('alf_ticket' => $this->ticket);
            $SESSION->active_alfresco_cmis_url = $this->options['alfresco_cmis_url'];
            $SESSION->active_andresco_repo_id = $repositoryid;
     
            //$this->client = new CMISService($this->server_url, $this->username, $this->password, $this->auth_options);
        }
        catch (Exception $e) {            
            unset($SESSION->{$this->sessname});
            //throw new Exception($e->getMessage());
            $this->disabled = TRUE;
        }

    }

    public function print_login() {
        if ($this->options['ajax']) {
            $user_field = new stdClass();
            $user_field->label = get_string('username', 'repository_alfresco') . ': ';
            $user_field->id = 'alfresco_username';
            $user_field->type = 'text';
            $user_field->name = 'al_username';

            $passwd_field = new stdClass();
            $passwd_field->label = get_string('password', 'repository_alfresco') . ': ';
            $passwd_field->id = 'alfresco_password';
            $passwd_field->type = 'password';
            $passwd_field->name = 'al_password';

            $ret = array();
            $ret['login'] = array($user_field, $passwd_field);
            return $ret;
        } else {
            echo '<table>';
            echo '<tr><td><label>' . get_string('username', 'repository_alfresco') . '</label></td>';
            echo '<td><input type="text" name="al_username" /></td></tr>';
            echo '<tr><td><label>' . get_string('password', 'repository_alfresco') . '</label></td>';
            echo '<td><input type="password" name="al_password" /></td></tr>';
            echo '</table>';
            echo '<input type="submit" value="Enter" />';
        }
    }

    public function logout() {
        global $SESSION;
        unset($SESSION->{$this->sessname});
        return $this->print_login();
    }

    public function check_login() {
        return true;
        global $SESSION;
        return !empty($SESSION->{$this->sessname});
    }

    /**
     * 
     * ////////////////////////////////////////////////////////
     * Deprecated and replaced by get_andresco_version_auth_url
     * ////////////////////////////////////////////////////////
     * 
     * Generates a url for the target content in a format that redirects to the andresco index.php which adds dynamic Moodle parameters
     * before redirecting to the Alfresco Moodle-Authentication script (auth.php) which takes care of things such as:
     * - Authentication ticket generation
     * - Verifying access to the content (and restricting access from non-Moodle, untrusted sources).
     *
     * @param Alfresco node being accessed
     * @param Alfresco repository ID to redirect to
     * @return Alfresco content URL in intermediate redirect format
     *
     */
    private function get_andresco_auth_url($node, $version_prefer, $repo_id) {
        // //finding the Alfresco base URL is now done in /repository/andresco_cmis/index.php and we just point to that file
        $version_id = $this->get_version_id($node);
        $uuid = str_replace('urn:uuid:', '', $node->uuid);
        $filename = urlencode($node->properties['cmis:name']);
        $base_url = $this->get_relative_root();
        if (isset($version_id) && !empty($version_id)) {
            if ($version_prefer == 1) {
                $target_url = "/repository/andresco_cmis/index.php?repo=$repo_id&uuid=$uuid&version=latest&filename=$filename";
            } else {
                $target_url = "/repository/andresco_cmis/index.php?repo=$repo_id&uuid=$version_id&version=" . $node->properties['cmis:versionLabel'] . "&filename=$filename";
            }
        } else {
            $target_url = "/repository/andresco_cmis/index.php?repo=$repo_id&uuid=$uuid&version=latest&filename=$filename";
        }
        // moodlename, courseid, courseshortname, username and userid are all generated dynamically at clicktime by index.php
        // and placed before the filename parameter (it is important for file typing that filename is the last parameter)


        return $base_url . $target_url;
    }

    /**
     * Generates a url for the target content in a format that redirects to the andresco index.php which adds dynamic Moodle parameters
     * before redirecting to the Alfresco Moodle-Authentication script (auth.php) which takes care of things such as:
     * - Authentication ticket generation
     * - Verifying access to the content (and restricting access from non-Moodle, untrusted sources).
     *
     * @param Alfresco node being accessed
     * @param User selection of version preference (always latest = 1) 
     * @param Specific version UUID in version2Store if "Always this version" selected
     * @param Alfresco repository ID to redirect to
     * @return Alfresco content URL in intermediate redirect format
     *
     */
    /* private function get_andresco_version_auth_url($node, $version_prefer, $version_id, $repo_id) {
      // finding the Alfresco base URL is now done in /repository/andresco_cmis/index.php and we just point to that file
      $uuid = str_replace('urn:uuid:', '', $node->uuid);
      $filename = urlencode($node->properties['cmis:name']);
      if ($version_prefer == 1) {
      $target_url = "/repository/andresco_cmis/index.php?repo=$repo_id&uuid=$uuid&version=latest&filename=$filename";
      } else {
      $target_url = "/repository/andresco_cmis/index.php?repo=$repo_id&uuid=$version_id&version=" . $node->properties['cmis:versionLabel'] . "&filename=$filename";
      }
      // moodlename, courseid, courseshortname, username and userid are all generated dynamically at clicktime by index.php
      // and placed before the filename parameter (it is important for file typing that filename is the last parameter)

      return $target_url;
      } */

    /**
     * Generates a url for the target content thumbnail
     *
     * @param Alfresco node being accessed
     * @return Alfresco content URL in Andresco format
     *
     */
    private function get_andresco_thumbnail_url($node) {
    	$ticket = $this->userlib->get_user_ticket($this->options, $this->repositoryid);
        $base_url = substr($this->options['alfresco_cmis_url'], 0, strpos($this->options['alfresco_cmis_url'], '/alfresco/s/cmis'));
        $uuid = str_replace('urn:uuid:', '', $node->uuid);
        $thumbnailurl = $base_url . '/alfresco/s/api/node/workspace/SpacesStore/' . $uuid . '/content/thumbnails/doclib?c=queue&ph=true&alf_ticket=' . $ticket;
        return $thumbnailurl;
    }

    private function get_andresco_share_url($node) {
        $base_url = substr($this->options['alfresco_cmis_url'], 0, strpos($this->options['alfresco_cmis_url'], '/alfresco/s/cmis'));
        $shareurl = $base_url . '/share/page/document-details?nodeRef=' . $node->id;
        return $shareurl;
    }

    private function get_url($node) {
        $result = null;
        if ($node->type == "{http://www.alfresco.org/model/content/1.0}content") {
            $contentData = $node->cm_content;
            if ($contentData != null) {
                $result = $contentData->getUrl();
            }
        } else {
            $result = "index.php?" .
                    "&uuid=" . $node->id .
                    "&name=" . $node->cm_name .
                    "&path=" . 'Company Home';
        }
        // BEGIN: Andresco
        // Remove the port from the returned URL if it is either 80 or 443 as they
        // are defaults for the relevant protocols (http/https) and not explicitly
        // required. This is purely to shorten the length of the URL.
        $result = str_replace(':80/', '/', $result);
        $result = str_replace(':443/', '/', $result);
        // Shorten the /download/direct/ part of the URL to /d/d/ as this is an
        // acceptable URL format for alfresco.
        $result = str_replace('/download/direct/', '/d/d/', $result);
        // Append the extension of the filename to the end of the URL (after the
        // ticket) if there is one. This is so that Moodle can correctly identitfy
        // the mimetype of the document from the URL.
        $url_split_at_ticket = explode('?ticket=', $result);
        // Assumes that extensions are 3 characters long. Otherwise ignores them.
        $extension = substr($url_split_at_ticket[0], strlen($url_split_at_ticket[0]) - 4, 4);
        // If there is an extension, append the extension parameter to the end of the url
        if (substr($extension, 0, 1) == '.') {
            $extension_param = '&ext=' . $extension;
            $result = $result . $extension_param;
        }
        // END: Andresco
        return $result;
    }

    /**
     * Get a file list from alfresco
     *
     * @param string $uuid a unique id of directory in alfresco
     * @param string $path path to a directory
     * @return array
     */

    public function get_listing($uuid = '', $path = '', $repo_id = '') {

        global $CFG, $SESSION, $OUTPUT, $DB, $USER, $COURSE;       

        //get property for display item
        $display_prop_data = $DB->get_records('display_item_config', array('repository_instance_id' => $repo_id), '', 'property_name');
        $display_prop_data_label = $DB->get_records('display_item_config', array('repository_instance_id' => $repo_id), '', 'property_name,display_label');
        foreach ($display_prop_data as $key1 => $value1) {
            $aspectprop = str_replace(']', '', $key1);
            $asparr = explode('[', $aspectprop);
            $prop = $asparr[1];
            $aspect = $asparr[0];
            $propasparrfinal[$aspect][$prop] = "test";
        }
        $propmap = array();
        $i = 0;
        foreach ($display_prop_data_label as $key2 => $value2) {
            $aspectprop1 = str_replace(']', '', $key2);
            $asparr1 = explode('[', $aspectprop1);
            $prop1 = $asparr1[1];
            $propmap[$i][$prop1] = $value2->display_label;
            $i++;
        }
        $istitle = 0;
        $isdescription = 0;
        $isauthor = 0;
        foreach ($propmap as $key3 => $value3) {
            if (isset($value3['cm:title'])) {
                $istitle = 1;
                unset($propmap[$key3]);
            }
            if (isset($value3['cm:description'])) {
                $isdescription = 1;
                unset($propmap[$key3]);
            } if (isset($value3['cm:author'])) {
                $isauthor = 1;
                unset($propmap[$key3]);
            }
        }
        $ticket = $this->userlib->get_user_ticket($this->options, $this->repositoryid);            
        $this->auth_options = array('alf_ticket' => $ticket);
        $this->client = new CMISService($this->server_url, $this->username, $this->password, $this->auth_options);
        // create user on alfresco mapped to andresco user
        $path_array = array();
        $path_array = explode('/', $this->options['starting_node_uuid']);
//        if (count($path_array) > 3) {
//            $error_message = get_string('level_error', 'repository_andresco_cmis');
//            throw new Exception($error_message);
//        }
        $ret = array();
        $ret['dynload'] = true;
        $ret['norefresh'] = false;
        $ret['list'] = array();
        $server_url = $this->options['alfresco_cmis_url'];
        $ret['path'] = array(array('name' => get_string('pluginname', 'repository_andresco_cmis'), 'path' => ''));
        try {
            if (isset($this->options['starting_node_uuid']) && strlen($this->options['starting_node_uuid']) > 0) {
                $current_folder = $this->client->getObjectByPath($this->options['starting_node_uuid']);
            }
            //throw new Exception($uuid);
            if (!isset($current_folder)) {
                $current_folder = $this->client->getObjectByPath("/");
            }
            if (empty($uuid)) {
                // BEGIN: Andresco
                // Use the starting node set above.
                $this->current_node = $current_folder;
                // END: Andresco
            } else {
                $this->current_node = $this->client->getObjectByPath($uuid);
            }
            if (isset($propasparrfinal) && count($propasparrfinal) > 0) {
                $nodes = $this->client->getChildrenCustom($this->current_node->id, array(), $propasparrfinal);
            } else {
                $nodes = $this->client->getChildren($this->current_node->id);
            }
            $i = 0;
            foreach ($nodes->objectList as $node) {

                if ($node->properties['cmis:baseTypeId'] == "cmis:document") {
                    if (!isset($node->properties['cm:title'])) {
                        $node->properties['cm:title'] = '';
                    }
                    if (!isset($node->properties['cm:description'])) {
                        $node->properties['cm:description'] = '';
                    }
                    if ($this->options['instance_default_label'] == 'TITLE') {
                        $fname = $node->properties['cm:title'] != '' ? $node->properties['cm:title'] : $node->properties['cmis:name'];
                    } else if ($this->options['instance_default_label'] == 'NAME') {
                        $fname = $node->properties['cmis:name'];
                    } else {
                        if (!empty($node->properties['cm:title'])) {
                            $fname = $node->properties['cm:title'] . ' (' . $node->properties['cmis:name'] . ')';
                        } else {
                            $fname = $node->properties['cmis:name'];
                        }
                    }

                    $ret['list'][$i] = array('title' => $node->properties['cmis:name'],
                        'usertitle' => $node->properties['cm:title'],
                        'description' => $node->properties['cm:description'],
                        'datemodified_f' => date("d-m-Y h:i:s A ", strtotime($node->properties['cmis:lastModificationDate'])),
                        'thumbnail' => $this->get_andresco_thumbnail_url($node),
                        'content_share_link' => $this->get_andresco_share_url($node),
                        'size' => $node->properties['cmis:contentStreamLength'],
                        'source' => $node->id,
                        'createdby' => $node->properties['cmis:createdBy'],
                        'mimetype' => $node->properties['cmis:contentStreamMimeType'],
                        'datecreated_f' => date("d-m-Y h:i:s A", strtotime($node->properties['cmis:creationDate'])),
                        'author' => $node->properties['cmis:createdBy'],
                        'istitle' => $istitle,
                        'isdescription' => $isdescription,
                        'isauthor' => $isauthor,
                        'repoid' => $repo_id,
                        'contentVersion' => $this->options['content_versioning_preferences'],
                    );
                    foreach ($propmap as $key4 => $value4) {
                        foreach ($value4 as $key5 => $value5) {
                            if (!empty($node->properties[$key5])) {
                                $propdata = explode(':', $key5);
                                $ret['list'][$i][strtolower($propdata[1])] = $node->properties[$key5];
                            }
                        }
                    }
                } else if ($node->properties['cmis:baseTypeId'] == "cmis:folder") {
                    if (!isset($node->properties['cm:title'])) {
                        $node->properties['cm:title'] = '';
                    }
                    if (!isset($node->properties['cm:description'])) {
                        $node->properties['cm:description'] = '';
                    }
                    if ($this->options['instance_default_label'] == 'TITLE') {
                        $fname = $node->properties['cm:title'] != '' ? $node->properties['cm:title'] : $node->properties['cmis:name'];
                    } else if ($this->options['instance_default_label'] == 'NAME') {
                        $fname = $node->properties['cmis:name'];
                    } else {
                        if (!empty($node->properties['cm:title'])) {
                            $fname = $node->properties['cm:title'] . ' (' . $node->properties['cmis:name'] . ')';
                        } else {
                            $fname = $node->properties['cmis:name'];
                        }
                    }

                    $ret['list'][$i] = array('title' => $node->properties['cmis:name'],
                        'usertitle' => $node->properties['cm:title'],
                        'description' => $node->properties['cm:description'],
                        'path' => $node->properties['cmis:path'],
                        'datemodified_f' => date("d-m-Y h:i:s A", strtotime($node->properties['cmis:lastModificationDate'])),
                        'thumbnail' => $OUTPUT->pix_url('f/folder-64') . '',
                        'createdby' => $node->properties['cmis:createdBy'],
                        'datecreated_f' => date("d-m-Y h:i:s A", strtotime($node->properties['cmis:creationDate'])),
                        'author' => $node->properties['cmis:createdBy'],
                        'children' => array(),
                        'source' => $node->id,
                        'istitle' => $istitle,
                        'isdescription' => $isdescription,
                        'isauthor' => $isauthor,
                        'repoid' => $repo_id,
                        'contentVersion' => $this->options['content_versioning_preferences']
                    );
                    foreach ($propmap as $key4 => $value4) {
                        foreach ($value4 as $key5 => $value5) {
                            if (!empty($node->properties[$key5])) {
                                $propdata = explode(':', $key5);
                                $ret['list'][$i][strtolower($propdata[1])] = $node->properties[$key5];
                            }
                        }
                    }
                }
                $i++;
            }
        } catch (Exception $e) {
            unset($SESSION->{$this->sessname});
            $readonly_message = get_string('readonly_message', 'repository_andresco_cmis');
            throw new Exception($e->getMessage());
        }
        $breadcrumb_array = array();
        $breadcrumb_node = $this->current_node;
        while ($breadcrumb_node->id != $current_folder->id) {
            $breadcrumb = array('name' => $breadcrumb_node->properties['cmis:name'], 'path' => $breadcrumb_node->properties['cmis:path']);
            array_push($breadcrumb_array, $breadcrumb);
            $breadcrumb_node = $this->client->getObject($breadcrumb_node->properties['cmis:parentId']);
        }
        $breadcrumb = array('name' => $current_folder->properties['cmis:name'], 'path' => $current_folder->properties['cmis:path']);
        array_push($breadcrumb_array, $breadcrumb);
        $hierarchy_level = 0;
        $breadcrumb_level = sizeof($breadcrumb_array) - 1;

        while ($breadcrumb_level >= 0) {
            $ret['path'][$hierarchy_level]['name'] = $breadcrumb_array[$breadcrumb_level]['name'];
            $ret['path'][$hierarchy_level]['path'] = $breadcrumb_array[$breadcrumb_level]['path'];
            $hierarchy_level++;
            $breadcrumb_level--;
        }
        $this->array_sort($ret['list'], 'usertitle');
        return $ret;
    }
    
    /**
     * Sort multi dimensional array based on key
     */
    private function array_sort(&$array, $key) {
    	$sorter=array();
    	$ret=array();
    	reset($array);
    	foreach ($array as $ii => $va) {
    		$sorter[$ii]=$va[$key];
    	}
    	asort($sorter);
    	foreach ($sorter as $ii => $va) {
    		$ret[$ii]=$array[$ii];
    	}
    	$array=$ret;
    }

    /**
     * Download a file from alfresco
     *
     * @param string $uuid a unique id of directory in alfresco
     * @param string $path path to a directory
     * @return array
     */
    public function get_file($uuid, $file = NULL) {
    	$ticket = $this->userlib->get_user_ticket($this->options, $this->repositoryid);            
        $this->auth_options = array('alf_ticket' => $ticket);
        $this->client = new CMISService($this->server_url, $this->username, $this->password, $this->auth_options);
        $repo_id = $this->id;
        try {
            $node = $this->client->getObject($uuid);
            $url = $this->get_andresco_auth_url($node, NULL, $repo_id);
            $path = $this->prepare_file($file);

            $this->store_file($url, $path);

            // Generate a fully qualified URL
            // Remove, redundant $url = $this->get_relative_root() . $url;

            return array('path' => $path, 'url' => $url);
        } catch (Exception $e) {
            $error_message = get_string('token_expire_error', 'repository_andresco_cmis');
            throw new Exception($error_message);
        }
    }

    /**
     * Select and store an Alfresco file in Moodle (either latest or specific version, and either by copying or linking)
     *
     * @param string $uuid a unique id of file in Alfresco
     * @param string $file file object
     * @param string? version latest or specific
     * @param string repo_id number
     * @return array
     */
    public function get_version_file($uuid, $file = '', $content_version, $repo_id) {
    	$ticket = $this->userlib->get_user_ticket($this->options, $this->repositoryid);            
        $this->auth_options = array('alf_ticket' => $ticket);
        $this->client = new CMISService($this->server_url, $this->username, $this->password, $this->auth_options);
        $node = $this->client->getObject($uuid);
        try {
            $url = $this->get_andresco_auth_url($node, $content_version, $repo_id);
            $path = $this->prepare_file($file);

            if ($this->options['content_access_method'] === 'copy') {
                $this->store_file($url, $path);
            } else {
                $this->get_file_by_reference($url);
            }

            // Generate a fully qualified URL
            // Remove, redundant $url = $this->get_relative_root() . $url;
            
            return array('path' => $path, 'url' => $url);
        } catch (Exception $e) {
            $error_message = get_string('token_expire_error', 'repository_andresco_cmis');
            throw new Exception('Get new version - ' . $error_message);
        }
    }

    /**
     * Function to get version Id for specified version
     * @param string $uuid a unique id of file in Alfresco
     * @return version_id or FALSE if unable to get version node
     */
    public function get_version_id($node) {
    	$ticket = $this->userlib->get_user_ticket($this->options, $this->repositoryid);            
        $this->auth_options = array('alf_ticket' => $ticket);
        $this->client = new CMISService($this->server_url, $this->username, $this->password, $this->auth_options);
        
        if (isset($node->uuid)) { //Fetch Version Id
        $node_uuid = $node->uuid;
        $node_uuid_arr = explode(':', $node_uuid);
        $nodeid = $node_uuid_arr[2];
        $versions_url = $this->server_url . '/s/workspace:SpacesStore/i/' . $nodeid . '/versions';
        $response = $this->client->doGet($versions_url);
        $obj = $this->client->extractObjectVersion($response->body);
        $latest_version_history = $obj->links['version-history'];
        $latest_version_history_arr = explode('/', $latest_version_history);
        $version_id = $latest_version_history_arr[9];
        return $version_id;
    }
        else {
            return FALSE;
        }
        
    }

    /**
     * Store a file in Moodle file store by downloading it from Alfresco.
     *
     * @param   URL (auth.php format) to Alfresco content
     * @param   Path to file to save to in Moodle (temporarily)
     * @return  TRUE if successful in storing file
     *          FALSE otherwise
     */
    public function store_file($auth_url = NULL, $path) {

        global $CFG;

        if (!$auth_url) {
            error_log('Andresco: store_file auth url not supplied');
            return FALSE;
        }

        if (!$path) {
            error_log('Andresco: store_file path not supplied');
            return FALSE;
        }

        $fp = fopen($path, 'w');

        // IMPORTANT NOTE: 
        // cURL was deliberately removed from this code block as it wasn't playing
        // nicely with https, and file_get_contents seems to work a LOT better with https.
        // Options for file_get_contents including setting referrer to "internal"
        // This tells auth.php that it is ok to allow through this request as it
        // comes from internal Andresco code.

        if ($auth_url[0] == '/') {
            // Generate a fully qualified URL
            $auth_url = $CFG->wwwroot . $auth_url;
        }

        $opts = array('http' => array('method' => "GET", 'header' => "Referer: internal\r\n"));
        $context = stream_context_create($opts);

        fwrite($fp, file_get_contents($auth_url, false, $context));
        fclose($fp);

        return TRUE;
    }

    /**
     * Store file reference to Alfresco content in the form of a Auth.php URL.
     *
     * @param   Reference URL
     * @return  TRUE if successful in storing file reference
     *          FALSE otherwise
     */
    public function get_file_by_reference($reference) {

        global $CFG;

        if (!$reference) {
            error_log('Andresco: store_file auth url not supplied:' . $reference);
            return NULL;
        }

        if (is_object($reference)) {
            $url = $reference->reference;
        } else {
            $url = $reference;
        }

        // URLs are typically in relative format so need to append the moodle wwwroot to them
        // to have a fully qualified URL.

        if (!strstr($url, 'http')) {            
            $url = $CFG->wwwroot . $url;
        }
    
        $opts = array('http' => array('method' => "GET", 'header' => "Referer: internal\r\n"));
        $context = stream_context_create($opts);

        file_get_contents($url, false, $context);

        // Returning filesize is the key here to creating a reference.
        // Moodle assumes that if you send back just a filesize, then a reference should be made.

        $filesize = 0;
        foreach ($http_response_header as $h) {
            if (strstr($h, 'Content-Length')) {
                $filesize = (int) str_replace('Content-Length: ', '', $h);
            }
        }

        if ($filesize > 0) {
            return (object) array('filesize' => $filesize);
        } else {
            error_log('Andresco get_file_by_reference Content-Length not found in headers');
            return NULL;
        }
    }

    /**
     * Repository method to create a file reference utilising the
     * {files_reference} table with a reference link in {files}.
     *
     * @param   source (url)
     * @param   Content version
     * @param   Andresco repository ID
     * @return  serialised, base64 encoded url
     *
     * */

    public function get_file_reference($uuid, $content_version = NULL, $repo_id = NULL) {
    	$ticket = $this->userlib->get_user_ticket($this->options, $this->repositoryid);            
        $this->auth_options = array('alf_ticket' => $ticket);
        $this->client = new CMISService($this->server_url, $this->username, $this->password, $this->auth_options);
        try {
            $node = $this->client->getObject($uuid);
            $url = $this->get_andresco_auth_url($node, $content_version, $repo_id);
            
            // Generate a fully qualified URL
            // Remove, redundant $url = $this->get_relative_root() . $url;
            
            return $url;
        } catch (Exception $e) {
            $error_message = get_string('token_expire_error', 'repository_andresco_cmis');
            throw new Exception($error_message);
        }
    }

    /**
     * Repository method to serve the referenced file from Alfresco.
     * Note this decodes the file's location in Alfresco from a serialised, base64 encoded
     * hash in the file references table and sends the result back to Moodle for loading.
     *
     * @param stored_file $storedfile the file that contains the reference
     * @param int $lifetime Number of seconds before the file should expire from caches (default 24 hours)
     * @param int $filter 0 (default)=no filtering, 1=all files, 2=html files only
     * @param bool $forcedownload If true (default false), forces download of file rather than view in browser/plugin
     * @param array $options additional options affecting the file serving
     */
    public function send_file($stored_file, $lifetime = 86400, $filter = 0, $forcedownload = false, array $options = null) {
        $url = $stored_file->get_reference();
        if ($url) {
            header('Location: ' . $url);
        } else {
            send_file_not_found();
        }
    }

    /**
     * Return file URL
     *
     * @param string $url the url of file
     * @param   Content version
     * @param   Andresco repository ID
     * @return string
     **/

    public function get_link($uuid, $content_version = NULL, $repo_id = NULL) {
    	$ticket = $this->userlib->get_user_ticket($this->options, $this->repositoryid);            
        $this->auth_options = array('alf_ticket' => $ticket);
        $this->client = new CMISService($this->server_url, $this->username, $this->password, $this->auth_options);
        $node = $this->client->getObject($uuid);
        //$node = $this->user_session->getNode($this->store, $uuid);
        // BEGIN: Andresco
        // Get the URL for Andresco Auth script on Alfresco Server instead of the
        // actual node URL to allow for authentication and permissions handling.
        $url = $this->get_andresco_auth_url($node, $content_version, $repo_id);
        // END: Andresco
        return $url;
    }

    public function print_search() {
        $str = parent::print_search();
        // BEGIN: Andresco
        // Hide search options for now
        /*
          $str .= '<label>Space: </label><br /><select name="space">';
          foreach ($this->user_session->stores as $v) {
          $str .= '<option ';
          if ($v->__toString() === 'workspace://SpacesStore') {
          $str .= 'selected ';
          }
          $str .= 'value="';
          $str .= $v->__toString().'">';
          $str .= $v->__toString();
          $str .= '</option>';
          }
          $str .= '</select>';
         */
        // END: Andresco
        return $str;
    }

    /**
     * Look for a file
     *
     * @param string $search_text
     * @return array
     */
    public function search($search_text, $page = 0, $repo_path = NULL) {
        global $OUTPUT, $DB;
        $ticket = $this->userlib->get_user_ticket($this->options, $this->repositoryid);            
        $this->auth_options = array('alf_ticket' => $ticket);
        $this->client = new CMISService($this->server_url, $this->username, $this->password, $this->auth_options);
        $propasparrfinal = array();

        $search_text = strtolower($search_text);
        $query_document = "SELECT D.*";
        $query_folder = "SELECT F.*";
        $search_properties_array = array();
        $search_text = preg_replace("([^a-z0-9\s]|(?<=['\"])s)", "", $search_text);
        $stop_words = array();
        $search_properties = array();
        $ret = array();
        $search_array = array();
        $ret['list'] = array();
        $current_folder = $this->client->getObjectByPath($repo_path);
        $ret['path'] = array(array('name' => $current_folder->properties['cmis:name'], 'path' => $repo_path));
        if ($search_text == '' || $search_text == 'Search') {
            $error_message = get_string('search_error', 'repository_andresco_cmis');
            throw new Exception($error_message);
        }
        $repo_config = get_config($this->options['type']);

        if (trim($this->options['search_properties']) != '') {
            $search_properties = explode('|', $this->options['search_properties']);
        }
        $search_properties_array = $this->group_search_properties($search_properties);
        if (trim($this->options['instance_stop_words']) != '') {
            $stop_words = explode(', ', $this->options['instance_stop_words']);
        }
        $search_array = $this->replace_stop_words($search_text, $stop_words);
        $custom_query = $this->get_custom_search_query($search_array, $this->options['instance_default_search'], $repo_path, $search_properties_array);
        foreach ($custom_query[2] as $alias) {
            $query_document .= ", $alias.*";
            $query_folder .= ", $alias.*";
        }
//          Perform search and return matching nodes//
        $search_query_document = "$query_document" . '' . "$custom_query[0]";
        $search_query_folder = "$query_folder" . '' . "$custom_query[1]";
        try {
            $nodes_folder = $this->client->queryCustom($search_query_folder, array(), $propasparrfinal);
        } catch (Exception $e) {
            $nodes_folder = new stdClass();
        }
        try {
            $nodes_document = $this->client->queryCustom($search_query_document, array(), $propasparrfinal);
        } catch (Exception $e) {
            $nodes_document = new stdClass();
        }
        //get property for display item
        $display_prop_data = $DB->get_records('display_item_config', array('repository_instance_id' => $this->id), '', 'property_name');
        $display_prop_data_label = $DB->get_records('display_item_config', array('repository_instance_id' => $this->id), '', 'property_name,display_label');
        foreach ($display_prop_data as $key1 => $value1) {
            $aspectprop = str_replace(']', '', $key1);
            $asparr = explode('[', $aspectprop);
            $prop = $asparr[1];
            $aspect = $asparr[0];
            $propasparrfinal[$aspect][$prop] = "test";
        }
        $propmap = array();
        $i = 0;
        foreach ($display_prop_data_label as $key2 => $value2) {
            $aspectprop1 = str_replace(']', '', $key2);
            $asparr1 = explode('[', $aspectprop1);
            $prop1 = $asparr1[1];
            $propmap[$i][$prop1] = $value2->display_label;
            $i++;
        }
        $istitle = 0;
        $isdescription = 0;
        $isauthor = 0;
        foreach ($propmap as $key3 => $value3) {
            if (isset($value3['cm:title'])) {
                $istitle = 1;
                unset($propmap[$key3]);
            }
            if (isset($value3['cm:description'])) {
                $isdescription = 1;
                unset($propmap[$key3]);
            } if (isset($value3['cm:author'])) {
                $isauthor = 1;
                unset($propmap[$key3]);
            }
        }
        // Perform search and return matching nodes//
//        $search_query_document = "$query_document FROM cmis:document AS D $custom_query[0]";
//        $search_query_folder = "$query_folder FROM cmis:folder AS F $custom_query[1]";
//        $nodes_folder = $this->client->queryCustom($search_query_folder, array(), $propasparrfinal);
//        $nodes_document = $this->client->queryCustom($search_query_document, array(), $propasparrfinal);

        $i = 0;
        foreach ($nodes_folder->objectList as $node) {
            if ($node->properties['cmis:baseTypeId'] == "cmis:folder") {
                $properties = $this->client->getPropertiesOfLatestVersionValidate($node->id, $propasparrfinal);
                if (isset($properties->properties['cm:title'])) {
                    $node->properties['cm:title'] = $properties->properties['cm:title'];
                }
                if (isset($properties->properties['cm:description'])) {
                    $node->properties['cm:description'] = $properties->properties['cm:description'];
                }
                if (isset($properties->properties['cm:author'])) {
                    $node->properties['cm:author'] = $properties->properties['cm:author'];
                }
                if (!isset($node->properties['cm:title'])) {
                    $node->properties['cm:title'] = '';
                }
                if (!isset($node->properties['cm:description'])) {
                    $node->properties['cm:description'] = '';
                }
                if ($this->options['instance_default_label'] == 'TITLE') {
                    $fname = $node->properties['cm:title'] != '' ? $node->properties['cm:title'] : $node->properties['cmis:name'];
                } else if ($this->options['instance_default_label'] == 'NAME') {
                    $fname = $node->properties['cmis:name'];
                } else {
                    if (!empty($node->properties['cm:title'])) {
                        $fname = $node->properties['cm:title'] . ' (' . $node->properties['cmis:name'] . ')';
                    } else {
                        $fname = $node->properties['cmis:name'];
                    }
                }

                $ret['list'][] = array('title' => $node->properties['cmis:name'],
                    'usertitle' => $node->properties['cm:title'],
                    'description' => $node->properties['cm:description'],
                    'path' => $node->properties['cmis:path'],
                    'datemodified_f' => date("d-m-Y h:i:s A", strtotime($node->properties['cmis:lastModificationDate'])),
                    'thumbnail' => $OUTPUT->pix_url('f/folder-64') . '',
                    'createdby' => $node->properties['cmis:createdBy'],
                    'datecreated_f' => date("d-m-Y h:i:s A", strtotime($node->properties['cmis:creationDate'])),
                    'author' => $node->properties['cmis:createdBy'],
                    'children' => array(),
                    'source' => $node->id,
                    'istitle' => $istitle,
                    'isdescription' => $isdescription,
                    'isauthor' => $isauthor,
                    'repoid' => $this->id
                );
                foreach ($propmap as $key4 => $value4) {
                    foreach ($value4 as $key5 => $value5) {
                        if (!empty($properties->properties[$key5])) {
                            $propdata = explode(':', $key5);
                            $ret['list'][$i][strtolower($propdata[1])] = $properties->properties[$key5];
                        }
                    }
                }
            }
            $i++;
        }
        $i = 0;
        foreach ($nodes_document->objectList as $node) {
            if ($node->properties['cmis:baseTypeId'] == "cmis:document") {
                $properties = $this->client->getPropertiesOfLatestVersionValidate($node->id, $propasparrfinal);
                if (isset($properties->properties['cm:title'])) {
                    $node->properties['cm:title'] = $properties->properties['cm:title'];
                }
                if (isset($properties->properties['cm:description'])) {
                    $node->properties['cm:description'] = $properties->properties['cm:description'];
                }
                if (isset($properties->properties['cm:author'])) {
                    $node->properties['cm:author'] = $properties->properties['cm:author'];
                }
                if (!isset($node->properties['cm:title'])) {
                    $node->properties['cm:title'] = '';
                }
                if (!isset($node->properties['cm:description'])) {
                    $node->properties['cm:description'] = '';
                }
                if ($this->options['instance_default_label'] == 'TITLE') {
                    $fname = $node->properties['cm:title'] != '' ? $node->properties['cm:title'] : $node->properties['cmis:name'];
                } else if ($this->options['instance_default_label'] == 'NAME') {
                    $fname = $node->properties['cmis:name'];
                } else {
                    if (!empty($node->properties['cm:title'])) {
                        $fname = $node->properties['cm:title'] . ' (' . $node->properties['cmis:name'] . ')';
                    } else {
                        $fname = $node->properties['cmis:name'];
                    }
                }
                $ret['list'][] = array('title' => $node->properties['cmis:name'],
                    'usertitle' => $node->properties['cm:title'],
                    'description' => $node->properties['cm:description'],
                    'datemodified_f' => date("d-m-Y h:i:s A ", strtotime($node->properties['cmis:lastModificationDate'])),
                    'thumbnail' => $this->get_andresco_thumbnail_url($node),
                    'content_share_link' => $this->get_andresco_share_url($node),
                    'size' => $node->properties['cmis:contentStreamLength'],
                    'source' => $node->id,
                    'createdby' => $node->properties['cmis:createdBy'],
                    'mimetype' => $node->properties['cmis:contentStreamMimeType'],
                    'datecreated_f' => date("d-m-Y h:i:s A", strtotime($node->properties['cmis:creationDate'])),
                    'author' => $node->properties['cmis:createdBy'],
                    'istitle' => $istitle,
                    'isdescription' => $isdescription,
                    'isauthor' => $isauthor,
                    'repoid' => $this->id
                );
                foreach ($propmap as $key4 => $value4) {
                    foreach ($value4 as $key5 => $value5) {
                        if (!empty($properties->properties[$key5])) {
                            $propdata = explode(':', $key5);
                            $ret['list'][$i][strtolower($propdata[1])] = $properties->properties[$key5];
                        }
                    }
                }
            }
            $i++;
        }
        return $ret;
    }

    /**
     * Replace stop words from a search string
     *
     * @return array
     */
    private function replace_stop_words($search_text, $stop_words) {
        $final_stop_array = array();
        foreach ($stop_words as $stop_word) {
            $stop_word = trim($stop_word);
            $final_stop_array[] = " $stop_word ";
        }
        $search_text = str_replace($final_stop_array, ' ', $search_text);
        $search_array = array();
        $search_text = preg_replace('!\s+!', ' ', $search_text);
        $search_array = explode(' ', $search_text);
        return $search_array;
    }

    /**
     * Group search properties on the basis of aspects
     *
     * @return array
     */
    public function group_search_properties($search_properties) {
        $final_properties_array = array();
        $i = 0;
        foreach ($search_properties as $property) {
            $temp = array();
            $temp = explode('(', $property);
            $final_properties_array[$i]['Aspect'] = $temp[0];
            $final_properties_array[$i]['Properties'] = str_replace(')', '', $temp[1]);
            $i++;
        }
        return $final_properties_array;
    }

    /**
     * Custom query for either AND or OR condition
     *
     * @return array
     */
    private function get_custom_search_query($search_array, $logic, $repo_path, $search_properties_array) {
        $ticket = $this->userlib->get_user_ticket($this->options, $this->repositoryid);            
        $this->auth_options = array('alf_ticket' => $ticket);
    	$this->client = new CMISService($this->server_url, $this->username, $this->password, $this->auth_options);
    	
        $path_filter_array = array();
        $aspect_filter_array = array();
        $prop_filter_array = array();
        $prop_filter_explode = array();
        $final_prop_array = array();
        $custom_query_document = '';
        $custom_query_folder = '';
        $join_query_document = '';
        $join_query_folder = '';
        $alias_array = array();
        $custom_path_query_document = "";
        $custom_path_query_folder = "";
        $custom_aspect_query_document = "";
        $custom_aspect_query_folder = "";
        $custom_prop_query_document = "";
        $custom_prop_query_folder = "";
        $custom_prop_joinquery_document = "";
        $custom_prop_joinquery_folder = "";
        if (trim($this->options['path_filter']) != "") {
            $path_filter_array = explode(',', $this->options['path_filter']);
        }
        if (trim($this->options['aspect_filter']) != "") {
            $aspect_filter_array = explode(',', $this->options['aspect_filter']);
        }
        if (trim($this->options['prop_filter']) != "") {
            $prop_filter_array = explode(',', $this->options['prop_filter']);
        }
        $current_folder = $this->client->getObjectByPath($repo_path);
        $current_folder_uuid = str_replace('urn:uuid:', '', $current_folder->uuid);
        $path_query = '';
        $path_query .= 'PATH:"//app:company_home//';
        if (isset($current_node->properties['cmis:name']) && $repo_path != '/') {
            $node_name = $current_node->properties['cmis:name'];
            $node_name = str_replace(' ', '_x0020_', $node_name);
            $path_query .= 'cm:' . $node_name . '//*"';
        } else {
            $path_query .= '*"';
        }
        //Implementation of path filter
        if (count($path_filter_array) != 0) {
            $i = 0;
            foreach ($path_filter_array as $path) {
                $path = trim($path);
                if ($path != '') {
                    if ($i == 0) {
                        $search_folder = $this->client->getObjectByPath($path);
                        $custom_path_query_document .= " (IN_TREE(D,'$search_folder->id')";
                        $custom_path_query_folder .= " (IN_TREE(F,'$search_folder->id')";
                    } else {
                        $search_folder = $this->client->getObjectByPath($path);
                        $custom_path_query_document .= " OR IN_TREE(D,'$search_folder->id')";
                        $custom_path_query_folder .= " OR IN_TREE(F,'$search_folder->id')";
                    }
                    $i++;
                }
            }
            $custom_path_query_document .= ") " . $this->options['instance_default_search'];
            $custom_path_query_folder .= ") " . $this->options['instance_default_search'];
        }
        //Implementation of aspect filter
        if (count($aspect_filter_array) != 0) {
            $i = 0;
            foreach ($aspect_filter_array as $asp) {
                $asp = trim($asp);
                if ($asp != '') {
                    if ($i == 0) {
                        $custom_aspect_query_document .= " (CONTAINS(D,'+ASPECT:\"$asp\"')";
                        $custom_aspect_query_folder .= " (CONTAINS(F,'+ASPECT:\"$asp\"')";
                    } else {
                        $custom_aspect_query_document .= " " . $this->options['aspect_conjunction'] . " CONTAINS(D,'+ASPECT:\"$asp\"')";
                        $custom_aspect_query_folder .= " " . $this->options['aspect_conjunction'] . " CONTAINS(F,'+ASPECT:\"$asp\"')";
                    }
                    $i++;
                }
            }
            $custom_aspect_query_document .= ") " . $this->options['instance_default_search'];
            $custom_aspect_query_folder .= ") " . $this->options['instance_default_search'];
        }
        //Implementation of property filter
        if (count($prop_filter_array) != 0) {
            $prop_array = array();
            $alias = '';
            foreach ($prop_filter_array as $prop) {
                $prop = trim($prop);
                if ($prop != '') {
                    $prop_filter_explode = explode('[', $prop);
                    if (!in_array($prop_filter_explode[0], $prop_array)) {
                        $final_prop_array[$prop_filter_explode[0]][] = str_replace(']', "", $prop_filter_explode[1]);
                        $prop_array[] = $prop_filter_explode[0];
                    } else {
                        $final_prop_array[$prop_filter_explode[0]][] = str_replace(']', "", $prop_filter_explode[1]);
                    }
                }
            }
            $prop_array_count = count($final_prop_array);
            $j = 1;
            foreach ($final_prop_array as $key => $value) {
                if ($j == 1) {
                    $custom_prop_query_document .= " (";
                    $custom_prop_query_folder .= " (";
                }
                $i = 0;
                $alias = $alias . 'A';
                $alias_array[] = $alias;
                $custom_prop_joinquery_document .= " JOIN $key AS $alias ON D.cmis:objectId = $alias.cmis:objectId";
                $custom_prop_joinquery_folder .= " JOIN $key AS $alias ON D.cmis:objectId = $alias.cmis:objectId";
                foreach ($value as $val) {
                    if ($i == 0) {
                        $custom_prop_query_document .= " (CONTAINS($alias,'$val')";
                        $custom_prop_query_folder .= " (CONTAINS($alias,'$val')";
                    } else {
                        $custom_prop_query_document .= " " . $this->options['prop_conjunction'] . " CONTAINS($alias,'$val')";
                        $custom_prop_query_folder .= " " . $this->options['prop_conjunction'] . " CONTAINS($alias,'$val')";
                    }
                    $i++;
                }
                if ($prop_array_count != $j) {
                    $custom_prop_query_document .= ") " . $this->options['prop_conjunction'];
                    $custom_prop_query_folder .= ") " . $this->options['prop_conjunction'];
                }
                $j++;
            }
            $custom_prop_query_document .= ")) " . $this->options['instance_default_search'];
            $custom_prop_query_folder .= ")) " . $this->options['instance_default_search'];
        }
        if (count($search_array) > 0) {
            foreach ($search_array as $string) {
                $str = str_split($string);
                $flag = false;
                foreach ($str as $letter) {
                    if ($letter == '%' || $letter == '*') {
                        $flag = true;
                    }
                }
                $i = 0;
                if ($custom_query_folder == '' && $custom_query_document == '') {
                    if ($flag) {
                        if (count($search_properties_array)) {
                            foreach ($search_properties_array as $properties) {
                                $alias = $alias . 'A';
                                $alias_array[] = $alias;
                                $aspect = explode(':', $properties['Aspect']);
                                $prop_to_override = explode(',', $properties['Properties']);
                                $join_query_document .="JOIN $properties[Aspect] AS $alias ON D.cmis:objectId = $alias.cmis:objectId";
                                $join_query_folder .="JOIN $properties[Aspect] AS $alias ON F.cmis:objectId = $alias.cmis:objectId";
                                if ($i == 0) {
                                    if (strtoupper($string) != 'OR' && strtoupper($string) != 'AND') {
                                        $custom_query_folder .= "((F.cmis:name LIKE '%" . $string . "%')";
                                        $custom_query_document .= "(CONTAINS('\'" . $string . "\''))";
                                    }
                                }
                                if (strtoupper($string) != 'OR' && strtoupper($string) != 'AND') {
                                    foreach ($prop_to_override as $prop) {
                                        $custom_query_folder .= " OR ($alias.$prop LIKE '%" . $string . "%')";
                                    }
                                }
                                $i++;
                            }
                            if (strtoupper($string) != 'OR' && strtoupper($string) != 'AND') {
                                $custom_query_folder .= ")";
                            }
                        } else {
                            if (strtoupper($string) != 'OR' && strtoupper($string) != 'AND') {
                                $custom_query_folder .= "(F.cmis:name LIKE '%" . $string . "%')";
                                $custom_query_document .= "(CONTAINS('\'" . $string . "\''))";
                            }
                        }
                    } else {
                        if (count($search_properties_array)) {
                            foreach ($search_properties_array as $properties) {
                                $alias = $alias . 'A';
                                $alias_array[] = $alias;
                                $prop_to_override = explode(',', $properties['Properties']);
                                $aspect = explode(':', $properties['Aspect']);
                                $join_query_document .=" JOIN $properties[Aspect] AS $alias ON D.cmis:objectId = $alias.cmis:objectId";
                                $join_query_folder .=" JOIN $properties[Aspect] AS $alias ON F.cmis:objectId = $alias.cmis:objectId";
                                if ($i == 0) {
                                    if (strtoupper($string) != 'OR' && strtoupper($string) != 'AND') {
                                        $custom_query_folder .= "((F.cmis:name LIKE '%" . $string . "%')";
                                        $custom_query_document .= "(CONTAINS(D,'" . $string . "') OR CONTAINS(D,'cmis:name:\'*" . $string . "*\'')";
                                    }
                                }
                                foreach ($prop_to_override as $prop) {
                                    if (strtoupper($string) != 'OR' && strtoupper($string) != 'AND') {
                                        $custom_query_folder .= " OR ($alias.$prop LIKE '%" . $string . "%')";
                                        $custom_query_document .= " OR CONTAINS($alias,'$prop:\'*" . $string . "*\'')";
                                    }
                                }
                                $i++;
                            }
                            if (strtoupper($string) != 'OR' && strtoupper($string) != 'AND') {
                                $custom_query_folder .= ")";
                                $custom_query_document .= ")";
                            }
                        } else {
                            if (strtoupper($string) != 'OR' && strtoupper($string) != 'AND') {
                                $custom_query_folder .= "(F.cmis:name LIKE '%" . $string . "%')";
                                $custom_query_document .= "(CONTAINS(D,'" . $string . "') OR CONTAINS(D,'cmis:name:\'*" . $string . "*\''))";
                            }
                        }
                    }
                } else {
                    if (strtoupper($string) != 'OR' && strtoupper($string) != 'AND') {
                        if ($temp == 'OR' || $temp == 'AND') {
                            $custom_query_document .= " $temp ";
                            $custom_query_folder .= " $temp ";
                            unset($temp);
                        } else {
                            $custom_query_document .= " $logic ";
                            $custom_query_folder .= " $logic ";
                        }
                    } else {
                        $temp = strtoupper($string);
                    }
                    $alias = '';
                    if ($flag) {
                        if (count($search_properties_array)) {
                            foreach ($search_properties_array as $properties) {
                                $alias = $alias . 'A';
                                $aspect = explode(':', $properties['Aspect']);
                                $prop_to_override = explode(',', $properties['Properties']);
                                if ($i == 0) {
                                    if (strtoupper($string) != 'OR' && strtoupper($string) != 'AND') {
                                        $custom_query_folder .= "((F.cmis:name LIKE '%" . $string . "%')";
                                        $custom_query_document .= "(CONTAINS('\'" . $string . "\''))";
                                    }
                                }
                                foreach ($prop_to_override as $prop) {
                                    if (strtoupper($string) != 'OR' && strtoupper($string) != 'AND') {
                                        $custom_query_folder .= " OR ($alias.$prop LIKE '%" . $string . "%')";
                                    }
                                }
                                $i++;
                            }
                            if (strtoupper($string) != 'OR' && strtoupper($string) != 'AND') {
                                $custom_query_folder .= ")";
                            }
                        } else {
                            if (strtoupper($string) != 'OR' && strtoupper($string) != 'AND') {
                                $custom_query_folder .= "(F.cmis:name LIKE '%" . $string . "%')";
                                $custom_query_document .= "(CONTAINS('\'" . $string . "\''))";
                            }
                        }
                    } else {
                        if (count($search_properties_array)) {
                            foreach ($search_properties_array as $properties) {
                                $alias = $alias . 'A';
                                $aspect = explode(':', $properties['Aspect']);
                                $prop_to_override = explode(',', $properties['Properties']);
                                if ($i == 0) {
                                    if (strtoupper($string) != 'OR' && strtoupper($string) != 'AND') {
                                        $custom_query_folder .= "((F.cmis:name LIKE '%" . $string . "%')";
                                        $custom_query_document .= "(CONTAINS(D,'" . $string . "') OR CONTAINS(D,'cmis:name:\'*" . $string . "*\'')";
                                    }
                                }
                                foreach ($prop_to_override as $prop) {
                                    if (strtoupper($string) != 'OR' && strtoupper($string) != 'AND') {
                                        $custom_query_folder .= " OR ($alias.$prop LIKE '%" . $string . "%')";
                                        $custom_query_document .= " OR CONTAINS($alias,'$prop:\'*" . $string . "*\'')";
                                    }
                                }
                                $i++;
                            }
                            if (strtoupper($string) != 'OR' && strtoupper($string) != 'AND') {
                                $custom_query_folder .= ")";
                                $custom_query_document .= ")";
                            }
                        } else {
                            if (strtoupper($string) != 'OR' && strtoupper($string) != 'AND') {
                                $custom_query_folder .= "(F.cmis:name LIKE '%" . $string . "%')";
                                $custom_query_document .= "(CONTAINS(D,'" . $string . "') OR CONTAINS(D,'cmis:name:\'*" . $string . "*\''))";
                            }
                        }
                    }
                }
            }
        }

        $main_query_document .= " FROM cmis:document AS D";
        $main_query_folder .= " FROM cmis:folder AS F";

        $exclude_hidden_documents = " AND CONTAINS(D,'NOT ASPECT:\'sys:hidden\'')";
        $exclude_hidden_folders = " AND CONTAINS(D,'NOT ASPECT:\'sys:hidden\'')";

        if ($custom_query_document != '') {
            $query_array[] = $main_query_document . ' ' . $custom_prop_joinquery_document . ' ' . $join_query_document . ' WHERE ' . $custom_path_query_document . ' ' . $custom_aspect_query_document . ' ' . $custom_prop_query_document . ' ' . $custom_query_document . $exclude_hidden_documents; //." AND IN_FOLDER('".$current_folder_uuid."')";
        } else {
            $query_array[] = $main_query_document . ' ' . $custom_prop_joinquery_document . ' ' . ' WHERE ' . $custom_path_query_document . ' ' . $custom_aspect_query_document . ' ' . $custom_prop_query_document . ' ' . $custom_query_document . $exclude_hidden_documents;
        }
        if ($custom_query_folder != '') {
            $query_array[] = $main_query_folder . ' ' . $custom_prop_joinquery_folder . ' ' . ' ' . $join_query_folder . ' WHERE ' . $custom_path_query_folder . ' ' . $custom_aspect_query_folder . ' ' . $custom_prop_query_folder . ' ' . $custom_query_folder . $exclude_hidden_folders; //." AND IN_TREE(F,'".$current_folder_uuid."')";
        } else {
            $query_array[] = $main_query_folder . ' ' . $custom_prop_joinquery_folder . ' ' . ' WHERE ' . $custom_path_query_folder . ' ' . $custom_aspect_query_folder . ' ' . $custom_prop_query_folder . ' ' . $custom_query_folder . $exclude_hidden_folders;
        }
        $query_array[] = $alias_array;
        return $query_array;
    }

    /**
     * Enable mulit-instance
     *
     * @return array
     */
    public static function get_instance_option_names() {
// BEGIN: Andresco
// return array('alfresco_url');
        return array(
            'alfresco_cmis_url',
            'connection_username',
            'connection_password',
            'connection_password_encrypted',
            'instance_default_search',
            'instance_default_label',
        		'show_hide_view_in_repo',
            'starting_node_uuid',
            'andresco_auth',
            'content_access_method',
            'content_versioning_preferences',
            'upload_file_btn_option',
            'create_folder_btn_option',            
            'instance_stop_words',
            'search_properties',
            'alfresco_username_default',
            'alfresco_username_teacher',
            'alfresco_username_admin',
            'search_parameters',
            'path_filter',
            'aspect_filter',
            'aspect_conjunction',
            'prop_filter',
            'prop_conjunction'
        );
// END: Andresco
    }

    /**
     * define a configuration form
     *
     * @return bool
     */
    public static function instance_config_form($mform, $instance = NULL) {
        global $CFG, $PAGE; // Andresco: Added $PAGE global
// BEGIN: Andresco
        $PAGE->requires->css('/repository/andresco_cmis/css/autocomplete.css');
        $PAGE->requires->js('/repository/andresco_cmis/js/andresco_cmis.js');
        $PAGE->requires->js('/totara/core/js/lib/jquery-2.1.0.min.js');
        $PAGE->requires->js('/repository/andresco_cmis/js/jquery.fcbkcomplete.js');
        $PAGE->requires->js('/repository/andresco_cmis/js/init.js');
// END: Andresco

        $mform->addElement('text', 'alfresco_cmis_url', get_string('alfresco_cmis_url', 'repository_andresco_cmis'), array('size' => '40'));
        $mform->addElement('static', 'alfreco_url_intro', '', get_string('alfrescourltext', 'repository_andresco_cmis'));
        $mform->addRule('alfresco_cmis_url', get_string('required'), 'required', null, 'client');
// BEGIN: Andresco
// Connection username
        $mform->addElement('text', 'connection_username', get_string('connection_username', 'repository_andresco_cmis'), array('size' => '20'));
        $mform->addRule('connection_username', get_string('required'), 'required', null, 'client');
// Connection password
        $mform->addElement('password', 'connection_password', get_string('connection_password', 'repository_andresco_cmis'), array('size' => '60'));
// TODO: Fix this to ensure password is provided but only if encrypted flag = No
//$mform->addRule('connection_password', get_string('required'), 'required', null, 'client');
        $mform->disabledIf('connection_password', 'connection_password_encrypted', 'eq', 1);
// TODO: Confirm that the supplied username and password authenticate. If not do not save.
        if (isset($_POST['connection_password'])) {
            if (isset($CFG->passwordsaltmain)) {
                $salt = $CFG->passwordsaltmain;
                if ($_POST['connection_password_encrypted'] == 0) {
                    $connection_password = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $salt, $_POST['connection_password'], MCRYPT_MODE_ECB);
// Encode in base64 so it will save without error to database
                    $connection_password = base64_encode($connection_password);
                    $_POST['connection_password'] = $connection_password;
                    $_POST['connection_password_encrypted'] = 1;
                }
            } else {
// TODO: warning about no salt being available, password will not be encrypted
            }
        }
// Connection password encrypted
        $mform->addElement('selectyesno', 'connection_password_encrypted', get_string('connection_password_encrypted', 'repository_andresco_cmis'));
        $mform->setDefault('connection_password_encrypted', 0);
        $mform->addElement('static', 'connection_password_help', '', get_string('connection_password_help', 'repository_andresco_cmis'));
// Test Connection button
        $mform->addElement('static', '', '', '');
        $mform->addElement('button', 'test_connection', get_string('test_connection', 'repository_andresco_cmis'));
        $mform->addElement('static', '', '', '');

// Default Alfresco group assigned to standard Alfresco users 
        $mform->addElement('text', 'alfresco_username_default', get_string('alfresco_username_default', 'repository_andresco_cmis'), array('size' => '50'));
        $mform->addElement('static', 'alfresco_username_help_1', '', get_string('alfresco_username_help_1', 'repository_andresco_cmis'));

// Alfresco group for new Editing Teacher role 
        $mform->addElement('text', 'alfresco_username_teacher', get_string('alfresco_username_teacher', 'repository_andresco_cmis'), array('size' => '50'));
        $mform->addElement('static', 'alfresco_username_help_2', '', get_string('alfresco_username_help_2', 'repository_andresco_cmis'));

        // Alfresco group for new Administrator role  
        $mform->addElement('text', 'alfresco_username_admin', get_string('alfresco_username_admin', 'repository_andresco_cmis'), array('size' => '50'));
        $mform->addElement('static', 'alfresco_username_help_3', '', get_string('alfresco_username_help_3', 'repository_andresco_cmis'));


// Default search selection
        $options = array(
            'AND' => 'Use AND keyword searching',
            'OR' => 'Use OR keyword searching'
        );
        $mform->addElement('select', 'instance_default_search', get_string('instance_default_search', 'repository_andresco_cmis'), $options);
        $mform->setDefault('instance_default_search', 'AND');
        $mform->addElement('static', 'default_search_help', '', get_string('default_search_help', 'repository_andresco_cmis'));
// Deafult Label shown (name/title)
        $options = array(
            'NAME' => 'Name',
            'TITLE' => 'Title',
            'BOTH' => 'Both'
        );

        $mform->addElement('select', 'instance_default_label', get_string('instance_default_label', 'repository_andresco_cmis'), $options);
        $mform->setDefault('instance_default_label', 'NAME');
        $mform->addElement('static', 'default_label_help', '', get_string('default_label_help', 'repository_andresco_cmis'));
//Setting for showing/hiding 'View in Repository' button
        $options = array(
        		'show' => get_string('show', 'repository_andresco_cmis'),
        		'hide' => get_string('hide', 'repository_andresco_cmis')
        );
        $mform->addElement('select', 'show_hide_view_in_repo', get_string('show_hide_view_in_repo_label', 'repository_andresco_cmis'), $options);
        $mform->setDefault('show_hide_view_in_repo', 'show');
// Starting path
        $mform->addElement('text', 'starting_node_uuid', get_string('starting_node_uuid', 'repository_andresco_cmis'), array('size' => '50'));
        $mform->addElement('static', 'starting_node_uuid_help', '', get_string('starting_node_uuid_help', 'repository_andresco_cmis'));
// Andresco Authentication Script
        $mform->addElement('text', 'andresco_auth', get_string('andresco_auth', 'repository_andresco_cmis'), array('size' => '50'));
        $mform->addElement('static', 'andresco_auth_help', '', get_string('andresco_auth_help', 'repository_andresco_cmis'));
// Content access option
        $content_access_methods = array(
            'link' => get_string('link', 'repository_andresco_cmis'),
            'copy' => get_string('copy', 'repository_andresco_cmis'),
            'link/reference' => get_string('nevercopy', 'repository_andresco_cmis')
        );
        $mform->addElement('select', 'content_access_method', get_string('content_access_method', 'repository_andresco_cmis'), $content_access_methods);
//content versioning preferences
        $content_versioning_preferences = array(
            '1' => get_string('automatically_update', 'repository_andresco_cmis'),
            '2' => get_string('version_permanently', 'repository_andresco_cmis'),
            '3' => get_string('new_versions_as_default', 'repository_andresco_cmis'),
            '4' => get_string('add_latest_version', 'repository_andresco_cmis')
        );
        $mform->addElement('select', 'content_versioning_preferences', get_string('content_versioning_preferences', 'repository_andresco_cmis'), $content_versioning_preferences);

        // Show upload file button
        $options = array(
            'show' => get_string('showuploadfilebtn', 'repository_andresco_cmis'),
            'hide' => get_string('hideuploadfilebtn', 'repository_andresco_cmis')
        );
        $mform->addElement('select', 'upload_file_btn_option', get_string('uploadfilebtnoptions', 'repository_andresco_cmis'), $options);

        // Show create folder button.
        // Yes could use Checkbox, but the UI is quite cramped, better
        // to use short label on left, longer drop down on right
        $options = array(
            'show' => get_string('showcreatefolderbtn', 'repository_andresco_cmis'),
            'hide' => get_string('hidecreatefolderbtn', 'repository_andresco_cmis')
        );
        $mform->addElement('select', 'create_folder_btn_option', get_string('createfolderbtnoptions', 'repository_andresco_cmis'), $options);

        //search parameters
        $options = array('AND' => 'AND', 'OR' => 'OR');
        $mform->addElement('header', 'search_prop_filter', get_string('search_filter_fieldset', 'repository_andresco_cmis'));
        // Instance stop words


        $mform->addElement('text', 'path_filter', get_string('search_path_filter', 'repository_andresco_cmis'), array('size' => '50'));
        $mform->addElement('static', 'search_path_help', '', get_string('search_path_help', 'repository_andresco_cmis'));

        $mform->addElement('text', 'aspect_filter', get_string('search_aspect_filter', 'repository_andresco_cmis'), array('size' => '50'));
        $mform->addElement('select', 'aspect_conjunction', get_string('aspect_conjunction', 'repository_andresco_cmis'), $options);
        $mform->addElement('static', 'search_aspect_help', '', get_string('search_aspect_help', 'repository_andresco_cmis'));

        $mform->addElement('text', 'prop_filter', get_string('search_prop_filter', 'repository_andresco_cmis'), array('size' => '50'));
        $mform->addElement('select', 'prop_conjunction', get_string('prop_conjunction', 'repository_andresco_cmis'), $options);
        $mform->addElement('static', 'search_prop_help', '', get_string('search_prop_help', 'repository_andresco_cmis'));

        $mform->addElement('text', 'instance_stop_words', get_string('instance_stop_words', 'repository_andresco_cmis'), array('size' => '50'));
        $mform->addElement('static', 'stop_words_help', '', get_string('stop_words_help', 'repository_andresco_cmis'));

// Instance search override properties
        // $mform->addElement('select', 'auto_select','autoselect',$options1);

        $html = '';
        $override_prop = '';
        if (!empty($instance->options['search_properties'])) {
            $override_prop = explode('|', $instance->options['search_properties']);
        }
        $final_options_html = "";
        if (is_array($override_prop)) {
            foreach ($override_prop as $prop_key => $prop_value) {
                $options_html = "<option value=" . $prop_value . " class='selected'>" . $prop_value . "</option>";
                $final_options_html = $final_options_html . $options_html;
            }
        }

        $html = "<div><div style='float:left;padding-right: 10px;padding-left: 37px;'>" . get_string('search_properties', 'repository_andresco_cmis') . "</div><select id='id_search_properties' name='search_properties'>" .
                $final_options_html . "</select><div>";

        $mform->addElement('html', $html);
        $mform->addElement('static', 'search_properties_help', '', get_string('search_properties_help', 'repository_andresco_cmis'));
// Version information
        $mform->addElement('header', 'version', 'Version');

        $andresco_version = get_string('version_information', 'repository_andresco_cmis');
        $mform->addElement('static', 'version_information', '', $andresco_version);

        return true;
    }

    // Praj 22/01/2014: This is deprecated and should be removed from the CMIS version (SoapClient check)

    /**
     * Check if SOAP extension enabled
     *
     * @return bool
     */
    public static function plugin_init() {
        if (!class_exists('SoapClient')) {
            print_error('soapmustbeenabled', 'repository_alfresco');
            return false;
        } else {
            return true;
        }
    }

    public function supported_returntypes() {
        // Andresco allow FILE_REFERENCE as a supported return type
        if ($this->options['content_access_method'] === 'copy') {
            return FILE_INTERNAL;
        } else {
            return FILE_EXTERNAL | FILE_REFERENCE;
        }
    }

    // BEGIN: Andresco
    /**
     * Display Upload Form on file picker.
     *
     * @param None
     * @return Displays upload form
     * */
    public function print_upload() {
        global $CFG;
        $ret = array();
        $ret['nologin'] = true;
        $ret['nosearch'] = true;
        $ret['norefresh'] = true;
        $ret['list'] = array();
        $ret['dynload'] = false;
        $ret['content_versioning_preferences'] = $this->options['content_versioning_preferences'];
        $ret['upload'] = array('label' => get_string('upload_file', 'repository_andresco'), 'id' => 'alfresco-upload-form');

        $ret['test'] = '<td class="mdl-left"></td>';
        return $ret;
    }

    public function evaluate_expression($expr_value, $repo_id) {
        global $DB;

        if (isset($expr_value) && $expr_value != '') {
            $default_value = $expr_value;

            $explode_prop_name = explode('REF', $expr_value);
            if (count($explode_prop_name) > 1) {
                $get_expr_name = $explode_prop_name[1];
                $get_expr_name = substr_replace($get_expr_name, "", -1); // replace last ]
                $get_expr_name = ltrim($get_expr_name, '['); // replace first ]                 
                $prop_name_expr_array = $DB->get_records_sql('SELECT * FROM {scafold_ui} WHERE property_name = ? AND repository_instance_id = ? ', array($get_expr_name, $repo_id));
                $prop_name_expr_array = reset($prop_name_expr_array);
                $default_value = $prop_name_expr_array->default_value_expression;
            }
            //----Andresco: Begin----
            //Code To check for moodle context variables
            preg_match_all("/CV\W\S*\W/", $default_value, $match_array);
            foreach ($match_array[0] as $match) {
                $cvar = trim($match);
                $exp = substr($cvar, 4, -1);
                if (strpos($exp, '.')) {
                    $arr_cxt = explode('.', $exp);
                }
                if (strpos($exp, '_')) {
                    $arr_cxt = explode('_', $exp);
                }
                if (strpos($exp, '-')) {
                    $arr_cxt = explode('-', $exp);
                }
                $key = strtoupper($arr_cxt[0]);
                if (array_key_exists($key, $GLOBALS)) {
                    $obj = $GLOBALS[$key];
                    if ((is_object($obj))) {
                        if (isset($obj->$arr_cxt[1])) {
                            $value = $obj->$arr_cxt[1];
                            // @review This strips HTML tags, but there may be use cases in some CV[] fields where we don't 
                            // want to do this, case in point links (<a></a>) and images (<img></img>). These should
                            // be taken into consideration with enhancements to Andresco.
                            $value = strip_tags(trim($value));                                
                            $default_value = str_replace($match, '' . $value . ' ', $default_value);
                        } else {
                            $default_value = '';
                        }
                    } else {
                        $default_value = '';
                    }
                } else {
                    $default_value = '';
                }
            }
            return $default_value;
        }
    }

    public function print_upload_repo($repo_id) {
        global $DB, $CFG, $PAGE, $ANDRESCOFILE, $THEME, $ANDRESCOCOURSE;
        $ANDRESCOFILE = new stdClass();
        $ANDRESCOFILE->name = "Sample File";
        $ANDRESCOFILE->type = "image/png";
        $ANDRESCOFILE->size = 15726;

        include_once('util/get_categoryinfo.php');
        $ANDRESCOCOURSE = create_andrescourse();

        $where3 = " WHERE ( sfui.repository_instance_id = '" . $repo_id . "' ) ";
        $scafold_array = $DB->get_records_sql("SELECT sfui.* FROM {scafold_ui} sfui $where3 ");
        $total_scafold_data = count($scafold_array);

        // Praj: I think this is a typo and should be $append_field instead of $appendField?
        // Causes error as $append_field is not initialised in the foreach loop below.
        // $appendField = $class_reqd = $show_asterik = $chkd_value = '';
        $append_field = $class_reqd = $show_asterik = $chkd_value = '';

        if ($total_scafold_data > 0) {
            $i = 0;
            foreach ($scafold_array as $values) {

                if ($values->is_mandatory == '1') {
                    $class_reqd = 'class="required_upload"';
                    $show_asterik = '<span style="color:red">*</span>';
                } else {
                    $class_reqd = 'class="not_required_upload"';
                    $show_asterik = '';
                }

                if ($values->is_constraint == '1') {
                    $aspectprop = str_replace(']', '', $values->property_name);
                    $asparr = explode('[', $aspectprop);
                    $prop = $asparr[1];
                    $prop_values = $this->fetch_search_values($prop);
                } else {
                    $prop_values = array();
                }
                if (count($prop_values) == 0) {
                    $prop_values[$values->default_value_expression] = $values->default_value_expression;
                }


                $get_expr_value = $this->evaluate_expression($values->default_value_expression, $values->repository_instance_id);


                if ($values->render_type == 'Single Line Textfield') {
                    $append_field .= '<tr class="{!}fp-saveas">
                    <td class="mdl-right"><label>' . $values->display_label . '</label>' . $show_asterik . ':</td>
                    <td class="mdl-left"><input ' . $class_reqd . ' name= "prop[' . $values->property_name . ']" type="text" value="' . htmlentities(trim($get_expr_value), ENT_QUOTES, 'UTF-8') . '"/></td></tr>';
                }
                if ($values->render_type == 'datetime') {
                    $id = 'datetime_' . $i;
                    $append_field .= '<tr class="{!}fp-saveas">
                     
                    <td class="mdl-right"><label>' . $values->display_label . '</label>' . $show_asterik . ':</td>
                    <td class="mdl-left"><input ' . $class_reqd . ' name= "prop[' . $values->property_name . ']" type="text" id=' . $id . ' onclick = "$(\'#\'+this.id).datepicker({dateFormat: \'yy-mm-dd\'});$(\'#\'+this.id).datepicker(\'show\');" autocomplete="off" value="' . htmlentities(trim($get_expr_value), ENT_QUOTES, 'UTF-8') . '"/></td></tr>';
                }

                if ($values->render_type == 'Do Not Render') {
                    $append_field .= '<tr class="{!}fp-saveas">
                    <td class="mdl-right"><label></label></td>
                    <td class="mdl-left"><input  ' . $class_reqd . '  name= "prop[' . $values->property_name . ']" type="hidden" value="' . htmlentities(trim($get_expr_value), ENT_QUOTES, 'UTF-8') . '"/></td></tr>';
                }

                if ($values->render_type == 'Display as Read Only') {
                    $append_field .= '<tr class="{!}fp-saveas">
                    <td class="mdl-right"><label>' . $values->display_label . '</label>' . $show_asterik . ':</td>
                    <td class="mdl-left"><input ' . $class_reqd . ' name= "prop[' . $values->property_name . ']" readonly=readonly type="text" value="' . htmlentities(trim($get_expr_value), ENT_QUOTES, 'UTF-8') . '"/></td></tr>';
                }

                if ($values->render_type == 'Multiline Textarea') {
                    $append_field .= '<tr class="{!}fp-saveas">
                    <td class="mdl-right"><label>' . $values->display_label . '</label>' . $show_asterik . ':</td>
                    <td class="mdl-left"><textarea  ' . $class_reqd . ' name= "prop[' . $values->property_name . ']" rows=5 cols=49 style="border: 1px solid #BBBBBB">' . htmlentities(trim($get_expr_value), ENT_QUOTES, 'UTF-8') . '</textarea></td></tr>';
                }

                if ($values->render_type == 'Drop Down List') {
                    $append_field .= '<tr class="{!}fp-saveas">
                    <td class="mdl-right"><label>' . $values->display_label . '</label>' . $show_asterik . ':</td>
                    <td class="mdl-left">
                    <select ' . $class_reqd . ' name= "prop[' . $values->property_name . ']"  >	';
                    foreach ($prop_values as $key => $value) {
                        if ($value == $get_expr_value) {
                            if ($key == '') {
                                $append_field.= '<option value = "" selected>' . $value . '</option>';
                            } else {
                                $append_field.= '<option value = ' . urlencode($key) . ' selected="selected">' . $value . '</option>';
                            }
                        } else {
                            if ($key == '') {
                                $append_field.= '<option value = "">' . $value . '</option>';
                            } else {
                                $append_field.= '<option value = ' . urlencode($key) . '>' . $value . '</option>';
                            }
                        }
                    }

                    '</select>        
                    </td></tr>';
                }

                if ($values->render_type == 'Multiple selection box') {
                    $append_field .= '<tr class="{!}fp-saveas">
                    <td class="mdl-right"><label>' . $values->display_label . '</label>' . $show_asterik . ':</td>
                    <td class="mdl-left">
                    <select ' . $class_reqd . '  style = "height:80px; width:auto; max-width:312px" name= "prop[' . $values->property_name . ']"  multiple>';
                    foreach ($prop_values as $key => $value) {
                        if ($value == $get_expr_value) {
                            if ($key == '') {
                                $append_field.= '<option value = "" selected>' . $value . '</option>';
                            } else {
                                $append_field.= '<option value = ' . urlencode($key) . ' selected="selected">' . $value . '</option>';
                            }
                        } else {
                            if ($key == '') {
                                $append_field.= '<option value = "">' . $value . '</option>';
                            } else {
                                $append_field.= '<option value = ' . urlencode($key) . '>' . $value . '</option>';
                            }
                        }
                    }

                    '
                    </select>        
                    </td></tr>';
                }

                if ($values->render_type == 'Checkbox') {
                    if ($values->default_value_expression == '1') {
                        $chkd_value = "checked=checked";
                    }

                    $append_field .= '<tr class="{!}fp-saveas">
                    <td class="mdl-right"><label>' . $values->display_label . '</label>' . $show_asterik . ':</td>
                    <td class="mdl-left"><input ' . $class_reqd . ' name= "prop[' . $values->property_name . ']"  type="checkbox"  ' . $chkd_value . '  ></td></tr>';
                }
                $i++;
            }
        }

        // Description may not be in $_POST
        $description = 'None'; // Change to blank space after testing
        if (isset($_POST['description'])) {
            $description = $_POST['description'];
        }

        $rv = '
<div class="fp-upload-form mdl-align">
    <div class="fp-content-center">
           <div>
           <strong>
           <h2>Adding a file to <label class = "fp-folder-name"></label></h2>
           </strong>
           </div>
           <div class="mdl-left" style="padding-left: 162px;">
           <span style="color:red;letter-spacing:2px">*</span> = mandatory
           </div><br/>
        <form enctype="multipart/form-data" method="POST">
            <table >
                <tr class="{!}fp-file">
                    <td class="mdl-right"><label>' . get_string('upload_filename', 'repository_andresco_cmis') . '</label><span style="color:red;">*</span>:</td>
                    <td class="mdl-left"><input type="file" class="required_upload" onchange="putVals(this)"/></td></tr>
                <tr class="{!}fp-saveas">
                    <td class="mdl-right"><label>' . get_string('uploadtitle', 'repository_andresco_cmis') . '</label>:</td>
                    <td class="mdl-left"><input type="text"/></td></tr>
                <tr class="{!}fp-description">
                    <td class="mdl-right"><label>' . get_string('uploaddescription', 'repository_andresco_cmis') . '</label>:</td>
                    <td class="mdl-left"><textarea name="description" rows=5 cols=49 style="border: 1px solid #BBBBBB">' . $_POST['description'] . '</textarea></td></tr>
                <tr class="{!}fp-setauthor">
                    <td class="mdl-right"><label>' . get_string('author', 'repository') . '</label>:</td>
                    <td class="mdl-left"><input type="text"/></td></tr>
        <tr class="{!}fp-setcontentversion">
                        <td class="mdl-right"><label>' . get_string('contentversion', 'repository_andresco_cmis') . '</label>:</td>
                     <td class="mdl-left">
						<select>	
							<option value="1">Always link to latest version</option>	
							<option value="2">Always link to this version</option>	
						</select>
                    </td></tr> ';

        $rv .= $append_field;

        $rv .= '</table>
        </form>
        <div>
            <button class="{!}fp-upload-btn">' . get_string('upload', 'repository') . '</button>
            <button class="{!}fp-upload-cancel-btn">' . get_string('upload_cancel', 'repository_andresco_cmis') . '</button></div>
    </div>
</div> ';
        return preg_replace('/\{\!\}/', '', $rv);
    }

    /**
     * Process add file request and upload to the relevant node in Alfresco.
     *
     * @param Save as filename
     * @param Max bytes
     * @param Upload UUID (UUID of target node where upload is going)
     * @param Environment (e.g. is this TinyMCE editor?)
     * @return JSON encoded result of file upload
     */
    public function upload($saveas_filename, $maxbytes, $upload_uuid = '', $env) {
        global $USER, $CFG, $SITE, $COURSE;
        $ticket = $this->userlib->get_user_ticket($this->options, $this->repositoryid);            
        $this->auth_options = array('alf_ticket' => $ticket);
        $this->client = new CMISService($this->server_url, $this->username, $this->password, $this->auth_options);
        $propasparr = array();
        $temparray = array();
        if ($_FILES['repo_upload_file']['name'] == '') {
            $this->get_listing();
            $error_message = get_string('upload_file_error', 'repository_andresco_cmis');
            throw new Exception($error_message);
        }
        $file_name = $_FILES['repo_upload_file']['name'];
        $file_tmp = $_FILES['repo_upload_file']['tmp_name'];
        $file_type = $_FILES['repo_upload_file']['type'];
        $file_size = $_FILES['repo_upload_file']['size'];
        $itemid = $_REQUEST['itemid'];

        $file_title = '';
        if (isset($_POST['usertitle'])) {
        $file_title = $_POST['usertitle'];
        }

        $file_description = '';
        if (isset($_POST['description'])) {
        $file_description = $_POST['description'];
        }

        $file_author = '';
        if (isset($_POST['author'])) {
        $file_author = $_POST['author'];
        }

        $file_comment = '';
        if (isset($_POST['comment'])) {
        $file_comment = $_POST['comment'];
        }
        
        $update_new_version_id = '';
        if (isset($_POST['update_node_id'])) {
        $update_new_version_id = $_POST['update_node_id'];
        }
        
        $content_version = '';
        if (isset($_POST['contentversion'])) {
        $content_version = $_POST['contentversion'];
        }
        
        $file_contenttype = $file_type;
                
        $file_content = file_get_contents($file_tmp, true);
        if (trim($file_contenttype) == '') {
            $file_contenttype = $file_type;
        }
        if (trim($file_comment) == "") {
            $file_comment = "Version uploaded from " . $SITE->fullname . " by user " . $USER->username . " in course " . $COURSE->fullname . " at " . date("Y-m-d H:i");
        }
        // Set the current node to the node the user requested to upload to
        // This is captured as the node the user is in when they click on
        // the upload button.
        if (!isset($upload_uuid) || $upload_uuid == "undefined") {
            $upload_uuid = "/";
        }
        $current_folder = $this->client->getObjectByPath($upload_uuid);
        $nodes = $this->client->getChildren($current_folder->id);
        $file_exists_as_node = FALSE;
        $upload = NULL;
        if (trim($update_new_version_id) != "no" && trim($update_new_version_id) != "") {
            $upload = $this->client->getObject($update_new_version_id);
            $file_exists_as_node = TRUE;
        } else {
            // Is there already a file with this file name in this folder node?
            foreach ($nodes->objectList as $node) {
                $child_node = $node->properties;
                if ($file_name == $node->properties['cmis:name']) {
                    $file_exists_as_node = TRUE;
                    $upload = $node;
                    break; // Stop looping we have a match
                }
            }
        }
        if (!isset($_POST['prop'])) {
            $propasparr[0]['cm:titled']['cm:title'] = $file_title;
            $propasparr[0]['cm:titled']['cm:description'] = $file_description;
            $propasparr[0]['cm:titled']['cm:author'] = $file_author;
        } else {
            foreach ($_POST['prop'] as $key1 => $value1) {
                $aspectprop = str_replace(']', '', $key1);
                $asparr = explode('[', $aspectprop);
                $prop = $asparr[1];
                $aspect = $asparr[0];
                $propasparr[$aspect][$prop] = urldecode($value1);
            }
            $i = 0;
            foreach ($propasparr as $key2 => $value2) {
                $propasparr[$i][$key2] = $value2;
                unset($propasparr[$key2]);
                $i++;
            }
        }
//        $propasparr[0]["cmis:objectTypeId"] = "cmis:document";

        if ($file_exists_as_node == FALSE && !(isset($upload))) {
            // No matching children (files in folder) create a new child and version it
            foreach ($propasparr as $single_aspect) {
                if (!isset($upload->uuid)) {
//                    $retain_default = $single_aspect;
                    $upload = $this->client->createDocumentForValidate($current_folder->id, $file_name, $single_aspect, $file_content, $file_contenttype);
                } else {
                    $this->client->updateProperties($upload->id, $single_aspect);
                    $upload = NULL;
                    $nodes = $this->client->getChildren($current_folder->id);
                    // Is there already a file with this file name in this folder node?
                    // How can there be if we are creating a new child - looks inefficient and unnecessary - XXX FIXME
                    foreach ($nodes->objectList as $node) {
                        $child_node = $node->properties;
                        if ($file_name == $node->properties['cmis:name']) {
                            $upload = $node;
                            break; // Stop looping we have a match
                        }
                    }
                }
            }
//            $this->client->updateProperties($upload->id, $retain_default);
            $upload = NULL;
            $nodes = $this->client->getChildren($current_folder->id);
            // Is there already a file with this file name in this folder node?
            // As per loop above - FIXME XXX
            foreach ($nodes->objectList as $node) {
                $child_node = $node->properties;
                if ($file_name == $node->properties['cmis:name']) {
                    $upload = $node;
                    break; // Stop looping we have a match
                }
            }
        }
        $version_url_type = 0;
        if (isset($_POST['contentversion']) && ($_POST['contentversion'] == 2 || $this->options['content_versioning_preferences'] == 2)) {
            $version_url_type = 2;
        } else if (isset($_POST['contentversion']) && ($_POST['contentversion'] == 1 || $this->options['content_versioning_preferences'] == 1)) {
            $version_url_type = 1;
        }

        //get node uuid
        // $aupload = explode(':', $upload->uuid);
        // Create version after save
        // Note only minor versioning available. Major versioning not implemented yet.
        if ($file_exists_as_node == TRUE) {
//            $retain_default = $propasparr[0];
            $this->client->setContentStream($upload->id, $file_content, $file_contenttype, $properties = array());
            foreach ($propasparr as $single_aspect) {
                $this->client->updateProperties($upload->id, $single_aspect);
            }
//            $this->client->updateProperties($upload->id, $retain_default);
            $upload = NULL;
            $nodes = $this->client->getChildren($current_folder->id);
            // Is there already a file with this file name in this folder node?
            // FIXME - XXX
            foreach ($nodes->objectList as $node) {
                $child_node = $node->properties;
                if ($file_name == $node->properties['cmis:name']) {
                    $upload = $node;
                    break; // Stop looping we have a match
                }
            }
        }
        //Fetch Version Id
        /* $node_uuid = $node->uuid;
          $node_uuid_arr = explode(':', $node_uuid);
          $nodeid = $node_uuid_arr[2];
          $versions_url = $this->server_url . '/s/workspace:SpacesStore/i/' . $nodeid . '/versions';
          $response = $this->client->doGet($versions_url);
          $obj = $this->client->extractObjectVersion($response->body);
          $latest_version_history = $obj->links['version-history'];
          $latest_version_history_arr = explode('/', $latest_version_history);
          $version_id = $latest_version_history_arr[9]; */

        $repo_id = $this->id;
        $temparray['success'] = "true";
        //END : ANDRESCO CMIS
        if ($file_exists_as_node == TRUE) {
            // Get URL for an existing node
            $url = $this->get_andresco_auth_url($upload, $version_url_type, $repo_id);
        }

        if ($file_exists_as_node == FALSE) {
            $url = $this->get_andresco_auth_url($upload, $version_url_type, $repo_id);
        }
        if ($env == 'url' || $env == 'editor') {
// Return the URL of the newly uploaded file
            $link = array();
            $link['file'] = $file_name;
            $link['title'] = $file_title;
            $link['description'] = $file_description;
            $link['type'] = 'link';
            $link['url'] = $url;
            $link['title'] = $file_title;
            $link['description'] = $node->properties['cm:description'];
            return $link;
        } else if ($env == 'filemanager') { 
            $context = context_user::instance($USER->id);
            $fs = get_file_storage();
            $record = new stdClass();
            $record->filearea = 'draft';
            $record->component = 'user';
            $record->filepath = "/";
            $record->itemid = $itemid;
            $record->license = $license;
            $record->author = $file_author;
            $record->source = self::build_source_field($url);
            $record->filename = $file_name;
            $record->contextid = $context->id;
            $record->userid = $USER->id;
            $record->description = $node->properties['cm:description'];
            if ($fs->file_exists($record->contextid, $record->component, $record->filearea, $record->itemid, $record->filepath, $record->filename)) {
                // Overwrite the existing file record.
                $unused_filename = repository::get_unused_filename($record->itemid, $record->filepath, $record->filename);
                $record->filename = $unused_filename;
                if ($this->options['content_access_method'] === 'copy') {
                    $stored_file = $fs->create_file_from_pathname($record, $_FILES['repo_upload_file']['tmp_name']);
                } else {
                    $stored_file = $fs->create_file_from_reference($record, $repo_id, $url);
                }
                $info = repository::overwrite_existing_draftfile($record->itemid, $record->filepath, $file_name, $record->filepath, $record->filename);
                return $info;
            } else {
                if ($this->options['content_access_method'] === 'copy') {
                    $stored_file = $fs->create_file_from_pathname($record, $_FILES['repo_upload_file']['tmp_name']);
                } else {
                    $stored_file = $fs->create_file_from_reference($record, $repo_id, $url);
                }
                return array(
                    'url' => $url,
                    'id' => $itemid,
                    'file' => $file_name,
                    'type' => 'file',
                    'title' => $file_title,
                    'description' => $record->description
                );
            }
        } else {
            // Return the file
            return $this->get_version_file($upload->id, '', $content_version, $repo_id);
        }
    }

// END: Andresco
    /**
     *
     * Process add folder request and upload to the relevant node in Alfresco.
     * @param string $folder_name
     * @param string $title
     * @param string $author
     * @param string $upload_uuid
     * @param string $env
     */
    public function uploadfolder($folder_name, $title, $author, $upload_uuid, $env) {
    	$ticket = $this->userlib->get_user_ticket($this->options, $this->repositoryid);            
        $this->auth_options = array('alf_ticket' => $ticket);
        $this->client = new CMISService($this->server_url, $this->username, $this->password, $this->auth_options);
        global $SITE, $USER, $COURSE;
        $folder_filter = "{http://www.alfresco.org/model/content/1.0}folder";
        $folder_title = $_POST['title'];
        $folder_description = $_POST['description'];
        $folder_author = $_POST['author'];
        $folder_comment = $_POST['comment'];
        if ($folder_comment == "") {
            $folder_comment = "Version uploaded from " . $SITE->fullname . " by user " . $USER->username . " in course " . $COURSE->fullname . " at " . date("Y-m-d H:i");
        }
        $folder_exists_as_node = FALSE;
        $upload = NULL;
// Set the upload node to the node the user requested to upload to
// This is captured as the node the user is in when they click on
// the upload button.
        $upload_node = $this->client->getObjectByPath($upload_uuid);
        $nodes = $this->client->getChildren($upload_node->id);
// Is there already a folder with this folder name in this folder node?
        foreach ($nodes->objectList as $node) {
            if ($folder_name == $node->properties['cmis:name'] && $node->properties['cmis:baseTypeId'] == "cmis:folder") {
                $folder_exists_as_node = TRUE;
                $upload = $node;
                break; // Stop looping we have a match
            }
        }
        if ($folder_exists_as_node == FALSE && !(isset($upload))) {
// No matching children (files in folder) create a new child and version it
//$upload = $upload_node->createChild('cm_folder', 'cm_contains', $folder_name);
        }
        $properties = array();
// Set meta-data
        $properties['cmis:name'] = $folder_name;
        //$properties['title'] = $folder_title;
        $properties['cmis:createdBy'] = $folder_author;
        //$properties['author'] = $folder_author;
        //$properties['description'] = $folder_description;
        //$properties['cmis:checkinComment'] = $folder_comment;
        //$properties['cmis:versionable'] = "true";
        //$properties['cmis:isImmutable'] = FALSE;
// Save changes
        $this->client->createFolder($upload_node->id, $folder_name, $properties);
// Create version after save
// Note only minor versioning available. Major versioning not implemented yet.
//$upload->createVersion($file_comment, false);
        echo json_encode(array('success' => true));
        die;
    }

    public function deleteNode($obj_url) {
        $ticket = $this->userlib->run_as_admin();
        $auth_options = array('alf_ticket' => $ticket);
        $this->client = new CMISService($this->server_url, $this->username, $this->password, $auth_options);
        $ret = $this->client->doDelete($obj_url);
        return $ret;
    }

    /**
     * Function to generate Random FileName.
     * @param type $length
     * @return type
     */
    public function randomString($length = 5) {
        $str = "";
        $characters = array_merge(range('A', 'Z'), range('a', 'z'), range('0', '9'));
        $max = count($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }
        return 'ALF' . $str . time();
    }

    public function uploadToValidate(&$data, $flag) {
        $ticket = $this->userlib->run_as_admin();
        $auth_options = array('alf_ticket' => $ticket);
        $this->client = new CMISService($this->server_url, $this->username, $this->password, $auth_options);
        global $USER, $CFG, $SITE, $COURSE;
        $temparray = array();
        $file_name = 'SFT12puririshabh3';
        //$file_tmp = $_FILES['repo_upload_file']['tmp_name'];
        $file_type = 'txt';
        // $file_size = $_FILES['repo_upload_file']['size'];
        // $itemid = $_REQUEST['itemid'];
        // $file_title = '1xcffc3ro';
        // $file_description = 'terfesffstrtr890ddet andr';
        // $file_author = 'apoor4cc34xsscxcxcddd';
        $file_comment = 'Initial File';
        $file_contenttype = 'application/octet-stream';
        // $update_new_version_id = $_POST['update_node_id'];
        // $content_version = $_POST['contentversion'];
        $file_content = "This is test";

        // Set the current node to the node the user requested to upload to
        // This is captured as the node the user is in when they click on
        // the upload button.
        $upload_uuid = "/";
        $current_folder = $this->client->getObjectByPath($upload_uuid);
        $nodes = $this->client->getChildren($current_folder->id);
        $file_exists_as_node = FALSE;
        foreach ($nodes->objectList as $node) {
            $child_node = $node->properties;
            if ($file_name == $node->properties['cmis:name']) {
                // Is there already a file with this file name in this folder node?
                $file_exists_as_node = TRUE;
                $upload = $node;
                break; // Stop looping we have a match
            }
        }
        $data['cmis:objectTypeId'] = 'cmis:document';
        $data['cmis:checkinComment'] = $file_comment;
        // $asp_prop = array('cm:titled' => array('cm:title' => $file_title, 'cm:description' => $file_description, 'cm:author' => $file_author), 'cmis:objectTypeId' => 'cmis:document', 'cmis:checkinComment' => $file_comment);
        $asp_prop = $data;
        if ($flag) {
            $upload = $this->client->createDocumentForValidate($current_folder->id, $file_name, $asp_prop, $file_content, $file_contenttype);
        } else {
            $this->client->updateProperties($upload->id, $asp_prop);
        }
        $response = $this->client->getPropertiesOfLatestVersionValidate($upload->id, $asp_prop);
        return $response;
    }

    public function getPropDatatype($prop, $id) {
        $ticket = $this->userlib->run_as_admin();
        $auth_options = array('alf_ticket' => $ticket);
        $this->client = new CMISService($this->server_url, $this->username, $this->password, $auth_options);
        $aspectprop = $prop;
        $aspectprop = str_replace(']', '', $aspectprop);
        $asparr = explode('[', $aspectprop);
        $prop = $asparr[1];
        $properties = array();
        $htm = '';
        $errcode = '';
        if (!empty($this->options['alfresco_cmis_url'])) {
            $repo_url = str_replace('s/cmis', '', $this->options['alfresco_cmis_url']);
            $url = $repo_url . "service/andresco/prop/constraints?props=" . $prop;
            try {
                $json_aspect = $this->client->doGet($url);
                $decode_json = json_decode($json_aspect->body);
            } catch (Exception $e) {
                $decode_json[0]->$prop = "invalid property";
                $errcode = "ERROR";
            }
            if (count($decode_json) != 0 && $errcode == '') {
                if ($decode_json[0]->$prop == "invalid property") {
                    $errcode = "NA";
                    $msg = "This property is not type of list constraint";
                } else {
                    $errcode = "A";
                    $msg = "This property is a type of list constraint";
                    $properties = $this->fetch_search_values($prop);
                    if (count($properties) != 0) {
                        $htm .= "<select onblur='getvalue(this)' class = 'default_value' name='default_value_expression_" . $id . "' id='default_value_expression_" . $id . "' style='width: 155px;'>";
                        foreach ($properties as $key => $value) {
                            $htm .= "<option value='" . $key . "'>" . $value . "</option>";
                        }
                        $htm .= "</select><br/><span id='error_span_" . $id . "' style='color:red;'></span><span id='span_" . $id . "' style='color:green;'></span>";
                    } else {
                        $htm = false;
                    }
                }
            } else if ($errcode == '') {
                $errcode = "INVALID";
                $msg = "Property format or name is invalid";
            }
            $data = array('errcode' => $errcode, 'msg' => $msg, 'list' => $htm);
            return json_encode($data);
        }
    }

    // Function to get the list type values

    public function fetch_search_values($property_name) {
        global $CFG;

        // Prefix property with cm: if a prefix is not supplied. Now required in validation.
        if (!strstr($property_name, ':')) {
            $property_name = 'cm:'.$property_name;
        }

        $alfresco_url = $this->options['alfresco_cmis_url'];
        $alfresco_url = str_replace('/s/cmis', '/service/andresco/prop/constraints', $alfresco_url);
        $post_url = $alfresco_url . "?props=" . $property_name;
        $retarr = array();
        
        try {
            $ch = curl_init($post_url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $curl_response = curl_exec($ch);
            curl_close($ch);
            $options_array = json_decode($curl_response);
            foreach ($options_array[0]->$property_name as $option) {
                $retarr[$option] = $option;
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
        return $retarr;
    }

    /**
     * Get the relative root path based on the configured wwwroot. This is particularly important
     * for multihomed instances where wwwroot doesn't translate directly to relative root.
     * 
     * @global   $CFG->wwwroot
     * @param    None
     * @return   Relative Root
     **/

    private function get_relative_root() {

        global $CFG;
        $url = $CFG->wwwroot;

        if ($first_slash_pos = strpos($url, '/', 8)) {
            $url_relative = substr($url, $first_slash_pos, strlen($url));
        }
        else {
            $url_relative = ''; 
        }    

        return $url_relative;

    }

    /**
     * Get a general file picker template customised with Andresco specific
     * items e.g. upload file and create folder buttons
     *
     * @param   None
     * @return  General template for Andresco (based on files/renderer.php)
     *
     **/

    public function get_general_template() {        
        
        global $OUTPUT;

        $rv = '
<div tabindex="0" class="file-picker fp-generallayout" role="dialog" aria-live="assertive">
    <div class="fp-repo-area">
        <ul class="fp-list">
            <li class="{!}fp-repo"><a href="#"><img class="{!}fp-repo-icon" alt=" " width="16" height="16" />&nbsp;<span class="{!}fp-repo-name"></span></a></li>
        </ul>
    </div>
    <div class="fp-repo-items" tabindex="0">
        <div class="fp-navbar">
            <div>
                <div class="{!}fp-toolbar">
                    <div class="{!}fp-tb-back"><a href="#">'.get_string('back', 'repository').'</a></div>
                    <div class="{!}fp-tb-search"><form></form></div>
                    <div class="{!}fp-tb-refresh"><a href="#"><img src="'.$OUTPUT->pix_url('a/refresh').'" /></a></div>';

        // Andresco upload file button
        if ($this->options['upload_file_btn_option'] == 'hide') {
            $rv .= '<div class="{!}fp-tb-upload-andresco"><a href="#"></a></div>'; // Still need a filler otherwise there are layout issues
        }
        else {
            $rv .= '<div class="{!}fp-tb-upload-andresco"><a href="#"><img src="' . $OUTPUT->pix_url('a/add_file') . '" /> ' . get_string('upload_file', 'repository_andresco_cmis') . '</a></div>';            
        }

        // Andresco create folder button
        if ($this->options['create_folder_btn_option'] == 'hide') {
            $rv .= '<div class="{!}fp-tb-create-folder-andresco"><a href="#"></a></div>'; // Still need a filler otherwise there are layout issues
        }
        else {
            $rv .= '<div class="{!}fp-tb-create-folder-andresco"><a href="#"><img src="' . $OUTPUT->pix_url('a/create_folder') . '" /> ' . get_string('upload_folder', 'repository_andresco_cmis') . '</a></div>';
        }

        $rv .= '<div class="{!}fp-tb-manage"><a href="#"><img src="'.$OUTPUT->pix_url('a/setting').'" /> '.get_string('manageurl', 'repository').'</a></div>
                    <div class="{!}fp-tb-help"><a href="#"><img src="'.$OUTPUT->pix_url('a/help').'" /> '.get_string('help').'</a></div>
                    <div class="{!}fp-tb-message"></div>
                </div>
                <div class="{!}fp-viewbar">
                    <a title="'. get_string('displayicons', 'repository') .'" class="{!}fp-vb-icons" href="#"><img alt="" src="'. $OUTPUT->pix_url('fp/view_icon_active', 'theme') .'" /></a>
                    <a title="'. get_string('displaydetails', 'repository') .'" class="{!}fp-vb-details" href="#"><img alt="" src="'. $OUTPUT->pix_url('fp/view_list_active', 'theme') .'" /></a>
                    <a title="'. get_string('displaytree', 'repository') .'" class="{!}fp-vb-tree" href="#"><img alt="" src="'. $OUTPUT->pix_url('fp/view_tree_active', 'theme') .'" /></a>
                </div>
                <div class="fp-clear-left"></div>
            </div>
            <div class="fp-pathbar">
                 <span class="{!}fp-path-folder"><a class="{!}fp-path-folder-name" href="#"></a></span>
            </div>
        </div>
        <div class="{!}fp-content"></div>
    </div>
</div>';
        return preg_replace('/\{\!\}/', '', $rv);        

    }

    /**
     * Get a select file picker template customised with Andresco specific
     * items e.g. view in share/alfresco and upload new version buttons.
     *
     * @param   None
     * @return  Select template for Andresco (based on files/renderer.php)
     *
     **/

    public function get_select_template() {

        global $DB, $OUTPUT;

        $display_prop_data_label = $DB->get_records('display_item_config', null, 'weightage ASC', 'id,repository_instance_id,display_label,render_type,property_name');

        $rv = '
<div class="file-picker fp-select">
    <div class="fp-select-loading">
        <img src="'.$OUTPUT->pix_url('i/loading_small').'" />
    </div>
    <form class="form-horizontal">
        <div class="fp-forminset">
                <div class="fp-linktype-2 control-group control-radio clearfix">
                    <label class="control-label control-radio">'.get_string('makefileinternal', 'repository').'</label>
                    <div class="controls control-radio">
                        <input type="radio"/>
                    </div>
                </div>
                <div class="fp-linktype-1 control-group control-radio clearfix">
                    <label class="control-label control-radio">'.get_string('makefilelink', 'repository').'</label>
                    <div class="controls control-radio">
                        <input type="radio"/>
                    </div>
                </div>
                <div class="fp-linktype-4 control-group control-radio clearfix">
                    <label class="control-label control-radio">'.get_string('makefilereference', 'repository').'</label>
                    <div class="controls control-radio">
                        <input type="radio"/>
                    </div>
                </div>
                <div class="fp-saveas control-group clearfix">
                    <label class="control-label">'.get_string('saveas', 'repository').'</label>
                    <div class="controls">
                        <input type="text"/>
                    </div>
                </div>
                <div class="fp-setauthor control-group clearfix">
                    <label class="control-label">'.get_string('author', 'repository').'</label>
                    <div class="controls">
                        <input type="text"/>
                    </div>
                </div>';

        $rv1 = '';
        foreach ($display_prop_data_label as $label => $value) {
            $aspectprop1 = str_replace(']', '', $value->property_name);
            $asparr1 = explode('[', $aspectprop1);
            $prop1 = $asparr1[1];
            $propdata = explode(':', $prop1);
            $rv1 = $rv1 . '<tr style ="display:none" class="{!}fp-setc' . strtolower($propdata[1]) . $value->repository_instance_id . ' input_type">
                <td class="mdl-right"><label>' . $value->display_label . '</label>:</td>';
            if ($value->render_type == 'Single Line Textfield') {
                $rv3 = '<td class="mdl-left"><input type="text" class = "render' . strtolower($propdata[1]) . $value->repository_instance_id . '"/></td></tr>';
            } else {
                $rv3 = '<td class="mdl-left"><textarea rows=3 cols=41 class = "render' . strtolower($propdata[1]) . $value->repository_instance_id . '" style="width : 276px;"></textarea></td></tr>';
            }
            $rv1 .= $rv3;
        }                

        $rv2 = '
        <div class="fp-setlicense control-group clearfix">
            <label class="control-label">'.get_string('chooselicense', 'repository').'</label>
            <div class="controls">
                <select></select>
            </div>
        </div>
        <div class="fp-setcontentversion control-group clearfix">
            <label class="control-label">'.get_string('contentversion', 'repository_andresco_cmis').'</label>
            <div class="controls">
                <select class="contentVersion"> 
                    <option value="1">Always link to latest version</option>    
                    <option value="2">Always link to this version</option>  
                </select>
            </div>
        </div>
       </div>       
       <div class="andresco-buttons">
            <input type="hidden" id="view_share_url" /><input type="hidden" id="node_mime_type" />
            <input type="hidden" id="node_description" /><input type="hidden" id="node_alf_title" /><input type="hidden" id="update_node_uuid" />
       </div>
       <div class="fp-select-buttons">
            <button class="fp-select-confirm btn-primary btn">'.get_string('getfile', 'repository').'</button>
            <button class="fp-select-view">' . get_string('view_on_share', 'repository_andresco_cmis') . '</button>
            <button class="fp-select-upload-version">' . get_string('upload_new_version', 'repository_andresco_cmis') . '</button>
            <button class="fp-select-cancel btn-cancel btn">'.get_string('cancel').'</button>
        </div>
    </form>
    <div class="fp-info clearfix">
        <div class="fp-hr"></div>
        <p class="fp-thumbnail"></p>
        <div class="fp-fileinfo">
            <div class="fp-datemodified">'.get_string('lastmodified', 'repository').'<span class="fp-value"></span></div>
            <div class="fp-datecreated">'.get_string('datecreated', 'repository').'<span class="fp-value"></span></div>
            <div class="fp-size">'.get_string('size', 'repository').'<span class="fp-value"></span></div>
            <div class="fp-license">'.get_string('license', 'repository').'<span class="fp-value"></span></div>
            <div class="fp-author">'.get_string('author', 'repository').'<span class="fp-value"></span></div>
            <div class="fp-dimensions">'.get_string('dimensions', 'repository').'<span class="fp-value"></span></div>
        </div>
    <div>
</div>';
        return $rv . $rv1 . $rv2;
    }

}

