<?php

/**
 * Lang strings.
 *
 * Language strings to be used by report/logs
 *
 * @package    report_studentsatrisk
 * @copyright  Androgogic
 */

$string['studantsatrisk:view'] = 'View students retention report';
$string['pluginname'] = 'Students at risk';
$string['studentsatrisk'] = 'Student retention report';
$string['field_student'] = 'Student (Site level and subject level)';

$string['field_subject'] = 'Subject (Subject level)';
$string['allcourses'] = 'All courses ({$a})';
$string['allfilteredcourses'] = 'All filtered courses ({$a->count}/{$a->total})';
$string['nofilteredcourses'] = 'No filtered courses (0/{$a})';
$string['allselectedcourses'] = 'All selected courses ({$a->count}/{$a->total})';
$string['noselectedcourses'] = 'No selected courses';
$string['available'] = 'Available';
$string['selected'] = 'Selected';
$string['addall'] = 'Add all';
$string['addsel'] = 'Add to selection';
$string['removeall'] = 'Remove all';
$string['removesel'] = 'Remove from selection';
$string['selectedlist'] = 'Selected course list...';

$string['legend_accountcreate'] = 'Account created date (Site level)';
$string['field_accountcreatestart'] = 'Starting from';
$string['field_accountcreateend'] = 'Ending at';

$string['legend_sitelogin'] = 'First login date (Site level)';
$string['field_siteloginstart'] = 'Starting from';
$string['field_siteloginend'] = 'Ending at';

$string['legend_lastlogin'] = 'Last login date (Site level)';
$string['field_lastloginstart'] = 'Starting from';
$string['field_lastloginend'] = 'Ending at';

$string['legend_lastcourseaccess'] = 'Last course access (Subject level)';
$string['field_lastcourseaccessstart'] = 'Starting from';
$string['field_lastcourseaccessend'] = 'Ending at';

$string['legend_subjectviews'] = 'Number of subject views (Subject level)';
$string['field_subjectviewsstart'] = 'Starting from';
$string['field_subjectviewsend'] = 'Ending at';

$string['legend_forumviews'] = 'Number of forum views (Subject level)';
$string['field_forumviewsstart'] = 'Starting from';
$string['field_forumviewsend'] = 'Ending at';

$string['legend_forumposts'] = 'Number of forum posts (Subject level)';
$string['field_forumpostsstart'] = 'Starting from';
$string['field_forumpostsend'] = 'Ending at';

$string['legend_assignmentattempts'] = 'Number of Assignment attempts (Subject level)';
$string['field_assignmentattemptsstart'] = 'Starting from';
$string['field_assignmentattemptsend'] = 'Ending at';

$string['field_pagetype'] = 'View as';

$string['field_search'] = 'Generate';
$string['field_clear'] = 'Clear filters';

$string['runreport'] = 'Report frequency';
$string['runreportlong'] = 'Frequency data is collected for the report'; 
$string['showrows'] = 'Display rows';
$string['showrowslong'] = 'Number of output rows to display per page (browser display only)';

$string['table_student'] = 'Site-level data';
$string['table_course'] = 'Subject-level data';

$string['report_studid'] = 'Student ID';
$string['report_firstname'] = 'Firstname';
$string['report_lastname'] = 'Surname';
$string['report_email'] = 'Email address';
$string['report_timeusercreated'] = 'Date account created';
$string['report_firstaccess'] = 'First login';
$string['report_lastlogin'] = 'Last login';
$string['report_logincount'] = 'Login count';
$string['report_logintime'] = 'Time logged in';
$string['report_code'] = 'Subject code';
$string['report_shortname'] = 'Subject short name';
$string['report_fullname'] = 'Subject long name';
$string['report_completion'] = 'Completion status';
$string['report_completiondate'] = 'Completion date';
$string['report_courseviews'] = 'Number of course homepage views';
$string['report_firstviewed'] = 'Date first viewed course homepage';
$string['report_lastviewed'] = 'Date last viewed course homepage';
$string['report_numforumviews'] = 'Number of forum views';
$string['report_dateforumlastview'] = 'Date of last forum view';
$string['report_numposts'] = 'Number of forum posts';
$string['report_dateposts'] = 'Date last forum post';
$string['report_timespentinforums'] = 'Time spent in forums in course';
$string['report_numassess'] = 'Number of assessments completed (assignments and quizzes)';
$string['report_datelastassess'] = 'Date last assessment attempted (assignment or quiz)';
$string['report_timespendasses'] = 'Time spent in assessments (assignment or quiz)';
