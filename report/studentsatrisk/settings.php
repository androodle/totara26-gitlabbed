<?php

/**
 * Links and settings
 *
 * Contains settings used by logs report.
 *
 * @package    report_studentsatrisk
 * @copyright  Androgogic
 */

defined('MOODLE_INTERNAL') || die;


// just a link to course report
$ADMIN->add('reports', new admin_externalpage('reportstudentsatrisk_view', get_string('studentsatrisk', 'report_studentsatrisk'), "$CFG->wwwroot/report/studentsatrisk/index.php", 'report/studentsatrisk:view'));

if ($ADMIN->fulltree)
{
	$options = array("300" => "5 Minutes", "1200" => "20 minutes", "3600" => "1 hour", "43200" => "12 Hours", "86400" => "24 Hours");
	
    $settings->add(new admin_setting_configselect('report_studentsatrisk_runreport', new lang_string('runreport', 'report_studentsatrisk'),
                       new lang_string('runreportlong', 'report_studentsatrisk'), '86400', $options));
                       
    $settings->add(new admin_setting_configtext('report_studentsatrisk_showrows', new lang_string('showrows', 'report_studentsatrisk'),
        new lang_string('showrowslong', 'report_studentsatrisk'), 10, PARAM_INT));
}
