<?php
/**
 * This file contains functions used by the studentsatrisk reports
 *
 *
 * @package    report_studentsatrisk
 * @copyright  Androgogic
 */
require_once($CFG->dirroot.'/report/studentsatrisk/locallib.php');

defined('MOODLE_INTERNAL') || die;

function report_studentsatrisk_cron()
{
	global $DB, $CFG;
	
	$studentcountinsert = 0;
	$studentcountupdate = 0;
	
	$frequency = $CFG->report_studentsatrisk_runreport;
	
	//STUDENT TABLE
	$current = time();
	
	$check_entry = $DB->get_record_sql("SELECT * FROM {report_studentsatrisk_stud} LIMIT 1");
	if($current - $check_entry->modified < $frequency)
	{
		print "Update not yet scheduled";
		return TRUE;
	}
	
	
	$tablesql = "{user} u LEFT JOIN (SELECT COUNT(id) AS logincount, userid FROM {log} log WHERE action='login' GROUP BY userid) log ON u.id=log.userid";
	$sqlparams = array(0);
	
	
	$results = $DB->get_recordset_sql("SELECT * FROM $tablesql WHERE u.deleted = ?", $sqlparams);
	
	if($results->valid())
	{
		
		
		//Code goes here
		foreach($results as $result)
		{
			$entry_exists = $DB->record_exists("report_studentsatrisk_stud", array("userid" => $result->id));
			
			$report = ($entry_exists) ? $DB->get_record("report_studentsatrisk_stud", array("userid" => $result->id)) : new stdClass;
			$report->studid = ($result->idnumber) ? $result->idnumber : "";
			$report->modified = time();
			$report->userid = $result->id;
			$report->firstname = $result->firstname;
			$report->lastname = $result->lastname;
			$report->email = $result->email;
			$report->timeusercreated = $result->timecreated ? $result->timecreated : NULL;
			$report->firstaccess = $result->firstaccess ? $result->firstaccess : NULL;
			$report->lastlogin = $result->lastlogin ? $result->lastlogin : NULL;
			$report->logincount = $result->logincount;
			$report->logintime = report_studentsatrisk_get_total_logintime($result->id);
			
			if($entry_exists)
			{
				$DB->update_record("report_studentsatrisk_stud", $report);
				$studentcountupdate++;
			}
			else
			{
				$DB->insert_record("report_studentsatrisk_stud", $report);
				$studentcountinsert++;
			}
		}
	}
	$results->close();
	
	//COURSE TABLE
	
	$coursecountinsert = 0;
	$coursecountupdate = 0;
	
	$fields = array(
		"id" => "ue.id",
		"userid" => "u.id",
		"courseid" => "c.id",
		"studid" => "u.idnumber",
		"firstname" => "u.firstname",
		"lastname" => "u.lastname",
		"email" => "u.email",
		"code" => "c.idnumber",
		"shortname" => "c.shortname",
		"fullname" => "c.fullname"
	);
	
	
	$fieldsql = report_studentsatrisk_build_field_sql($fields);
	
	$tablesql = "{user_enrolments} ue LEFT JOIN {user} u ON ue.userid = u.id LEFT JOIN {enrol} e ON ue.enrolid = e.id LEFT JOIN {course} c ON e.courseid = c.id";
	$sqlparams = array(0);
	
	
	$results = $DB->get_recordset_sql("SELECT $fieldsql FROM $tablesql WHERE u.deleted = ?", $sqlparams);
	$current = time();
	foreach($results as $result)
	{
		$entry_exists = $DB->record_exists("report_studentsatrisk_course", array("userid" => $result->userid, "courseid" => $result->courseid));
		
		$report = ($entry_exists) ? $DB->get_record("report_studentsatrisk_course", array("userid" => $result->userid, "courseid" => $result->courseid)) : new stdClass;
		
		$completion = $DB->get_record("course_completions", array("userid" => $result->userid, "course" => $result->courseid));
			
		//	print_r(array);
		$courseviews = $DB->get_records("log", array('userid' => $result->userid, "course" => $result->courseid, "action" => "view", "module" => "course"));
			
		$countcourseviews = count($courseviews);
		$firstcourseview = reset($courseviews);
		$lastcourseview = end($courseviews);
			
		$forumviews = $DB->get_records("log", array('userid' => $result->userid, "course" => $result->courseid, "action" => "view forum", "module" => "forum"));
		
		$countforumviews = count($forumviews);
		$lastforumview = end($forumviews);
		
		
		$sql = "SELECT * FROM {log} WHERE userid=? AND course=? AND (action=? OR action=?) AND module=?";
		$forumposts = $DB->get_records_sql($sql, array($result->userid, $result->courseid, "add post", "update post", "forum"));
		
		$countforumposts = count($forumposts);
		$lastforumpost = end($forumposts);
			
			
		$forumtime = report_studentsatrisk_get_module_logintime($result->userid, $result->courseid, "forum");
			
		$sql = "SELECT COUNT(*) FROM {course_modules_completion} cmc LEFT JOIN {course_modules} cm ON cm.id = cmc.coursemoduleid LEFT JOIN {modules} m ON m.id = cm.module WHERE cmc.completionstate = ? AND cmc.userid = ? AND cm.course = ? AND (m.name=? OR m.name=? OR m.name=?)";
		$modcount = $DB->count_records_sql($sql, array(TRUE, $result->userid, $result->courseid, "assign", "assignment", "quiz"));
		$lastquizattempt = $DB->get_records("log", array('userid' => $result->userid, "course" => $result->courseid, "action" => "attempt", "module" => "quiz"), "time DESC", "*", 0, 1);
		$lastassignattempt = $DB->get_records("log", array('userid' => $result->userid, "course" => $result->courseid, "action" => "submit for grading", "module" => "assign"), "time DESC", "*", 0, 1);
		
		$lastquizattempt = reset($lastquizattempt);
		$lastassignattempt = reset($lastassignattempt);
		
		$lastquiztime = empty($lastquizattempt) ? 0 : $lastquizattempt->time;
		$lastassigntime = empty($lastassignattempt) ? 0 : $lastassignattempt->time;
		
		$lastmodattempt = ($lastquiztime > $lastassigntime) ? $lastquiztime : $lastassigntime;	
			
		$quiztime = report_studentsatrisk_get_module_logintime($result->userid, $result->courseid, "quiz");
			
		$assigntime = report_studentsatrisk_get_module_logintime($result->userid, $result->courseid, "assign");
			
		$modtime = $quiztime + $assigntime;
		
		
		$report->studid = ($result->studid) ? $result->studid : "";
		$report->modified = time();
		$report->userid = $result->userid;
		$report->courseid = $result->courseid;
		$report->firstname = $result->firstname;
		$report->lastname = $result->lastname;
		$report->email = $result->email;
		$report->code = $result->code;
		$report->shortname = $result->shortname;
		$report->fullname = $result->fullname;
		$report->completion = ($completion && $completion->timecompleted) ? TRUE : FALSE;
		$report->completiondate = ($completion && $completion->timecompleted) ? $completion->timecompleted : NULL;
		$report->courseviews = $countcourseviews;
		$report->firstviewed = ($countcourseviews > 0) ? $firstcourseview->time : NULL;
		$report->lastviewed = ($countcourseviews > 0) ? $lastcourseview->time : NULL;
		$report->numforumviews = $countforumviews;
		$report->dateforumlastview = ($countforumviews > 0) ? $lastforumview->time : NULL;
		$report->numposts = $countforumposts;
		$report->dateposts = ($countforumposts > 0) ? $lastforumpost->time : NULL;
		$report->timespentinforums = $forumtime;
		$report->numassess = $modcount;
		$report->datelastassess = ($lastmodattempt > 0) ? $lastmodattempt : NULL;
		$report->timespendasses = $modtime;
		
		
		if($entry_exists)
		{
			$DB->update_record("report_studentsatrisk_course", $report);
			$coursecountupdate++;
		}
		else
		{
			$DB->insert_record("report_studentsatrisk_course", $report);
			$coursecountinsert++;
		}
	}
	$results->close();
	print "Students updated: $studentcountupdate Students inserted $studentcountinsert Courses updated: $coursecountupdate Courses inserted $coursecountinsert <br/>\r\n";
}

