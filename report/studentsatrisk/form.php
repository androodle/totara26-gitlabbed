<?php

/**
 * Students At Risk report filter form
 * 
 **/

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');
}

require_once($CFG->dirroot.'/lib/formslib.php');

class studentsatrisk_form extends moodleform {
		 
    function definition() {
		
        global $USER, $CFG, $DB;
				 
        $mform =& $this->_form;
        
        $acount =& $this->_customdata['acount'];
        $scount =& $this->_customdata['scount'];
        $acourse =& $this->_customdata['acourse'];
        $scourse =& $this->_customdata['scourse'];
        $total  =& $this->_customdata['total'];
        
        
        $mform->addElement('hidden', 'orderstudent', '');
        $mform->setType('orderstudent', PARAM_RAW_TRIMMED);
        $mform->addElement('hidden', 'ordercourse', '');
        $mform->setType('ordercourse', PARAM_RAW_TRIMMED);
        
        //Student select
        
        $users = array("" => "Choose...");        $concat_field = $DB->sql_concat("firstname", '\' \' ', 'lastname');
        
        $users = $users + $DB->get_records_menu('user', array("deleted" => 0), 'firstname', "id, $concat_field"); 
        $mform->addElement('select', 'user', get_string('field_student', 'report_studentsatrisk'), $users); // Add elements to your form
        $mform->setType('user', PARAM_INT);                   //Set type of element
        $mform->setDefault('user', "");
        
        
        //Subject select
        $achoices = array();
        $schoices = array();

        if (is_array($acourse))
        {
            if ($total == $acount)
            {
                $achoices[0] = get_string('allcourses', 'report_studentsatrisk', $total);
            }
            else
            {
                $a = new stdClass();
                $a->total  = $total;
                $a->count = $acount;
                $achoices[0] = get_string('allfilteredcourses', 'report_studentsatrisk', $a);
            }
            $achoices = $achoices + $acourse;

        }
        else
        {
            $achoices[-1] = get_string('nofilteredcourses', 'report_studentsatrisk', $total);
        }

        if (is_array($scourse))
        {
            $a = new stdClass();
            $a->total  = $total;
            $a->count = $scount;
            $schoices[0] = get_string('allselectedcourses', 'report_studentsatrisk', $a);
            $schoices = $schoices + $scourse;

        }
        else
        {
            $schoices[-1] = get_string('noselectedcourses', 'report_studentsatrisk');
        }
        
        foreach($schoices as $id=>&$choice)
        {
			if($id == -1 || $id == 0)
			{
				continue;
			}
			$choice = $achoices[$id];
		}
        
        
        
        
        $objs = array();
        $objs[0] =& $mform->createElement('select', 'acourse', get_string('available', 'report_studentsatrisk'), $achoices, 'size="15"');
        $objs[0]->setMultiple(true);
        $objs[1] =& $mform->createElement('select', 'scourse', get_string('selected', 'report_studentsatrisk'), $schoices, 'size="15"');
        $objs[1]->setMultiple(true);
        
        $grp =& $mform->addElement('group', 'usersgrp', get_string('field_subject', 'report_studentsatrisk'), $objs, ' ', false);
        //$mform->addHelpButton('usersgrp', 'users', 'bulkusers');

        $mform->addElement('static', 'comment');

        $objs = array();
        $objs[] =& $mform->createElement('submit', 'addsel', get_string('addsel', 'report_studentsatrisk'));
        $objs[] =& $mform->createElement('submit', 'removesel', get_string('removesel', 'report_studentsatrisk'));
        $objs[] =& $mform->createElement('submit', 'addall', get_string('addall', 'report_studentsatrisk'));
        $objs[] =& $mform->createElement('submit', 'removeall', get_string('removeall', 'report_studentsatrisk'));
        $grp =& $mform->addElement('group', 'buttonsgrp', get_string('selectedlist', 'report_studentsatrisk'), $objs, array(' ', '<br />'), false);
        
        $renderer =& $mform->defaultRenderer();
        $template = '<label class="qflabel" style="vertical-align:top">{label}</label> {element}';
        $renderer->setGroupElementTemplate($template, 'usersgrp');
        
        //Subject select
        /*$course = $DB->get_records_menu('course', array(), 'fullname', 'id, fullname');  
        $select = $mform->addElement('select', 'course', get_string('field_subject', 'report_studentsatrisk'), $course); // Add elements to your form
        $mform->setType('course', PARAM_RAW_TRIMMED);                   //Set type of element         
        $select->setMultiple(true);*/
        
        $options = array("optional" => TRUE);
        
        //Account created date
        $mform->addElement('header', 'accountcreate', get_string('legend_accountcreate', 'report_studentsatrisk'));
        $mform->addElement('date_selector', 'accountcreatestart', get_string('field_accountcreatestart', 'report_studentsatrisk'), $options);
        $mform->addElement('date_selector', 'accountcreateend', get_string('field_accountcreateend', 'report_studentsatrisk'), $options);
        
        //Site login date
        $mform->addElement('header', 'sitelogin', get_string('legend_sitelogin', 'report_studentsatrisk'));
        $mform->addElement('date_selector', 'siteloginstart', get_string('field_siteloginstart', 'report_studentsatrisk'), $options);
        $mform->addElement('date_selector', 'siteloginend', get_string('field_siteloginend', 'report_studentsatrisk'), $options);
        
        //Last login date
        $mform->addElement('header', 'lastlogin', get_string('legend_lastlogin', 'report_studentsatrisk'));
        $mform->addElement('date_selector', 'lastloginstart', get_string('field_lastloginstart', 'report_studentsatrisk'), $options);
        $mform->addElement('date_selector', 'lastloginend', get_string('field_lastloginend', 'report_studentsatrisk'), $options);
        
        //Last course access 
        $mform->addElement('header', 'lastcourseaccess', get_string('legend_lastcourseaccess', 'report_studentsatrisk'));
        $mform->addElement('date_selector', 'lastcourseaccessstart', get_string('field_lastcourseaccessstart', 'report_studentsatrisk'), $options);
        $mform->addElement('date_selector', 'lastcourseaccessend', get_string('field_lastcourseaccessend', 'report_studentsatrisk'), $options);
        
        
        $selection = array();
        
        $selection["-1"] = "Select";
        
        for($i=0; $i<=50; $i++)
        {
			$selection["$i"] = $i;
		}
        
        //Subject views
        $mform->addElement('header', 'subjectviews', get_string('legend_subjectviews', 'report_studentsatrisk'));
        $mform->addElement('select', 'subjectviewsstart', get_string('field_subjectviewsstart', 'report_studentsatrisk'), $selection);
        $mform->setType('subjectviewsstart', PARAM_INT);
        $mform->setDefault('subjectviewsstart', "-1");
        $mform->addElement('select', 'subjectviewsend', get_string('field_subjectviewsend', 'report_studentsatrisk'), $selection);
        $mform->setType('subjectviewsend', PARAM_INT);
        $mform->setDefault('subjectviewsend', "-1");
        
        
        //Number of forum views
        $mform->addElement('header', 'forumviews', get_string('legend_forumviews', 'report_studentsatrisk'));
        $mform->addElement('select', 'forumviewsstart', get_string('field_forumviewsstart', 'report_studentsatrisk'), $selection);
        $mform->setType('forumviewsstart', PARAM_INT);
        $mform->setDefault('forumviewsstart', "-1");
        $mform->addElement('select', 'forumviewsend', get_string('field_forumviewsend', 'report_studentsatrisk'), $selection);
        $mform->setType('forumviewsend', PARAM_INT);
        $mform->setDefault('forumviewsend', "-1");
         
         
        //Number of forum posts
        $mform->addElement('header', 'forumposts', get_string('legend_forumposts', 'report_studentsatrisk'));
        $mform->addElement('select', 'forumpostsstart', get_string('field_forumpostsstart', 'report_studentsatrisk'), $selection);
        $mform->setType('forumpostsstart', PARAM_INT);
        $mform->setDefault('forumpostsstart', "-1");
        $mform->addElement('select', 'forumpostsend', get_string('field_forumpostsend', 'report_studentsatrisk'), $selection);
        $mform->setType('forumpostsend', PARAM_INT);
        $mform->setDefault('forumpostsend', "-1");
        
        //Number of assignmnet attempts
        $mform->addElement('header', 'assignmentattempts', get_string('legend_assignmentattempts', 'report_studentsatrisk'));
        $select = $mform->addElement('select', 'assignmentattemptsstart', get_string('field_assignmentattemptsstart', 'report_studentsatrisk'), $selection);
		//$select->setSelected('-1');
        $mform->setType('assignmentattemptsstart', PARAM_INT);
        $mform->setDefault('assignmentattemptsstart', -1);
        $select = $mform->addElement('select', 'assignmentattemptsend', get_string('field_assignmentattemptsend', 'report_studentsatrisk'), $selection);
        //$select->setSelected('-1');
        $mform->setType('assignmentattemptsend', PARAM_INT);
        $mform->setDefault('assignmentattemptsend', -1);
       
		$mform->closeHeaderBefore('pagetype');
		 
		 $page_types = array(
			"html" => "Display in browser",
			"csv" => "Download as a CSV file",
			"xls" => "Download as an Excel file",
		 );
		 //Download type
        $mform->addElement('select', 'pagetype', get_string('field_pagetype', 'report_studentsatrisk'), $page_types); // Add elements to your form
        $mform->setType('pagetype', PARAM_RAW_TRIMMED);                   //Set type of element
		
		//Generate report/clear filters buttons
		$objs = array();
        $objs[] =& $mform->createElement('submit', 'generate', get_string('field_search', 'report_studentsatrisk'));
        $objs[] =& $mform->createElement('submit', 'clear', get_string('field_clear', 'report_studentsatrisk'));
        $mform->addElement('group', 'actionsgrp', "With report", $objs, ' ', false);
    }
	
}

// End of report/studentsatrisk/form.php
