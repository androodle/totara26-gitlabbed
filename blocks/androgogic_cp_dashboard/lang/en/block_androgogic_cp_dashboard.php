<?php

/** 
 * Androgogic CP Dashboard Block: Language Pack
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     01/08/2014
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 **/
$string['pluginname'] = 'Androgogic CP Dashboard';
$string['plugintitle'] = 'Androgogic CP Dashboard';

//spares
//$string[''] = '';
//$string[''] = '';
//$string[''] = '';

//cron strings
$string['cron_hours'] = 'Cron hours';
$string['cron_hours_explanation'] = 'The hour at which the cron should execute in 24 hour format(0-23). Can be multiple: if so use semi-colon delimited list';

//common items
$string['datasubmitted'] = 'The data has been submitted';
$string['itemdeleted'] = 'The item has been deleted';
$string['noresults'] = 'There were no results from your search';
$string['block_androgogic_cp_dashboard:edit'] = 'Edit objects within the Androgogic CP Dashboard block';
$string['block_androgogic_cp_dashboard:delete'] = 'Delete objects within the Androgogic CP Dashboard block';
$string['created_by'] = 'Created By';
$string['date_created'] = 'Date Created';
$string['modified_by'] = 'Modified By';
$string['date_modified'] = 'Date Modified';
$string['id'] = 'Id';

$string['user_id'] = 'User';