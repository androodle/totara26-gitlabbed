<?php
/** 
 * Androgogic Cp Dashboard Block: Class
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     01/08/2014
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 **/

class block_androgogic_cp_dashboard extends block_base {

	function init(){
		$this->title = get_string('plugintitle','block_androgogic_cp_dashboard');
	} //end function init

	function get_content(){
		global $CFG,$USER,$DB;
                require_once($CFG->dirroot . '/blocks/androgogic_training_history/lib.php');
                if ($this->content !== null) {
			return $this->content;
		}
                $this->content = new stdClass;
                if(isset($USER->id)){
                
                    //let's get their targets, for which we need the lowest hanging match on the org/pos hierarchies 
                    //that matches their primary pos assignment
                    $user_org_pos = block_androgogic_training_history_get_user_org_pos($USER);
                    if(!$user_org_pos){
                        // then it's all over
                        $this->content->text = "";
                        return $this->content;
                    }
                    $org_pos_period = block_androgogic_training_history_get_org_pos_period(
                            $user_org_pos->org_framework_id,$user_org_pos->pos_framework_id,
                            $user_org_pos->pos_sortthread,$user_org_pos->org_sortthread);
                    if(!$org_pos_period){
                        // then it's all over
                        $this->content->text = "";
                        return $this->content;
                    }

                    list($period_start_date,$period_end_date) = block_androgogic_training_history_get_cpd_period($USER, $org_pos_period);
                    
                    //armed with this let's get their targets
                    $sql = "select sum(cpd_target) as total_cpd_target 
                    from mdl_androgogic_training_history_cpd_targets
                    where org_pos_period_id = $org_pos_period->id";
                    $cpd_target = $DB->get_record_sql($sql);

                    $total_cpd_target = $cpd_target->total_cpd_target;
                    
                    //add/subtract points if need be
                    $sql = "select sum(points) as points_override 
                    from mdl_androgogic_user_points_override
                    where user_id = $USER->id";
                    $points_override = $DB->get_field_sql($sql);
                    
                    $total_cpd_target = $total_cpd_target+$points_override;
                    
                    //prorata if need be
                    $days_in_period = ($period_end_date-$period_start_date)/(60*60*24);
                    $prorata = $days_in_period/365;                    
                    $total_cpd_target = $total_cpd_target*$prorata;
                    
                    //and points earned
                    $sql = "select ifnull(sum(cpd_points),0) as total_th_cpd_points 
                        from mdl_androgogic_training_history_competencies_cpd_points c
                        inner join mdl_androgogic_training_history h on c.training_history_id = h.id
                        where user_id = $USER->id  "
                            . "and h.date_issued between $period_start_date and $period_end_date";
                    $th_cpd_points_earned = $DB->get_field_sql($sql);
                    $sql = "select ifnull(sum(cpd_points),0) as total_course_cpd_points 
                        FROM mdl_course_completions cc
                        INNER JOIN mdl_course c ON cc.course = c.id
                        INNER JOIN mdl_androgogic_training_history_course_competencies_cpd_points cpd on c.id = cpd.course_id
                        WHERE STATUS >= 50
                        AND cc.userid = $USER->id  "
                        . "and cc.timecompleted between $period_start_date and $period_end_date";
                    $course_cpd_points_earned = $DB->get_field_sql($sql);
                    $total_cpd_points_earned = $th_cpd_points_earned + $course_cpd_points_earned;
                    if($total_cpd_target > 0){
                        $percentage_cpd_points_earned = $total_cpd_points_earned*100/$total_cpd_target;
                    }
                    else{
                        $percentage_cpd_points_earned = 0;
                    }
                    $sql = "select * 
                    from mdl_androgogic_training_history_cpe_targets
                    where org_pos_period_id = $org_pos_period->id";
                    $cpe_targets = $DB->get_records_sql($sql);

                    $total_cpe_target = 0;
                    $total_cpe_hours_earned = 0;
                    foreach($cpe_targets as $cpe_target){
                        $total_cpe_target += $cpe_target->cpe_target;
                        $sql = "select cpe_hours from mdl_androgogic_training_history_competencies_cpe_hours c
                        inner join mdl_androgogic_training_history h on c.training_history_id = h.id
                        where user_id = $USER->id and c.competency_id = $cpe_target->competency_id "
                                . "and h.date_issued between $period_start_date and $period_end_date";
                        $cpe_hours_earned = $DB->get_field_sql($sql);
                        if($cpe_hours_earned > $cpe_target->cpe_target){
                            $cpe_hours_earned = $cpe_target->cpe_target;
                        }
                        $total_cpe_hours_earned += $cpe_hours_earned;
                    } 
                    if($total_cpe_target > 0){
                        //get any cpe overrides
                        $sql = "select sum(hours) as hours_override 
                        from mdl_androgogic_user_hours_override
                        where user_id = $USER->id";
                        $hours_override = $DB->get_field_sql($sql);
                        $total_cpe_target += $hours_override;
                        //prorata
                        $total_cpe_target = $total_cpe_target*$prorata;
                        $percentage_cpe_hours_earned = $total_cpe_hours_earned*100/$total_cpe_target;
                    }
                    else{
                        $percentage_cpe_hours_earned = 0;
                    }
                    ob_start();
                    $progressbarcpd = new progress_bar('',200);
                    $progressbarcpd->create();
                    $progressbarcpd->update_full($percentage_cpd_points_earned, $total_cpd_points_earned);
                    $progressbarcpd_output = ob_get_contents();
                    ob_end_clean();
                    $progressbarcpd_output = str_ireplace('center','left',$progressbarcpd_output);                
                    $progressbarcpd_output = str_ireplace('margin:0 auto;','',$progressbarcpd_output);
                    //$progressbarcpd_output = str_ireplace($total_cpd_points_earned,$total_cpd_target,$progressbarcpd_output);
                    $progressbarcpd_output = str_ireplace('update_progress_bar','update_progress_bar_dashboard',$progressbarcpd_output);

                    ob_start();
                    $progressbarcpe = new progress_bar('',200);
                    $progressbarcpe->create();
                    $progressbarcpe->update_full($percentage_cpe_hours_earned, $total_cpe_hours_earned);
                    $progressbarcpe_output = ob_get_contents();
                    ob_end_clean();
                    $progressbarcpe_output = str_ireplace('center','left',$progressbarcpe_output);
                    $progressbarcpe_output = str_ireplace('margin:0 auto;','',$progressbarcpe_output);
                    //$progressbarcpe_output = str_ireplace($total_cpe_hours_earned,$total_cpe_target,$progressbarcpe_output);
                    $progressbarcpe_output = str_ireplace('update_progress_bar','update_progress_bar_dashboard',$progressbarcpe_output);

                    $total_cpd_target_int = (int)$total_cpd_target;
                    $total_cpe_target_int = (int)$total_cpe_target;
                    $this->content->text = "<table border='0'>"
                            . "<tr><td colspan='2'>CPD</td></tr><tr><td>$progressbarcpd_output</td><td valign='middle'>&nbsp;$total_cpd_target_int</td></tr>"
                            . "<tr><td colspan='2'>CPE</td></tr><tr><td>$progressbarcpe_output</td><td valign='middle'>&nbsp;$total_cpe_target_int</td></tr>"
                            . "</table>";
                    $period_statement = "<p>Period</p><p>" . date('d/m/Y',$period_start_date) ." to " . date('d/m/Y',$period_end_date) . "</p>";
                    $this->content->text .= $period_statement;
		
                }
                else{
                    $this->content->text = "";
                }
                return $this->content;

	} //end function get_content

	function has_config(){
            return true;
	} //end function has_config

} //end class block_androgogic_cp_dashboard

// End of blocks/androgogic_cp_dashboard/block_androgogic_cp_dashboard.php