<?php
/** 
 * Androgogic Cp Dashboard Block: Permissions
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     01/08/2014
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 **/

$capabilities = array(
'block/androgogic_cp_dashboard:edit' => array(
'captype' => 'write',
'contextlevel' => CONTEXT_SYSTEM,
'archetypes' => array(
'manager' => CAP_ALLOW,
)
),
'block/androgogic_cp_dashboard:delete' => array(
'captype' => 'write',
'contextlevel' => CONTEXT_SYSTEM,
'archetypes' => array(
'manager' => CAP_ALLOW,
)
),
);

// End of blocks/androgogic_cp_dashboard/db/access.php