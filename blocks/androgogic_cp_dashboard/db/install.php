<?php
/** 
 * Androgogic Cp Dashboard Block: Install DB scripts
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     01/08/2014
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 **/

function xmldb_block_androgogic_cp_dashboard_install() {
global $DB;
$result = true;
// create tables for the block
if (!$DB->get_manager()->table_exists('androgogic_cp_dashboard')) {
$sql = "CREATE TABLE `mdl_androgogic_cp_dashboard` ( `id` bigint(11) NOT NULL AUTO_INCREMENT, `startdate` bigint(11) NOT NULL, `enddate` bigint(11) NOT NULL, `user_id` bigint(11) NOT NULL, `created_by` bigint(10) NOT NULL COMMENT 'relates to user table', `date_created` datetime NOT NULL, `modified_by` bigint(10) DEFAULT NULL COMMENT 'relates to user table', `date_modified` datetime DEFAULT NULL, PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=latin1";
$DB->execute($sql);
}
return $result;
} 