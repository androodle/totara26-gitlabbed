<?php
/** 
 * Androgogic Cp Dashboard Block: Version
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     01/08/2014
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 **/

$plugin->version = 2014080300;
$plugin->requires = 2010112400; // Indicate we need at least Moodle 2.0

// End of blocks/androgogic_cp_dashboard/version.php