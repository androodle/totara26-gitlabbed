<?php

/** 
 * Androgogic Support Block: Delete object
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     06/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Delete one of the server_statuses
 *
 **/

$id = required_param('id', PARAM_INT);
$DB->delete_records('androgogic_server_status',array('id'=>$id));
echo $OUTPUT->notification(get_string('itemdeleted','block_androgogic_support'), 'notifysuccess');

?>
