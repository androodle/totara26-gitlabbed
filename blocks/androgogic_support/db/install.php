<?php

/**
 * Androgogic Support Block: Install DB scripts
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     03/07/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 * */
function xmldb_block_androgogic_support_install() {
    global $DB;
    $result = true;
    // create tables for the block
    if (!$DB->get_manager()->table_exists('androgogic_faq')) {
        $sql = "CREATE TABLE `mdl_androgogic_faq` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `question` text NOT NULL,
  `answer` text NOT NULL,
  `listing_order` smallint(6) NOT NULL,
  `created_by` bigint(10) NOT NULL COMMENT 'relates to user table',
  `date_created` datetime NOT NULL,
  `modified_by` bigint(10) DEFAULT NULL COMMENT 'relates to user table',
  `date_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";
        $DB->execute($sql);
    }
     if (!$DB->get_manager()->table_exists('androgogic_server_status')) {
        $sql = "CREATE TABLE `mdl_androgogic_server_status` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `summary` text NOT NULL,
  `active` tinyint(4) NOT NULL,
  `listing_order` smallint(6) NOT NULL,
  `created_by` bigint(10) NOT NULL COMMENT 'relates to user table',
  `date_created` datetime NOT NULL,
  `modified_by` bigint(10) DEFAULT NULL COMMENT 'relates to user table',
  `date_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";
        $DB->execute($sql);
    }
    if (!$DB->get_manager()->table_exists('androgogic_support_log')) {
        $sql = "CREATE TABLE `mdl_androgogic_support_log` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `contact_number` varchar(20) NOT NULL,
  `problem_description` varchar(100) NOT NULL,
  `steps_to_reproduce` text NOT NULL,
  `uploaded_file_id` bigint(11) NOT NULL,
  `site_name` varchar(50) NOT NULL,
  `page_before_support` varchar(500) NOT NULL,
  `user_id` bigint(10) NOT NULL COMMENT 'relates to user table',
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";
        $DB->execute($sql);
    }
    return $result;
}    
