<?php

/** 
 * Androgogic Training History Block: Settings
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     17/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 **/



defined('MOODLE_INTERNAL') || die(); 



if ($ADMIN->fulltree) {
//cron hours
$settings->add(new admin_setting_configtext('block_androgogic_training_history/run_cron_hours', get_string('cron_hours', 'block_androgogic_training_history'),
               get_string('cron_hours_explanation', 'block_androgogic_training_history'), '', PARAM_RAW,50));
//workflow
$settings->add(new admin_setting_configcheckbox('block_androgogic_training_history/approval_workflow', get_string('approval_workflow', 'block_androgogic_training_history'),
               get_string('approval_workflow_explanation', 'block_androgogic_training_history'), '', PARAM_BOOL));

//cpd framework
$frameworks = $DB->get_records_menu('comp_framework',array(),'fullname','id,fullname');
$settings->add(new admin_setting_configselect('block_androgogic_training_history/cpd_framework_id', get_string('select_cpd_framework', 'block_androgogic_training_history'),
               '', null,$frameworks));
//competency type
$competency_types = $DB->get_records_menu('comp_type',array(),'fullname','id,fullname');
$settings->add(new admin_setting_configselect('block_androgogic_training_history/comp_type_id', get_string('select_comp_type', 'block_androgogic_training_history'),
               '', null,$competency_types));

//deal with cpe?
$settings->add(new admin_setting_configcheckbox('block_androgogic_training_history/do_cpe_hours', get_string('do_cpe_hours', 'block_androgogic_training_history'),
               get_string('do_cpe_hours_explanation', 'block_androgogic_training_history'), '', PARAM_BOOL));

//cpe competencies
$competencies = $DB->get_records_menu('comp',array(),'fullname','id,fullname');
$settings->add(new admin_setting_configmultiselect('block_androgogic_training_history/cpe_competencies', get_string('select_cpe_competencies', 'block_androgogic_training_history'),
               '', null, $competencies));
}



// End of blocks/androgogic_training_history/settings.php
