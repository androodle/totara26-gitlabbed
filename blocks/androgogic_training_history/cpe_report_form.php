<?php

/** 
 * Androgogic Training History Block: Search form
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     17/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Provides search form for the object.
 * This is used by search page
 *
 **/

if (!defined('MOODLE_INTERNAL')) {
die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}
require_once($CFG->libdir . '/formslib.php');
class cpe_report_form extends moodleform {
function definition() {
global $DB;
$mform =& $this->_form;
foreach($this->_customdata as $custom_key=>$custom_value){
$$custom_key = $custom_value;
}
$config = get_config('block_androgogic_training_history');
if(isset($config->cpe_competencies)){
    $cpe_competency_ids = $config->cpe_competencies;
}
else{
    $cpe_competency_ids = '0';
}
$mform->addElement('html','<div>');
// controls: activities
$dboptions = $DB->get_records_menu('androgogic_activities',array(),'name','id,name');
unset($options);
$options[0] = 'Any';
foreach($dboptions as $key=>$value){
$options[$key] = $value;
}
$select = $mform->addElement('select', 'androgogic_activities_id', get_string('activity','block_androgogic_training_history'), $options);
// dimensions
$dboptions = $DB->get_records_menu('androgogic_dimensions',array(),'name','id,name');
unset($options);
$options[0] = 'Any';
foreach($dboptions as $key=>$value){
$options[$key] = $value;
}
$select = $mform->addElement('select', 'dimension_id', get_string('dimension','block_androgogic_training_history'), $options);
//knowledge areas
$sql = "SELECT id, fullname FROM mdl_comp WHERE id IN($cpe_competency_ids)";
$dboptions = $DB->get_records_sql_menu($sql);
unset($options);
$options[0] = 'Any';
foreach($dboptions as $key=>$value){
$options[$key] = $value;
}
$select = $mform->addElement('select', 'competency_id', get_string('competency','block_androgogic_training_history'), $options);

$mform->addElement('date_selector','startdate',get_string('startdate', 'block_androgogic_training_history'), array('optional'=>true));
$mform->addElement('date_selector','enddate',get_string('enddate', 'block_androgogic_training_history'), array('optional'=>true));

//if(!isset($_POST['startdate'])){
//    $yearagotime = strtotime("-1 year", time());
//    $mform->setConstant('startdate', $yearagotime);
//}
// if admin they can search across users
if(has_capability('block/androgogic_training_history:admin', $context)){
    $mform->addElement('text','user_search',get_string('cpd_report_user_search_instructions', 'block_androgogic_training_history'));
    $mform->addElement('advcheckbox', 'assessed_only', get_string('assessed', 'block_androgogic_training_history'));
}
//hiddens
$mform->addElement('hidden','tab',$tab);
$mform->addElement('hidden','sort',$sort);
$mform->addElement('hidden','dir',$dir);
$mform->addElement('hidden','perpage',$perpage);
$mform->addElement('hidden','user_id',$user_id);
//button
$mform->addElement('submit','submit','Search');
$mform->addElement('html','</div>');
}
}
