<?php

/**
 * Androgogic Training History Block: Edit form
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     05/08/2014
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Provides edit form for the object.
 * This is used by both new and edit pages
 *
 * */
if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}
require_once($CFG->libdir . '/formslib.php');

class user_points_override_edit_form extends moodleform {

    protected $user_points_override;

    function definition() {
        global $USER, $courseid, $DB, $PAGE;
        $mform = & $this->_form;
        $context = get_context_instance(CONTEXT_SYSTEM);
        $user_id = required_param('user_id', PARAM_INT);
        if (isset($_REQUEST['user_id'])) {
            $q = "select DISTINCT a.* , mdl_comp.fullname as competency
from mdl_androgogic_user_points_override a 
LEFT JOIN mdl_comp  on a.competency_id = mdl_comp.id
where a.user_id = {$_REQUEST['user_id']} ";
            $user_points_override = $DB->get_records_sql($q);
        } else {
            $user_points_override = $this->_customdata['$user_points_override']; // this contains the data of this form
        }
        $tab = 'user_points_override_edit';

        $mform->addElement('html', '<div>');

// include the kas and points etc
        block_androgogic_training_history_course_edit_form($mform);

//set values if we are in edit mode
        if ($user_points_override) {
            block_androgogic_training_history_load_overrides_data($mform, $user_id);
        }
//hiddens
        $mform->addElement('hidden', 'tab', $tab);
        if (isset($_REQUEST['user_id'])) {
            $mform->addElement('hidden', 'user_id', $_REQUEST['user_id']);
        } elseif (isset($user_id)) {
            $mform->addElement('hidden', 'user_id', $user_id);
        }
        $this->add_action_buttons(false);
        $mform->addElement('html', '</div>');
    }

}
