<?php

/**
 * Androgogic User period Block: Edit form
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     01/08/2014
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Provides edit form for the object.
 * This is used by both new and edit pages
 *
 * */
if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}
require_once($CFG->libdir . '/formslib.php');

class user_period_edit_form extends moodleform {

    protected $user_period;

    function definition() {
        global $USER, $courseid, $DB, $PAGE;
        $mform = & $this->_form;
        $context = get_context_instance(CONTEXT_SYSTEM);
        $user_id = required_param('user_id', PARAM_INT);
        if (isset($_REQUEST['id'])) {
            $q = "select DISTINCT a.* , CONCAT(mdl_user.firstname,' ',mdl_user.lastname) as user, 
                mdl_user.firstname, mdl_user.lastname
            from mdl_androgogic_user_period a 
            LEFT JOIN mdl_user  on a.user_id = mdl_user.id
            where a.id = {$_REQUEST['id']} ";
            $user_period = $DB->get_record_sql($q);
            $user_fullname = fullname($user_period);
        } else if (isset($this->_customdata['$user_period'])) {
            $user_period = $this->_customdata['$user_period']; // this contains the data of this form
        }
        else{
            $user = $DB->get_record('user',array('id'=>$user_id));
            $user_fullname = fullname($user);
        }
        if(isset($user_period->enddate)){
            $enddate = date('j F Y', $user_period->enddate);
        }
        else{
            $user = new stdClass();
            $user->id = $user_id;
            list($default_start_date,$default_end_date) = block_androgogic_training_history_get_default_period_for_user($user);
            $enddate = date('j F Y', $default_end_date);
        }
        $tab = 'user_period_new'; // from whence we were called
        if (!empty($user_period->id)) {
            $tab = 'user_period_edit';
        }
        $mform->addElement('html', '<div>');
//user
        $mform->addElement('static', 'user', get_string('user_id', 'block_androgogic_training_history'), $user_fullname);

//They can change the startdate, but not the enddate
//startdate
        $mform->addElement('date_selector', 'startdate', get_string('startdate', 'block_androgogic_training_history'));
        $mform->addRule('startdate', get_string('required'), 'required', null, 'server');

//enddate
        $mform->addElement('static', 'enddate_default', get_string('enddate', 'block_androgogic_training_history'), $enddate);


//points override
//        $mform->addElement('text', 'points_override', get_string('points_override', 'block_androgogic_training_history'));
//        $mform->setType('points_override', PARAM_INT);
        
       
        
//set values if we are in edit mode
        if (!empty($user_period->id) && isset($_GET['id'])) {
            $mform->setConstant('startdate', $user_period->startdate);
            //$mform->setConstant('points_override', $user_period->points_override);
            
        }
//hiddens
        $mform->addElement('hidden', 'tab', $tab);
        $mform->addElement('hidden', 'user_id', $user_id);
        if(isset($default_end_date)){
            $mform->addElement('hidden', 'enddate', $default_end_date);
        }
        else if (isset($user_period->enddate)){
            $mform->addElement('hidden', 'enddate', $user_period->enddate);
        }
        if (isset($_REQUEST['id'])) {
            $mform->addElement('hidden', 'id', $_REQUEST['id']);
        } elseif (isset($id)) {
            $mform->addElement('hidden', 'id', $id);
        }
        $this->add_action_buttons(false);
        $mform->addElement('html', '</div>');
    }
    function validation($data, $files) {
        global $DB,$CFG;

        $errors = array();
        $user = new stdClass();
        $user->id = $data['user_id'];
        $user_org_pos = block_androgogic_training_history_get_user_org_pos($user);
        if(!$user_org_pos){
            // no good - can't assign one of these to a user unless they have an org and pos (which then gives us the default period)
            $a = new stdClass();
            $a->url = html_writer::link($CFG->wwwroot . "/user/positions.php?user={$user->id}&type=primary","Go to user position config",array('target'=>'_blank'));
            $errors['user'] = get_string('userhasnoorgpos', 'block_androgogic_training_history', $a);
        }
        else{
            $org_pos_period = block_androgogic_training_history_get_org_pos_period(
                    $user_org_pos->org_framework_id,$user_org_pos->pos_framework_id,
                    $user_org_pos->pos_sortthread,$user_org_pos->org_sortthread);
            if(!$org_pos_period){
                // extension of above condition
                $errors['user_id'] = get_string('userhasnoorgposperiod', 'block_androgogic_training_history');
            }
            else{
                list($period_start_date,$period_end_date) = block_androgogic_training_history_get_cpd_period($user, $org_pos_period);
                if($data['startdate'] < $period_start_date){
                    //can't precede the org pos start date
                    $data['startdate'] = $period_start_date;
                    $errors['startdate'] = get_string('startdatecannotprecedeorgposstartdate', 'block_androgogic_training_history',date('d-m-Y',$period_start_date));
                }
                if($data['startdate'] > $period_end_date){
                    //can't exceed the org pos start date
                    $data['startdate'] = $period_start_date;
                    $errors['startdate'] = get_string('startdatecannotexceedorgposenddate', 'block_androgogic_training_history',date('d-m-Y',$period_end_date));
                }
            }
        }
//        

        return $errors;
    }

}
