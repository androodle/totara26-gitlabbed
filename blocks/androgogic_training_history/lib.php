<?php

/**
 * Androgogic Training History Block: Lib
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     17/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 * */
function block_androgogic_training_history_cron($source = 'cron') {
    global $CFG, $DB, $config;
    $day = date('j');
    $month = date('n');
    $hour = date('G');
    echo '<pre>the hour:';
    var_dump($hour);
    echo 'the androgogic_training_history run cron hours: ';
    var_dump($config->run_cron_hours);
    echo '</pre>';
    $run_cron = false;
    if($source == 'test cron'){
        $run_cron = true;
    }
    else{
        if (isset($config->run_cron_hours) && in_array($hour, explode(';', $config->run_cron_hours))) {
            if($day == 1){
                if($month == 1 or $month == 7){
                    $run_cron = true;
                }
            }
        }
    }
    
    if ($run_cron) {
        mtrace("\n\n");
        mtrace("Running cron for androgogic_training_history" . "<br><br>");
        $time = date("d-m-Y h-i-s", time());
        echo $time . '<br/>';
        echo '<br>before block_androgogic_training_history_change_expired_periods:<br>';
        flush();
        block_androgogic_training_history_change_expired_periods();
        $time = date("d-m-Y h-i-s", time());
        echo 'Finish time' . $time . '<br/>';
    } else {
        mtrace("\n\n");
        mtrace("not one of the run cron hours for androgogic_training_history");
    }
}
// change any current period that has expired and make a new one
function block_androgogic_training_history_change_expired_periods() {
    global $DB;
    $sql = "SELECT * FROM mdl_androgogic_training_history_periods where is_current = 1";
    $results = $DB->get_records_sql($sql);    
    foreach($results as $period){
        $do_change = false;
        if($period->type == 'financial' and $period->end_year == date('Y')){
            $do_change = true;
        }
        elseif($period->type == 'calendar' and $period->end_year < date('Y')){
            $do_change = true;
        }
        if($do_change){
            $period->is_current = 0;
            $DB->update_record('androgogic_training_history_periods',$period);
            $new_period = stdClass();
            $new_period->start_year = $period->start_year+1;
            $new_period->end_year = $period->end_year+1;
            $new_period->is_current = 1;
            $new_period->type = $period->type;
            $new_period->date_modified = time();
            $DB->insert_record('androgogic_training_history_periods',$new_period);
        }
    }
}
// get the current period that matches the data
function block_androgogic_training_history_get_period_id($data) {
    global $DB;
    //enddate is immutable
    $end_year = date('Y',$data->enddate);
    $end_month = date('n',$data->enddate);
    if ($end_month == 6){
        $period_type = 'financial';
    }
    else{
        $period_type = 'calendar';
    }
    $sql = "SELECT * FROM mdl_androgogic_training_history_periods where is_current = 1 and type = '$period_type' and end_year = $end_year";
    $result = $DB->get_record_sql($sql);
    if($result){
        return $result->id;
    }
    else{
        return 0;
    }
}
function block_androgogic_training_history_get_prorata($period_start_date,$period_end_date) {
    // plus one day because we are going from e.g. 1 Jan 00:00:00 to 31 Dec 00:00:00, which ends up being 364 days
    $day = 60*60*24;
    $prorata_days = ($period_end_date - $period_start_date + $day)/$day; 
    $prorata = $prorata_days/365;
    return $prorata;
}
// this function wraps up the complexity of the getting of the targets, for a bit more happiness on the consuming end
function block_androgogic_training_history_get_targets($user,$org_pos_period,$and_clause,$period_start_date,$period_end_date) {
    global $DB;
    $return = new stdClass();
    
    //points targets and overrides
    $sql = "select ct.*, c.fullname, ifnull(o.points,0) as override_points
    from mdl_androgogic_training_history_cpd_targets ct
    inner join mdl_comp c on ct.competency_id = c.id
    left join mdl_androgogic_user_points_override o on ct.competency_id = o.competency_id and o.user_id = $user->id
    where org_pos_period_id = $org_pos_period->id "
            . " $and_clause";
    $return->cpd_targets = $DB->get_records_sql($sql);     
    
    //hours targets and overrides
    $sql = "select ct.*, c.fullname, ifnull(o.hours,0) as override_hours 
    from mdl_androgogic_training_history_cpe_targets ct
    inner join mdl_comp c on ct.competency_id = c.id
    left join mdl_androgogic_user_hours_override o on ct.competency_id = o.competency_id and o.user_id = $user->id
    where org_pos_period_id = $org_pos_period->id "
            . " $and_clause";
    $return->cpe_targets = $DB->get_records_sql($sql);
    
    //unallocated points override
    $sql = "SELECT * FROM mdl_androgogic_user_points_override WHERE user_id = $user->id AND competency_id = 0";
    $return->unallocated_points_override = $DB->get_record_sql($sql);
    
    //work out the totals too: CPD
    $total_cpd_target = 0;
    foreach($return->cpd_targets as $cpd_target){
        $override_result = $cpd_target->cpd_target + $cpd_target->override_points;
        // if the override result is less than zero then it is an illegal one: protect against this
        if($override_result >= 0){
            $total_cpd_target += $override_result;
        }        
    }
    // CPE
    $total_cpe_target = 0;
    foreach($return->cpe_targets as $cpe_target){
        $override_result = $cpd_target->cpd_target + $cpe_target->override_hours;
        // if the override result is less than zero then it is an illegal one: protect against this
        if($override_result >= 0){
            $total_cpe_target += $override_result;
        }        
    }
    //apply the unallocated override to the total CPD only - no CPE equivalent
    if($return->unallocated_points_override){
        $total_cpd_target += $return->unallocated_points_override->points;
    }
    //apply the prorata to both
    $prorata = block_androgogic_training_history_get_prorata($period_start_date,$period_end_date);
    $total_cpd_target = $total_cpd_target*$prorata;
    $total_cpe_target = $total_cpe_target*$prorata;
    
    $return->total_cpd_target = $total_cpd_target;  
    $return->total_cpe_target = $total_cpe_target;  
    return $return;
}
function block_androgogic_training_history_save_upload_over_existing($form_data, $block_name, $table_name, $foreign_key_name) {
    global $DB;
    $form_elements = $form_data->_form->_elements;
    //reformat for ease of use
    $data = new stdClass();
    foreach ($form_elements as $element) {
//       echo '<pre> the $element:';
//       var_dump($element);
//       echo '</pre>';
        if (isset($element->_attributes['name']) && isset($element->_attributes['value'])) {
            $data->{$element->_attributes['name']} = $element->_attributes['value'];
        } else {
            //echo 'missing attribute for this element: ' . print_r($element);
        }
    }
//   echo '<pre> the $data:';
//   var_dump($data);
//   echo '</pre>';
    //change the reference to the itemid in the block table
    $sql = "update mdl_$table_name set $foreign_key_name = " . $data->itemid . " where id = " . $data->objectid;
    $DB->execute($sql);
    //change the component and filearea of the file entry
    $sql = "UPDATE mdl_files
        SET filearea = 'content',component='$block_name'
        WHERE itemid = '$data->itemid'";
    $DB->execute($sql);
}

function block_androgogic_training_history_pluginfile($course, $birecord, $context, $filearea, $args, $forcedownload) {
    global $DB;
    $sql = 'select * from mdl_files 
            where itemid = ' . $args[0] . ' 
                and filename = \'' . $args[1] . '\'
                and contextid = ' . $context->id . ' 
                and filearea = \'' . $filearea . '\'';
    $file = $DB->get_record_sql($sql);
    if (!$file) {
        send_file_not_found();
    } else {
        $fs = get_file_storage();
        $stored_file = $fs->get_file_by_hash($file->pathnamehash);
    }
    $forcedownload = true;

    session_get_instance()->write_close();
    send_stored_file($stored_file, 60 * 60, 0, $forcedownload);
}

/**
 * the user (or an admin) has put in a new training history record. let their manager know this by email
 * @param object $training_history_record
 */
function block_androgogic_training_history_advise_manager($training_history_record) {
    global $DB, $CFG, $OUTPUT;
    $th_user = $DB->get_record('user', array('id' => $training_history_record->user_id));
    $q = "SELECT u.* FROM mdl_pos_assignment pa inner join mdl_user u on pa.managerid = u.id where pa.userid = $training_history_record->user_id";
    $manager = $DB->get_record_sql($q);
    if ($manager) {
        $subject = get_string('email_to_manager_subject', 'block_androgogic_training_history');
        $a = new stdClass();
        $a->url = $CFG->wwwroot . '/blocks/androgogic_training_history/index.php?tab=training_history_edit&id=' . $training_history_record->id;
        $body = get_string('email_to_manager_body', 'block_androgogic_training_history', $a);
        email_to_user($manager, $th_user, $subject, $body, $body);
    } else {
        echo $OUTPUT->notification(get_string('userhasnomanager', 'block_androgogic_training_history'), 'notifyfailure');
    }
}

/**
 * the manager has approved the training history record. let the user know this by email
 * @param object $training_history_record
 */
function block_androgogic_training_history_advise_user($training_history_record) {
    global $DB, $CFG, $OUTPUT;
    $th_user = $DB->get_record('user', array('id' => $training_history_record->user_id));
    $q = "SELECT u.* FROM mdl_pos_assignment pa inner join mdl_user u on pa.managerid = u.id where pa.userid = $training_history_record->user_id";
    $manager = $DB->get_record_sql($q);
    if ($manager) {
        $subject = get_string('email_to_user_subject', 'block_androgogic_training_history');
        $a = new stdClass();
        $a->url = $CFG->wwwroot . '/blocks/androgogic_training_history/index.php?tab=training_history_edit&id=' . $training_history_record->id;
        $body = get_string('email_to_user_body', 'block_androgogic_training_history', $a);
        email_to_user($th_user, $manager, $subject, $body, $body);
    } else {
        echo $OUTPUT->notification(get_string('userhasnomanager', 'block_androgogic_training_history'), 'notifyfailure');
    }
}

/**
 * is the user a manager of others?
 */
function block_androgogic_training_history_is_manager() {
    global $USER, $DB;
    $is_manager = false;
    if ($DB->get_records('pos_assignment', array('managerid' => $USER->id))) {
        $is_manager = true;
    }
    return $is_manager;
}
function block_androgogic_training_history_cpd_points_update($data) {
    global $DB;
    // did we get any cpd points?
    //out with the old
    $DB->delete_records('androgogic_training_history_competencies_cpd_points', array('training_history_id' => $data->id));
    //in with the new   
    foreach ($data as $key => $value) {
        if (substr($key, 0, 10) == 'cpd_points') {
            $th_comp_cpd = new stdClass();
            $th_comp_cpd->competency_id = substr($key, 11);
            $th_comp_cpd->cpd_points = $value;
            $th_comp_cpd->training_history_id = $data->id;
            if ($value > 0) {
                $DB->insert_record('androgogic_training_history_competencies_cpd_points', $th_comp_cpd);
            }
        }
    }
}

function block_androgogic_training_history_dimensions_update($data) {
    global $DB;
    $DB->delete_records('androgogic_training_history_dimensions',array('training_history_id'=>$data->id));
    //many to many relationship: androgogic_training_history_dimensions
    if(isset($data->dimension_id)){
        foreach($data->dimension_id as $dimension_id){
            $insert = new stdClass();
            $insert->training_history_id = $data->id;
            $insert->dimension_id = $dimension_id;
            $DB->insert_record('androgogic_training_history_dimensions', $insert);
        }
    }
}

function block_androgogic_training_history_cpe_hours_update($data) {
    global $DB;
    //any cpe hours items?
    //out with the old
    $DB->delete_records('androgogic_training_history_competencies_cpe_hours', array('training_history_id' => $data->id));
    //in with the new   
    foreach ($data as $key => $value) {
        if (substr($key, 0, 9) == 'cpe_hours') {
            $th_comp_cpe = new stdClass();
            $th_comp_cpe->competency_id = substr($key, 10);
            $th_comp_cpe->cpe_hours = $value;
            $th_comp_cpe->training_history_id = $data->id;
            if ($value > 0) {
                $DB->insert_record('androgogic_training_history_competencies_cpe_hours', $th_comp_cpe);
            }
        }
    }
}

// this function does update for course level cpd points
function block_androgogic_training_history_course_cpd_points_update($data) {
    global $DB;
    // did we get any cpd points?
    //out with the old
    $DB->delete_records('androgogic_training_history_course_competencies_cpd_points', array('course_id' => $data->id));
    //in with the new   
    foreach ($data as $key => $value) {
        if (substr($key, 0, 10) == 'cpd_points') {
            $th_comp_cpd = new stdClass();
            $th_comp_cpd->competency_id = substr($key, 11);
            $th_comp_cpd->cpd_points = $value;
            $th_comp_cpd->course_id = $data->id;
            if ($value > 0) {
                $DB->insert_record('androgogic_training_history_course_competencies_cpd_points', $th_comp_cpd);
            }
        }
    }
}

// this function does update for course level cpe hours
function block_androgogic_training_history_course_cpe_hours_update($data) {
    global $DB;
    //any cpe hours items?
    //out with the old
    $DB->delete_records('androgogic_training_history_course_competencies_cpe_hours', array('course_id' => $data->id));
    //in with the new   
    foreach ($data as $key => $value) {
        if (substr($key, 0, 9) == 'cpe_hours') {
            $th_comp_cpe = new stdClass();
            $th_comp_cpe->competency_id = substr($key, 10);
            $th_comp_cpe->cpe_hours = $value;
            $th_comp_cpe->course_id = $data->id;
            if ($value > 0) {
                $DB->insert_record('androgogic_training_history_course_competencies_cpe_hours', $th_comp_cpe);
            }
        }
    }
}
function block_androgogic_training_history_course_dimensions_update($data) {
    global $DB;
    $DB->delete_records('androgogic_course_dimensions',array('course_id'=>$data->id));
    //many to many relationship: androgogic_course_dimensions
    if(isset($data->dimension_id)){
        foreach($data->dimension_id as $dimension_id){
            $insert = new stdClass();
            $insert->course_id = $data->id;
            $insert->dimension_id = $dimension_id;
            $DB->insert_record('androgogic_course_dimensions', $insert);
        }
    }
}
// this function does update for cpd targets
function block_androgogic_training_history_cpd_targets_update($data) {
    global $DB, $USER;
    // did we get any cpd points?
    //out with the old
    $DB->delete_records('androgogic_training_history_cpd_targets', array('org_pos_period_id' => $data->id));
    //in with the new   
    foreach ($data as $key => $value) {
        if (substr($key, 0, 10) == 'cpd_points') {
            $th_comp_cpd = new stdClass();
            $th_comp_cpd->competency_id = substr($key, 11);
            $th_comp_cpd->cpd_target = $value;
            $th_comp_cpd->org_pos_period_id = $data->id;
            $th_comp_cpd->created_by = $USER->id;
            $th_comp_cpd->date_created = date('Y-m-d H:i:s');
            if ($value > 0) {
                $DB->insert_record('androgogic_training_history_cpd_targets', $th_comp_cpd);
            }
        }
    }
}

// this function does update for cpe targets
function block_androgogic_training_history_cpe_targets_update($data) {
    global $DB, $USER;
    //any cpe hours items?
    //out with the old
    $DB->delete_records('androgogic_training_history_cpe_targets', array('org_pos_period_id' => $data->id));
    //in with the new   
    foreach ($data as $key => $value) {
        if (substr($key, 0, 9) == 'cpe_hours') {
            $th_comp_cpe = new stdClass();
            $th_comp_cpe->competency_id = substr($key, 10);
            $th_comp_cpe->cpe_target = $value;
            $th_comp_cpe->org_pos_period_id = $data->id;
            $th_comp_cpe->created_by = $USER->id;
            $th_comp_cpe->date_created = date('Y-m-d H:i:s');
            if ($value > 0) {
                $DB->insert_record('androgogic_training_history_cpe_targets', $th_comp_cpe);
            }
        }
    }
}

// this function does update for cpd overrides
function block_androgogic_training_history_cpd_overrides_update($data) {
    global $DB, $USER;
    // did we get any cpd points?
    //out with the old
    $DB->delete_records('androgogic_user_points_override', array('user_id' => $data->user_id));
    //in with the new   
    foreach ($data as $key => $value) {
        if (substr($key, 0, 10) == 'cpd_points') {
            $th_comp_cpd = new stdClass();
            $th_comp_cpd->competency_id = substr($key, 11);
            $th_comp_cpd->points = $value;
            $th_comp_cpd->user_id = $data->user_id;
            $th_comp_cpd->created_by = $USER->id;
            $th_comp_cpd->date_created = date('Y-m-d H:i:s');
            if ($value != 0) {
                $DB->insert_record('androgogic_user_points_override', $th_comp_cpd);
            }
        }
    }
}

// this function does update for cpe overrides
function block_androgogic_training_history_cpe_overrides_update($data) {
    global $DB, $USER;
    //any cpe hours items?
    //out with the old
    $DB->delete_records('androgogic_user_hours_override', array('user_id' => $data->user_id));
    //in with the new   
    foreach ($data as $key => $value) {
        if (substr($key, 0, 9) == 'cpe_hours') {
            $th_comp_cpe = new stdClass();
            $th_comp_cpe->competency_id = substr($key, 10);
            $th_comp_cpe->hours = $value;
            $th_comp_cpe->user_id = $data->user_id;
            $th_comp_cpe->created_by = $USER->id;
            $th_comp_cpe->date_created = date('Y-m-d H:i:s');
            if ($value != 0) {
                $DB->insert_record('androgogic_user_hours_override', $th_comp_cpe);
            }
        }
    }
}
function block_androgogic_training_history_load_target_data($mform, $org_pos_period) {
    global $DB;
    $config = get_config('block_androgogic_training_history');
    $sql = "SELECT * FROM mdl_androgogic_training_history_cpd_targets cp
                where org_pos_period_id = $org_pos_period->id";
    $cpd_items = $DB->get_records_sql($sql);
    foreach ($cpd_items as $cpd_item) {
        if (isset($mform->_elementIndex['cpd_points_' . $cpd_item->competency_id])) {
            $mform->setConstant('cpd_points_' . $cpd_item->competency_id, $cpd_item->cpd_target);
        }
    }
    if ($config->do_cpe_hours) {
        $sql = "SELECT * FROM mdl_androgogic_training_history_cpe_targets cp
                where org_pos_period_id = $org_pos_period->id";
        $cpe_items = $DB->get_records_sql($sql);
        foreach ($cpe_items as $cpe_item) {
            if (isset($mform->_elementIndex['cpe_hours_' . $cpe_item->competency_id])) {
                $mform->setConstant('cpe_hours_' . $cpe_item->competency_id, $cpe_item->cpe_target);
            }
        }
    }
}
function block_androgogic_training_history_load_overrides_data($mform, $user_id) {
    global $DB;
    $config = get_config('block_androgogic_training_history');
    $sql = "SELECT * FROM mdl_androgogic_user_points_override cp
                where user_id = $user_id";
    $cpd_items = $DB->get_records_sql($sql);
    foreach ($cpd_items as $cpd_item) {
        if (isset($mform->_elementIndex['cpd_points_' . $cpd_item->competency_id])) {
            $mform->setConstant('cpd_points_' . $cpd_item->competency_id, $cpd_item->points);
        }
    }
    if ($config->do_cpe_hours) {
        $sql = "SELECT * FROM mdl_androgogic_user_hours_override cp
                where user_id = $user_id";
        $cpe_items = $DB->get_records_sql($sql);
        foreach ($cpe_items as $cpe_item) {
            if (isset($mform->_elementIndex['cpe_hours_' . $cpe_item->competency_id])) {
                $mform->setConstant('cpe_hours_' . $cpe_item->competency_id, $cpe_item->hours);
            }
        }
    }
}

function block_androgogic_training_history_course_edit_form($mform) {
    global $DB;
    $config = get_config('block_androgogic_training_history');
    // we use this in a number of different ways: course edit form, org_pos_period_edit_form and new/edit training history record
    if ($mform->_formName == 'course_edit_form') {
        $section_header_text = get_string('pluginname', 'block_androgogic_training_history');
        $cpd_header = get_string('cpd_points', 'block_androgogic_training_history');
        $cpe_header = get_string('cpe_hours', 'block_androgogic_training_history');
    } else if ($mform->_formName == 'org_pos_period_edit_form') {
        //$section_header_text = get_string('competency_plural', 'block_androgogic_training_history');
        $cpd_header = get_string('cpd_target', 'block_androgogic_training_history');
        $cpe_header = get_string('cpe_target', 'block_androgogic_training_history');
    } else if ($mform->_formName == 'user_points_override_edit_form') {
        //$section_header_text = get_string('competency_plural', 'block_androgogic_training_history');
        $cpd_header = get_string('cpd_points_override', 'block_androgogic_training_history');
        $cpe_header = get_string('cpe_hours_override', 'block_androgogic_training_history');
    } else {
        //$section_header_text = get_string('competency_plural', 'block_androgogic_training_history');
        $cpd_header = get_string('cpd_points', 'block_androgogic_training_history');
        $cpe_header = get_string('cpe_hours', 'block_androgogic_training_history');
    }
    
    if(in_array($mform->_formName, array('course_edit_form'))){
        $mform->addElement('header', 'androgogic_training_history_header', $section_header_text);
    }
    //from the configured competency framework, get all of the comptencies and present them        
    $cpd_framework_id = $config->cpd_framework_id;
    $sql = "SELECT * FROM mdl_comp c where frameworkid = $cpd_framework_id";
    if (isset($config->comp_type_id)) {
        $sql .= " and typeid = $config->comp_type_id";
    }
    $competencies = $DB->get_records_sql($sql);

    $mform->addElement('html', html_writer::start_tag('table'));
    $html = html_writer::start_tag('tr') . html_writer::tag('th', get_string('competency', 'block_androgogic_training_history'));
    $html .= html_writer::tag('th', $cpd_header);
    $html .= html_writer::end_tag('tr');
    $mform->addElement('html', $html);
    
    if($mform->_formName == 'user_points_override_edit_form'){
        //add a fake competency
        $fake_competency = new stdClass();
        $fake_competency->id = 0;
        $fake_competency->fullname = get_string('unallocated_points','block_androgogic_training_history');
        array_unshift($competencies,$fake_competency);
        
    }

    foreach ($competencies as $competency) {
        $html = html_writer::start_tag('tr');
        $html .= html_writer::tag('td', $competency->fullname) . html_writer::start_tag('td');
        $mform->addElement('html', $html);

        $mform->addElement('text', 'cpd_points_' . $competency->id, '', array('size' => 10));
        $mform->addRule('cpd_points_' . $competency->id, 'Points have to be full numbers with up to 2 decimal points', 'numeric', '', 'client');

        $html = html_writer::end_tag('td') . html_writer::end_tag('tr');
        $mform->addElement('html', $html);
    }
    //cpe hours
    if ($config->do_cpe_hours) {
        $html = html_writer::start_tag('tr') . html_writer::tag('th', get_string('competency', 'block_androgogic_training_history'));
        $html .= html_writer::tag('th', $cpe_header);
        $html .= html_writer::end_tag('tr');
        $mform->addElement('html', $html);
        $sql = "SELECT * FROM mdl_comp where id in ($config->cpe_competencies)";
        $cpe_competencies = $DB->get_records_sql($sql);
        foreach ($cpe_competencies as $cpe_competency) {
            $html = html_writer::start_tag('tr');
            $html .= html_writer::tag('td', $cpe_competency->fullname) . html_writer::start_tag('td');
            $mform->addElement('html', $html);

            $mform->addElement('text', 'cpe_hours_' . $cpe_competency->id, '', array('size' => 10));
            $mform->addRule('cpe_hours_' . $cpe_competency->id, 'Hours have to be full numbers with up to 2 decimal points', 'numeric', '', 'client');

            $html = html_writer::end_tag('td') . html_writer::end_tag('tr');
            $mform->addElement('html', $html);
        }
    }
    $mform->addElement('html', html_writer::end_tag('table'));
    
    if (!in_array($mform->_formName, array('org_pos_period_edit_form','user_points_override_edit_form'))){
        //make me into cb array?
        $options = $DB->get_records_menu('androgogic_dimensions',array(),'name','id,name');
        $select = $mform->addElement('select', 'dimension_id', get_string('dimension','block_androgogic_training_history'), $options,array('size'=>10));
        $select->setMultiple(true);
    }
}

function block_androgogic_training_history_save_course_data($data) {
    block_androgogic_training_history_course_cpd_points_update($data);
    block_androgogic_training_history_course_cpe_hours_update($data);
    block_androgogic_training_history_course_dimensions_update($data);
}

function block_androgogic_training_history_load_course_data($mform, $course) {
    global $DB;
    $config = get_config('block_androgogic_training_history');
    $sql = "SELECT * FROM mdl_androgogic_training_history_course_competencies_cpd_points cp
                where course_id = $course->id";
    $cpd_items = $DB->get_records_sql($sql);
    foreach ($cpd_items as $cpd_item) {
        $mform->setConstant('cpd_points_' . $cpd_item->competency_id, $cpd_item->cpd_points);
    }
    if ($config->do_cpe_hours) {
        $sql = "SELECT * FROM mdl_androgogic_training_history_course_competencies_cpe_hours cp
                where course_id = $course->id";
        $cpe_items = $DB->get_records_sql($sql);
        foreach ($cpe_items as $cpe_item) {
            $mform->setConstant('cpe_hours_' . $cpe_item->competency_id, $cpe_item->cpe_hours);
        }
    }
    
    //dimensions
    $sql = "SELECT * FROM mdl_androgogic_course_dimensions 
                where course_id = $course->id";
    $result = $DB->get_records_sql($sql);
    $dimension_ids = array();
    foreach ($result as $row) {
        $dimension_ids[] = $row->dimension_id;
    }
    if (count($dimension_ids) > 0) {
        $mform->setConstant('dimension_id', $dimension_ids);
    }
}

function block_androgogic_training_history_org_pos_unique($data) {
    global $DB;
    $sql = "SELECT * FROM mdl_androgogic_org_pos_periods where org_id = $data->org_id and pos_id = $data->pos_id";
    // if it is a new one, this won't be set, if an update it ought to be
    if (isset($data->id)) {
        $sql .= " and id != $data->id";
    }
    if (!$DB->get_records_sql($sql)) {
        // if no records then it is unique
        return true;
    } else {
        return false;
    }
}

function block_androgogic_training_history_get_user_org_pos($user) {
    global $DB;
    //get the user's org and pos from their primary pos
    $sql = "SELECT pa.*, p.frameworkid as pos_framework_id, p.path as pos_path, 
            o.frameworkid as org_framework_id, o.path as org_path, p.sortthread as pos_sortthread, 
            o.sortthread as org_sortthread
        FROM mdl_pos_assignment pa
        inner join mdl_pos p on pa.positionid = p.id
        inner join mdl_org o on pa.organisationid = o.id
        where pa.userid = $user->id and pa.type = 1";
    return $DB->get_record_sql($sql);
}

function block_androgogic_training_history_get_org_pos_period($org_framework_id, $pos_framework_id, $position_sortthread,$organisation_sortthread) {
    global $DB;
    //get the org pos period from the 2 framework ids that is equal or greater to the given pos sortthread 
    // (note this is lowest hanging relation based on pos)
    $sql = "select pp.* 
        from mdl_androgogic_org_pos_periods pp
        inner join mdl_org o on pp.org_id = o.id
        inner join mdl_pos p on pp.pos_id = p.id
        where o.frameworkid = $org_framework_id 
        and p.frameworkid = $pos_framework_id "
            . "AND p.visible = 1 "
            . "AND o.visible = 1 "
            . "and p.sortthread <= '$position_sortthread' "
            . "and o.sortthread <= '$organisation_sortthread' "
            . "ORDER BY p.sortthread DESC LIMIT 1";

    return $DB->get_record_sql($sql);
}

function block_androgogic_training_history_get_cpd_period($user, $org_pos_period) {
    global $DB;
    //get their current period override record if any
    $sql = "select u.* from mdl_androgogic_user_period u
        inner join mdl_androgogic_training_history_periods p on u.period_id = p.id
        where user_id = $user->id and p.is_current = 1";
    $user_period_user_record = $DB->get_record_sql($sql);

    if (!$user_period_user_record) {
        $period_type = $org_pos_period->period;

        $this_year = date('Y');
        $last_year = $this_year - 1;
        $next_year = $this_year + 1;
        $this_month = date('m');
        if ($period_type == 'financial') {
            if ($this_month > 6) {
                $start_year = $this_year;
                $end_year = $next_year;
            } else {
                $start_year = $last_year;
                $end_year = $this_year;
            }
            $period_start_date = strtotime("07/01/$start_year");
            $period_end_date = strtotime("06/30/$end_year");
        } else {
            //calendar                        
            $period_start_date = strtotime("01/01/$this_year");
            $period_end_date = strtotime("12/31/$this_year");
        }
    } else {
        $period_start_date = $user_period_user_record->startdate;
        $period_end_date = $user_period_user_record->enddate;
    }
    return array($period_start_date, $period_end_date);
}
function block_androgogic_training_history_get_default_period_for_user($user){
    $user_org_pos = block_androgogic_training_history_get_user_org_pos($user);
    if(!$user_org_pos){
        // then it's all over
        return false;
    }
    $org_pos_period = block_androgogic_training_history_get_org_pos_period(
            $user_org_pos->org_framework_id,$user_org_pos->pos_framework_id,
            $user_org_pos->pos_sortthread,$user_org_pos->org_sortthread);
    if(!$org_pos_period){
        // then it's all over
        return false;
    }

    list($period_start_date,$period_end_date) = block_androgogic_training_history_get_cpd_period($user, $org_pos_period);
    return array($period_start_date,$period_end_date);
}
function block_androgogic_training_history_export_data($download_format, $table, $filename,$report_name_lang,$table2=null) {
    $stripped_table = block_androgogic_training_history_strip_tags_from_table($table);
    $function = "block_androgogic_training_history_export_data_" . $download_format;
    if($table2){
        $stripped_table2 = block_androgogic_training_history_strip_tags_from_table($table2);
        $function($stripped_table, $filename,$report_name_lang,$stripped_table2);
    }
    else{
        $function($stripped_table, $filename,$report_name_lang);
    }
}

function block_androgogic_training_history_export_data_excel($table, $filename,$report_name_lang,$table2=null) {
    global $CFG;

    require_once("$CFG->libdir/excellib.class.php");

    $filename = clean_filename($filename) . '.xls';

    $workbook = new MoodleExcelWorkbook('-');
    $workbook->send($filename);

    $worksheet = array();

    $worksheet[0] = $workbook->add_worksheet('');
    $col = 0;
    foreach ($table->head as $cell) {
        $worksheet[0]->write(0, $col, $cell);
        $col++;
    }

    $row = 1;
    foreach ($table->data as $row_data) {
        $col = 0;
        foreach ($row_data as $cell) {
            $worksheet[0]->write($row, $col, $cell);
            $col++;
        }
        $row++;
    }
    if($table2){
        //add empty row
        $worksheet[0]->write($row, 0, 0);
        $row++;
        $col = 0;
        foreach ($table2->head as $cell) {
            $worksheet[0]->write($row, $col, $cell);
            $col++;
        }
        $row++;
        foreach ($table2->data as $row_data) {
            $col = 0;
            foreach ($row_data as $cell) {
                $worksheet[0]->write($row, $col, $cell);
                $col++;
            }
            $row++;
        }
    }

    $workbook->close();
    die;
}

function block_androgogic_training_history_export_data_csv($table, $filename,$report_name_lang,$table2=null) {
    global $CFG;

    require_once($CFG->libdir . '/csvlib.class.php');

    $filename = clean_filename($filename) . '.csv';

    $csvexport = new csv_export_writer();
    $csvexport->set_filename($filename);
    $csvexport->add_data($table->head);

    foreach ($table->data as $row_data) {
        $csvexport->add_data($row_data);
    }
    if($table2){
        $empty_row = array();
        $csvexport->add_data($empty_row);
        $csvexport->add_data($table2->head);

        foreach ($table2->data as $row_data) {
            $csvexport->add_data($row_data);
        }
    }
    $csvexport->download_file();
    die;
}

function block_androgogic_training_history_strip_tags_from_table($table) {
    //strip any html out of the table head and rows
    $stripped_table = new stdClass();
    foreach ($table->head as $table_head_cell) {
        $stripped_table->head[] = strip_tags($table_head_cell);
    }
    foreach ($table->data as $row_data) {
        $stripped_row_data = array();
        foreach ($row_data as $cell) {
            $stripped_row_data[] = strip_tags($cell);
        }
        $stripped_table->data[] = $stripped_row_data;
    }
    return $stripped_table;
}

function block_androgogic_training_history_export_data_pdf_portrait($table, $filename, $report_name_lang_entry,$table2=null) {
    block_androgogic_training_history_export_data_pdf($table, $filename, $report_name_lang_entry, true,$table2);
}

function block_androgogic_training_history_export_data_pdf_landscape($table, $filename, $report_name_lang_entry,$table2=null) {
    block_androgogic_training_history_export_data_pdf($table, $filename, $report_name_lang_entry, false,$table2);
}

function block_androgogic_training_history_export_data_pdf($table, $filename, $report_name_lang_entry, $portrait = true,$table2=null) {
    global $CFG;
    //set up some values - these might go to settings page at some stage
    $margin_footer = 10;
    $margin_bottom = 20;
    $font_size_title = 20;
    $font_size_record = 14;
    $font_size_data = 10;
    $memory_limit = 1024;

    require_once $CFG->libdir . '/pdflib.php';

    // Increasing the execution time to no limit.
    set_time_limit(0);
    $filename = clean_filename($filename . '.pdf');

    // Table.
    $html = '';
    $numfields = count($table->head);

    // Layout options.
    if ($portrait) {
        $pdf = new PDF('P', 'mm', 'A4', true, 'UTF-8');
    } else {
        $pdf = new PDF('L', 'mm', 'A4', true, 'UTF-8');
    }

    $pdf->setTitle($filename);
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    $pdf->SetFooterMargin($margin_footer);
    $pdf->SetAutoPageBreak(true, $margin_bottom);
    $pdf->AddPage();

    // Get current language to set the font properly.
    $language = current_language();
    if (in_array($language, array('zh_cn', 'ja'))) {
        $font = 'droidsansfallback';
    } else if ($language == 'th') {
        $font = 'cordiaupc';
    } else {
        $font = 'dejavusans';
    }
    // Check if language is RTL.
    if (right_to_left()) {
        $pdf->setRTL(true);
    }
    if($report_name_lang_entry != ''){
        $report_header = get_string($report_name_lang_entry, 'block_androgogic_training_history');
        $pdf->SetFont($font, 'B', $font_size_title);
        $pdf->Write(0, format_string($report_header), '', 0, 'L', true, 0, false, false, 0);    
    }
    
    $count = count($table->data);
    $resultstr = $count == 1 ? 'record' : 'records';
    $recordscount = get_string('x' . $resultstr, 'totara_reportbuilder', $count);
    $pdf->SetFont($font, 'B', $font_size_record);
    if(!$table2){
        $pdf->Write(0, $recordscount, '', 0, 'L', true, 0, false, false, 0);
    }
    $pdf->SetFont($font, '', $font_size_data);

    if (is_array($restrictions) && count($restrictions) > 0) {
        $pdf->Write(0, get_string('reportcontents', 'totara_reportbuilder'), '', 0, 'L', true, 0, false, false, 0);
        foreach ($restrictions as $restriction) {
            $pdf->Write(0, $restriction, '', 0, 'L', true, 0, false, false, 0);
        }
    }

    // Add report caching data.
    if ($cache) {
        $usertz = totara_get_clean_timezone();
        $a = userdate($cache['lastreport'], '', $usertz);
        $lastcache = get_string('report:cachelast', 'totara_reportbuilder', $a);
        $pdf->Write(0, $lastcache, '', 0, 'L', true, 0, false, false, 0);
    }
    //make a space between the header and the table
    $html .= '<p>&nbsp;</p>';
    $html .= '<table border="1" cellpadding="2" cellspacing="0">
                        <thead>
                            <tr style="background-color: #CCC;">';
    foreach ($table->head as $field) {
        $html .= '<th>' . $field . '</th>';
    }
    $html .= '</tr></thead><tbody>';

    foreach ($table->data as $record_data) {
        $html .= '<tr>';
        for ($j = 0; $j < $numfields; $j++) {
            if (isset($record_data[$j])) {
                $cellcontent = html_entity_decode($record_data[$j], ENT_COMPAT, 'UTF-8');
            } else {
                $cellcontent = '';
            }
            $html .= '<td>' . $cellcontent . '</td>';
        }
        $html .= '</tr>';

        // Check memory limit.
        $mramuse = ceil(((memory_get_usage(true) / 1024) / 1024));
        if ($memory_limit <= $mramuse) {
            // Releasing resources.
            $records->close();
            // Notice message.
            print_error('exportpdf_mramlimitexceeded', 'totara_reportbuilder', '', $memory_limit);
        }
    }
    $html .= '</tbody></table>';
    
    if($table2){
        //reset the numfields
        $numfields = count($table2->head);
        //make a space between the tables
        $html .= '<p>&nbsp;</p>';
        //make some more html for table2
        $html .= '<table border="1" cellpadding="2" cellspacing="0">
                    <thead>
                        <tr style="background-color: #CCC;">';
        foreach ($table2->head as $field) {
            $html .= '<th>' . $field . '</th>';
        }
        $html .= '</tr></thead><tbody>';

        foreach ($table2->data as $record_data) {
            $html .= '<tr>';
            for ($j = 0; $j < $numfields; $j++) {
                if (isset($record_data[$j])) {
                    $cellcontent = html_entity_decode($record_data[$j], ENT_COMPAT, 'UTF-8');
                } else {
                    $cellcontent = '';
                }
                $html .= '<td>' . $cellcontent . '</td>';
            }
            $html .= '</tr>';

            // Check memory limit.
            $mramuse = ceil(((memory_get_usage(true) / 1024) / 1024));
            if ($memory_limit <= $mramuse) {
                // Releasing resources.
                $records->close();
                // Notice message.
                print_error('exportpdf_mramlimitexceeded', 'totara_reportbuilder', '', $memory_limit);
            }
        }
        $html .= '</tbody></table>';
    }

    // Closing the pdf.
    $pdf->WriteHTML($html, true, false, false, false, '');

    // Returning the complete pdf.
    if (!$file) {
        $pdf->Output($filename, 'D');
    } else {
        $pdf->Output($file, 'F');
    }
}

function block_androgogic_training_history_output_download_links($moodle_url, $report_name_lang_entry='') {
    $url = $moodle_url->out(false);
    $report_name_param = "&report_name_lang=$report_name_lang_entry";
    echo 'Download: ';
    $download_report_url = $url . "&download=csv";
    echo html_writer::link($download_report_url, 'CSV');
    echo "&nbsp;";
    $download_report_url = $url . "&download=excel";
    echo html_writer::link($download_report_url, 'Excel');
    echo "&nbsp;";
    $download_report_url = $url . "&download=pdf_portrait" . $report_name_param;
    echo html_writer::link($download_report_url, 'PDF Portrait');
    echo "&nbsp;";
    $download_report_url = $url . "&download=pdf_landscape" . $report_name_param;
    echo html_writer::link($download_report_url, 'PDF Landscape');
}
