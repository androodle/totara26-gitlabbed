<?php

/**
 * Androgogic Training History Block: CPE Report
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     17/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Report on user's CPE hours
 *
 * */
//params
$sort = optional_param('sort', 'title_of_training', PARAM_RAW);
$dir = optional_param('dir', 'ASC', PARAM_ALPHA);
$page = optional_param('page', 0, PARAM_INT);
$perpage = optional_param('perpage', 50, PARAM_INT);
$tab = optional_param('tab', 'cpe_report', PARAM_TEXT);
$user_search = optional_param('user_search', '', PARAM_TEXT);
$assessed_only = optional_param('assessed_only', false, PARAM_BOOL);
$androgogic_activities_id = optional_param('androgogic_activities_id', 0, PARAM_INT);
$dimension_id = optional_param('dimension_id', 0, PARAM_INT);
$competency_id = optional_param('competency_id', 0, PARAM_INT);
$user_id = optional_param('user_id', 0, PARAM_INT);

if (isset($_POST['startdate']['enabled'])) {
    //make it into a unix time
    $startdate = mktime(0, 0, 0, $_POST['startdate']['month'], $_POST['startdate']['day'], $_POST['startdate']['year']) - 1;
} 
if (isset($_POST['enddate']['enabled'])) {
    //make it into a unix time
    $enddate = mktime(0, 0, 0, $_POST['enddate']['month'], $_POST['enddate']['day'], $_POST['enddate']['year']) + 1;
}
// prepare url for paging bar
$PAGE->set_url($PAGE->url, compact('sort', 'dir', 'page', 'perpage', 'tab','androgogic_activities_id','user_id'));

//are we filtering on user?
if (!has_capability('block/androgogic_training_history:admin', $context)) {
    $user_id = $USER->id;
}

// prepare columns for results table
$columns = array(
    "title_of_training",
    "activity",
    "provider",
    "date_issued",
    "total_cpe_hours",
    "assessed", 
    "assessment_code", 
    
);
if ($user_id == 0) {
    $columns[] = "user";
    $columns[] = "username";
}
foreach ($columns as $column) {
    $string[$column] = get_string("$column", 'block_androgogic_training_history');
    if ($sort != $column) {
        $columnicon = '';
        $columndir = 'ASC';
    } else {
        $columndir = $dir == 'ASC' ? 'DESC' : 'ASC';
        $columnicon = $dir == 'ASC' ? 'down' : 'up';
    }
    if ($column != 'details') {
        $$column = "<a href='$PAGE->url&amp;dir=$columndir&amp;sort=$column'>$string[$column]</a>";
    } else {
        $$column = $string[$column];
    }
}
//figure out the and clause from what has been submitted
$and = '';
$and2 = '';

//are we filtering on androgogic_activities?
if ($androgogic_activities_id > 0) {
    $and .= " and mdl_androgogic_activities.id = $androgogic_activities_id ";
    //let's see which one they picked
    $chosen_activity = $DB->get_record('androgogic_activities', array('id' => $androgogic_activities_id));
    if ($chosen_activity->name != 'LMS course') {
        $and2 .= " and 1 = 0 ";
    }
}
if ($dimension_id > 0) {
    $and .= " and a.id IN ( select training_history_id from mdl_androgogic_training_history_dimensions where dimension_id = $dimension_id )";
    $and2 .= " and c.id IN ( select course_id from mdl_androgogic_course_dimensions where dimension_id = $dimension_id )";
}

if ($competency_id > 0) {
    $and .= " and cpe.competency_id = $competency_id ";    
    $and2 .= " and cpe.competency_id = $competency_id ";
}

if ($user_id > 0) {
    $and .= " and mdl_user.id = $user_id ";
    $and2 .= " and mdl_user.id = $user_id ";
}
if ($user_search != '') {
    $and .= " and concat(firstname, ' ', lastname, ' ', username) like '%$user_search%' ";
    $and2 .= " and concat(firstname, ' ', lastname, ' ', username) like '%$user_search%' ";
}
if (isset($startdate)) {
    $and .= " and (a.date_issued > $startdate)";
    $and2 .= " and (cc.timecompleted > $startdate)";
}
if (isset($enddate)) {
    $and .= " and (a.date_issued < $enddate)";
    $and2 .= " and (cc.timecompleted < $enddate)";
}
if($assessed_only){
    $and .= " and a.assessed = 1";
}
//had to do this fancy sequencing to allow both the join to work and for moodle's insistence on the first col being id and values being unique in it
$q = "SELECT @rownum:=@rownum+1 id, title_of_training, provider, date_issued, total_cpe_hours, assessed, assessment_code,activity, user, username, training_history_id, user_id 
    FROM(
SELECT DISTINCT  a.title_of_training, a.provider, a.date_issued, sum(cpe_hours) as total_cpe_hours, a.assessed, a.assessment_code,  
mdl_androgogic_activities.name AS activity, CONCAT(mdl_user.firstname,' ',mdl_user.lastname) AS user, username, a.id as training_history_id, a.user_id
FROM mdl_androgogic_training_history a 
inner join mdl_androgogic_training_history_competencies_cpe_hours cpe on a.id = cpe.training_history_id
LEFT JOIN mdl_androgogic_activities  ON a.activity_id = mdl_androgogic_activities.id
LEFT JOIN mdl_user  ON a.user_id = mdl_user.id
WHERE 1 = 1
$and
group by a.title_of_training, a.provider, a.date_issued, a.assessed, a.assessment_code,  
mdl_androgogic_activities.name, mdl_user.firstname,mdl_user.lastname, username, a.id, a.user_id
UNION
SELECT c.fullname AS title_of_training, '{$SITE->shortname}' AS provider, cc.timecompleted AS date_issued, sum(cpe_hours) as total_cpe_hours, '', '',
'LMS course' AS activity, CONCAT(mdl_user.firstname,' ',mdl_user.lastname) AS user, username, 0, mdl_user.id as user_id
FROM mdl_course_completions cc
INNER JOIN mdl_course c ON cc.course = c.id
INNER JOIN mdl_androgogic_training_history_course_competencies_cpe_hours cpe on c.id = cpe.course_id
LEFT JOIN mdl_user ON cc.userid = mdl_user.id
WHERE STATUS >= 50
$and2
group by c.fullname, cc.timecompleted, mdl_user.firstname,mdl_user.lastname, username,mdl_user.id
) b , (SELECT @rownum:=0) r  
order by $sort $dir";
if (isset($_GET['debug'])) {
    echo '$query : ' . $q . '';
}
//get a page worth of records
$results = $DB->get_records_sql($q, array(), $page * $perpage, $perpage);
if($download == ''){
    //also get the total number we have of these
    $results_for_count = $DB->get_records_sql($q);
    $result_count = count($results_for_count);

    echo $OUTPUT->heading(get_string('cpe_report_heading', 'block_androgogic_training_history'));

    if ($user_id > 0) {
        $subject_user = $DB->get_record('user', array('id' => $user_id));
        echo $OUTPUT->heading(fullname($subject_user));
        echo $OUTPUT->heading(fullname($subject_user->idnumber));
    }

    require_once('cpe_report_form.php');
    $mform = new cpe_report_form(null, array('sort' => $sort, 'dir' => $dir, 'perpage' => $perpage, 'tab' => $currenttab, 'androgogic_activities_id' => $androgogic_activities_id, 'user_id' => $user_id, 'context' => $context));
    $mform->display();

    echo '<table width="100%"><tr><td width="50%">';
    echo $result_count . ' ' . get_string('training_history_plural', 'block_androgogic_training_history') . " found" . '<br>';
    echo '</td><td style="text-align:right;">';
    if(has_capability('block/androgogic_training_history:admin', $context)){
    echo "<a href='index.php?tab=training_history_new'>" . get_string('training_history_new', 'block_androgogic_training_history') . "</a>";
    }
    echo '</td></tr></table>';
}

//RESULTS
if (!$results) {
    if($download == ''){
        echo $OUTPUT->heading(get_string('noresults', 'block_androgogic_training_history'));
    }
} else {
    $kas = array();
    foreach ($results as $result) {
        //any KAs for this one?
        if($result->training_history_id != 0){
            $sql = "SELECT cpe.*, c.fullname "
                    . "from mdl_androgogic_training_history_competencies_cpe_hours cpe "
                    . "inner join mdl_comp c on cpe.competency_id = c.id "
                    . "where cpe.training_history_id = " . $result->training_history_id;
            $these_kas = $DB->get_records_sql($sql);
        }
        $kas = array_merge($kas,$these_kas);
    }
    //need to get a unique set of ka fullnames from the set now
    $ka_fullnames = array();
    foreach($kas as $ka){
        if(!in_array($ka->fullname, $ka_fullnames)){
            $ka_fullnames[] = $ka->fullname;
        }
    }
    $table = new html_table();
    $table->head = array();
    if ($user_id == 0) {
        $table->head[] = $user;
        $table->head[] = $username;
    }
    $table->head[] = $title_of_training;
    $table->head[] = $activity;
    $table->head[] = $provider;
    $table->head[] = $date_issued;    
    $table->head[] = $assessed; 
    $table->head[] = $assessment_code;
    $table->head[] = $total_cpe_hours;
    foreach($ka_fullnames as $ka_fullname){
        $table->head[] = $ka_fullname;
    }
    
    
    $total_points = 0;
    foreach ($results as $result) {
        $td_array = array();
        if ($user_id == 0) {
            $td_array[] = html_writer::link($PAGE->url . '&user_id=' . $result->user_id, $result->user);
            $td_array[] = $result->username;
        }
        
        $td_array[] = $result->title_of_training;
        $td_array[] = $result->activity;
        $td_array[] = $result->provider;
        $td_array[] = date('Y-m-d', $result->date_issued);
        $td_array[] = $result->assessed;
        $td_array[] = $result->assessment_code;            
        $td_array[] = $result->total_cpe_hours; // we want last col to have this, and sometimes it has already been assigned
        
        //can we match up the record to its kas?
        foreach($kas as $ka){
            
            if($ka->training_history_id == $result->training_history_id){
                //run through the table columns and figure out which one we want to assign the points to
                $col_index = 0;
                foreach($table->head as $column_header){
                    if($ka->fullname == $column_header){
                        $td_array[$col_index] = $ka->cpe_hours;
                    }
                    $col_index++;
                }
            }
            
            
        }
        //any missing array cells? if so, assign empty string to them
        for ($index1 = 0; $index1 < count($table->head); $index1++) {
            if(!isset($td_array[$index1])){
                $td_array[$index1] = '';
            }
        }
        $table->data[] = $td_array;
        $total_points += $result->total_cpe_hours;
    }
    if ($user_id > 0) {
        //total row
        $total_row = array();
        $total_row[] = "<strong>Total</strong>";
        for ($index = 1; $index < $col_index-1; $index++) {
            $total_row[] = "";
        }
        $total_row[$col_index] = "<strong>$total_points</strong>";
        $table->data[] = $total_row;
    }
}
if($download != ''){
    //export the table to whatever they asked for
    block_androgogic_training_history_export_data($download,$table,$tab,$report_name_lang);
}
elseif (!empty($table)) {
    echo html_writer::table($table);
    $pagingbar = new paging_bar($result_count, $page, $perpage, $PAGE->url);
    $pagingbar->pagevar = 'page';
    echo $OUTPUT->render($pagingbar);
    block_androgogic_training_history_output_download_links($PAGE->url,'cpe_report_heading');
}
?>
