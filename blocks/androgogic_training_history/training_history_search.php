<?php

/**
 * Androgogic Training History Block: Search
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     17/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Search training_histories
 * Also provides access to edit and delete functions if user has sufficient permissions
 *
 * */
//params
$sort = optional_param('sort', 'title_of_training', PARAM_RAW);
$dir = optional_param('dir', 'ASC', PARAM_ALPHA);
$page = optional_param('page', 0, PARAM_INT);
$perpage= optional_param('perpage', 20, PARAM_INT); 
$search = optional_param('search', '', PARAM_TEXT);
$tab = optional_param('tab', 'training_history_search', PARAM_FILE);
$androgogic_activities_id = optional_param('androgogic_activities_id', 0, PARAM_INT);
$user_id = optional_param('user_id', 0, PARAM_INT);
$user_search = optional_param('user_search', '', PARAM_TEXT);
$androgogic_dimensions_id = optional_param('androgogic_dimensions_id', 0, PARAM_INT); 

if (isset($_POST['startdate']['enabled'])) {
    //make it into a unix time
    $startdate = mktime(0, 0, 0, $_POST['startdate']['month'], $_POST['startdate']['day'], $_POST['startdate']['year']) - 1;
}
if (isset($_POST['enddate']['enabled'])) {
    //make it into a unix time
    $enddate = mktime(0, 0, 0, $_POST['enddate']['month'], $_POST['enddate']['day'], $_POST['enddate']['year']) + 1;
}
// prepare url for paging bar
$PAGE->set_url($PAGE->url, compact('sort', 'dir', 'page', 'perpage', 'search', 'tab'));
// prepare columns for results table
$columns = array(
    "title_of_training",
    "activity",
    "date_issued",
    "provider",
    "file",
    "user",
    "assessed",
    "assessment_code",
);
foreach ($columns as $column) {
    $string[$column] = get_string("$column", 'block_androgogic_training_history');
    if ($sort != $column) {
        $columnicon = '';
        $columndir = 'ASC';
    } else {
        $columndir = $dir == 'ASC' ? 'DESC' : 'ASC';
        $columnicon = $dir == 'ASC' ? 'down' : 'up';
    }
    if ($column != 'details') {
        $$column = "<a href='$PAGE->url&amp;dir=$columndir&amp;sort=$column'>$string[$column]</a>";
    } else {
        $$column = $string[$column];
    }
}
//figure out the and clause from what has been submitted
$and = '';
if ($search != '') {
    $and .= " and concat(
a.title_of_training,
a.provider,
a.assessment_code
) like '%$search%'";
}
//are we filtering on androgogic_activities?
if ($androgogic_activities_id > 0) {
    $and .= " and mdl_androgogic_activities.id = $androgogic_activities_id ";
}
//are we filtering on user?
//is the user a manager of others?
$is_manager = block_androgogic_training_history_is_manager();

if (!has_capability('block/androgogic_training_history:admin', $context)) {
    if ($is_manager) {
        $and .= " and ( mdl_user.id in (select userid from mdl_pos_assignment where managerid = $USER->id) OR mdl_user.id = $USER->id )";
    } else {
        $user_id = $USER->id;
    }
}
if ($user_id > 0) {
    $and .= " and mdl_user.id = $user_id ";
}

//are we filtering on androgogic_dimensions?
if($androgogic_dimensions_id > 0){ 
$and .= " and mdl_androgogic_dimensions.id = $androgogic_dimensions_id ";
} 

if (isset($startdate)) {
    $and .= " and (a.date_issued > $startdate)";
}
if (isset($enddate)) {
    $and .= " and (a.date_issued < $enddate)";
}
if ($user_search != '') {
    $and .= " and concat(firstname, ' ', lastname, ' ', username) like '%$user_search%' ";
    $and2 .= " and concat(firstname, ' ', lastname, ' ', username) like '%$user_search%' ";
}
$q = "select DISTINCT a.* , mdl_androgogic_activities.name as activity, mdl_files.contextid, mdl_files.component, mdl_files.filearea, mdl_files.filename, mdl_files.itemid, mdl_files.id as fileid, CONCAT(mdl_user.firstname,' ',mdl_user.lastname) as user 
from mdl_androgogic_training_history a 
LEFT JOIN mdl_androgogic_activities  on a.activity_id = mdl_androgogic_activities.id
LEFT JOIN mdl_files  on a.file_id = mdl_files.itemid and mdl_files.itemid != 0 and mdl_files.filename != '.'
LEFT JOIN mdl_user  on a.user_id = mdl_user.id
LEFT JOIN mdl_androgogic_training_history_dimensions on a.id = mdl_androgogic_training_history_dimensions.training_history_id
LEFT JOIN mdl_androgogic_dimensions on mdl_androgogic_training_history_dimensions.dimension_id = mdl_androgogic_dimensions.id
where 1 = 1 
$and 
order by $sort $dir";
if (isset($_GET['debug'])) {
    echo '$query : ' . $q . '';
}
//get a page worth of records
$results = $DB->get_records_sql($q, array(), $page * $perpage, $perpage);
//also get the total number we have of these
$q = "SELECT COUNT(DISTINCT a.id)
from mdl_androgogic_training_history a 
LEFT JOIN mdl_androgogic_activities  on a.activity_id = mdl_androgogic_activities.id
LEFT JOIN mdl_files  on a.file_id = mdl_files.itemid and mdl_files.itemid != 0 and mdl_files.filename != '.'
LEFT JOIN mdl_user  on a.user_id = mdl_user.id
LEFT JOIN mdl_androgogic_training_history_dimensions on a.id = mdl_androgogic_training_history_dimensions.training_history_id
LEFT JOIN mdl_androgogic_dimensions on mdl_androgogic_training_history_dimensions.dimension_id = mdl_androgogic_dimensions.id
 where 1 = 1  $and";

if (isset($_GET['debug'])) {
    echo '$query : ' . $q . '<br>';
}

$result_count = $DB->get_field_sql($q);
require_once('training_history_search_form.php');
$mform = new training_history_search_form(null, array('sort' => $sort, 'dir' => $dir, 'perpage' => $perpage, 'search' => $search, 'tab' => $currenttab, 'androgogic_activities_id' => $androgogic_activities_id,'androgogic_dimensions_id'=>$androgogic_dimensions_id, 'user_id' => $user_id, 'context' => $context));
$mform->display();
echo '<table width="100%"><tr><td width="50%">';
echo $result_count . ' ' . get_string('training_history_plural', 'block_androgogic_training_history') . " found" . '<br>';
echo '</td><td style="text-align:right;">';
//if(has_capability('block/androgogic_training_history:admin', $context)){
echo "<a href='index.php?tab=training_history_new'>" . get_string('training_history_new', 'block_androgogic_training_history') . "</a>";
//}
echo '</td></tr></table>';

flush();

//RESULTS
if (!$results) {
    $match = array();
    echo $OUTPUT->heading(get_string('noresults', 'block_androgogic_training_history', $search));
} else {
    $table = new html_table();
    $table->head = array(
        $title_of_training,
        $activity,
        $date_issued,
        $provider,
        $file,
        $user,
        $assessed,
        $assessment_code
    );
    if(has_capability('block/androgogic_training_history:admin', $context)){
        $table->head[] = 'Action';
    }
$table->align = array ("left","left","left","left","left","left","left","left","left","left","left","left","left",);
    $table->width = "95%";
$table->size = array("8%","8%","8%","8%","8%","8%","8%","8%","8%","8%","8%","8%","8%",);
    foreach ($results as $result) {
        $view_link = ''; //enable if wanted: "<a href='index.php?tab=training_history_view&id=$result->id'>View</a> "; 
        $edit_link = "";
        $delete_link = "";
if(has_capability('block/androgogic_training_history:admin', $context)){
// then we show the edit link
        $edit_link = "<a href='index.php?tab=training_history_edit&id=$result->id'>Edit</a> ";
// and the delete link
        $delete_link = "<a href='index.php?tab=training_history_delete&id=$result->id' onclick='return confirm(\"Are you sure you want to delete this item?\")'>Delete</a> ";
}
//link for file
        $file_link = '';
        if (isset($result->filename)) {
            $file_link = new moodle_url("/pluginfile.php/{$result->contextid}/{$result->component}/{$result->filearea}/" . $result->itemid . '/' . $result->filename);
        }
        $user_link = new moodle_url("/user/profile.php?id=" . $result->user_id);
        $table->data[] = array(
            "$result->title_of_training",
            "$result->activity",
            date('Y-m-d', $result->date_issued),
            "$result->provider",
            html_writer::link($file_link, $result->filename),
            html_writer::link($user_link, $result->user),
            $result->assessed,
            "$result->assessment_code",            
            $view_link . $edit_link . $delete_link
        );
    }
}
if (!empty($table)) {
    echo html_writer::table($table);
    $pagingbar = new paging_bar($result_count, $page, $perpage, $PAGE->url);
    $pagingbar->pagevar = 'page';
    echo $OUTPUT->render($pagingbar);
}
?>
