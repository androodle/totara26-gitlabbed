<?php

/**
 * Androgogic User period Block: Search
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     01/08/2014
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Search user_periods
 * Also provides access to edit and delete functions if user has sufficient permissions
 *
 * */
//params
$sort = optional_param('sort', 'firstname', PARAM_RAW);
$dir = optional_param('dir', 'ASC', PARAM_ALPHA);
$page = optional_param('page', 0, PARAM_INT);
$perpage = optional_param('perpage', 20, PARAM_INT);
$search = optional_param('search', '', PARAM_TEXT);
$tab = optional_param('tab', 'user_period_search', PARAM_FILE);
$user_id = optional_param('user_id', 0, PARAM_INT);
// prepare url for paging bar
$PAGE->set_url($PAGE->url, compact('sort', 'dir', 'page', 'perpage', 'search', 'tab'));
// prepare columns for results table
$columns = array(
    "user",
    "organisation", 
    "position",
    
); //"points_override" //might want to reinstate this as a total?
foreach ($columns as $column) {
    $string[$column] = get_string("$column", 'block_androgogic_training_history');
    if ($sort != $column) {
        $columnicon = '';
        $columndir = 'ASC';
    } else {
        $columndir = $dir == 'ASC' ? 'DESC' : 'ASC';
        $columnicon = $dir == 'ASC' ? 'down' : 'up';
    }
    if ($column != 'details') {
        $$column = "<a href='$PAGE->url&amp;dir=$columndir&amp;sort=$column'>$string[$column]</a>";
    } else {
        $$column = $string[$column];
    }
}
//figure out the and clause from what has been submitted
$and = '';
if ($search != '') {
    $and .= " and concat(firstname, ' ', lastname, ' ', username) like '%$search%' ";
}

$q = "select u.id, a.startdate, a.enddate , CONCAT(u.firstname,' ',u.lastname) as user, 
        p.fullname as position, o.fullname as organisation, a.id as override_id, 
        o.frameworkid as org_framework_id, p.frameworkid as pos_framework_id, p.sortthread as pos_sortthread, 
        o.sortthread as org_sortthread
from mdl_user u
left join mdl_pos_assignment pa on u.id = pa.userid and type = 1
LEFT JOIN  mdl_androgogic_user_period a on a.user_id = u.id 
    and period_id in (
    select id from mdl_androgogic_training_history_periods where is_current = 1)
left join mdl_pos p on pa.positionid = p.id 
left join mdl_org o on pa.organisationid = o.id
where deleted = 0 
$and 
order by $sort $dir";
if (isset($_GET['debug'])) {
    echo '$query : ' . $q . '';
}
//get a page worth of records
$results = $DB->get_records_sql($q, array(), $page * $perpage, $perpage);
//also get the total number we have of these
$q = "SELECT COUNT(DISTINCT u.id)
from mdl_user u
where deleted = 0   $and";

if (isset($_GET['debug'])) {
    echo '$query : ' . $q . '<br>';
}

$result_count = $DB->get_field_sql($q);
require_once('user_period_search_form.php');
$mform = new user_period_search_form(null, array('sort' => $sort, 'dir' => $dir, 'perpage' => $perpage, 'search' => $search, 'tab' => $currenttab, 'user_id' => $user_id));
$mform->display();
echo '<table width="100%"><tr><td width="50%">';
echo $result_count . ' ' . get_string('user_period_plural', 'block_androgogic_training_history') . " found" . '<br>';
echo '</td><td style="text-align:right;">';

echo '</td></tr></table>';

flush();

//RESULTS
if (!$results) {
    $match = array();
    echo $OUTPUT->heading(get_string('noresults', 'block_androgogic_training_history', $search));
} else {
    $table = new html_table();
    $table->head = array(
        $user, 
        $organisation, 
        $position,
        // $points_override,
        get_string('startdate', 'block_androgogic_training_history'),
        get_string('enddate', 'block_androgogic_training_history'),        
        get_string('period_type', 'block_androgogic_training_history'),
        
        'Action'
    );
    $table->align = array("left", "left", "left", "left", "left", "left", "left", "left",);
    $table->width = "95%";
    $table->size = array("13%", "13%", "13%", "13%", "13%", "13%", "13%", "13%",);
    foreach ($results as $result) {
        $view_link = ''; //enable if wanted: "<a href='index.php?tab=user_period_view&id=$result->id'>View</a> "; 
        $edit_link = "";
        $delete_link = "";
        if (has_capability('block/androgogic_training_history:admin', $context)) {
            $points_link = "<a href='index.php?tab=user_points_override_edit&user_id=$result->id'>Edit Points</a> ";
// then we show the edit link
            if (isset($result->override_id) and isset($result->organisation) and isset($result->position)){
                $edit_link = "<a href='index.php?tab=user_period_edit&id=$result->override_id&user_id=$result->id'>Edit Period</a> ";
            }
            elseif(isset($result->organisation) and isset($result->position)){
                $edit_link = "<a href='index.php?tab=user_period_new&user_id=$result->id'>New Period</a> ";
            }
        
// then we show the delete link
            if (isset($result->override_id)  and isset($result->organisation) and isset($result->position)){
                $delete_link = "<a href='index.php?tab=user_period_delete&id=$result->override_id' onclick='return confirm(\"Are you sure you want to delete this item?\")'>Delete Period</a> ";
            }
        }
        // get the default dates if we can
        unset($org_pos_period);
        if(isset($result->org_framework_id) and isset($result->pos_framework_id) and isset($result->pos_sortthread)){
            $org_pos_period = block_androgogic_training_history_get_org_pos_period(
                    $result->org_framework_id,$result->pos_framework_id,
                    $result->pos_sortthread,$result->org_sortthread);
            list($period_start_date,$period_end_date) = block_androgogic_training_history_get_cpd_period($result, $org_pos_period);
       
        }
        if ($result->startdate) {
            $startdate = date('Y-m-d', $result->startdate);
        } 
        else if(isset($org_pos_period)){
            $startdate = date('Y-m-d', $period_start_date);
        }
        else {
            $startdate = '';
        }
        if ($result->enddate) {
            $enddate = date('Y-m-d', $result->enddate);
        }
        else if(isset($org_pos_period)){
            $enddate = date('Y-m-d', $period_end_date);
        }
        else {
            $enddate = '';
        }
        $user_link = html_writer::link($CFG->wwwroot . '/user/positions.php?type=primary&user=' . $result->id, $result->user, array('target' => '_blank'));
        $period_type = '';
        if (isset($result->override_id)) {
            $period_type = 'override';
        }
        else if(isset($org_pos_period)){
            $period_type = 'default';
        }
        
        $table->data[] = array(
            $user_link,
            $result->organisation,
            $result->position,
            //$result->points_override,
            $startdate,
            $enddate,
            $period_type,
            $points_link . $edit_link . $delete_link
        );
    }
}
if (!empty($table)) {
    echo html_writer::table($table);
    $pagingbar = new paging_bar($result_count, $page, $perpage, $PAGE->url);
    $pagingbar->pagevar = 'page';
    echo $OUTPUT->render($pagingbar);
}
?>
