<?php

/** 
 * Androgogic Training History Block: Edit form
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     17/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Provides edit form for the object.
 * This is used by both new and edit pages
 *
 **/

if (!defined('MOODLE_INTERNAL')) {
die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}
require_once($CFG->libdir . '/formslib.php');
class activity_edit_form extends moodleform {
protected $activity;
function definition() {
global $USER,$courseid,$DB,$PAGE;
$mform =& $this->_form;
$context = get_context_instance(CONTEXT_SYSTEM);
if(isset($_REQUEST['id'])){
$q = "select DISTINCT a.*  
from mdl_androgogic_activities a 
where a.id = {$_REQUEST['id']} ";
$activity = $DB->get_record_sql($q);
}
else{
$activity = $this->_customdata['$activity']; // this contains the data of this form
}
$tab = 'activity_new'; // from whence we were called
if (!empty($activity->id)) {
$tab = 'activity_edit';
}
$mform->addElement('html','<div>');

//name
$mform->addElement('text', 'name', get_string('name','block_androgogic_training_history'), array('size'=>50));
$mform->addRule('name', get_string('required'), 'required', null, 'server');
$mform->addRule('name', 'Maximum 50 characters', 'maxlength', 50, 'client');
//set values if we are in edit mode
if (!empty($activity->id) && isset($_GET['id'])) {
$mform->setConstant('name', $activity->name);
}
//hiddens
$mform->addElement('hidden','tab',$tab);
if(isset($_REQUEST['id'])){
$mform->addElement('hidden','id',$_REQUEST['id']);
}
elseif(isset($id)){
$mform->addElement('hidden', 'id', $id);
}
$this->add_action_buttons(false);
$mform->addElement('html','</div>');
}
}
