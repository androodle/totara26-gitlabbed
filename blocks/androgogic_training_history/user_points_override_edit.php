<?php

/** 
 * Androgogic Training History Block: Edit object
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     05/08/2014
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Edit one of the user_points_override
 *
 **/

global $OUTPUT;
require_capability('block/androgogic_training_history:admin', $context);
require_once('user_points_override_edit_form.php');
$user_id = required_param('user_id', PARAM_INT);
$user = $DB->get_record('user',array('id'=>$user_id));
$mform = new user_points_override_edit_form();
if ($data = $mform->get_data()){
    $data->modified_by = $USER->id;
    $data->date_modified = date('Y-m-d H:i:s');
    
    block_androgogic_training_history_cpd_overrides_update($data);
    block_androgogic_training_history_cpe_overrides_update($data);
    echo $OUTPUT->notification(get_string('datasubmitted','block_androgogic_training_history'), 'notifysuccess');

}
else{
    echo $OUTPUT->heading(fullname($user));
    echo $OUTPUT->heading(get_string('user_points_override_edit', 'block_androgogic_training_history'));
    $mform->display();
}

?>
