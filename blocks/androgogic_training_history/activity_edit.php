<?php

/** 
 * Androgogic Training History Block: Edit object
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     17/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Edit one of the activities
 *
 **/

global $OUTPUT;
require_once('activity_edit_form.php');
$id = required_param('id', PARAM_INT);
$q = "select DISTINCT a.*  
from mdl_androgogic_activities a 
where a.id = $id ";
$activity = $DB->get_record_sql($q);
$mform = new activity_edit_form();
if ($data = $mform->get_data()){
$data->id = $id;
$data->modified_by = $USER->id;
$data->date_modified = date('Y-m-d H:i:s');
$DB->update_record('androgogic_activities',$data);
echo $OUTPUT->notification(get_string('datasubmitted','block_androgogic_training_history'), 'notifysuccess');
}
else{
echo $OUTPUT->heading(get_string('activity_edit', 'block_androgogic_training_history'));
$mform->display();
}

?>
