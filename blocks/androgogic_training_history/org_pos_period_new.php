<?php

/**
 * Androgogic Training History Block: Create object
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     05/08/2014
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Create new org_pos_period
 *
 * */
global $OUTPUT;
require_capability('block/androgogic_training_history:admin', $context);
require_once('org_pos_period_edit_form.php');
$mform = new org_pos_period_edit_form();
if ($data = $mform->get_data()) {
    $data->created_by = $USER->id;
    $data->date_created = date('Y-m-d H:i:s');

//check for unique
    $is_unique = block_androgogic_training_history_org_pos_unique($data);
    if (!$is_unique) {
        echo $OUTPUT->notification(get_string('orgposcombonotunique', 'block_androgogic_training_history'), 'notifyfailure');
    } else {
        $newid = $DB->insert_record('androgogic_org_pos_periods', $data);
        block_androgogic_training_history_cpd_targets_update($data);
        block_androgogic_training_history_cpe_targets_update($data);
        echo $OUTPUT->notification(get_string('datasubmitted', 'block_androgogic_training_history'), 'notifysuccess');
        echo $OUTPUT->action_link($PAGE->url, 'Create another item');
    }
} else {
    echo $OUTPUT->heading(get_string('org_pos_period_new', 'block_androgogic_training_history'));
    $mform->display();
}
?>
