<?php

/** 
 * Androgogic Training History Block: Edit object
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     20/08/2014
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Edit one of the dimensions
 *
 **/

global $OUTPUT;
require_capability('block/androgogic_training_history:admin', $context);
require_once('dimension_edit_form.php');
$id = required_param('id', PARAM_INT);
$q = "select DISTINCT a.*  
from mdl_androgogic_dimensions a 
where a.id = $id ";
$dimension = $DB->get_record_sql($q);
$mform = new dimension_edit_form();
if ($data = $mform->get_data()){
$data->id = $id;
$data->modified_by = $USER->id;
$data->date_modified = date('Y-m-d H:i:s');
$DB->update_record('androgogic_dimensions',$data);
echo $OUTPUT->notification(get_string('datasubmitted','block_androgogic_training_history'), 'notifysuccess');
}
else{
echo $OUTPUT->heading(get_string('dimension_edit', 'block_androgogic_training_history'));
$mform->display();
}

?>
