<?php

/**
 * Androgogic Training History Block: Install DB scripts
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     03/07/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 * */
function xmldb_block_androgogic_training_history_install() {
    global $DB;
    $result = true;
// create tables for the block
    if (!$DB->get_manager()->table_exists('androgogic_course_dimensions')) {
        $sql = "CREATE TABLE `mdl_androgogic_course_dimensions` ( `id` bigint(11) NOT NULL AUTO_INCREMENT, `course_id` bigint(11) NOT NULL COMMENT 'relates to mdl_course table', `dimension_id` bigint(11) NOT NULL COMMENT 'relates to mdl_androgogic_dimensions table', PRIMARY KEY (`id`), UNIQUE KEY `unq_course_dimension` (`course_id`,`dimension_id`) ) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COMMENT='many to many linking table'";
        $DB->execute($sql);
    }
    if (!$DB->get_manager()->table_exists('androgogic_dimensions')) {
        $sql = "CREATE TABLE `mdl_androgogic_dimensions` ( `id` bigint(11) NOT NULL AUTO_INCREMENT, `name` varchar(50) NOT NULL, `created_by` bigint(10) NOT NULL COMMENT 'relates to user table', `date_created` datetime NOT NULL, `modified_by` bigint(10) DEFAULT NULL COMMENT 'relates to user table', `date_modified` datetime DEFAULT NULL, PRIMARY KEY (`id`) ) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1";
        $DB->execute($sql);
    }
    if (!$DB->get_manager()->table_exists('androgogic_org_pos_periods')) {
        $sql = "CREATE TABLE `mdl_androgogic_org_pos_periods` ( `id` bigint(11) NOT NULL AUTO_INCREMENT, `org_id` bigint(11) NOT NULL, `pos_id` bigint(11) NOT NULL, `period` varchar(10) NOT NULL, `created_by` bigint(10) NOT NULL COMMENT 'relates to user table', `date_created` datetime NOT NULL, `modified_by` bigint(10) DEFAULT NULL COMMENT 'relates to user table', `date_modified` datetime DEFAULT NULL, PRIMARY KEY (`id`), UNIQUE KEY `org_pos_unq` (`org_id`,`pos_id`) ) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8";
        $DB->execute($sql);
    }
    if (!$DB->get_manager()->table_exists('androgogic_training_history')) {
        $sql = "CREATE TABLE `mdl_androgogic_training_history` ( `id` bigint(11) NOT NULL AUTO_INCREMENT, `title_of_training` varchar(128) NOT NULL, `activity_id` int(10) DEFAULT NULL, `date_issued` bigint(20) NOT NULL, `provider` varchar(256) DEFAULT NULL, `file_id` bigint(10) DEFAULT NULL, `user_id` bigint(10) NOT NULL, `created_by` bigint(10) NOT NULL COMMENT 'relates to user table', `date_created` datetime NOT NULL, `modified_by` bigint(10) DEFAULT NULL COMMENT 'relates to user table', `date_modified` datetime DEFAULT NULL, `assessed` tinyint(4) DEFAULT '0', `assessment_code` varchar(128) DEFAULT NULL, `approved` tinyint(4) DEFAULT '0', PRIMARY KEY (`id`) ) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8";
        $DB->execute($sql);
    }
    if (!$DB->get_manager()->table_exists('androgogic_training_history_competencies_cpd_points')) {
        $sql = "CREATE TABLE `mdl_androgogic_training_history_competencies_cpd_points` ( `id` bigint(11) NOT NULL AUTO_INCREMENT, `training_history_id` bigint(11) NOT NULL COMMENT 'relates to mdl_androgogic_training_history table', `competency_id` bigint(11) NOT NULL COMMENT 'relates to mdl_comp table', `cpd_points` decimal(5,2) NOT NULL, PRIMARY KEY (`id`), UNIQUE KEY `unq_training_history_competency_cpd_points` (`training_history_id`,`competency_id`,`cpd_points`) ) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COMMENT='many to many linking table'";
        $DB->execute($sql);
    }
    if (!$DB->get_manager()->table_exists('androgogic_training_history_competencies_cpe_hours')) {
        $sql = "CREATE TABLE `mdl_androgogic_training_history_competencies_cpe_hours` ( `id` bigint(11) NOT NULL AUTO_INCREMENT, `training_history_id` bigint(11) NOT NULL COMMENT 'relates to mdl_androgogic_training_history table', `competency_id` bigint(11) NOT NULL COMMENT 'relates to mdl_comp table', `cpe_hours` decimal(5,2) NOT NULL, PRIMARY KEY (`id`), UNIQUE KEY `unq_training_history_competency` (`training_history_id`,`competency_id`,`cpe_hours`) ) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='many to many linking table'";
        $DB->execute($sql);
    }
    if (!$DB->get_manager()->table_exists('androgogic_training_history_course_competencies_cpd_points')) {
        $sql = "CREATE TABLE `mdl_androgogic_training_history_course_competencies_cpd_points` ( `id` bigint(11) NOT NULL AUTO_INCREMENT, `course_id` bigint(11) NOT NULL COMMENT 'relates to mdl_course table', `competency_id` bigint(11) NOT NULL COMMENT 'relates to mdl_comp table', `cpd_points` decimal(5,2) NOT NULL, PRIMARY KEY (`id`), UNIQUE KEY `unq_course_competency_cpd_points` (`course_id`,`competency_id`,`cpd_points`) ) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=latin1 COMMENT='many to many linking table'";
        $DB->execute($sql);
    }
    if (!$DB->get_manager()->table_exists('androgogic_training_history_course_competencies_cpe_hours')) {
        $sql = "CREATE TABLE `mdl_androgogic_training_history_course_competencies_cpe_hours` ( `id` bigint(11) NOT NULL AUTO_INCREMENT, `course_id` bigint(11) NOT NULL COMMENT 'relates to mdl_course table', `competency_id` bigint(11) NOT NULL COMMENT 'relates to mdl_comp table', `cpe_hours` decimal(5,2) NOT NULL, PRIMARY KEY (`id`), UNIQUE KEY `unq_course_competency_cpe_hours` (`course_id`,`competency_id`,`cpe_hours`) ) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1 COMMENT='many to many linking table'";
        $DB->execute($sql);
    }
    if (!$DB->get_manager()->table_exists('androgogic_training_history_cpd_targets')) {
        $sql = "CREATE TABLE `mdl_androgogic_training_history_cpd_targets` ( `id` bigint(11) NOT NULL AUTO_INCREMENT, `competency_id` bigint(11) NOT NULL, `org_pos_period_id` bigint(11) NOT NULL, `cpd_target` decimal(5,2) NOT NULL, `created_by` bigint(10) NOT NULL COMMENT 'relates to user table', `date_created` datetime NOT NULL, `modified_by` bigint(10) DEFAULT NULL COMMENT 'relates to user table', `date_modified` datetime DEFAULT NULL, PRIMARY KEY (`id`) ) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8";
        $DB->execute($sql);
    }
    if (!$DB->get_manager()->table_exists('androgogic_training_history_cpe_targets')) {
        $sql = "CREATE TABLE `mdl_androgogic_training_history_cpe_targets` ( `id` bigint(11) NOT NULL AUTO_INCREMENT, `competency_id` bigint(11) NOT NULL, `org_pos_period_id` bigint(11) NOT NULL, `cpe_target` decimal(5,2) NOT NULL, `created_by` bigint(10) NOT NULL COMMENT 'relates to user table', `date_created` datetime NOT NULL, `modified_by` bigint(10) DEFAULT NULL COMMENT 'relates to user table', `date_modified` datetime DEFAULT NULL, PRIMARY KEY (`id`) ) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8";
        $DB->execute($sql);
    }
    if (!$DB->get_manager()->table_exists('androgogic_training_history_dimensions')) {
        $sql = "CREATE TABLE `mdl_androgogic_training_history_dimensions` ( `id` bigint(11) NOT NULL AUTO_INCREMENT, `training_history_id` bigint(11) NOT NULL COMMENT 'relates to mdl_androgogic_training_history table', `dimension_id` bigint(11) NOT NULL COMMENT 'relates to mdl_androgogic_dimensions table', PRIMARY KEY (`id`), UNIQUE KEY `unq_training_history_dimension` (`training_history_id`,`dimension_id`) ) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 COMMENT='many to many linking table'";
        $DB->execute($sql);
    }
    if (!$DB->get_manager()->table_exists('androgogic_user_period')) {
        $sql = "CREATE TABLE `mdl_androgogic_user_period` ( `id` bigint(11) NOT NULL AUTO_INCREMENT, `startdate` bigint(20) NOT NULL, `enddate` bigint(20) NOT NULL, `user_id` bigint(11) NOT NULL, `created_by` bigint(10) NOT NULL COMMENT 'relates to user table', `date_created` datetime NOT NULL, `modified_by` bigint(10) DEFAULT NULL COMMENT 'relates to user table', `date_modified` datetime DEFAULT NULL, PRIMARY KEY (`id`) ) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1";
        $DB->execute($sql);
    }
    return $result;
}
