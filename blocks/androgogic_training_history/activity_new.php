<?php

/** 
 * Androgogic Training History Block: Create object
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     17/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Create new activity
 *
 **/

global $OUTPUT;
require_once('activity_edit_form.php');
$mform = new activity_edit_form();
if ($data = $mform->get_data()){
$data->created_by = $USER->id;
$data->date_created = date('Y-m-d H:i:s');
$newid = $DB->insert_record('androgogic_activities',$data);
echo $OUTPUT->notification(get_string('datasubmitted','block_androgogic_training_history'), 'notifysuccess');
echo $OUTPUT->action_link($PAGE->url, 'Create another item');
}
else{
echo $OUTPUT->heading(get_string('activity_new', 'block_androgogic_training_history'));
$mform->display();
}

?>
