<?php

/** 
 * Androgogic Training History Block: Language Pack
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     17/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 **/

$string['pluginname'] = 'Androgogic Training History';
$string['plugintitle'] = 'Androgogic Training History';

//spares
//$string['user_points_override_edit'] = 'Edit user points override';
//$string['points_override'] = 'Points override';
//$string[''] = '';

//cron strings
$string['cron_hours'] = 'Cron hours';
$string['cron_hours_explanation'] = 'The hour at which the cron should execute in 24 hour format(0-23). Can be multiple: if so use semi-colon delimited list. Note that if this is not set then the period expiry mechanism will not work.';
//approval_workflow
$string['approval_workflow'] = 'Require approval workflow';
$string['approval_workflow_explanation'] = 'If checked then the manager of the user will need to approve their training history records';
$string['approved'] = 'Approved';
$string['approved_story'] = 'If checked the training history record will show up in reports';

//training_history items
$string['training_history'] = 'Training History';
$string['title_of_training'] = 'Title Of Training';
$string['activity'] = 'Activity';
$string['date_issued'] = 'Date Completed';
$string['provider'] = 'Awarding authority';
$string['cpd_points'] = 'CPD Points';
$string['approved'] = 'Approved';
$string['assessed'] = 'Assessed';
$string['assessment_code'] = 'Assessment code';
$string['file'] = 'File';
$string['user'] = 'User';
$string['username'] = 'Username';
$string['user_id'] = 'User';
$string['training_history_search'] = 'Training Histories';
$string['training_history_plural'] = 'Training Histories';
$string['training_history_new'] = 'New Training History';
$string['training_history_edit'] = 'Edit Training History';
//common items
$string['datasubmitted'] = 'The data has been submitted';
$string['itemdeleted'] = 'The item has been deleted';
$string['noresults'] = 'There were no results from your search';
$string['block_androgogic_training_history:admin'] = 'See and edit all histories within the Androgogic Training History block';
$string['created_by'] = 'Created By';
$string['date_created'] = 'Date Created';
$string['modified_by'] = 'Modified By';
$string['date_modified'] = 'Date Modified';
$string['id'] = 'Id';
$string['training_history_search_instructions'] = 'Search by Title Of Training or  Provider';
//activity items
$string['name'] = 'Name';
$string['activity_search'] = 'Activities';
$string['activity_plural'] = 'Activities';
$string['activity_new'] = 'New Activity';
$string['activity_edit'] = 'Edit Activity';
$string['activity_search_instructions'] = 'Search by Name';
$string['activity'] = 'Activity';
$string['editthisfile'] = 'Edit this file';
$string['upload'] = 'Upload';
$string['startdate'] = 'Date completed between: ';
$string['enddate'] = 'and';
$string['cpd_report'] = 'CPD report';
$string['cpd_report_heading'] = 'Continuing Professional Development (CPD) Achievement Report';
$string['cpe_report'] = 'CPE report';
$string['cpe_report_heading'] = 'Continuing Professional Education (CPE) Achievement Report';
$string['targets_report'] = 'CPD and CPE targets report';
$string['targets_report_heading'] = 'CPD and CPE targets report';

//approval emails
$string['email_to_manager_subject'] = 'Training history record approval request';
$string['email_to_manager_body'] = '<p>A Totara user for whom you are the manager, has requested your approval for a Training History record.</p>
    
<p>Please login to the Totara and approve it if you feel that it is valid.</p>

<p>{$a->url}</p>
';
$string['email_to_user_subject'] = 'Training history record has been approved';
$string['email_to_user_body'] = '<p>Congratulations,</p> 
    
<p>Your manager has approved your Training history record.</p>

<p>{$a->url}</p>
';
$string['userhasnomanager'] = 'Approval could not be sought for this training record, as the user has no manager';
$string['cantapproveowntraininghistory'] = 'You cannot approve your own training history record. However, the record has been saved, without the approval';

//training_history_competency_cpe_hours items
$string['training_history_competency_cpe_hours'] = 'Training History Competency CPE Hours';
$string['training_history'] = 'Training History';
$string['competency_plural'] = 'Knowledge areas';
$string['cpe_hours'] = 'CPE Hours';
$string['training_history_competency_cpe_hours_plural'] = 'Training History Competency CPE Hours';
$string['training_history_competency_cpe_hours_new'] = 'Add new CPE hours';
$string['training_history_competency_cpe_hours_edit'] = 'Edit Training History Competency CPE Hours';

//training_history_competency_cpd_points items
$string['training_history_competency_cpd_points'] = 'Training History Competency CPD Points';
$string['training_history'] = 'Training History';
$string['competency'] = 'Knowledge area';
$string['cpd_points'] = 'CPD Points';
$string['total_cpd_points'] = 'Total CPD Points';
$string['total_cpe_hours'] = 'Total CPE Hours';
$string['cpd_target'] = 'CPD target';
$string['cpe_target'] = 'CPE target';
$string['dimension'] = 'Dimension';
$string['training_history_competency_cpd_points_plural'] = 'Training History Competency CPD Points';
$string['training_history_competency_cpd_points_new'] = 'Add new CPD points';
$string['training_history_competency_cpd_points_edit'] = 'Edit Training History Competency CPD Points';
$string['select_cpd_framework'] = 'Choose framework for CPD knowledge areas';
$string['select_comp_type'] = 'Choose competency type for CPD knowledge areas';
$string['select_cpe_competencies'] = 'Choose one or more competencies for CPE knowledge areas';
$string['do_cpe_hours'] = 'Do CPE hours';
$string['do_cpe_hours_explanation'] = 'Choose this if you want to assign CPE hours to training history records';

//user_period items
$string['user_period'] = 'Override';
$string['startdate'] = 'Startdate';
$string['enddate'] = 'Enddate';
$string['user'] = 'User';
$string['user_period_search'] = 'User overrides';
$string['user_period_plural'] = 'User overrides';
$string['user_period_new'] = 'New override';
$string['user_period_edit'] = 'Edit override';
$string['user_period_search_instructions'] = 'Search on Firstname, Lastname or Username';
$string['userhasnoorgpos'] = 'The selected user is missing a related organisation or position. <br>Because of this, we cannot override their period.<br>{$a->url}';
$string['userhasnoorgposperiod'] = 'The organisation and position pair has no period defined';
$string['cantchangeenddate'] = 'You cannot override the end date of the default period({$a})';
$string['startdatecannotprecedeorgposstartdate'] = 'The start date for the override must come after the default start date({$a})';
$string['startdatecannotexceedorgposenddate'] = 'The start date for the override must come before the default end date({$a})';
//$string[''] = '';
$string['points_override'] = 'Points override';

//org_pos_period items
$string['org_pos_period'] = 'Organisation Position Period';
$string['org'] = 'Organisation';
$string['pos'] = 'Position';
$string['period'] = 'Period';
$string['org_pos_period_search'] = 'Organisation Position Periods';
$string['org_pos_period_plural'] = 'Organisation Position Periods';
$string['org_pos_period_new'] = 'New Organisation Position Period';
$string['org_pos_period_edit'] = 'Edit Organisation Position Period';
$string['org_pos_period_search_instructions'] = 'Search by Period';
$string['orgposcombonotunique'] = 'The organisation and postion combination you selected already exists. Only one of these combinations is allowed, therefore your changes have not been saved.';
$string['organisation'] = 'Organisation';
$string['position'] = 'Position';
$string['period_type'] = 'Period type';
$string['cpd_report_user_search_instructions'] = 'Search on Firstname, Lastname or Username';

//targets report items
$string['target_points'] = 'Target points';
$string['completed_assessed_points'] = 'Completed assessed points';
$string['completed_unassessed_points'] = 'Completed unassessed points';
$string['total_completed_points'] = 'Total completed points';
$string['over_under'] = 'Over/under';
$string['percent_target_assessed'] = 'Percent target assessed';
$string['cpd_target_total'] = 'CPD target total';
$string['cpe_target_total'] = 'CPE target total';
$string['completed_cpd_total'] = 'Completed CPD total';
$string['completed_cpe_total'] = 'Completed CPE total';
$string['target_results'] = 'results';
$string['practice'] = 'Practice';

//dimension items
$string['dimension'] = 'Dimension';
$string['name'] = 'Name';
$string['dimension_search'] = 'Dimensions';
$string['dimension_plural'] = 'Dimensions';
$string['dimension_new'] = 'New Dimension';
$string['dimension_edit'] = 'Edit Dimension';
$string['created_by'] = 'Created By';
$string['date_created'] = 'Date Created';
$string['modified_by'] = 'Modified By';
$string['date_modified'] = 'Date Modified';
$string['id'] = 'Id';
$string['dimension_search_instructions'] = 'Search by Name';

//points override
$string['user_points_override_edit'] = 'Edit user points override';
$string['unallocated_points'] = 'Unallocated';
$string['cpd_points_override'] = 'CPD points override';
$string['cpe_hours_override'] = 'CPE hours override';

//period items
$string['period'] = 'Period';
$string['is_current'] = 'Is Current';
$string['period_search'] = 'Periods';
$string['period_plural'] = 'Periods';
$string['period_new'] = 'New Period';
$string['period_edit'] = 'Edit Period';
$string['period_search_instructions'] = 'Search by period type, start year, end year';
$string['start_year'] = 'Start year';
$string['end_year'] = 'End year';
