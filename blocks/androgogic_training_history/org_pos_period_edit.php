<?php

/** 
 * Androgogic Training History Block: Edit object
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     05/08/2014
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Edit one of the org_pos_periods
 *
 **/

global $OUTPUT;
require_capability('block/androgogic_training_history:admin', $context);
require_once('org_pos_period_edit_form.php');
$id = required_param('id', PARAM_INT);
$q = "select DISTINCT a.* , mdl_org.shortname as org, mdl_pos.shortname as pos 
from mdl_androgogic_org_pos_periods a 
LEFT JOIN mdl_org  on a.org_id = mdl_org.id
LEFT JOIN mdl_pos  on a.pos_id = mdl_pos.id
where a.id = $id ";
$org_pos_period = $DB->get_record_sql($q);
$mform = new org_pos_period_edit_form();
if ($data = $mform->get_data()){
$data->id = $id;
$data->modified_by = $USER->id;
$data->date_modified = date('Y-m-d H:i:s');
//check for unique
    $is_unique = block_androgogic_training_history_org_pos_unique($data);
    if (!$is_unique) {
        echo $OUTPUT->notification(get_string('orgposcombonotunique', 'block_androgogic_training_history'), 'notifyfailure');
    } else {
        $DB->update_record('androgogic_org_pos_periods',$data);
        block_androgogic_training_history_cpd_targets_update($data);
        block_androgogic_training_history_cpe_targets_update($data);
        echo $OUTPUT->notification(get_string('datasubmitted','block_androgogic_training_history'), 'notifysuccess');
    }

}
else{
echo $OUTPUT->heading(get_string('org_pos_period_edit', 'block_androgogic_training_history'));
$mform->display();
}

?>
