<?php

/**
 * Androgogic Training History Block: CPD/CPE targets Report
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     17/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Report on user's CPD/CPE points and targets
 *
 * */
//params
$sort = optional_param('sort', 'user_id', PARAM_RAW);
$dir = optional_param('dir', 'ASC', PARAM_ALPHA);
$page = optional_param('page', 0, PARAM_INT);
$perpage = optional_param('perpage', 50, PARAM_INT);
$tab = optional_param('tab', 'targets_report', PARAM_TEXT);
$user_search = optional_param('user_search', '', PARAM_TEXT);
$org_id = optional_param('org_id', 0, PARAM_INT);
$dimension_id = optional_param('dimension_id', 0, PARAM_INT);
$competency_id = optional_param('competency_id', 0, PARAM_INT);
$user_id = optional_param('user_id', 0, PARAM_INT);

if (isset($_POST['startdate']['enabled'])) {
    //make it into a unix time
    $startdate = mktime(0, 0, 0, $_POST['startdate']['month'], $_POST['startdate']['day'], $_POST['startdate']['year']) - 1;
} 
if (isset($_POST['enddate']['enabled'])) {
    //make it into a unix time
    $enddate = mktime(0, 0, 0, $_POST['enddate']['month'], $_POST['enddate']['day'], $_POST['enddate']['year']) + 1;
}
// prepare url for paging bar
$PAGE->set_url($PAGE->url, compact('sort', 'dir', 'page', 'perpage', 'tab','androgogic_activities_id','user_id'));

//are we filtering on user?
if (!has_capability('block/androgogic_training_history:admin', $context)) {
    $user_id = $USER->id;
}

// prepare columns for results table
$columns = array();
if ($user_id == 0) {
    $columns[] = "user";
    $columns[] = "username";
}
foreach ($columns as $column) {
    $string[$column] = get_string("$column", 'block_androgogic_training_history');
    if ($sort != $column) {
        $columnicon = '';
        $columndir = 'ASC';
    } else {
        $columndir = $dir == 'ASC' ? 'DESC' : 'ASC';
        $columnicon = $dir == 'ASC' ? 'down' : 'up';
    }
    if ($column != 'details') {
        $$column = "<a href='$PAGE->url&amp;dir=$columndir&amp;sort=$column'>$string[$column]</a>";
    } else {
        $$column = $string[$column];
    }
}
//figure out the and clause from what has been submitted
$and = '';
//$and2 = '';
$and_date1 = '';
$and_date2 = '';

//are we filtering on org?
if ($org_id > 0) {
    $and .= " and o.id = $org_id ";    
}

if ($user_id > 0) {
    $and .= " and u.id = $user_id ";
//    $and2 .= " and mdl_user.id = $user_id ";
}
if ($user_search != '') {
    $and .= " and concat(firstname, ' ', lastname, ' ', username) like '%$user_search%' ";
//    $and2 .= " and concat(firstname, ' ', lastname, ' ', username) like '%$user_search%' ";
}
if (isset($startdate)) {
    $and_date1 .= " and (th.date_issued > $startdate)";
    $and_date2 .= " and (cc.timecompleted > $startdate)";
}
if (isset($enddate)) {
    $and_date1 .= " and (th.date_issued < $enddate)";
    $and_date2 .= " and (cc.timecompleted < $enddate)";
}

//get any users with a primary pos and org (which then means they might have a cpd/cpe target)
$q = "SELECT DISTINCT  pa.userid as user_id,  CONCAT(u.firstname,' ',u.lastname) AS user, username 
FROM mdl_pos_assignment pa
inner join mdl_pos p on pa.positionid = p.id
inner join mdl_org o on pa.organisationid = o.id
inner join mdl_user u on pa.userid = u.id
where 1 = 1
$and
order by $sort $dir";
if (isset($_GET['debug'])) {
    echo '$query : ' . $q . '';
}
//get a page worth of records
$results = $DB->get_records_sql($q, array(), $page * $perpage, $perpage);
if($download == ''){
    //also get the total number we have of these
    $results_for_count = $DB->get_records_sql($q);
    $result_count = count($results_for_count);

    echo $OUTPUT->heading(get_string('targets_report_heading', 'block_androgogic_training_history'));

    if ($user_id > 0) {
        $subject_user = $DB->get_record('user', array('id' => $user_id));
        echo $OUTPUT->heading(fullname($subject_user));
        echo $OUTPUT->heading(fullname($subject_user->idnumber));
    }

    require_once('targets_report_form.php');
    $mform = new targets_report_form(null, array('sort' => $sort, 'dir' => $dir, 'perpage' => $perpage, 'tab' => $currenttab, 'user_id' => $user_id, 'context' => $context));
    $mform->display();

    //flush();
}

//RESULTS
if (!$results) {
    if($download == ''){
        echo $OUTPUT->heading(get_string('noresults', 'block_androgogic_training_history'));
    }
} else {
    $kas = array();
    $augmented_results = array();
    foreach ($results as $result) {
        $and3 = "";
        $and4 = "";
        if ($dimension_id > 0) {
            $and3 .= " and c.id IN ( SELECT id FROM mdl_comp WHERE parentid = $dimension_id )";            
        }
        
        if ($competency_id > 0) {
            $and3 .= " and c.id = $competency_id ";    
            
        }
        //any KAs for this one?
        //moodle insists on treating first column as unique id, hence the rownum stuff here:
        $sql = "SELECT @rownum:=@rownum+1 id, competency_id, fullname, assessed, cpd_points FROM(
            SELECT competency_id, c.fullname, assessed,  sum(cpd_points) as cpd_points
            from mdl_androgogic_training_history_competencies_cpd_points cpd 
            inner join mdl_comp c on cpd.competency_id = c.id 
            inner join mdl_androgogic_training_history th on cpd.training_history_id = th.id
            where user_id = $result->user_id
            $and3
            $and_date1
            group by assessed, competency_id, c.fullname
            union
            SELECT competency_id, c.fullname, 1, sum(cpd_points)
            FROM mdl_course_completions cc
            INNER JOIN mdl_course crs ON cc.course = crs.id
            INNER JOIN mdl_androgogic_training_history_course_competencies_cpd_points cpd on crs.id = cpd.course_id
            inner join mdl_comp c on cpd.competency_id = c.id
            INNER JOIN mdl_user ON cc.userid = mdl_user.id
            WHERE STATUS >= 50
            and cc.userid = $result->user_id
            $and4
            $and_date2
            group by competency_id, c.fullname
            ) b , (SELECT @rownum:=0) r "; //TODO: add the filters in 
        
        $these_cpd_kas = $DB->get_records_sql($sql);
        
        $result->cpd_knowledge_areas = $these_cpd_kas;        
        
        //also we want to get the cpe knowledge areas and their cpe hours
        $sql = "SELECT @rownum:=@rownum+1 id, competency_id, fullname, assessed, cpe_hours FROM(
            SELECT competency_id, c.fullname, assessed,  sum(cpe_hours) as cpe_hours
            from mdl_androgogic_training_history_competencies_cpe_hours cpe 
            inner join mdl_comp c on cpe.competency_id = c.id 
            inner join mdl_androgogic_training_history th on cpe.training_history_id = th.id
            where user_id = $result->user_id
            $and3
            $and_date1
            group by assessed, competency_id, c.fullname
            union
            SELECT competency_id, c.fullname, 1, sum(cpe_hours)
            FROM mdl_course_completions cc
            INNER JOIN mdl_course crs ON cc.course = crs.id
            INNER JOIN mdl_androgogic_training_history_course_competencies_cpe_hours cpe on crs.id = cpe.course_id
            inner join mdl_comp c on cpe.competency_id = c.id
            INNER JOIN mdl_user ON cc.userid = mdl_user.id
            WHERE STATUS >= 50
            $and4
            $and_date2
            and cc.userid = $result->user_id
            group by competency_id, c.fullname
            ) b , (SELECT @rownum:=0) r "; //TODO: add the filters in 
        
        $these_cpe_kas = $DB->get_records_sql($sql);
        
        $result->cpe_knowledge_areas = $these_cpe_kas;
        
        //these will become the basis for the columns a bit later on
        $kas = array_merge($kas,$these_cpd_kas);
        
        //let's get their targets, for which we need the lowest hanging match on the org/pos hierarchies 
        //that matches their primary pos assignment
        $this_user = new stdClass();
        $this_user->id = $result->user_id;
        $user_org_pos = block_androgogic_training_history_get_user_org_pos($this_user);
        if($user_org_pos){
            $org_pos_period = block_androgogic_training_history_get_org_pos_period(
                    $user_org_pos->org_framework_id,$user_org_pos->pos_framework_id,
                    $user_org_pos->pos_sortthread,$user_org_pos->org_sortthread);
            if($org_pos_period){
            list($period_start_date,$period_end_date) = block_androgogic_training_history_get_cpd_period($this_user, $org_pos_period);
            $result->period_start_date = $period_start_date;
            $result->period_end_date = $period_end_date;
            
            //let's get their targets
            $targets = block_androgogic_training_history_get_targets($this_user,$org_pos_period,$and3,$period_start_date,$period_end_date);
            
            $result->cpd_targets = $targets->cpd_targets;           
            $result->cpe_targets = $targets->cpe_targets;            
            $result->total_cpe_target = $targets->total_cpe_target;
            $result->total_cpd_target = $targets->total_cpd_target;
            }
        }
        $augmented_results[] = $result;
    }
    //need to get a unique set of ka ids from the set now, so as to add col headers
    $unique_kas = array();
    foreach($kas as $ka){
        if(!in_array($ka->competency_id, array_keys($unique_kas))){
            $unique_kas[$ka->competency_id] = $ka->fullname;
        }
    }
    //add in the kas from the targets as well
    foreach($result->cpd_targets as $cpd_target){
        if(!in_array($cpd_target->competency_id, array_keys($unique_kas))){
            $unique_kas[$cpd_target->competency_id] = $cpd_target->fullname;
        }
    }

    $table = new html_table();
    
    $table->head = array();
    if ($user_id == 0) {
        $table->head[] = $user;
        $table->head[] = $username;
    } 
    //totals
    $table->head[] = get_string('cpd_target_total','block_androgogic_training_history');
    $table->head[] = get_string('completed_cpd_total','block_androgogic_training_history');
    $table->head[] = get_string('over_under','block_androgogic_training_history');
    $table->head[] = get_string('cpe_target_total','block_androgogic_training_history');
    $table->head[] = get_string('completed_cpe_total','block_androgogic_training_history');
    $table->head[] = get_string('over_under','block_androgogic_training_history');
    
    if($user_id != 0){
        $ka_table = new html_table();
        $ka_table->head[] = get_string('competency','block_androgogic_training_history');
        $ka_table->head[] = get_string('target_points','block_androgogic_training_history');
        $ka_table->head[] = get_string('completed_assessed_points','block_androgogic_training_history');
        $ka_table->head[] = get_string('completed_unassessed_points','block_androgogic_training_history');
        $ka_table->head[] = get_string('total_completed_points','block_androgogic_training_history');
        $ka_table->head[] = get_string('over_under','block_androgogic_training_history');
        $ka_table->head[] = get_string('percent_target_assessed','block_androgogic_training_history');
    }
    else{
        foreach($unique_kas as $unique_ka_key=>$unique_ka_value){
            $table->head[] = get_string('competency','block_androgogic_training_history');
            $table->head[] = get_string('target_points','block_androgogic_training_history');
            $table->head[] = get_string('completed_assessed_points','block_androgogic_training_history');
            $table->head[] = get_string('completed_unassessed_points','block_androgogic_training_history');
            $table->head[] = get_string('total_completed_points','block_androgogic_training_history');
            $table->head[] = get_string('over_under','block_androgogic_training_history');
            $table->head[] = get_string('percent_target_assessed','block_androgogic_training_history');
        }
    }
    
    $total_points = 0;
    foreach ($augmented_results as $result) {
        //init totals values
        $cpd_target_total = 0;
        $completed_cpd_total = 0;
        $over_under_cpd_totals = 0;
        $cpe_target_total = 0;
        $completed_cpe_total = 0;
        $over_under_cpe_totals = 0;
        $td_array = array(); // this one will hold the whole row, but we have to add to it in a particular order: userdetails, then totals, then ka details
        $ka_array = array(); // this one is going to hold the series of ka values (which are repeated within the row)
        if ($user_id == 0) {
            $td_array[] = html_writer::link($PAGE->url . '&user_id=' . $result->user_id, $result->user);
            $td_array[] = $result->username;
        }
        
        foreach($unique_kas as $unique_ka_key=>$unique_ka_value){
            //init
            $target_points = 0;
            $completed_assessed_points = 0;
            $completed_unassessed_points = 0;
            $total_completed_points = 0;
            $over_under = 0;
            $percent_target_assessed = 0;
            //has the user got any values for this one?
            foreach($result->cpd_knowledge_areas as $user_knowledge_area){
                if($user_knowledge_area->competency_id == $unique_ka_key){
                    if($user_knowledge_area->assessed == 1){
                        $completed_assessed_points += $user_knowledge_area->cpd_points;
                    }
                    else{
                        $completed_unassessed_points += $user_knowledge_area->cpd_points;
                    }
                    $total_completed_points += $user_knowledge_area->cpd_points;                    
                }
            }
            if(isset($result->cpd_targets)){
            foreach($result->cpd_targets as $user_target){
                if($user_target->competency_id == $unique_ka_key){
                    $target_points += $user_target->cpd_target;
                }
            }
            }
            if($target_points > 0 and $total_completed_points > 0){
                $percent_target_assessed = $total_completed_points*100/$target_points;
                if($percent_target_assessed > 100){$percent_target_assessed = 100;}                
            }
            else if ($target_points == 0){
                $percent_target_assessed = "N/A";
            }
            $over_under = $total_completed_points - $target_points;            
            
            $ka_array[] = $unique_ka_value;
            $ka_array[] = $target_points;
            $ka_array[] = $completed_assessed_points;
            $ka_array[] = $completed_unassessed_points;
            $ka_array[] = $total_completed_points;
            $ka_array[] = $over_under;
            if ($percent_target_assessed != 'N/A'){
                $ka_array[] = number_format($percent_target_assessed,2);
            }
            else{
                $ka_array[] = $percent_target_assessed;
            }
            if($user_id != 0){
                $ka_table->data[] = $ka_array;
                //reset it
                $ka_array = array();
            }
            
            // add to the totals...
            //$cpd_target_total += $target_points;
            $completed_cpd_total += $total_completed_points;
            $over_under_cpd_totals += $over_under;
            
        }
        $cpe_target_total = $result->total_cpe_target;
        $cpd_target_total = $result->total_cpd_target;
        
        $completed_cpe_total = 0;
        $over_under_hours = 0;
        
        foreach($result->cpe_knowledge_areas as $user_knowledge_area){
            if(isset($user_knowledge_area->cpe_hours)){
                $completed_cpe_total += $user_knowledge_area->cpe_hours;
            }
        }        
        
        $over_under_cpe_totals = $completed_cpe_total - $cpe_target_total;
        $over_under_cpd_totals = $completed_cpd_total - $cpd_target_total;
        
        //totals
        $td_array[] = number_format($cpd_target_total,2);
        $td_array[] = $completed_cpd_total;
        $td_array[] = number_format($over_under_cpd_totals,2);
        $td_array[] = number_format($cpe_target_total,2);
        $td_array[] = $completed_cpe_total;
        $td_array[] = number_format($over_under_cpe_totals,2);
        if($user_id == 0){
            foreach($ka_array as $ka_item){
                $td_array[] = $ka_item;
            }
        }
        $table->data[] = $td_array;
        
    }

}
if($download != ''){
    //export the table to whatever they asked for
    if($user_id != 0){
        block_androgogic_training_history_export_data($download,$table,$tab,$report_name_lang,$ka_table);
    }
    else{
        block_androgogic_training_history_export_data($download,$table,$tab,$report_name_lang);
    }
}
elseif (!empty($table)) {
    echo html_writer::table($table);
    if($user_id != 0){
        echo html_writer::table($ka_table);
    }
    $pagingbar = new paging_bar($result_count, $page, $perpage, $PAGE->url);
    $pagingbar->pagevar = 'page';
    echo $OUTPUT->render($pagingbar);
    block_androgogic_training_history_output_download_links($PAGE->url,'targets_report_heading');
}
?>
