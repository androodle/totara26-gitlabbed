<?php

/** 
 * Androgogic User period Block: Create object
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     01/08/2014
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Create new user_period
 *
 **/

global $OUTPUT;
require_capability('block/androgogic_training_history:admin', $context);
require_once('user_period_edit_form.php');
$mform = new user_period_edit_form();
if ($data = $mform->get_data() and $mform->is_validated()){
$data->created_by = $USER->id;
$data->date_created = date('Y-m-d H:i:s');
$data->period_id = block_androgogic_training_history_get_period_id($data);
$newid = $DB->insert_record('androgogic_user_period',$data);
echo $OUTPUT->notification(get_string('datasubmitted','block_androgogic_training_history'), 'notifysuccess');
echo $OUTPUT->action_link($PAGE->url, 'Create another item');
}
else{
echo $OUTPUT->heading(get_string('user_period_new', 'block_androgogic_training_history'));
$mform->display();
}

?>
