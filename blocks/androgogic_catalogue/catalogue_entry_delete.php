<?php

/** 
 * Androgogic Catalogue Block: Delete object
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     15/05/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Delete one of the catalogue_entries
 *
 **/

$id = required_param('id', PARAM_INT);
$DB->delete_records('androgogic_catalogue_entries',array('id'=>$id));
$DB->delete_records('androgogic_catalogue_entry_locations',array('catalogue_entry_id'=>$id));
$DB->delete_records('androgogic_catalogue_entry_courses',array('catalogue_entry_id'=>$id));
$DB->delete_records('androgogic_catalogue_entry_programs',array('catalogue_entry_id'=>$id));
$DB->delete_records('androgogic_catalogue_entry_organisations',array('catalogue_entry_id'=>$id));
$DB->delete_records('androgogic_catalogue_entry_positions',array('catalogue_entry_id'=>$id));
$DB->delete_records('androgogic_catalogue_entry_competencies',array('catalogue_entry_id'=>$id));
echo $OUTPUT->notification(get_string('itemdeleted','block_androgogic_catalogue'), 'notifysuccess');

?>
