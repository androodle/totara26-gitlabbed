<?php

/**
 * Androgogic Catalogue Block: Search
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     15/05/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Search catalogue_entries
 * Also provides access to edit and delete functions if user has sufficient permissions
 *
 * */
//params
$sort = optional_param('sort', 'name', PARAM_RAW);
$dir = optional_param('dir', 'ASC', PARAM_ALPHA);
$page = optional_param('page', 0, PARAM_INT);
$perpage = optional_param('perpage', 20, PARAM_INT);
$search = optional_param('search', '', PARAM_TEXT);
$tab = optional_param('tab', '$catalogue_entry_search', PARAM_TEXT);
$androgogic_catalogue_locations_id = optional_param('androgogic_catalogue_locations_id', 0, PARAM_INT);
$comp_id = optional_param('comp_id', 0, PARAM_INT);
$debug = optional_param('debug', 0, PARAM_INT);
if (isset($_POST['startdate']['enabled'])) {
    //make it into a unix time
    $startdate = mktime(0, 0, 0, $_POST['startdate']['month'], $_POST['startdate']['day'], $_POST['startdate']['year']) - 1;
}
if (isset($_POST['enddate']['enabled'])) {
    //make it into a unix time
    $enddate = mktime(0, 0, 0, $_POST['enddate']['month'], $_POST['enddate']['day'], $_POST['enddate']['year']) + 1;
}
// prepare url for paging bar
$PAGE->set_url($PAGE->url, compact('sort', 'dir', 'page', 'perpage', 'search', 'tab'));
//figure out the and clause from what has been submitted
$and = '';
if ($search != '') {
    $and .= " and concat(
a.name,
a.description
) like '%$search%'";
}
//are we filtering on androgogic_catalogue_locations?
if ($androgogic_catalogue_locations_id > 0) {
    $and .= " and mdl_androgogic_catalogue_locations.id = $androgogic_catalogue_locations_id ";
}
//are we filtering on comp?
if ($comp_id > 0) {
    $and .= " and mdl_comp.id = $comp_id ";
}
if (isset($startdate)) {
    $and .= " and (mdl_course.startdate > $startdate or mdl_prog.availablefrom > $startdate)";
}
if (isset($enddate)) {
    $and .= " and (mdl_course.startdate < $enddate or mdl_prog.availablefrom < $enddate)";
}

$is_logged_in = true;
// if they are not logged in, they don't get to see non-public entries, and they can't have the pos or org logic applied, so don't show them these
if (!isset($USER) || $USER->id == 0) {
    $and .= " AND a.public = 1 and mdl_org.id is null and mdl_pos.id is null";
    $is_logged_in = false;
}
else {
    $user_pos_org_data = get_user_pos_org_data();
}
$joined_tables = "from mdl_androgogic_catalogue_entries a 
LEFT JOIN mdl_androgogic_catalogue_entry_locations on a.id = mdl_androgogic_catalogue_entry_locations.catalogue_entry_id
LEFT JOIN mdl_androgogic_catalogue_locations on mdl_androgogic_catalogue_entry_locations.location_id = mdl_androgogic_catalogue_locations.id
LEFT JOIN mdl_androgogic_catalogue_entry_courses on a.id = mdl_androgogic_catalogue_entry_courses.catalogue_entry_id
LEFT JOIN mdl_course on mdl_androgogic_catalogue_entry_courses.course_id = mdl_course.id
LEFT JOIN mdl_androgogic_catalogue_entry_programs on a.id = mdl_androgogic_catalogue_entry_programs.catalogue_entry_id
LEFT JOIN mdl_prog on mdl_androgogic_catalogue_entry_programs.program_id = mdl_prog.id
LEFT JOIN mdl_androgogic_catalogue_entry_organisations on a.id = mdl_androgogic_catalogue_entry_organisations.catalogue_entry_id
LEFT JOIN mdl_org on mdl_androgogic_catalogue_entry_organisations.organisation_id = mdl_org.id
LEFT JOIN mdl_androgogic_catalogue_entry_positions on a.id = mdl_androgogic_catalogue_entry_positions.catalogue_entry_id
LEFT JOIN mdl_pos on mdl_androgogic_catalogue_entry_positions.position_id = mdl_pos.id
LEFT JOIN mdl_androgogic_catalogue_entry_competencies on a.id = mdl_androgogic_catalogue_entry_competencies.catalogue_entry_id
LEFT JOIN mdl_comp on mdl_androgogic_catalogue_entry_competencies.competency_id = mdl_comp.id";

$q = "select DISTINCT a.*  
$joined_tables
where 1 = 1 
$and 
order by $sort $dir";
if ($debug == 1) {
    echo '$query : ' . $q . '<br>';
}
//get a page worth of records
$results = $DB->get_records_sql($q, array(), $page * $perpage, $perpage);
//also get the total number we have of these
$q = "SELECT COUNT(DISTINCT a.id)
$joined_tables
where 1 = 1  
$and";

if ($debug == 1) {
    echo '$query : ' . $q . '<br>';
}

$result_count = $DB->get_field_sql($q);
require_once('catalogue_entry_search_form.php');
$mform = new catalogue_entry_search_form(null, array('sort' => $sort, 'dir' => $dir, 'perpage' => $perpage, 'search' => $search, 'tab' => $currenttab, 'androgogic_catalogue_locations_id' => $androgogic_catalogue_locations_id, 'comp_id' => $comp_id));
$mform->display();
//only show the result count if they have edit - if not the results may be filtered by org/pos
if (has_capability('block/androgogic_catalogue:edit', $context)) {
    echo '<table width="100%"><tr><td width="50%">';
    echo $result_count . ' ' . get_string('catalogue_entry_plural', 'block_androgogic_catalogue') . " found" . '<br>';
    echo '</td><td style="text-align:right;">';
    echo "<a href='index.php?tab=catalogue_entry_new'>" . get_string('catalogue_entry_new', 'block_androgogic_catalogue') . "</a>";
    echo '</td></tr></table>';
}
flush();

//RESULTS
if (!$results) {
    $match = array();
    echo $OUTPUT->heading(get_string('noresults', 'block_androgogic_catalogue', $search));
} else {
    echo '<hr>';
    foreach ($results as $result) {
        
        $edit_link = "";
        $delete_link = "";
        if(has_capability('block/androgogic_catalogue:edit', $context) || has_capability('block/androgogic_catalogue:delete', $context)){
            if (has_capability('block/androgogic_catalogue:edit', $context)) {
    // then we show the edit link
                $edit_link = "<a href='index.php?tab=catalogue_entry_edit&id=$result->id'>Edit</a> ";
            }
            if (has_capability('block/androgogic_catalogue:delete', $context)) {
    // then we show the delete link
                $delete_link = "<a href='index.php?tab=catalogue_entry_delete&id=$result->id' onclick='return confirm(\"Are you sure you want to delete this item?\")'>Delete</a> ";
            }
        }
        elseif($is_logged_in){
            //we may need to exclude the user from seeing the entry, based on org or pos
            if(!user_can_see_catalogue_entry($result,$user_pos_org_data)){
                 if ($debug == 1) {
                    echo 'this cat entry can\'t be seen by the user due to org/pos config: '.$result->name . '<br/>';
                }
                continue;
            }
        }
        echo '<h3>' . $result->name . '</h3>';
        if($edit_link != "" || $delete_link != ""){
            echo $edit_link . $delete_link . '<br>';
        }
//        $result->description = trim(preg_replace('/\s+/', ' ', $result->description));
//        $result->description = addslashes($result->description);
        echo format_text($result->description,FORMAT_HTML) . '<br/>';
//get any courses that are linked to the cat entry
        $sql = "SELECT mdl_course.* 
    FROM mdl_androgogic_catalogue_entries a
    LEFT JOIN mdl_androgogic_catalogue_entry_courses on a.id = mdl_androgogic_catalogue_entry_courses.catalogue_entry_id
    LEFT JOIN mdl_course on mdl_androgogic_catalogue_entry_courses.course_id = mdl_course.id 
    WHERE a.id = {$result->id}";
        $courses = $DB->get_records_sql($sql);
        if (!empty($courses)) {            
            $ctr = 0;
            foreach ($courses as $course) {
                if($ctr > 0){ echo '<br>';}
                    // Redirect course with learning plans to an intermediate page that allows the user to set up or select a plan to use.
                    $enrol_methods = $DB->get_fieldset_select('enrol', 'enrol', 'courseid = ?', array('courseid' => $course->id));
                    $has_self_enrolment = in_array('self', $enrol_methods);
                    $has_lp_enrolment = in_array('totara_learningplan', $enrol_methods);

                    if ($has_lp_enrolment & !$has_self_enrolment) {
                        echo '<a href="' . $CFG->wwwroot . "/blocks/androgogic_catalogue/add_to_learningplan.php?courseid=" . $course->id . '">' . $course->fullname . '</a>';
                    } else {
                        echo '<a href="' . $CFG->wwwroot . "/course/view.php?id=" . $course->id . '">' . $course->fullname . '</a>';
                    }
                
                $ctr++;
            }
        }
//get any progs that are linked to the cat entry
        $sql = "SELECT mdl_prog.* 
    FROM mdl_androgogic_catalogue_entries a
    LEFT JOIN mdl_androgogic_catalogue_entry_programs on a.id = mdl_androgogic_catalogue_entry_programs.catalogue_entry_id
    LEFT JOIN mdl_prog on mdl_androgogic_catalogue_entry_programs.program_id = mdl_prog.id 
    WHERE a.id = {$result->id}";
        $programs = $DB->get_records_sql($sql);
        if (!empty($programs)) {
            $ctr = 0;
            foreach ($programs as $program) {
                if($ctr > 0){ echo '<br>';}
                echo '<br><a href="' . $CFG->wwwroot . "/totara/program/view.php?id=" . $program->id . '">' . $program->fullname . '</a>';
                $ctr++;
            }
        }
        echo '<hr>';
        $pagingbar = new paging_bar($result_count, $page, $perpage, $PAGE->url);
        $pagingbar->pagevar = 'page';
        echo $OUTPUT->render($pagingbar);
    }
}
?>
