<?php

/** 
 * Androgogic Catalogue Block: Search
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     13/05/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Search locations
 * Provides access to edit and delete functions based on permissions of user
 *
 **/

//params
$sort   = optional_param('sort', 'name', PARAM_RAW);
$dir    = optional_param('dir', 'ASC', PARAM_ALPHA);
$page   = optional_param('page', 0, PARAM_INT);
$perpage= optional_param('perpage', 20, PARAM_INT); 
$search= optional_param('search', '', PARAM_TEXT);
$tab= optional_param('tab', '$location_search', PARAM_TEXT);
// prepare url for paging bar
$PAGE->set_url($PAGE->url, compact('sort', 'dir', 'page','perpage','search','tab')); 
$columns = array(
"name",
);
foreach ($columns as $column) {
$string[$column] = get_string("$column",'block_androgogic_catalogue');
if ($sort != $column) {
$columnicon = '';
$columndir = 'ASC';
} else {
$columndir = $dir == 'ASC' ? 'DESC':'ASC';
$columnicon = $dir == 'ASC' ? 'down':'up';
}
if($column != 'details'){
$$column = "<a href='$PAGE->url&amp;dir=$columndir&amp;sort=$column'>$string[$column]</a>";
}
else{
 $$column = $string[$column];
}
}
$and = '';
if($search != ''){
$and .= " and name like '%$search%'";
} 
$q = "select a.*  
from mdl_androgogic_catalogue_locations a 
where 1 = 1 
$and 
order by $sort $dir";
if(isset($_GET['debug'])){echo '$query : ' . $q . ''   ;}
//get a page worth of records
$results = $DB->get_records_sql($q,array(),$page*$perpage, $perpage);
//also get the total number we have of these
$q = "select count(*) from mdl_androgogic_catalogue_locations a
 where 1 = 1  $and";

if(isset($_GET['debug'])){echo '$query : ' . $q . '<br>'   ;}

$result_count = $DB->get_field_sql($q);

echo $OUTPUT->heading(get_string('location_search', 'block_androgogic_catalogue'));

$OUTPUT->heading("$result_count ".get_string('result_count','block_androgogic_catalogue'));
echo '<div >';
echo '<form action="" method="post">';
echo '<table cellpadding="1">';
echo '<tr><td>';
echo get_string('location_search_instructions', 'block_androgogic_catalogue'); 
echo '</td><td>';
echo '<input type="text" name="search" value="'.$search.'">';
echo '</td><td>';
echo '<input type="submit" value="submit">';
echo '</td><td width="50px">';
echo '&nbsp;';
echo '</td><td>';
echo "<a href='index.php?tab=location_new'>" . get_string('location_new','block_androgogic_catalogue') . "</a>";
echo '</td></tr>';
echo '</table>';
echo '<input type="hidden" name="tab" value="'.$currenttab.'">';
echo '<input type="hidden" name="sort" value="'.$sort.'">';
echo '<input type="hidden" name="dir" value="'.$dir.'">';
echo '<input type="hidden" name="perpage" value="'.$perpage.'">';
echo $result_count . ' ' . get_string('location_plural','block_androgogic_catalogue') . " found" . '<br>';
echo '</form>';
echo '</div>';
flush();
if (!$results) {
$match = array();
$OUTPUT->heading(get_string('noresults','block_androgogic_catalogue',$search));
$table = NULL;
} else {
$table = new html_table();
$table->head = array (
$name,
);
$table->align = array ("left","left","left","left","left","left",);
$table->width = "95%";
$table->size = array("17%","17%","17%","17%","17%","17%",);
foreach ($results as $result) {
$view_link = '';//enable if wanted: "<a href='index.php?tab=location_view&id=$result->id'>View</a> "; 
$edit_link = "";
$delete_link = "";
if(has_capability('block/androgogic_catalogue:edit', $context)){
// then we show the edit link
$edit_link = "<a href='index.php?tab=location_edit&id=$result->id'>Edit</a> ";
}
if(has_capability('block/androgogic_catalogue:delete', $context)){
// then we show the delete link
$delete_link = "<a href='index.php?tab=location_delete&id=$result->id' onclick='return confirm(\"Are you sure you want to delete this item?\")'>Delete</a> ";
}
$table->data[] = array (
"$result->name",
$view_link . $edit_link . $delete_link
);
}
}
if (!empty($table)) {
echo html_writer::table($table);
$pagingbar = new paging_bar($result_count, $page, $perpage,$PAGE->url);
$pagingbar->pagevar = 'page';
echo $OUTPUT->render($pagingbar);
}

?>
