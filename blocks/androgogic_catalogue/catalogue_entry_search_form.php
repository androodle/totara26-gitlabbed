<?php

/** 
 * Androgogic Catalogue Block: Search form
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     15/05/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Provides search form for the object.
 * This is used by search page
 *
 **/

if (!defined('MOODLE_INTERNAL')) {
die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}
require_once($CFG->libdir . '/formslib.php');
class catalogue_entry_search_form extends moodleform {
function definition() {
global $DB;
$mform =& $this->_form;
foreach($this->_customdata as $custom_key=>$custom_value){
$$custom_key = $custom_value;
}
$debug = optional_param('debug', 0, PARAM_INT);
$mform->addElement('html','<div>');
//search controls
$mform->addElement('text','search',get_string('catalogue_entry_search_instructions', 'block_androgogic_catalogue'));
$dboptions = $DB->get_records_menu('androgogic_catalogue_locations',array(),'name','id,name');
unset($options);
$options[0] = 'Any';
foreach($dboptions as $key=>$value){
$options[$key] = $value;
}
$select = $mform->addElement('select', 'androgogic_catalogue_locations_id', get_string('location','block_androgogic_catalogue'), $options);
$dboptions = $DB->get_records_menu('comp',array(),'fullname','id,fullname');
unset($options);
$options[0] = 'Any';
foreach($dboptions as $key=>$value){
$options[$key] = $value;
}
$select = $mform->addElement('select', 'comp_id', get_string('competency','block_androgogic_catalogue'), $options);
$mform->addElement('date_selector','startdate',get_string('startdate', 'block_androgogic_catalogue'), array('optional'=>true));
$mform->addElement('date_selector','enddate',get_string('enddate', 'block_androgogic_catalogue'), array('optional'=>true));

//hiddens
$mform->addElement('hidden','tab',$tab);
$mform->addElement('hidden','sort',$sort);
$mform->addElement('hidden','dir',$dir);
$mform->addElement('hidden','perpage',$perpage);
$mform->addElement('hidden','debug',$debug);
//button
$mform->addElement('submit','submit','Search');
$mform->addElement('html','</div>');
}
}
