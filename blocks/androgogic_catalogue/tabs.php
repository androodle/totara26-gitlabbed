<?php
/** 
 * Androgogic Catalogue Block: Tabs
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     13/05/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 **/

// This file to be included so we can assume config.php has already been included.
if (empty($currenttab)) {
error('You cannot call this script in that way');
}
$tabs = array();
$row = array();
$row[] = new tabobject('catalogue_entry', $CFG->wwwroot.'/blocks/androgogic_catalogue/index.php?tab=catalogue_entry_search', get_string('catalogue_entry_search','block_androgogic_catalogue'));
if(has_capability('block/androgogic_catalogue:edit', $context)){
    $row[] = new tabobject('location', $CFG->wwwroot.'/blocks/androgogic_catalogue/index.php?tab=location_search', get_string('location_search','block_androgogic_catalogue'));
}
$tabs[] = $row;
// Print out the tabs and continue!
print_tabs($tabs, $currenttab);

// End of blocks/androgogic_catalogue/tabs.php