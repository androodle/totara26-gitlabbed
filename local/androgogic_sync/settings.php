<?php
/** 
 * Androgogic Sync
 *
 * @author      Keith Buss <kbuss@outlook.com>
 * @version     May 2015
 *
 **/

defined('MOODLE_INTERNAL') || die(); 

if ($hassiteconfig) { // needs this condition or there is error on login page   

	$ADMIN->add('root', new admin_category('androgogic_sync', get_string('pluginname', 'local_androgogic_sync')));

	$ADMIN->add("androgogic_sync", new admin_externalpage('managesources', 
			get_string('managesources', 'local_androgogic_sync'), 
			new moodle_url('/local/androgogic_sync/sources.php'),
			'local/androgogic_sync:managesources'));

	$ADMIN->add('androgogic_sync', new admin_externalpage('runsync',
			get_string('runsync', 'local_androgogic_sync'),
			new moodle_url('/local/androgogic_sync/runsync.php'),
			'local/androgogic_sync:runsync'));

	$ADMIN->add('androgogic_sync', new admin_externalpage('synclog',
			get_string('synclog', 'local_androgogic_sync'),
			new moodle_url('/local/androgogic_sync/synclog.php'),
			'local/androgogic_sync:synclog'));
			
    $settings = new admin_settingpage('local_androgogic_sync', get_string('generalsettings', 'local_androgogic_sync'), 'local/androgogic_sync:settings');
    
    $settings->add(new admin_setting_configtext('local_androgogic_sync/cron_hours', get_string('cronhours', 'local_androgogic_sync'), 
        get_string('cronhours_help', 'local_androgogic_sync'), '', PARAM_RAW));

    $settings->add(new admin_setting_configtext('local_androgogic_sync/staging_retention', get_string('stagingretention', 'local_androgogic_sync'), 
    	get_string('stagingretention_help', 'local_androgogic_sync'), 180, PARAM_INT)); 
           
	// notifications         
    $settings->add(new admin_setting_heading('notifications', get_string('notifications', 'local_androgogic_sync'),''));
    	     	  
    $settings->add(new admin_setting_configtext('local_androgogic_sync/notify_emailto', get_string('notifyemailto', 'local_androgogic_sync'), 
    	get_string('notifyemailto_help', 'local_androgogic_sync'), '', PARAM_EMAIL, 60));
    	
    $settings->add(new admin_setting_configcheckbox('local_androgogic_sync/notify_info', get_string('notifyinfo', 'local_androgogic_sync'),
        '', 0));    
    $settings->add(new admin_setting_configcheckbox('local_androgogic_sync/notify_warning', get_string('notifywarning', 'local_androgogic_sync'),
        '', 0));
    $settings->add(new admin_setting_configcheckbox('local_androgogic_sync/notify_error', get_string('notifyerror', 'local_androgogic_sync'),
        '', 0));    
         
    $ADMIN->add('androgogic_sync', $settings);
}
