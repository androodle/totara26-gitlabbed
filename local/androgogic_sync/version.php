<?php
/** 
 * Androgogic Sync
 *
 * @author      Keith Buss <kbuss@outlook.com>
 * @version     May 2015
 *
 **/

$plugin->version = 2015062300;					// The current plugin version (Date: YYYYMMDDXX).
$plugin->requires = 2010112400; 				// Requires this Moodle version.
$plugin->component = 'local_androgogic_sync';	// Full name of the plugin (used for diagnostics)
