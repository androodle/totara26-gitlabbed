<?php
/**
 * Androgogic Sync
 *
 * @author      Keith Buss <kbuss@outlook.com>
 * @version     May 2015
 *
 **/

require_once(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->libdir.'/adminlib.php');
require_once('classes/editsourceform.class.php');

require_login();

$context = context_system::instance();
require_capability('local/androgogic_sync:managesources', $context);

// Get params
$id = optional_param('id', 0, PARAM_INT);    // 0 if creating adding a new source 
$source = optional_param('source', '', PARAM_ALPHA);  
$element = optional_param('element', '', PARAM_ALPHA); 

$heading = get_string($id==0 ? 'add' : 'edit')." $source $element source settings";
$url_params = array('id'=>$id, 'source'=>$source, 'element'=>$element);
$PAGE->set_url(new moodle_url('/local/androgogic_sync/editsource.php', $url_params));
$PAGE->set_context($context);
$PAGE->set_pagelayout('admin');
$PAGE->set_title($heading);

// create form
$mform = new editsourceform(null, $url_params);
if ($id <> 0) {
    // Editing existing source
	if (!$row = $DB->get_record('androgogic_sync_source', array('id'=>$id))) {
		throw new Exception($DB->get_last_error());	
	}
	
	// load field mappings
	if ($fields = $DB->get_records('androgogic_sync_field', array('sourceid'=>$id))) {
		foreach ($fields as $field) {
			if (substr($field->dbfieldname, 0, 13) == 'profilefield_') {
				$elementname = $field->dbfieldname;
			} else {
				$elementname = 'field_'.$field->dbfieldname;
			}
			$row->$elementname = $field->csvcolumnno;
		}
	}
	$mform->set_data($row);
}

// cancelled
if ($mform->is_cancelled()) {

    redirect('sources.php');

// Update data
} else if ($new = $mform->get_data()) {

    $notification = new stdClass();

    // Save file settings
    $new->timemodified = time();
    $new->usermodified = $USER->id;
    
    if ($new->id == 0) {
    
        // New record
        unset($new->id);

        $new->timecreated = $new->timemodified;
		$new->visible = 1;

    	$max = $DB->get_field_sql("SELECT MAX(sortorder) AS max FROM {androgogic_sync_source} WHERE deleted=0");
    	$new->sortorder = $max + 1;
    	
        if (!$new->id = $DB->insert_record('androgogic_sync_source', $new)) {
        	throw new Exception($DB->get_last_error());	
        }

        // Log
        //add_to_log(SITEID, $prefix, 'framework create', "index.php?prefix=$prefix&amp;frameworkid={$new->id}", "$new->fullname (ID $new->id)");
        $notification->text = "Added source";

    } else {
    
        // Existing record
        if (!$DB->update_record('androgogic_sync_source', $new)) {
        	throw new Exception($DB->get_last_error());	
        }

        // Log
       // add_to_log(SITEID, $prefix, 'framework update', "framework/view.php?prefix=$prefix&amp;frameworkid={$new->id}", "$row->fullname (ID $row->id)");
        $notification->text = "Updated source";
    }
    
    // save field mapping
	$DB->delete_records('androgogic_sync_field', array('sourceid'=>$new->id));
	$child = new stdClass();
	$child->sourceid = $new->id;
	foreach ($new as $name=>$value) {
		// do not save unmapped fields
		if (!empty($value)) {
			// identify field names
			$child->dbfieldname = '';
			if (substr($name, 0, 6) == 'field_') {
				$child->dbfieldname = substr($name, 6);
			} else if (substr($name, 0, 13) == 'profilefield_') {
				$child->dbfieldname = $name;
			}
			
			if (!empty($child->dbfieldname)) {
				$child->csvcolumnno = $value;
				if (!$DB->insert_record('androgogic_sync_field', $child)) {
        			throw new Exception($DB->get_last_error());	
				}
			}
		}
	}
	
    totara_set_notification($notification->text, 'sources.php', array('class'=>'notifysuccess'));
}

///
/// Display page
///
$PAGE->navbar->add(get_string('managesources', 'local_androgogic_sync'), new moodle_url('/local/androgogic_sync/sources.php'));
$PAGE->navbar->add($heading);

echo $OUTPUT->header();
echo $OUTPUT->heading($heading);

$mform->display();

echo $OUTPUT->footer();
