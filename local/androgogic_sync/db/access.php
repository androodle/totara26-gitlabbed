<?php
/** 
 * Androgogic Sync
 *
 * @author      Keith Buss <kbuss@outlook.com>
 * @version     May 2015
 *
 **/

defined('MOODLE_INTERNAL') || die();

$capabilities = array(
   'local/androgogic_sync:settings'=>array(
	    'captype'=>'write',
		'contextlevel'=>CONTEXT_SYSTEM,
		'archetypes'=>array('manager'=>CAP_ALLOW)
	),

   'local/androgogic_sync:managesources'=>array(
	    'captype'=>'write',
		'contextlevel'=>CONTEXT_SYSTEM,
		'archetypes'=>array('manager'=>CAP_ALLOW)
	),
	
   'local/androgogic_sync:synclog'=>array(
	    'captype'=>'read',
		'contextlevel'=>CONTEXT_SYSTEM,
		'archetypes'=>array('manager'=>CAP_ALLOW)
	),
	
	'local/androgogic_sync:runsync'=>array(
		'captype'=>'write',
		'contextlevel'=>CONTEXT_SYSTEM,
		'archetypes'=>array('manager'=>CAP_ALLOW)
	),
);
