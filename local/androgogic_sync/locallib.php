<?php
/**
 * Androgogic Sync
 *
 * @author      Keith Buss <kbuss@outlook.com>
 * @version     May 2015
 *
 **/

defined('MOODLE_INTERNAL') || die();

require_once('classes/usermatch.class.php');
require_once('classes/synclog.class.php');
require_once($CFG->dirroot . '/totara/hierarchy/prefix/position/lib.php');
require_once($CFG->dirroot . '/totara/hierarchy/prefix/organisation/lib.php');
require_once($CFG->dirroot . '/user/lib.php');
require_once($CFG->dirroot . '/user/profile/lib.php');

function androgogic_sync_process_user($log, $source) {
    global $DB;
    
	assert(is_object($log), '$log must be an object');
	assert(is_object($source), '$source must be an object');

    if (empty($source->auth)) {
        throw new Exception('Authentication setting is required');
    }	
    
    if (empty($source->dateformat)) {
        throw new Exception('Date format setting is required');
    }

    $log->count_created = 0;
    $log->count_updated = 0;
    $log->count_deleted = 0;
    $log->count_skipped = 0;
    $log->count_review = 0;
    
    if ($source->allrecords == 1) {
		// Get users to be deleted
		// 1. deleted flag has been set on 
		// 2. obsolete users with no matching sync record
		$sql = "SELECT u.id, u.idnumber, u.username, u.firstname, u.lastname, u.suspended
				  FROM {user} u 
	   LEFT OUTER JOIN {androgogic_sync_user} s ON s.idnumber = u.idnumber AND TRIM(s.idnumber) <> '' 			
				 WHERE runid=$log->runid AND sourceid=$source->id AND processed=0 
				   AND u.deleted = 0 AND TRIM(u.idnumber) <> ''
				   AND (s.deleted = 1 OR s.id IS NULL)";
		$rs = $DB->get_recordset_sql($sql);
		if ($rs->valid()) {
			$hierarchy = new position();
			foreach ($rs as $user) {
				$log->lmsid = $user->id;
				try {
    				if ($source->userdeletion == 1) {
						if (delete_user($user)) {
							$log->add_to_log(SyncLog::TYPE_TRACE, 'user deleted', "$user->firstname $user->lastname, username=$user->username");   
							$log->count_deleted++;
						/*	
							// Attempt to load user's position assignment
							$pa = new position_assignment(array('userid'=>$staging->id, 'type'=>POSITION_TYPE_PRIMARY));
							if (!empty($pa->positionid)) {
								// delete user's position
								if (!$hierarchy->delete_hierarchy_item($pa->positionid)) {
									$log->add_to_log(SyncLog::TYPE_ERROR, "cannot delete position: $pa->fullname (ID $pa->id)");
								} else {
									$log->add_to_log(SyncLog::TYPE_TRACE, 'position deleted', "$pa->fullname (ID $pa->id)");
								}
							}
						*/
						}
					} else if ($user->suspended == 0) {
						$user->suspended = 1;	
						user_update_user($user, false, true);
						$log->add_to_log(SyncLog::TYPE_TRACE, 'user suspended', "$user->firstname $user->lastname, username=$user->username");
						$event = \totara_core\event\user_suspended::create(
							array(
								'objectid'=>$user->id,
								'context'=>context_user::instance($user->id),
								'other'=>array(
									'username'=>$user->username,
								)
							)
						);
						$event->trigger();
						$log->count_updated++;
					}
					
				} catch (Exception $e) {
					$log->add_to_log(SyncLog::TYPE_ERROR, 'cannot delete user', "$user->firstname $user->lastname, username=$user->username");
					throw $e;
				}
			}
		}
		$rs->close();
	}
	
    $rs = $DB->get_recordset_sql("SELECT * FROM {androgogic_sync_user} WHERE runid=$log->runid AND sourceid=$source->id AND processed=0 ORDER BY id");
    if ($rs->valid()) {    	
        foreach ($rs as $staging) {
            $success = false;
            $log->stagingid = $staging->id;
            unset($staging->id); // avoid conflict with other tables

			androgogic_sync_load_staging_user_profile($log->stagingid, $source->dateformat, $staging);
				
			$staging->username = strtolower($staging->username);  // Usernames always lowercase in moodle.

            if (empty($staging->firstname)) {

                $log->count_skipped++;
                $log->add_to_log(SyncLog::TYPE_WARNING, 'user skipped: firstname is required', "idnumber=$staging->idnumber, email=$staging->email");
            
            } else if (empty($staging->lastname)) {

                $log->count_skipped++;
                $log->add_to_log(SyncLog::TYPE_WARNING, 'user skipped: lastname is required', "idnumber=$staging->idnumber, email=$staging->email");
            
            } else if (empty($staging->email)) {

                $log->count_skipped++;
                $log->add_to_log(SyncLog::TYPE_WARNING, 'user skipped: email is required', "$staging->firstname $staging->lastname, idnumber=$staging->idnumber");
            				
            } else if ($staging->username != clean_param($staging->username, PARAM_USERNAME)) {
            
                $log->count_skipped++;
                $log->add_to_log(SyncLog::TYPE_WARNING, 'user skipped: username contains invalid characters', "$staging->firstname $staging->lastname, username=$staging->username");
				
            } else {
            
                // split first name and middle name to improve accuracy of user matching
                $staging->firstname = trim($staging->firstname);
                $pos = strpos($staging->firstname, ' ');
                if ($pos > 0 and empty($staging->middlename)) {
                    $names = explode(' ', $staging->firstname);
                    $staging->firstname = $names[0];
                    $staging->middlename = $names[1];
                }
		
                $staging->orgframeworkid = $source->orgframeworkid;
                $staging->posframeworkid = $source->orgframeworkid;

                // search for existing user
                $UserMatch = new UserMatch();
                $matchcount = $UserMatch->findMatch($staging);
                if ($matchcount == 0) {
                    //
                    // unmatched
                    //
                	$log->add_to_log(SyncLog::TYPE_TRACE, 'user account deduplication found no match');

					if (empty($staging->auth)) {
						$staging->auth = $source->auth;
					}
                    if (empty($staging->username)) {
                        $log->count_skipped++;
                        $log->add_to_log(SyncLog::TYPE_WARNING, 'user skipped: cannot create user, missing username', "$staging->firstname $staging->lastname, email=$staging->email");
                    } else if ($DB->record_exists('user', array('username'=>$staging->username))) {
                        $log->count_skipped++;
                        $log->add_to_log(SyncLog::TYPE_WARNING, "user skipped: cannot create user, username $staging->username already exists", "$staging->firstname $staging->lastname, idnumber=$staging->idnumber");
                    } else {
                        //
                        // create user
                        //
                        
                        // set defaults
                        if (empty($staging->auth)) {
                        	$staging->auth = $source->auth;
                        }
                        $staging->policyagreed = $source->policyagreed;
                        
						$success = androgogic_sync_create_user($log, $source, $staging);
                        if ($success) {                            
							if ($source->forcepasswordchange == 1) {
								$userauth = get_auth_plugin($staging->auth);
								if ($userauth->is_internal() and $userauth->can_change_password()) {
									set_user_preference('auth_forcepasswordchange', 1, $staging->userid);
								}
								unset($userauth);
							}

                            // send new account email to new user 
                            if (!empty($source->senduseremail)) {
                                if (androgogic_sync_send_email($staging, 'new_account_email')) {
                                    $log->add_to_log(SyncLog::TYPE_TRACE, "new account email sent to $staging->email", "$staging->firstname $staging->lastname, username=$staging->username");
                                } else {
                                    $log->add_to_log(SyncLog::TYPE_ERROR, "cannot send new account email to $staging->email", "$staging->firstname $staging->lastname, username=$staging->username");
                                }
                            }
                        }

                    }
                } elseif ($matchcount == 1 and !$UserMatch->isReviewRequired()) {
                    //
                    // matched
                    //
                    $oldusername = $staging->username;
					$staging->userid = $UserMatch->getMatchID(1);
                    $staging->username = $UserMatch->getMatchUsername(1);
                    $log->lmsid = $staging->userid;
                	$log->add_to_log(SyncLog::TYPE_TRACE, 'user account deduplication found 1 complete match: '.$UserMatch->getMatchInfo(1), "username=$staging->username");
					                   
					//
                    // update user
                    //
                    $success = androgogic_sync_update_user($log, $source, $staging); 
                    
                    if ($success and $oldusername <> $staging->username) {
                        
                        $log->add_to_log(SyncLog::TYPE_INFO, "username changed from $oldusername to $staging->username", "$staging->firstname $staging->lastname, username=$staging->username");

                        // send new username email to user
                        if ($source->senduseremail == 1) {
                            if (sf_send_email($staging, 'newusernameemail')) {
                                $log->add_to_log(SyncLog::TYPE_TRACE, "new username email sent to $staging->email", "$staging->firstname $staging->lastname, username=$staging->username");
                            } else {
                                $log->add_to_log(SyncLog::TYPE_ERROR, "cannot send new username email to $staging->email", "$staging->firstname $staging->lastname, username=$staging->username");
                            }
                        }
                    }
                    
                } elseif ($matchcount == 1 and $UserMatch->isReviewRequired()) {
                    //
                    // partial match 
                    //
					$log->lmsid = $UserMatch->getMatchID(1);
                	$log->add_to_log(SyncLog::TYPE_REVIEW, 'user account deduplication found 1 partial match: '.$UserMatch->getMatchInfo(1), 'username='.$UserMatch->getMatchUsername(1));
					$log->count_review++;

                    $user = $DB->get_record('user', array('id'=>$log->lmsid), 'id, firstname, lastname, username');
                    $log->add_to_log(SyncLog::TYPE_REVIEW, "unable to update: $staging->firstname $staging->lastname, idnumber=$staging->idnumber", "$user->firstname $user->lastname, username=$user->username");

                } else {
                    //
                    // multiple matches 
                    //
					$log->count_review++;
                    for ($i = 1; $i <= $matchcount; $i++) {
                        $log->lmsid = $UserMatch->getMatchID($i);
                		$log->add_to_log(SyncLog::TYPE_REVIEW, "user account deduplication found $i of $matchcount possible matches: ".$UserMatch->getMatchInfo($i), 'username='.$UserMatch->getMatchUsername($i));
                    }
                    $log->add_to_log(SyncLog::TYPE_REVIEW, "possible duplicate accounts, unable to update: $staging->firstname $staging->lastname, idnumber=$staging->idnumber");
                }

                if ($success) {
                    //
                    // successfully created or updated user
                    //
					
					// check for duplicate email 
					//if ($source->allowduplicatedemails == 0) {
                    //	if ($DB->record_exists_sql("SELECT id FROM {user} WHERE email=\"$staging->email\" AND id <> $staging->userid")) {
                    //   	$log->add_to_log(SyncLog::TYPE_WARNING, "duplicate email: $staging->email", "$staging->firstname $staging->lastname, username=$staging->username");
                    //	}
                	//}
                }
            }
            
            $DB->execute("UPDATE {androgogic_sync_user} SET processed=1 WHERE id=$log->stagingid");
        }
    }
    $rs->close();

    $log->stagingid = NULL;
    $log->lmsid = NULL;
    $log->add_to_log(SyncLog::TYPE_INFO, "user totals: $log->count_created created, $log->count_updated updated, $log->count_skipped skipped, review $log->count_review");
    return;
}

function androgogic_sync_process_hierarchy($log, $source, $hierarchy) {
    global $DB,$CFG;
    
	assert(is_object($log), '$log must be an object');
    
    $log->count_created = 0;
    $log->count_updated = 0;
    $log->count_deleted = 0;
    $log->count_skipped = 0;

	$frameworkid = 0;
	if ($hierarchy->shortprefix == 'org') {
    	$frameworkid = $source->orgframeworkid;
    } else if ($hierarchy->shortprefix == 'pos') {
    	$frameworkid = $source->posframeworkid;
	}
    if (empty($frameworkid)) {
        throw new Exception("$hierarchy->prefix framework setting is required");
    }
    
    if ($source->allrecords == 1) {
		// get items to be deleted
		$rs = $DB->get_recordset_sql("SELECT h.* FROM {{$hierarchy->shortprefix}} h
			 LEFT OUTER JOIN {androgogic_sync_{$hierarchy->shortprefix}} s ON s.idnumber = h.idnumber AND TRIM(s.idnumber) <> '' AND runid=$log->runid AND sourceid=$source->id AND processed=0 
			WHERE h.frameworkid = $frameworkid
			  AND s.id IS NULL");
		if ($rs->valid()) {
			foreach ($rs as $staging) {
				$log->stagingid = $staging->id;
				androgogic_sync_delete_hierarchy_item($log, $hierarchy, $frameworkid, $staging->idnumber);
			}
		}
		$rs->close();
	}
	
    // get items to be created
    $rs = $DB->get_recordset_sql("SELECT s.* FROM {androgogic_sync_{$hierarchy->shortprefix}} s
		 LEFT OUTER JOIN {{$hierarchy->shortprefix}} h ON s.idnumber = h.idnumber AND TRIM(s.idnumber) <> '' AND h.frameworkid = $frameworkid
		WHERE runid=$log->runid AND sourceid=$source->id AND processed=0 
		  AND h.id IS NULL");
    if ($rs->valid()) {
        // first create all new items without setting parent in case some new items are also parents
        foreach ($rs as $staging) {
            $log->stagingid = $staging->id;
			$staging->parentidnumber = '';
			androgogic_sync_create_hierarchy_item($log, $hierarchy, $frameworkid, $staging);
        }
/*
 		// update parent of newly created items 
 		$rs->MoveFirst();
        foreach ($rs as $staging) {
            $log->stagingid = $staging->id;
			$staging->parentid = 0;  // default to top level
			if (!empty($staging->parentidnumber)) {
				$parentid = $DB->get_field('$hierarchy->shortprefix', 'id', array('idnumber'=>$staging->parentidnumber, 'frameworkid'=>$frameworkid));
				if (!$parentid) {
					$log->add_to_log(SyncLog::TYPE_WARNING, "cannot set $hierarchy->prefix parent, parentidnumber not found in LMS: parentidnumber=$staging->parentidnumber", "$staging->fullname, idnumber=$staging->idnumber");
				} else {
					androgogic_sync_update_hierarchy_item($log, $hierarchy, $frameworkid, $staging);
				}
			}
			$DB->execute("UPDATE {androgogic_sync_{$hierarchy->shortprefix}} SET processed=1 WHERE id=$staging->id");
		}
*/
	}
    $rs->close();
    
    $rs = $DB->get_recordset_sql("SELECT s.*, h.id AS itemid FROM {androgogic_sync_{$hierarchy->shortprefix}} s
		 LEFT OUTER JOIN {{$hierarchy->shortprefix}} h ON h.idnumber = s.idnumber AND TRIM(s.idnumber) <> '' 
		WHERE runid=$log->runid AND sourceid=$source->id AND processed=0  
		  AND h.frameworkid = $frameworkid 
	 ORDER BY s.id");
    if ($rs->valid()) {
    
        foreach ($rs as $staging) {
            $log->stagingid = $staging->id;

            if (empty($staging->idnumber)) {

                $log->count_skipped++;
                $log->add_to_log(SyncLog::TYPE_WARNING, "$hierarchy->prefix skipped: idnumber is required", "$staging->fullname");
            
            } else if (!empty($staging->deleted)) {

				androgogic_sync_delete_hierarchy_item($log, $hierarchy, $frameworkid, $staging->idnumber);
            	$log->count_deleted++;
            	$log->lmsid = $hierarchy->id;
            	$log->add_to_log(SyncLog::TYPE_TRACE, "$hierarchy->prefix deleted", "$staging->fullname, idnumber=$staging->idnumber");
            
            } else if (empty($staging->fullname)) {

                $log->count_skipped++;
                $log->add_to_log(SyncLog::TYPE_WARNING, "$hierarchy->prefix skipped: fullname is required", "idnumber=$staging->idnumber");
            
            } else {
                
                if (empty($staging->itemid)) {
					androgogic_sync_create_hierarchy_item($log, $hierarchy, $frameworkid, $staging);
				} else {
					androgogic_sync_update_hierarchy_item($log, $hierarchy, $frameworkid, $staging);
				}
            }
            
            $DB->execute("UPDATE {androgogic_sync_{$hierarchy->shortprefix}} SET processed=1 WHERE id=$log->stagingid");
        }
    }
    $rs->close();
    
    // update remaining items 
    $rs = $DB->get_recordset_sql("SELECT * FROM {androgogic_sync_{$hierarchy->shortprefix}} WHERE runid=$log->runid AND sourceid=$source->id AND processed=0");
    if ($rs->valid()) {
        foreach ($rs as $staging) {
            $log->stagingid = $staging->id;

            if (empty($staging->idnumber)) {

                $log->count_skipped++;
                $log->add_to_log(SyncLog::TYPE_WARNING, "$hierarchy->prefix skipped: idnumber is required", "$staging->fullname");
            
            } else if (empty($staging->fullname)) {

                $log->count_skipped++;
                $log->add_to_log(SyncLog::TYPE_WARNING, "$hierarchy->prefix skipped: fullname is required", "idnumber=$staging->idnumber");
            
            } else {

                $staging->typeid = 0;
                if (!empty($staging->typeidnumber)) {
                    $typeid = $DB->get_field($hierarchy->shortprefix.'_type', 'id', array('idnumber'=>$staging->typeidnumber));
                    if (!$typeid) {
                        $log->add_to_log(SyncLog::TYPE_WARNING, "unable to set $hierarchy->prefix type, typeidnumber not found in LMS: typeidnumber=$staging->typeidnumber", "$staging->fullname, idnumber=$staging->idnumber");
                    } else {
                    	$staging->typeid = $typeid;
                    }
                }
                
                $staging->parentid = 0;  // default to top level
                if (!empty($staging->parentidnumber)) {
                    $parentid = $DB->get_field($hierarchy->shortprefix, 'id', array('idnumber'=>$staging->parentidnumber, 'frameworkid'=>$frameworkid));
                    if (!$parentid) {
                        $log->add_to_log(SyncLog::TYPE_WARNING, "unable to set $hierarchy->prefix parent, parentidnumber not found in LMS: parentidnumber=$staging->parentidnumber", "$staging->fullname, idnumber=$staging->idnumber");
                    } else {
                    	$staging->parentid = $parentid;
                    }
                }
                androgogic_sync_update_hierarchy_item($log, $hierarchy, $frameworkid, $staging);
            }
            $DB->execute("UPDATE {androgogic_sync_{$hierarchy->shortprefix}} SET processed=1 WHERE id=$staging->id");
        }
    }
    $rs->close();
    
    $log->stagingid = NULL;
    $log->lmsid = NULL;
    $log->add_to_log(SyncLog::TYPE_INFO, "$hierarchy->prefix totals: $log->count_created created, $log->count_updated updated, $log->count_deleted deleted, $log->count_skipped skipped");
}

function androgogic_sync_send_email($user, $messagestringname) {
    global $CFG;
    
	assert(is_object($user), '$user must be an object');
	assert(is_string($messagestringname), '$messagestringname must be a string');

    $site = get_site();
    $supportuser = core_user::get_support_user();

    $username = urlencode($user->username);
    $username = str_replace('.', '%2E', $username); // prevent problems with trailing dots

    $userauth = get_auth_plugin($user->auth);
    if ($userauth->can_change_password() and $userauth->change_password_url()) {
        $data->changepasswordurl = $userauth->change_password_url();
    }
    $data = new stdClass();
    $data->firstname = $user->firstname;
    $data->sitename = format_string($site->fullname);
    $data->siteurl = $CFG->wwwroot;
    $data->username = $username;

    $message = get_string($messagestringname, 'local_androgogic_sync', $data);
    $messagehtml = text_to_html(get_string($messagestringname, 'local_androgogic_sync', $data), false, false, true);
    $subject = $site->fullname;

    $user->mailformat = 1;  // Always send HTML version as well
    return email_to_user($user, $supportuser, $subject, $message, $messagehtml);
}

function androgogic_sync_load_csvfile($log, $source, $filename) {
    global $DB;

	assert(is_object($log), '$log must be an object');
	assert(is_object($source), '$log must be an object');
	assert(is_string($filename), '$filename must be a string');
	
	if (empty($source->archivedir)) {
		throw new Exception('Archive directory setting is required');
	}
						
    if (!is_dir($source->archivedir)) {
        throw new Exception("file archive directory $source->archivedir is not found");
    }
    
    if (!is_readable($filename)) {
        throw new Exception("CSV file $filename is not readable");
    }
	
	$fieldmapping = androgogic_sync_get_field_mapping($source->id);

	if (($handle = fopen($filename, 'r')) === FALSE) {
		throw new Exception('unable to read CSV file '.$filename);
	}
	
	$reccount = 0;
	$linecount = 0;
	while (($data = fgetcsv($handle, 0, $source->csvdelimiter, '"')) !== FALSE) {
		$linecount++;
		// skip first line if CSV file has header row
		if ($linecount > 1 or ($linecount == 1 and $source->csvheader == 0)) {
			$time = time();
			$new = array('created'=>$time, 'runid'=>$log->runid, 'sourceid'=>$source->id, 'processed'=>0);
			foreach ($fieldmapping->csvcolumns as $dbfieldname=>$csvcolumnno) {
				if (isset($data[$csvcolumnno-1])) {
					$new[$dbfieldname] = $data[$csvcolumnno-1];
				}
			}
			$tablename = 'androgogic_sync_'.strtolower($source->element);
			$stagingid = $DB->insert_record($tablename, $new, true, true);
			$reccount++;

			if (count($fieldmapping->customfields)>0) {
				$new = array('created'=>$time, 'stagingid'=>$stagingid);
				foreach ($fieldmapping->customfields as $shortname=>$csvcolumnno) {
					if (isset($data[$csvcolumnno-1])) {
						$new['shortname'] = $shortname;
						$new['data'] = $data[$csvcolumnno-1];
						$DB->insert_record('androgogic_sync_user_custom', $new, true, true);
					}
				}
			}
		}
	}
	fclose($handle);

    $log->add_to_log(SyncLog::TYPE_INFO, "loaded $reccount records from $filename");
    
    // Move file to archive folder
    $archivefilename = $source->archivedir . '/' . basename($filename);
    if (!rename($filename, $archivefilename)) {
        throw new Exception("cannot rename sync file $filename to $archivefilename");
    }
    
    return $reccount;
}

function androgogic_sync_get_field_mapping($sourceid) {
	global $DB;
	
	assert(is_numeric($sourceid), '$sourceid must be an integer');
	assert($DB->record_exists('androgogic_sync_source', array('id'=>$sourceid)), "invalid sourceid: $sourceid");	

	// load field mapping
	$fieldmapping = new stdClass();
	$fieldmapping->csvcolumns = array();
	$fieldmapping->customfields = array();
	
	if ($fields = $DB->get_records('androgogic_sync_field', array('sourceid'=>$sourceid))) {
		foreach ($fields as $field) {
			if (substr($field->dbfieldname, 0, 13) == 'profilefield_') {
				$shortname = substr($field->dbfieldname, 13);
				$fieldmapping->customfields[$shortname] = $field->csvcolumnno;
			} else {
				$fieldmapping->csvcolumns[$field->dbfieldname] = $field->csvcolumnno;
			}
		}
	}
	return $fieldmapping;
}

function androgogic_sync_get_matching_files($filedir, $fileprefix) {

	assert(is_string($filedir), '$filedir must be a string');
	assert(is_string($fileprefix), '$fileprefix must be a string');
	assert(!empty($filedir), '$filedir is required');
	assert(!empty($fileprefix), '$fileprefix is required');
	
    if (!is_dir($filedir)) {
        throw new Exception("$filedir is not a valid directory");
    }
	
	$matchingfiles = array();
	
	$dir = scandir($filedir);
	if ($dir<>false) {
		foreach($dir as $filename) {
			if (strtolower(substr($filename, 0 , strlen($fileprefix))) == strtolower($fileprefix)) {
				$filepath = $filedir.'/'.$filename;
				$filemd5 = md5_file($filepath);
				while (true) {
					// Ensure file is not currently being written to
					sleep(2);
					$newmd5 = md5_file($filepath);
					if ($filemd5 != $newmd5) {
						$filemd5 = $newmd5;
					} else {
						break;
					}
				}
				$matchingfiles[] = $filepath;
				break;
			}
		}   
	} 
	return $matchingfiles;
}

function androgogic_sync_validate_csvfile($log, $source, $filename) {
	
	assert(is_object($log), '$log must be an object');
	assert(is_object($source), '$source must be an object');
	assert(is_string($filename), '$filename must be a string');
	assert(!empty($filename), '$filename is required');
	
	$linecount = 0;
	$reccount = 0;
	$errorcount = 0;
	$fieldcount = 0;
	
	if (($handle = fopen($filename, 'r')) === FALSE) {
        throw new Exception('unable to read CSV file '.$filename);
	}
		
	while (($data = fgetcsv($handle, 0, $source->csvdelimiter, '"')) !== FALSE) {
		$linecount++;
		if ($linecount == 1) {
			$fieldcount = count($data);
			if ($source->csvheader == 0) {
				$reccount++;
			}
		} else {	
			$reccount++;
			$num = count($data);
			if ($num <> $fieldcount) {
				$errorcount++;
				$log->add_to_log(SyncLog::TYPE_ERROR, "CSV file format error: line $linecount contains $num fields, expecting $fieldcount fields");
			}
		}
	}
	fclose($handle);
	
	if ($errorcount > 0) {
		throw new Exception("unable to load CSV file $filename");
	}
    $log->add_to_log(SyncLog::TYPE_TRACE, 'validated CSV file '.basename($filename).", contains $reccount records");

	return ($errorcount == 0);
}

