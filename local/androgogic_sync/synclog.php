<?php
/** 
 * Androgogic Sync
 *
 * @author      Keith Buss <kbuss@outlook.com>
 * @version     May 2015
 *
 **/

require_once('../../config.php');
require_once($CFG->dirroot . '/totara/reportbuilder/lib.php');

$debug  = optional_param('debug', false, PARAM_BOOL);
$sid = optional_param('sid', '0', PARAM_INT);
$format = optional_param('format', '', PARAM_TEXT); // export format

$context = context_system::instance();
require_capability('local/androgogic_sync:synclog', $context);
$PAGE->set_context($context);
$PAGE->set_url('/local/androgogic_sync/synclog.php');

if ($CFG->forcelogin) {
    require_login();
}

$renderer = $PAGE->get_renderer('totara_reportbuilder');
$strheading = get_string('synclogreport', 'local_androgogic_sync');
$shortname = 'report_androgogic_sync_log';

if (!$report = reportbuilder_get_embedded_report($shortname, null, false, $sid)) {
    print_error('error:couldnotgenerateembeddedreport', 'totara_reportbuilder');
}

$logurl = $PAGE->url->out_as_local_url();
if ($format != '') {
    add_to_log(SITEID, 'rbembedded', 'export report', $logurl, $report->fullname);
    $report->export_data($format);
    die;
}

add_to_log(SITEID, 'rbembedded', 'view report', $logurl, $report->fullname);

$report->include_js();

$fullname = format_string($report->fullname);
$pagetitle = format_string(get_string('report', 'totara_core') . ': ' . $fullname);

$PAGE->set_pagelayout('admin');
$PAGE->navbar->add(get_string('view'));
$PAGE->set_title($pagetitle);
$PAGE->set_button($report->edit_button());
$PAGE->set_heading('');
echo $OUTPUT->header();

$countfiltered = $report->get_filtered_count();
$countall = $report->get_full_count();

$heading = $strheading . ': ' .
$renderer->print_result_count_string($countfiltered, $countall);
echo $OUTPUT->heading($heading);

print $renderer->print_description($report->description, $report->_id);

$report->display_search();

// Print saved search buttons if appropriate.
echo $report->display_saved_search_options();

if ($countfiltered > 0) {
    $report->display_table();

    // export button
    $renderer->export_select($report->_id, $sid);
}

echo $OUTPUT->footer();

?>
