<?php
/** 
 * Androgogic Sync
 *
 * @author      Keith Buss <kbuss@outlook.com>
 * @version     May 2015
 *
 **/
 
defined('MOODLE_INTERNAL') || die();

$string['pluginname'] = 'Androgogic Sync';

///
/// Capabilities
///
$string['androgogic_sync:managesources'] = 'Manage sources';
$string['androgogic_sync:settings'] = 'General settings';
$string['androgogic_sync:synclog'] = 'View sync log';
$string['androgogic_sync:runsync'] = 'Run sync';

///
/// Admin menu
///
$string['managesources'] = 'Manage sources';
$string['synclog'] = 'Sync log';
$string['runsync'] = 'Run sync';
$string['generalsettings'] = 'General settings';

//
// Cron
// 
$string['syncstarted'] = 'Androgogic Sync started';
$string['syncsuccess'] = 'Androgogic Sync completed successfully';
$string['syncproblem'] = 'Androgogic Sync terminated with error';
$string['nocsvfiles'] = 'no CSV files to sync';

///
/// Sources
///
$string['CSV'] = 'CSV file';
$string['WEBSERVICE'] = 'Web service';

///
/// Elements
///
$string['USER'] = 'User';
$string['ORG'] = 'Organisation';
$string['POS'] = 'Position';
$string['COMP'] = 'Course completion';

///
/// Manage sources
///
$string['deletesource'] = 'Delete source';
$string['editsource'] = 'Edit source';
$string['addsource'] = 'Add source';

$string['name'] = 'Name';
$string['dateformat'] = 'Date format';
$string['senduseremail'] = 'Send email to users';
$string['senduseremail_help'] = 'Notification of new account or username change';

$string['fieldmapping'] = 'Field mapping';
$string['customfields'] = 'Custom field mapping';
$string['idnumberuid'] = 'idnumber is required for UID';
$string['requiredfield'] = 'required field';

$string['userdeletion'] = 'User deletion';
$string['suspenduser'] = 'Suspend user account in LMS';
$string['deleteuser'] = 'Delete user account in LMS';

///
/// Source file settings
///
$string['filesettings'] = 'Source settings';
$string['csvfileprefix'] = 'File prefix';

$string['allrecords'] = 'Source contains all records';
$string['allrecords_help'] = 'Does the source provide all sync records, everytime OR are only records that need to be updated/deleted provided? If "No" (only records to be updated/deleted), then the source must use the "delete" flag.';

$string['csvheader'] = 'CSV File header';
$string['csvheader_help'] = 'Is first row of the CSV file a header record containing column names';

$string['syncdir'] = 'Sync directory';
$string['archivedir'] = 'Archive directory';

///
/// Source field defaults
///
$string['fielddefaults'] = 'Field defaults';

$string['auth'] = 'Authentication';

$string['policyagreed'] = 'Policy agreed';
$string['policyagreed_help'] = 'Force new users to accept site policy';

$string['forcepasswordchange'] = 'Force password change for new users';

$string['orgframework'] = 'Organisation framework';
$string['posframework'] = 'Position framework';

///
/// CSV delimiters
///
$string['csvdelimiter'] = 'Field delimiter';
$string['comma'] = 'Comma (,)';
$string['semicolon'] = 'Semi-colon (;)';
$string['colon'] = 'Colon (:)';
$string['tab'] = 'Tab (\t)';
$string['pipe'] = 'Pipe (|)';

///
/// General settings
///
$string['settingssaved'] = 'Settings saved';

$string['cronhours'] = 'CRON hours';
$string['cronhours_help'] = 'A comma-separated list of hours in 24 hour format (0 - 23) when the CRON should execute';

$string['stagingretention'] = 'Staging data retention period';
$string['stagingretention_help'] = 'Number of days the staging data is kept (0 = retain all staging data)';

$string['notifications'] = 'Notifications';
$string['notifyemailto'] = 'Email Sync Log messages to';
$string['notifyemailto_help'] = 'A comma-separated list of email addresses to which notifications should be sent';
$string['notifyinfo'] = 'Send log info';
$string['notifywarning'] = 'Send log warnings';
$string['notifyerror'] = 'Send log errors';

$string['notifymessage'] = 'Server time: {$a->time}, Element: {$a->element}, Action: {$a->action}, {$a->logtype}: {$a->info}';
$string['notifymessagestart'] = '{$a->count} new Totara sync log messages ({$a->logtypes}) since {$a->since}. See below for most recent messages:';
$string['notifysubject'] = '{$a} :: Totara sync notification';
$string['syncnotifications'] = 'Totara sync notifications';
$string['syncloghere'] = 'For more information, view the sync log at {$a}';

///
/// Log notification email
///
$string['notifymessage'] = 'Server time: {$a->time}, Element: {$a->element}, Action: {$a->action}, {$a->logtype}: {$a->info}';
$string['notifymessagestart'] = '{$a->count} new Androgogic Sync log messages ({$a->logtypes}) since {$a->since}. See below for most recent messages:';
$string['notifysubject'] = '{$a} :: Androgogic Sync exception notification';
$string['syncnotifications'] = 'Androgogic Sync exception notification';
$string['syncloghere'] = 'For more information, view the sync log at {$a}';

///
/// New account email
///
$string['newaccountemail'] = 'Dear {$a->firstname}

Your account has been created on the {$a->sitename} which is available at {$a->siteurl} . Your username is: {$a->username} 

Please set your password at: {$a->changepasswordurl}';

///
/// Log report
///
$string['synclogreport'] = 'Androgogic Sync Log';
$string['logid'] = 'Log ID';
$string['logtype'] = 'Log type';
$string['source'] = 'Source';
$string['element'] = 'Element';
$string['action'] = 'Action';
$string['info'] = 'Info';
$string['runid'] = 'Run ID';
$string['lmsid'] = 'LMS ID';
$string['datetime'] = 'Date/Time';
?>