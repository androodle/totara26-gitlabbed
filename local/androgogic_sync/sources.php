<?php
/**
 * Androgogic Sync
 *
 * @author      Keith Buss <kbuss@outlook.com>
 * @version     May 2015
 *
 **/

require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once("{$CFG->libdir}/adminlib.php");
require_once('classes/addsourceform.class.php');

global $DB;

require_login();

$context = context_system::instance();
require_capability('local/androgogic_sync:managesources', $context);

// Get params.
$edit        = optional_param('edit', -1, PARAM_BOOL);
$hide        = optional_param('hide', 0, PARAM_INT);
$show        = optional_param('show', 0, PARAM_INT);
$moveup      = optional_param('moveup', 0, PARAM_INT);
$movedown    = optional_param('movedown', 0, PARAM_INT);
    
$heading = get_string('managesources', 'local_androgogic_sync');

admin_externalpage_setup('managesources');

///
/// Process actions
///

// create form
$mform = new addsourceform();

// form results check
if ($new = $mform->get_data()) {

    if (!empty($new->submitbutton)) {
    	redirect(new moodle_url('editsource.php', array('source'=>$new->source, 'element'=>$new->element)));
    }
}

if ($hide) {
	$DB->execute("UPDATE {androgogic_sync_source} SET visible=0 WHERE id=$hide");

} elseif ($show) {
	$DB->execute("UPDATE {androgogic_sync_source} SET visible=1 WHERE id=$show");
	
} elseif ($moveup) {
	move_source($moveup, true);
	
} elseif ($movedown) {
	move_source($movedown, false);
}

///
/// Generate page
///
$str_edit     = get_string('edit');
$str_delete   = get_string('delete');
$str_hide     = get_string('hide');
$str_show     = get_string('show');
$str_moveup   = get_string('moveup');
$str_movedown = get_string('movedown');
$str_settings = get_string('settings');

// Get sources for this page.
$sources = $DB->get_records_sql("SELECT * FROM {androgogic_sync_source} WHERE deleted=0 ORDER BY sortorder ASC");
if ($sources) {

    // Create display table.
    $table = new html_table();
    $table->attributes['class'] = 'generaltable fullwidth edit';

    // Setup column headers.
    $table->head = array(
    	get_string('source', 'local_androgogic_sync'), 
    	get_string('element', 'local_androgogic_sync'), 
    	get_string('name', 'local_androgogic_sync'),
    	get_string('settings'),
		get_string('actions'));

    // Add rows to table.
    $rowcount = 1;
    foreach ($sources as $source) {
        $row = array();

        $cssclass = !$source->visible ? 'dimmed' : '';
              	
		$row[] = html_writer::tag('span', format_string($source->source), array('class'=>$cssclass));
		
		$row[] = html_writer::tag('span', format_string($source->element), array('class'=>$cssclass));

        $row[] = html_writer::tag('span', format_string($source->name), array('class'=>$cssclass));
		
		$edit_url = new moodle_url('editsource.php', array('source'=>$source->source, 'element'=>$source->element, 'id'=>$source->id));
        $row[] = $OUTPUT->action_link($edit_url, format_string($str_settings), null, array('class'=>$cssclass));

        $buttons = array();
		$buttons[] = $OUTPUT->action_icon($edit_url,
			new pix_icon('t/edit', $str_edit), null, array('title'=>$str_edit));
		if ($source->visible) {
			$buttons[] = $OUTPUT->action_icon(new moodle_url('sources.php', array('hide'=>$source->id)),
				new pix_icon('t/hide', $str_hide), null, array('title'=>$str_hide));
		} else {
			$buttons[] = $OUTPUT->action_icon(new moodle_url('sources.php', array('show'=>$source->id)),
				new pix_icon('t/show', $str_show), null, array('title'=>$str_show));
		}

		$buttons[] = $OUTPUT->action_icon(new moodle_url('deletesource.php', array('id'=>$source->id)),
				new pix_icon('t/delete', $str_delete), null, array('title'=>$str_delete));
				
		if ($rowcount != 1) {
			$buttons[] = $OUTPUT->action_icon(new moodle_url('sources.php', array('moveup'=>$source->id)),
					new pix_icon('t/up', $str_moveup), null, array('title'=>$str_moveup));
		} else {
			$buttons[] = $OUTPUT->spacer(array('height'=>11, 'width'=>11));
		}
		if ($rowcount != count($sources)) {
			$buttons[] = $OUTPUT->action_icon(new moodle_url('sources.php', array('movedown'=>$source->id)),
					new pix_icon('t/down', $str_movedown), null, array('title'=>$str_movedown));
		} else {
			$buttons[] = $OUTPUT->spacer(array('height'=>11, 'width'=>11));
		}
		$rowcount++;

        if ($buttons) {
            $row[] = implode($buttons, '');
        }
        $table->data[] = $row;
    }
}

///
/// Display page
///
echo $OUTPUT->header();

echo $OUTPUT->heading($heading);

if ($sources) {
	echo html_writer::table($table);
}

// display mform
$mform->display();

//add_to_log(SITEID, $prefix, 'view framework', "framework/index.php?prefix=$prefix", '');
echo $OUTPUT->footer();


   /**
     * Move the framework in the sortorder
     * @var int - id to move
     * @var boolean $up - up if true, down if false
     * @return boolean success
     */
    function move_source($id, $up) {
    
        global $DB;
        $move = NULL;
        $swap = NULL;
        $max = $DB->get_field_sql("SELECT MAX(sortorder) AS max, 1 FROM {androgogic_sync_source} WHERE deleted=0");
        $sortoffset = $max + 1000;

        $move = $DB->get_record('androgogic_sync_source', array('id'=>$id));

        if ($up) {
            $swap = $DB->get_record_sql(
                    "SELECT *
                    FROM {androgogic_sync_source}
                    WHERE deleted=0 AND sortorder < ?
                    ORDER BY sortorder DESC", array($move->sortorder), IGNORE_MULTIPLE
                    );
        } else {
            $swap = $DB->get_record_sql(
                    "SELECT *
                    FROM {androgogic_sync_source}
                    WHERE deleted=0 AND sortorder > ?
                    ORDER BY sortorder ASC", array($move->sortorder), IGNORE_MULTIPLE
                    );
        }

        if ($move && $swap) {
            $transaction = $DB->start_delegated_transaction();

            $DB->set_field('androgogic_sync_source', 'sortorder', $sortoffset, array('id'=>$swap->id));
            $DB->set_field('androgogic_sync_source', 'sortorder', $swap->sortorder, array('id'=>$move->id));
            $DB->set_field('androgogic_sync_source', 'sortorder', $move->sortorder, array('id'=>$swap->id));

            $transaction->allow_commit();
            return true;
        }
        return false;
    }
