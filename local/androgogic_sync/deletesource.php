<?php
/**
 * Androgogic Sync
 *
 * @author      Keith Buss <kbuss@outlook.com>
 * @version     May 2015
 *
 **/
 
require_once(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->libdir.'/adminlib.php');

require_login();

$context = context_system::instance();
require_capability('local/androgogic_sync:managesources', $context);

// Get params
$id = required_param('id', PARAM_INT);

// Delete confirmation hash
$delete = optional_param('delete', '', PARAM_ALPHANUM);

if (!$row = $DB->get_record('androgogic_sync_source', array('id'=>$id))) {
    throw new Exception($DB->get_last_error());	
}
	
$heading = get_string('deletesource', 'local_androgogic_sync');
$url_params = array('id'=>$id, 'delete'=>$delete);
$PAGE->set_url(new moodle_url('/local/androgogic_sync/deletesource.php', $url_params));
$PAGE->set_context($context);
$PAGE->set_pagelayout('admin');
$PAGE->set_title($heading);
    
///
/// Display page
///

$PAGE->navbar->add(get_string('managesources', 'local_androgogic_sync'), new moodle_url('sources.php'));
$PAGE->navbar->add($heading);

if (!$delete) {
    echo $OUTPUT->header();

    echo $OUTPUT->heading($heading);

    echo $OUTPUT->confirm("Are you sure you want to delete the $row->source $row->element source?" . html_writer::empty_tag('br') . html_writer::empty_tag('br'), "deletesource.php?id={$id}&amp;delete=".md5($row->timemodified)."&amp;sesskey={$USER->sesskey}", "sources.php");

    echo $OUTPUT->footer();
    exit;
}

///
/// Delete source
///

if ($delete != md5($row->timemodified)) {
	throw new Exception("unable to delete, record has been modified by another");	
}
if (!confirm_sesskey()) {
	throw new Exception("invalid sesskey");	
}

$DB->execute("UPDATE {androgogic_sync_source} SET deleted=1 WHERE id=$id");

// Log
//add_to_log(SITEID, $prefix, 'framework delete', "framework/index.php?prefix=$prefix", "$framework->fullname (ID $row->id)");
totara_set_notification('Deleted source', 'sources.php', array('class'=>'notifysuccess'));

echo $OUTPUT->footer();
