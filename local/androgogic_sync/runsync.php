<?php
/** 
 * Androgogic Sync
 *
 * @author      Keith Buss <kbuss@outlook.com>
 * @version     May 2015
 *
 **/
 
require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once("{$CFG->libdir}/adminlib.php");
require_once($CFG->dirroot.'/local/androgogic_sync/lib.php');

require_login();

$context = context_system::instance();
require_capability('local/androgogic_sync:runsync', $context);

admin_externalpage_setup('runsync');

//$PAGE->set_context($context);
//$PAGE->set_url('/local/androgogic_sync/runsync.php');
//$PAGE->set_pagelayout('admin');
//$PAGE->set_title(get_string('runsync', 'local_androgogic_sync'));
//$PAGE->set_heading('');

$execute = optional_param('execute', null, PARAM_BOOL);

echo $OUTPUT->header();
echo $OUTPUT->heading(get_string('runsync', 'local_androgogic_sync'));

$buttons = $OUTPUT->single_button(new moodle_url('/local/androgogic_sync/runsync.php', array('execute'=>1)), get_string('runsync', 'local_androgogic_sync'), 'post');
	
if ($execute) {
	require_sesskey();
	
	// Run the sync
	androgogic_sync_import_sync(true);
	$spacer = $OUTPUT->spacer(array('height'=>15, 'width'=>15)); // should be done with CSS instead
	$buttons .= $spacer.$OUTPUT->single_button(new moodle_url('/local/androgogic_sync/synclog.php'), get_string('synclog', 'local_androgogic_sync'), 'post');
} 

echo $OUTPUT->container_start('buttons mdl-align');
echo $buttons;
echo $OUTPUT->container_end();

echo $OUTPUT->footer();

?>
