<?php
/** 
 * Androgogic Sync
 *
 * @author      Keith Buss <kbuss@outlook.com>
 * @version     May 2015
 *
 **/

defined('MOODLE_INTERNAL') || die(); 

require_once('classes/synclog.class.php');
require_once('locallib.php');

function androgogic_sync_import_sync($forcerun=false) {
    global $DB, $USER;

	try {	
		raise_memory_limit(MEMORY_EXTRA);
		// Stop time outs, this might take a while
		set_time_limit(0);

    	$log = new SyncLog();
    	$log->showmessage = $forcerun;
		$log->add_to_log(SyncLog::TYPE_INFO, get_string('syncstarted', 'local_androgogic_sync'), 'run by '.($forcerun ? "$USER->firstname $USER->lastname, username=$USER->username" : 'CRON'));	
		$timestart = microtime(true);

		// remove expired staging data
		$retention = get_config('local_androgogic_sync', 'staging_retention');
		if (!empty($retention)) {
			if ($DB->get_record_sql("SELECT id FROM {androgogic_sync_user} WHERE TIMESTAMPDIFF(DAY,FROM_UNIXTIME(created),CURDATE()) > $retention", null, IGNORE_MULTIPLE)
			or $DB->get_record_sql("SELECT id FROM {androgogic_sync_org} WHERE TIMESTAMPDIFF(DAY,FROM_UNIXTIME(created),CURDATE()) > $retention", null, IGNORE_MULTIPLE)
			or $DB->get_record_sql("SELECT id FROM {androgogic_sync_pos} WHERE TIMESTAMPDIFF(DAY,FROM_UNIXTIME(created),CURDATE()) > $retention", null, IGNORE_MULTIPLE)) {
				$DB->execute("DELETE p FROM {androgogic_sync_user_custom} p
					INNER JOIN {androgogic_sync_user} u ON u.id = p.stagingid
					WHERE TIMESTAMPDIFF(DAY,FROM_UNIXTIME(u.created),CURDATE()) > $retention");
				$DB->execute("DELETE FROM {androgogic_sync_user} 
					WHERE TIMESTAMPDIFF(DAY,FROM_UNIXTIME(created),CURDATE()) > $retention");
				$DB->execute("DELETE FROM {androgogic_sync_org} 
					WHERE TIMESTAMPDIFF(DAY,FROM_UNIXTIME(created),CURDATE()) > $retention");		
				$DB->execute("DELETE FROM {androgogic_sync_pos} 
					WHERE TIMESTAMPDIFF(DAY,FROM_UNIXTIME(created),CURDATE()) > $retention");
				$log->add_to_log(SyncLog::TYPE_TRACE, "deleted staging data older than $retention days");
			}		
		} 

    	if ($sources = $DB->get_records_sql("SELECT * FROM {androgogic_sync_source} WHERE deleted=0 AND visible=1 ORDER BY sortorder ASC")) {		
			foreach ($sources as $source) {
            	$log->sourceid = $source->id;

				if ($source->source == 'CSV') {
					$csvfiles = androgogic_sync_get_matching_files($source->syncdir, $source->csvfileprefix);
					if (count($csvfiles) == 0) {
						$log->add_to_log(SyncLog::TYPE_INFO, get_string('nocsvfiles', 'local_androgogic_sync').", file prefix: $source->csvfileprefix");
					} else {
						foreach ($csvfiles as $filename) {
							if (androgogic_sync_validate_csvfile($log, $source, $filename) === true) {
								androgogic_sync_load_csvfile($log, $source, $filename);
							}
						}
					}
				} 
				
				$tablename = 'androgogic_sync_'.strtolower($source->element);
				$reccount = $DB->count_records($tablename, array('runid'=>$log->runid, 'sourceid'=>$source->id));
				if ($reccount > 0) {
					if ($source->element == 'USER') {
					
						androgogic_sync_process_user($log, $source);
						
					} else if ($source->element == 'ORG') {
					
						$hierarchy = new organisation();
						androgogic_sync_process_hierarchy($log, $source, $hierarchy);
						
					} else if ($source->element == 'POS') {
					
						$hierarchy = new position();
						androgogic_sync_process_hierarchy($log, $source, $hierarchy);
					}
				}
			}
		}
		
		$timeend = microtime(true);
		$log->stagingid = NULL;
        $log->sourceid = NULL;
		$log->add_to_log(SyncLog::TYPE_INFO, get_string('syncsuccess', 'local_androgogic_sync'), 'execution time '.number_format($timeend - $timestart,2).'s');

	} catch (Exception $e) {
	
		$log->error_to_log($e);
        $log->stagingid = NULL;
        $log->sourceid = NULL;
		$log->add_to_log(SyncLog::TYPE_ERROR, get_string('syncproblem', 'local_androgogic_sync'));

	}

	$notifyemailto = get_config('local_androgogic_sync', 'notify_emailto');
	if (!empty($notifyemailto)) {
		$notifyinfo = get_config('local_androgogic_sync', 'notify_info');
		$notifywarning = get_config('local_androgogic_sync', 'notify_warning');
		$notifyerror = get_config('local_androgogic_sync', 'notify_error');
		$logtypes = array();
		if (!empty($notifyinfo)) {
			$logtypes[] = SyncLog::TYPE_INFO;
		}
		if (!empty($notifywarning)) {
			$logtypes[] = SyncLog::TYPE_WARNING;
		}
		if (!empty($notifyerror)) {
			$logtypes[] = SyncLog::TYPE_ERROR;
		}
		if (count($logtypes) > 0) {
			androgogic_sync_notify($runid, $logtypes);
		}
	}
    return;
}

function androgogic_sync_create_hierarchy_item($log, $hierarchy, $frameworkid, $staging) {
	global $DB;

	assert(is_object($log), '$log must be an object');
	assert(is_object($hierarchy), '$hierarchy must be an object');
	assert(is_numeric($frameworkid), '$frameworkid must be numeric');
	assert(is_object($staging), '$staging must be an object');
    	
    $log->lmsid = null;

	$item = new stdClass;
	$item->fullname = $staging->fullname;
	$item->idnumber = $staging->idnumber;
	$item->frameworkid = $frameworkid;
	$item->visible = 1;
	$item->usermodified = get_admin()->id;
	$item->timecreated = time();
	$item->timemodified = $item->timecreated;

	$item->parentid = 0;  // default to top level
	if (!empty($staging->parentidnumber)) {
		$parentid = $DB->get_field($hierarchy->shortprefix, 'id', array('idnumber'=>$staging->parentidnumber, 'frameworkid'=>$frameworkid));
		if (!$parentid) {
			$log->add_to_log(SyncLog::TYPE_WARNING, "cannot set $hierarchy->prefix parent, parentidnumber not found in LMS: parentidnumber=$staging->parentidnumber", "$staging->fullname, idnumber=$staging->idnumber");
		} else {
			$item->parentid = $parentid;
		}
	}
	
	$item->typeid = 0;
	if (!empty($staging->typeidnumber)) {
		$typeid = $DB->get_field($hierarchy->shortprefix.'_type', 'id', array('idnumber'=>$staging->typeidnumber));
		if (!$typeid) {
			$log->add_to_log(SyncLog::TYPE_WARNING, "cannot set $hierarchy->prefix typeid, typeidnumber not found in LMS: typeidnumber=$staging->typeidnumber", "$staging->fullname, idnumber=$staging->idnumber");
		} else {
			$item->typeid = $typeid;
		}
	}
	
	if (!$hierarchy->add_hierarchy_item($item, $item->parentid, $frameworkid, false, true, false)) {
		$log->add_to_log(SyncLog::TYPE_WARNING, "cannot create $hierarchy->prefix hierarchy item: $item->fullname", "idnumber=$item->idnumber, frameworkid=$frameworkid");
	} else {
    	$log->lmsid = $item->id;
	    $log->count_created++;
        $log->add_to_log(SyncLog::TYPE_TRACE, "$hierarchy->prefix created", "$item->fullname, idnumber=$item->idnumber");
	}
}

function androgogic_sync_delete_hierarchy_item($log, $hierarchy, $frameworkid, $idnumber) {
	global $DB;
	
	assert(is_object($log), '$log must be an object');
	assert(is_object($hierarchy), '$hierarchy must be an object');
	assert(is_numeric($frameworkid), '$frameworkid must be numeric');
	assert(is_string($idnumber), '$idnumber must be a string');
    
	$item = $DB->get_record($hierarchy->shortprefix, array('idnumber'=>$idnumber, 'frameworkid'=>$frameworkid));
	if ($item === false) {
    	$log->lmsid = null;
		$log->add_to_log(SyncLog::TYPE_ERROR, "$hierarchy->prefix hierarchy item not found: idnumber=$idnumber, frameworkid=$frameworkid");
	} else {
    	$log->lmsid = $item->id;

		if (!$hierarchy->delete_hierarchy_item($item->id)) {
			$log->add_to_log(SyncLog::TYPE_ERROR, "failed to delete $hierarchy->prefix", "$item->fullname, idnumber=$item->idnumber");
		} else {
			$log->count_deleted++;
			$log->add_to_log(SyncLog::TYPE_TRACE, "$hierarchy->prefix deleted", "$item->fullname, idnumber=$item->idnumber");
		}
	}
}

function androgogic_sync_update_hierarchy_item($log, $hierarchy, $frameworkid, $staging) {
	global $DB;
		
	assert(is_object($log), '$log must be an object');
	assert(is_object($hierarchy), '$hierarchy must be an object');
	assert(is_numeric($frameworkid), '$frameworkid must be numeric');
	assert(is_object($staging), '$staging must be an object');

	$item = $DB->get_record($hierarchy->shortprefix, array('idnumber'=>$staging->idnumber, 'frameworkid'=>$frameworkid));
	if ($item === false) {
    	$log->lmsid = null;
		$log->add_to_log(SyncLog::TYPE_ERROR, "$hierarchy->prefix hierarchy item not found: idnumber=$staging->idnumber, frameworkid=$frameworkid");
	} else {
		$log->lmsid = $item->id;

		$staging->parentid = 0;  // default to top level
		if (!empty($staging->parentidnumber)) {
			$parentid = $DB->get_field($hierarchy->shortprefix, 'id', array('idnumber'=>$staging->parentidnumber, 'frameworkid'=>$frameworkid));
			if (!$parentid) {
		
				$log->add_to_log(SyncLog::TYPE_WARNING, "cannot set $hierarchy->prefix parent, parentidnumber not found in LMS: parentidnumber=$staging->parentidnumber", "$staging->fullname, idnumber=$staging->idnumber");
			} else {
				$staging->parentid = $parentid;
			}
		}
	
		$staging->typeid = 0;
		if (!empty($staging->typeidnumber)) {
			$typeid = $DB->get_field($hierarchy->shortprefix.'_type', 'id', array('idnumber'=>$staging->typeidnumber));
			if (!$typeid) {
				$log->add_to_log(SyncLog::TYPE_WARNING, "cannot set $hierarchy->prefix typeid, typeidnumber not found in LMS: typeidnumber=$staging->typeidnumber", "$staging->fullname, idnumber=$staging->idnumber");
			} else {
				$staging->typeid = $typeid;
			}
		}
	
		if ($item->fullname <> $staging->fullname or $item->parentid <> $staging->parentid or $item->typeid <> $staging->typeid) {
			$item->parentid = $staging->parentid;
			$item->fullname = $staging->fullname;
			$item->typeid = $staging->typeid;
			$item->usermodified = get_admin()->id;
			$item->timemodified = time();
			
			if (!$hierarchy->update_hierarchy_item($item->id, $item, false, true, false)) {
				$log->add_to_log(SyncLog::TYPE_ERROR, "cannot update $hierarchy->prefix hierarchy item: $item->fullname, idnumber=$item->idnumber, frameworkid=$frameworkid");
			} else {
				$log->count_updated++;	
				$log->add_to_log(SyncLog::TYPE_TRACE, "$hierarchy->prefix updated", "$item->fullname, idnumber=$item->idnumber");
			}
		}
	}
}

function androgogic_sync_create_user($log, $source, $user) {
	global $CFG, $DB;
	
	assert(is_object($log), '$log must be an object');
	assert(is_object($source), '$source must be an object');
	assert(is_object($user), '$user must be an object');
	
	$success = false;
		
	$transaction = $DB->start_delegated_transaction();
	try {	
		// set defaults
		$user->confirmed = 1; 
		$user->mnethostid = $CFG->mnet_localhost_id;
		//$user->lang = $CFG->lang;
		$user->lang = 'en';
		$user->timezone = '99';	
		$user->htmleditor = 1;
		$user->deleted = 0;	
		
		if (empty($user->country)) {
			$user->country = $CFG->country;
		}
		
		// phone1 cannot be null
		if ($user->phone1 === null) {
			$user->phone1 = '';
		}
		// phone2 cannot be null
		if ($user->phone2 === null) {
			$user->phone2 = '';
		}		
		// address cannot be null
		if ($user->address === null) {
			$user->address = '';
		}
		// city cannot be null
		if ($user->city === null) {
			$user->city = '';
		}
		
    	//
		// Create user
		//
		$updatepassword = !empty($user->password);
		$user->id = user_create_user($user, $updatepassword, false); 
		$staging->userid = $user->id;
		$log->lmsid = $user->id;
        
		//
		// Save user profile custom fields
		//
		profile_save_data($user);
		
		$transaction->allow_commit();

		// trigger event
		\core\event\user_created::create_from_userid($user->id)->trigger();
		
		$log->add_to_log(SyncLog::TYPE_TRACE, 'user created', "$user->firstname $user->lastname, username=$user->username");
		$log->count_created++;
		
        // Create user position assignment
        androgogic_sync_pos_assignments($log, $source->dateformat, $staging);
        
		if ($user->suspended) {
			$log->add_to_log(SyncLog::TYPE_TRACE, 'user suspended', "$user->firstname $user->lastname, username=$user->username");
			$event = \totara_core\event\user_suspended::create(
				array(
					'objectid'=>$user->id,
					'context'=>context_user::instance($user->id),
					'other'=>array(
						'username'=>$user->username,
					)
				)
			);
			$event->trigger();
		}	
		$success = true;

	} catch (Exception $e) {
	
		$transaction->rollback($e);
		$log->add_to_log(SyncLog::TYPE_ERROR, 'cannot create user', "$user->firstname $user->lastname, username=$user->username");
		throw $e;
	}
	return $success;
}

function androgogic_sync_update_user($log, $source, $staging) {
	global $DB;

	assert(is_object($log), '$log must be an object');
	assert(is_object($source), '$source must be an object');
	assert(is_object($staging), '$staging must be an object');

	$success = false;

	$transaction = $DB->start_delegated_transaction();
	try {    
		$user = $DB->get_record('user', array('id'=>$staging->userid));
		if ($user === false) {
			throw new Exception("user ID $staging->userid not found");
		}
		
		$log->lmsid = $user->id;

		$profile = new stdClass;
		$profile->id = $user->id;
		profile_load_data($profile);

		// convert date custom fields 
		if ($rows = $DB->get_records('user_info_field', array('datatype'=>'datetime'))) {
			foreach ($rows as $row) {
				$fieldname = 'profile_field_'.$row->shortname;
				if (!empty($profile->$fieldname)) {
					$profile->$fieldname = androgogic_sync_convert_userdate($profile->$fieldname, '%Y-%m-%d');
				}
			}
		}
	
		if ($user->deleted == 1 and empty($staging->deleted)) {
			// Revive previously deleted user.
			if (undelete_user($user)) {
				$log->add_to_log(SyncLog::TYPE_TRACE, 'user undeleted', "$user->firstname $user->lastname, username=$user->username");

				// Tag the revived user for new password generation (if applicable).
				$userauth = get_auth_plugin($user->auth);
				if ($userauth->is_internal() and $userauth->can_change_password()) {
					set_user_preference('auth_forcepasswordchange', 1, $user->id);
					set_user_preference('create_password',          1, $user->id);
				}
				unset($userauth);
				
			} else {
				throw new Exception("cannot undelete user: $user->firstname $user->lastname, username=$user->username");
			}
		}
		
		// Check if the user is going to be suspended
		$suspended = ($staging->suspended == 1 and $user->suspended == 0);		 
	
		//
		// Update user
		//
		$staging->username = strtolower(clean_param($staging->username, PARAM_USERNAME));  // Usernames always lowercase in moodle. 
		androgogic_sync_fields($user, $staging);
		
		$updatepassword = !empty($user->password);
		user_update_user($user, $updatepassword, false);

		//
		// Update user profile fields
		//
		$profilemodified = androgogic_sync_fields($profile, $staging);
		profile_save_data($profile);

		$transaction->allow_commit();

		// trigger event
		\core\event\user_updated::create_from_userid($user->id)->trigger();

		$log->add_to_log(SyncLog::TYPE_TRACE, 'user updated', "$user->firstname $user->lastname, username=$user->username");
		$log->count_updated++;	
        
        // Update user position assignment
        androgogic_sync_pos_assignments($log, $source->dateformat, $staging);
        
		if ($suspended) {
			$log->add_to_log(SyncLog::TYPE_TRACE, 'user suspended', "$user->firstname $user->lastname, username=$user->username");
			$event = \totara_core\event\user_suspended::create(
				array(
					'objectid'=>$user->id,
					'context'=>context_user::instance($user->id),
					'other'=>array(
						'username'=>$user->username,
					)
				)
			);
			$event->trigger();
		}
		$success = true;

	} catch (Exception $e) {

		$transaction->rollback($e);
		$log->add_to_log(SyncLog::TYPE_ERROR, 'cannot update user', "$user->firstname $user->lastname, username=$user->username");
		throw $e;
	}
	return $success;
}

function androgogic_sync_pos_assignments($log, $dateformat, $staging) {
	global $DB, $CFG;

	assert(is_object($log), '$log must be an object');
	assert(is_object($staging), '$staging must be an object');
		
	$log->lmsid = $staging->userid;

	$posid = null;
	if (!empty($staging->posidnumber)) {
		//
		// search for position hierarchy item
		//
		$posid = $DB->get_field('pos', 'id', array('fullname'=>$staging->posidnumber, 'frameworkid'=>$staging->posframeworkid));
		if (!$posid)  {  
			$log->add_to_log(SyncLog::TYPE_WARNING, "position idnumber not found in hierarchy: $staging->posidnumber");
			$posid = null;
		}
    }
    
    $orgid = null;
	if (!empty($staging->orgidnumber)) {
		//
		// search for organisation hierarchy item
		//
		$orgid = $DB->get_field('org', 'id', array('idnumber'=>$staging->orgidnumber, 'frameworkid'=>$staging->orgframeworkid));
		if (!$orgid)  { 
			$log->add_to_log(SyncLog::TYPE_WARNING, "organisation idnumber not found in hierarchy: $staging->orgidnumber");
    		$orgid = null;
		}
	}
	
	$managerid = null;
	if (!empty($staging->manageridnumber)) {
		//
		// search for manager 
		//
		$managerid = $DB->get_field('user', 'id', array('idnumber'=>$staging->manageridnumber));
		if (!$managerid)  { 
			$log->add_to_log(SyncLog::TYPE_WARNING, "manager idnumber not found in LMS: $staging->manageridnumber");
			$managerid = null;
		}
	}
		
	$appraiserid = null;
	if (!empty($staging->appraiseridnumber)) {
		//
		// search for appraiser 
		//
		$appraiserid = $DB->get_field('user', 'id', array('idnumber'=>$staging->appraiseridnumber));
		if (!$appraiserid)  { 
			$log->add_to_log(SyncLog::TYPE_WARNING, "appraiser idnumber not found in LMS: $staging->appraiseridnumber");
			$appraiserid = null;
		}
	}	
	if (!empty($staging->posstartdate) or !empty($user->posenddate)) {
		$dateformat = get_config('local_androgogic_sync', 'user_dateformat');
    	if (empty($dateformat)) {
        	$dateformat = 'Y-m-d';
        }
        
		$timevalidfrom = null;
		if (!empty($staging->posstartdate)) {
			$timevalidfrom = totara_date_parse_from_format($dateformat, $staging->posstartdate);
			if ($timevalidfrom == -1) {
				$log->add_to_log(SyncLog::TYPE_WARNING, "unable to set position start date, invalid posstartdate: $staging->posstartdate");
				$timevalidfrom = null;
			}
		}
	
		$timevalidto = null;
		if (!empty($user->posenddate)) {
			$timevalidto = totara_date_parse_from_format($dateformat, $user->posenddate);
			if ($timevalidto == -1) {
				$log->add_to_log(SyncLog::TYPE_WARNING, "unable to set position finish date, invalid posenddate: $staging->posenddate");
				$timevalidto = null;
			}
		}
	}	
		
    // Attempt to load position assignment
    $pa = new position_assignment(array('userid'=>$staging->userid, 'type'=>POSITION_TYPE_PRIMARY));
	if (empty($pa->id)) {
		//
		// create position assignment
		//
		if (!empty($staging->postitle) or !empty($orgid) or !empty($posid) or !empty($managerid) or !empty($appraiserid)) {
			$pa->fullname = $staging->postitle;
			$pa->organisationid = $orgid;
			$pa->positionid = $posid;
			$pa->managerid = $managerid;
			$pa->appraiserid = $appraiserid;
			$pa->timevalidfrom = $timevalidfrom;
			$pa->timevalidto = $timevalidto;
			$pa->timecreated = time();
			$pa->timemodified = $pa->timecreated;
			$pa->usermodified = get_admin()->id;

			assign_user_position($pa);
			$log->add_to_log(SyncLog::TYPE_TRACE, 'primary position assignment created', "$pa->fullname (ID $pa->id)");
		}

	} else if ($pa->fullname <> $staging->postitle or $pa->organisationid <> $orgid 
	or $pa->positionid <> $posid or $pa->managerid <> $managerid or $pa->appraiserid <> $appraiserid
	or $pa->timevalidfrom <> $timevalidfrom or $pa->timevalidto <> $timevalidto) {	
		//
		// Update position assignment
		//
		$pa->fullname = $staging->postitle;
		$pa->organisationid = $orgid;
		$pa->positionid = $posid;
		$pa->managerid = $managerid;
		$pa->appraiserid = $appraiserid;
		$pa->timevalidfrom = $timevalidfrom;
		$pa->timevalidto = $timevalidto;
		$pa->timemodified = time();
		$pa->usermodified = get_admin()->id;
		
		assign_user_position($pa);
		$log->add_to_log(SyncLog::TYPE_TRACE, 'primary position assignment updated', "$pa->fullname (ID $pa->id)");
	}	
}

function androgogic_sync_convert_userdate($date, $format) {
	if ($date == 0) {
		return '';
	}
	return userdate($date, $format);
}

function androgogic_sync_array2string($array) {
	$retval = '';
	foreach ($array as $key=>$value) {
		if (strlen($retval) > 0) {
			$retval .= ', ';
		}
		$retval .= $key.'='.strval($value);
	}
	return $retval;
}
	
function androgogic_sync_fields($dbfields, $stagingfields) { 
	$modified = false;
	foreach($dbfields as $key=>$value) {
		if (isset($stagingfields->$key)) {
			if ($value != $stagingfields->$key) {
				$dbfields->$key = $stagingfields->$key;
				$modified = true;
			}
		}
	}
	return $modified;
}

function androgogic_sync_get_user_profile_field($userid, $shortname) {
	global $DB;
	
	assert(is_int($userid), '$userid must be an integer');
	assert(is_string($shortname), '$shortname must be a string');

	$sql = "SELECT d.data 
			  FROM {user_info_data} d 
			  JOIN {user_info_field} f ON d.fieldid = f.id
			 WHERE d.userid = $userid
			   AND f.shortname = '$shortname'";
	return $DB->get_field_sql($sql);
}

function androgogic_sync_load_staging_user_profile($stagingid, $dateformat, $staging) {
	global $DB;
	
	// load user profile custom fields from staging table
	if ($rows = $DB->get_records('androgogic_sync_user_custom', array('stagingid'=>$stagingid))) {
		foreach ($rows as $row) {
			$staging->{'profile_field_'.$row->shortname} = $row->data;
		}
	
		if ($dateformat <> 'Y-m-d') {
			// convert custom field dates when CSV date format is not Y-m-d
			if ($rows = $DB->get_records('user_info_field', array('datatype'=>'datetime'))) {
				foreach ($rows as $row) {
					$fieldname = 'profile_field_'.$row->shortname;
					if (!empty($staging->$fieldname)) {
						$time = totara_date_parse_from_format($dateformat, $staging->$fieldname);
						$staging->$fieldname = date('Y-m-d', $time);
					}
				}
			}
		}
	}
	return $staging;
}
			
function androgogic_sync_cron_run() {
	global $CFG;

	$starthour = date('G');
	$cronhours = get_config('local_androgogic_sync', 'cron_hours');	
	if (!empty($cronhours) && in_array($starthour, explode(',', $cronhours))) {
		androgogic_sync_import_sync();
	}
	return true; 
}

function androgogic_sync_notify($runid, $notifytypes) {
    global $CFG, $DB;
/*
    $now = time();
    $dateformat = get_string('strftimedateseconds', 'langconfig');
    $notifyemailtos = get_config('local_androgogic_sync', 'notify_mailto');
    $notifyemailtos = empty($notifyemailto) ? array() : explode(',', $notifyemailto);

    // Get most recent log messages of type.
    list($sqlin, $params) = $DB->get_in_or_equal($notifytypes);
    $params = array_merge($params, array($lastnotify));
    $logitems = $DB->get_records_select('androgogic_sync_log', "logtype {$sqlin} AND runid = ?", $params,
                                        'id DESC', '*', 0, TOTARA_SYNC_LOGTYPE_MAX_NOTIFICATIONS);
    if (!$logitems) {
        // Nothing to report.
        return true;
    }

    // Build email message.
    $logcount = count($logitems);
    $sitename = get_site();
    $sitename = format_string($sitename->fullname);
    $notifytypes_str = array_map(create_function('$type', "return get_string(\$type.'plural', 'tool_totara_sync');"), $notifytypes);
    $subject = get_string('notifysubject', 'tool_totara_sync', $sitename);

    $a = new stdClass();
    $a->logtypes = implode(', ', $notifytypes_str);
    $a->count = $logcount;
    $a->since = date_format_string($lastnotify, $dateformat);
    $message = get_string('notifymessagestart', 'tool_totara_sync', $a);
    $message .= "\n\n";
    foreach ($logitems as $logentry) {
        $logentry->time = date_format_string($logentry->time, $dateformat);
        $logentry->logtype = get_string($logentry->logtype, 'tool_totara_sync');
        $message .= get_string('notifymessage', 'tool_totara_sync', $logentry) . "\n\n";
    }
    $message .= "\n" . get_string('syncloghere', 'tool_totara_sync',
            $CFG->wwwroot . '/admin/tool/totara_sync/admin/synclog.php');

    // Send emails.
    mtrace("\n{$logcount} relevant totara sync log messages since " .
            date_format_string($lastnotify, $dateformat)) . ". Sending notifications...";
    $supportuser = core_user::get_support_user();
    foreach ($notifyemailtos as $emailaddress) {
        $userto = \totara_core\totara_user::get_external_user(trim($emailaddress));
        email_to_user($userto, $supportuser, $subject, $message);
    }
*/
    return true;
}
