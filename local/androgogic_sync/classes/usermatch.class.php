<?php
/**
 * Generic User matching/deduplication class 
 *
 * Fields used for matching: idnumber, firstname, lastname, email, profile_field_dateofbirth
 *
 * @author      Keith Buss <kbuss@outlook.com>
 * @version     May 2015
 *
 **/

defined('MOODLE_INTERNAL') || die();

class UserMatch {
	
	protected $_user;
	protected $_matchcount;
	protected $_matchid;
	protected $_matchusername;
	protected $_matchfields;

	public function findMatch($user) {
		global $DB;
		
		assert(is_object($user), '$user must be an object');
		$this->_user = $user;
		$this->_matchcount = 0;
		$this->_matchid = array();
		$this->_matchusername = array();
		$this->_matchfields = array();
		
		$select = 'SELECT u.id, u.firstname, u.lastname, u.username FROM {user} u ';
		$joincustomfields = ' JOIN {user_info_data} d ON d.userid = u.id
				 JOIN {user_info_field} f ON d.fieldid = f.id ';		
		$namematch = " AND TRIM(u.firstname) = TRIM(\"$user->firstname\")
				AND TRIM(u.lastname) = TRIM(\"$user->lastname\")";
		
		// idnumber contains UID
		if (!empty($user->idnumber)) {  
			$sql = $select."WHERE u.idnumber = '$user->idnumber'";
			if ($this->_findPartialMatches($sql, array('idnumber')) > 0) {
				return $this->_matchcount;
			}
		}	

		if (!empty($user->email)) {
			$sql = $select."WHERE u.email = \"$user->email\"".$namematch;
			if ($this->_findExactMatch($sql, array('Email', 'Firstname', 'Lastname')) > 0) {
				return $this->_matchcount;
			}
		}
		
		if (!empty($user->profile_field_dateofbirth)) {
			$sql = $select.$joincustomfields."WHERE f.shortname = 'dateofbirth' 
				AND LEFT(FROM_UNIXTIME(d.data),10) = '$user->profile_field_dateofbirth'".$namematch; 
			if ($this->_findExactMatch($sql, array('DOB', 'Firstname', 'Lastname')) > 0) {
				return $this->_matchcount;
			}
		}
	}
	
	public function getMatchCount() {
		return $this->_matchcount;
	}
	
	public function isReviewRequired() {
		// review is required when matched multiple users OR matched single user and insufficient matched fields
		return ($this->_matchcount > 1 or ($this->_matchcount == 1 and count($this->_matchfields[1]) < 2));
	}
	
	public function getMatchID($index) {
		assert(is_int($index), 'index must be an integer');
		assert($index>0 && $index<=$this->_matchcount, "index $index is out of range");
		return $this->_matchid[$index];
	}	
	
	public function getMatchUsername($index) {
		assert(is_int($index), 'index must be an integer');
		assert($index>0 && $index<=$this->_matchcount, "index $index is out of range");
		return $this->_matchusername[$index];
	}		
	
	public function getMatchInfo($index) {
		// returns comma-delimitted list of matched fields
		assert(is_int($index), 'index must be an integer');
		assert($index>0 && $index<=$this->_matchcount, "index $index is out of range");
		return implode(', ', $this->_matchfields[$index]);
	}
    
	private function _findPartialMatches($sql, $matchfields) {
		global $DB;
		assert(is_array($matchfields), 'matchfields must be an array');

 		$rows = $DB->get_records_sql($sql); 
		foreach ($rows as $row) {
			$this->_matchcount++;
			$this->_matchfields[$this->_matchcount] = $matchfields;
			$this->_matchid[$this->_matchcount] = $row->id;
			$this->_matchusername[$this->_matchcount] = $row->username;
			
			if (strtolower($row->firstname) == strtolower($this->_user->firstname)) {
				$this->_matchfields[$this->_matchcount][] = 'Firstname';
			}
			if (strtolower($row->lastname) == strtolower($this->_user->lastname)) {
				$this->_matchfields[$this->_matchcount][] = 'Lastname';
			}			

			if (!empty($this->_user->profile_field_dateofbirth)) {
				$sql = "SELECT LEFT(FROM_UNIXTIME(d.data),10) 
						  FROM {user_info_data} d 
						  JOIN {user_info_field} f ON d.fieldid = f.id
						 WHERE d.userid = $row->id
						   AND f.shortname = 'dateofbirth'";
				$dateofbirth = $DB->get_field_sql($sql);
				if ($dateofbirth == $this->_user->profile_field_dateofbirth) {
					$this->_matchfields[$this->_matchcount][] = 'DOB';
				}
			}
		}	
		return $this->_matchcount;
	} 
	   
	private function _findExactMatch($sql, $matchfields) {
		global $DB;
		assert(is_array($matchfields), 'matchfields must be an array');
		
 		$rows = $DB->get_records_sql($sql); 
		foreach ($rows as $row) {
			$this->_matchcount++;
			$this->_matchfields[$this->_matchcount] = $matchfields;
			$this->_matchid[$this->_matchcount] = $row->id;
			$this->_matchusername[$this->_matchcount] = $row->username;
		}
		return $this->_matchcount;
	}
}
