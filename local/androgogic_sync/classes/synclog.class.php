<?php
/**
 * Androgogic Sync
 *
 * @author      Keith Buss <kbuss@outlook.com>
 * @version     May 2015
 *
 **/

defined('MOODLE_INTERNAL') || die();

class SyncLog {

	const TYPE_ERROR		= 'ERROR';
	const TYPE_WARNING		= 'WARNING';
	const TYPE_INFO			= 'INFO';
	const TYPE_TRACE		= 'TRACE';
	const TYPE_REVIEW		= 'REVIEW';
	const TYPE_DEBUG		= 'DEBUG';
	
	const ELEMENT_USER		= 'USER';
	const ELEMENT_POS		= 'POS';
	const ELEMENT_ORG		= 'ORG';
	const ELEMENT_COMP		= 'COMP';

	public $runid = NULL;
	public $sourceid = NULL;
	public $lmsid = NULL;
	public $stagingid = NULL;
	public $showmessage = false;
	
	public $count_created, $count_updated, $count_deleted, $count_skipped, $count_review;

	protected $_tablename = 'androgogic_sync_log';

	public function __construct() {				

		if (empty($this->_tablename)) {
			throw new Exception('log tablename is missing');
		}
		$this->runid = $this->get_last_runid() + 1;
	}

	/**
	 * Returns the run id of the last sync run
	 *
	 * @return int latest runid
	 */
	
	public function get_last_runid() {
		global $DB;

		$runid = $DB->get_field_sql("SELECT MAX(runid) FROM {{$this->_tablename}}");

		if (empty($runid)) {
			return 0;
		} else {
			return $runid;
		}
	}
	
	public function error_to_log($e) {
		$this->add_to_log(self::TYPE_ERROR, $e->getMessage().(empty($e->debuginfo)?'':', '.$e->debuginfo), $e->getTraceAsString());
	}
	
	/**
	 * Method for adding sync log messages
	 * 
	 * @param string $type the log message type
	 * @param string $action the action which caused the log message
	 * @param string $info the log message
	 * @param boolean $showmessage shows error messages on the main page when running sync if it is true
	 */
	public function add_to_log($type, $action, $info='') {
		global $DB, $OUTPUT;
	
		$log = new stdClass;
		$log->logtype = $type;
		$log->time = time();
		$log->runid = $this->runid;
		$log->action = substr($action, 0, 255);
		$log->info = $info;

		if (!empty($this->sourceid)) {
			$log->sourceid = $this->sourceid;
		}			
		if (!empty($this->lmsid)) {
			$log->lmsid = $this->lmsid;
		}		
		if (!empty($this->stagingid)) {
			$log->stagingid = $this->stagingid;
		}
		
		$DB->insert_record($this->_tablename, $log);
	
    	if ($this->showmessage && $type <> self::TYPE_TRACE) {
    	    $class = $type == self::TYPE_ERROR ? 'notifyproblem' : 'notifynotice';
    	    echo $OUTPUT->notification($type . ': ' . $action, $class);
    	    flush();
    	}
		return;
	}
}
