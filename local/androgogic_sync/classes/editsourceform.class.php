<?php
/**
 * Androgogic Sync
 *
 * @author      Keith Buss <kbuss@outlook.com>
 * @version     May 2015
 *
 **/

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/lib/formslib.php');

class editsourceform extends moodleform {

    // Define the form
    function definition() {
        global $DB;

        $mform =& $this->_form;
        $source = $this->_customdata['source'];
        $element = $this->_customdata['element'];
        
        $mform->addElement('text', 'name', get_string('name', 'local_androgogic_sync'), array("size"=>60));
        $mform->setType('name', PARAM_TEXT);
        
        if ($source == 'CSV') {
			$mform->addElement('header', 'filesettings', get_string('filesettings', 'local_androgogic_sync'));

			$mform->addElement('text', 'syncdir', get_string('syncdir', 'local_androgogic_sync'), array("size"=>60));
			$mform->setType('syncdir', PARAM_TEXT);      
        	$mform->addRule('syncdir', get_string('requiredfield', 'local_androgogic_sync'), 'required', null, 'client');
			$mform->setDefault('syncdir', '/var/integrations/transfer/from_client');

			$mform->addElement('text', 'archivedir', get_string('archivedir', 'local_androgogic_sync'), array("size"=>60));
			$mform->setType('archivedir', PARAM_TEXT);
        	$mform->addRule('archivedir', get_string('requiredfield', 'local_androgogic_sync'), 'required', null, 'client');
			$mform->setDefault('archivedir', '/var/integrations/transfer/from_client/processed');

        	$mform->addElement('text', 'csvfileprefix', get_string('csvfileprefix', 'local_androgogic_sync'));
        	$mform->setType('csvfileprefix', PARAM_TEXT);
        	$mform->addRule('csvfileprefix', null, 'required', null, 'client');
        	
			$options = array(
				','=>get_string('comma', 'local_androgogic_sync'),
				';'=>get_string('semicolon', 'local_androgogic_sync'),
				':'=>get_string('colon', 'local_androgogic_sync'),
				'\t'=>get_string('tab', 'local_androgogic_sync'),
				'|'=>get_string('pipe', 'local_androgogic_sync')
			);
			$mform->addElement('select', 'csvdelimiter', get_string('csvdelimiter', 'local_androgogic_sync'), $options);
			$mform->setType('csvdelimiter', PARAM_TEXT);

			$mform->addElement('selectyesno', 'csvheader', get_string('csvheader', 'local_androgogic_sync'));
			$mform->addHelpButton('csvheader', 'csvheader', 'local_androgogic_sync');
			$mform->setType('csvheader', PARAM_INT);
			$mform->setDefault('csvheader', 1);

			$mform->addElement('selectyesno', 'allrecords', get_string('allrecords', 'local_androgogic_sync'));
			$mform->addHelpButton('allrecords', 'allrecords', 'local_androgogic_sync');
			$mform->setType('allrecords', PARAM_INT);
			//$mform->disabledIf('allrecords', 'source', 'WEBSERVICE');
		}
		
        if ($element == 'USER') {			
			$dateformats = array(
				'Y-m-d',
				'Y/m/d',
				'Y M d',
				'Y-M-d',
				'd-m-Y',
				'd/m/Y',
				'd.m.Y',
				'd/m/y',
				'd M Y',
				'd-M-Y',
				'm/d/Y'
			);				
			
			$date = new DateTime('now');
			$options = array();
			foreach ($dateformats as $dateformat) {
				$dateformatoptions[$dateformat] = $dateformat.'   ('.$date->format($dateformat).')';
			}
			$mform->addElement('select', 'dateformat', get_string('dateformat', 'local_androgogic_sync'), $dateformatoptions);
			$mform->setType('dateformat', PARAM_TEXT);

			$mform->addElement('header', 'Default', get_string('fielddefaults', 'local_androgogic_sync'));

			$auths = get_plugin_list('auth');
			$enabled = get_string('pluginenabled', 'core_plugin');
			$disabled = get_string('plugindisabled', 'core_plugin');
			$options = array($enabled=>array(), $disabled=>array());
			foreach ($auths as $auth=>$unused) {
				if (is_enabled_auth($auth)) {
					$options[$enabled][$auth] = get_string('pluginname', "auth_{$auth}");
				} else {
					$options[$disabled][$auth] = get_string('pluginname', "auth_{$auth}");
				}
			}
			$mform->addElement('selectgroups', 'auth', get_string('auth', 'local_androgogic_sync'), $options);
        	$mform->setType('auth', PARAM_TEXT);
        	        				
			$mform->addElement('selectyesno', 'forcepasswordchange', get_string('forcepasswordchange', 'local_androgogic_sync'));
			$mform->setType('forcepasswordchange', PARAM_INT);
			
			$mform->addElement('selectyesno', 'policyagreed', get_string('policyagreed', 'local_androgogic_sync'));
			$mform->addHelpButton('policyagreed', 'policyagreed', 'local_androgogic_sync');
			$mform->setType('policyagreed', PARAM_INT);
			
			$mform->addElement('selectyesno', 'senduseremail', get_string('senduseremail', 'local_androgogic_sync'));
			$mform->addHelpButton('senduseremail', 'senduseremail', 'local_androgogic_sync');
			$mform->setType('senduseremail', PARAM_INT);
			
			$options = array(
				'0'=>get_string('suspenduser', 'local_androgogic_sync'),
				'1'=>get_string('deleteuser', 'local_androgogic_sync')
			);
			$mform->addElement('select', 'userdeletion', get_string('userdeletion', 'local_androgogic_sync'), $options);
			$mform->setType('userdeletion', PARAM_INT);
        }	
        
        if ($element == 'USER' or $element == 'ORG') {
            // organisation framework
   			$options = $DB->get_records_menu('org_framework',array(),'fullname','id,fullname');
        	if ($element == 'USER') {
        		// optional
   				array_unshift($options, '');
   				$options[0] = '';
   			}
			$mform->addElement('select', 'orgframeworkid', get_string('orgframework', 'local_androgogic_sync'), $options);
        	$mform->setType('orgframeworkid', PARAM_INT); 
        }
        
        if ($element == 'USER' or $element == 'POS') {
        
            // position framework
   			$options = $DB->get_records_menu('pos_framework',array(),'fullname','id,fullname');
        	if ($element == 'USER') {
        		// optional
   				array_unshift($options, '');
   				$options[0] = '';
   			}
			$mform->addElement('select', 'posframeworkid', get_string('posframework', 'local_androgogic_sync'), $options);
        	$mform->setType('posframeworkid', PARAM_INT);
        }
        
		// field mapping
		if ($source == 'CSV') {
			if ($element == 'USER') {
			
				//user fields
				$fieldnames = array(
					'idnumber',
					'firstname',
					'lastname',
					'alternatename',
					'middlename',
					'username',
					'auth',
					'email',
					'address',
					'city',
					'country',
					'phone1',
					'phone2',
					'orgname',
					'orgidnumber',
					'managername',
					'manageridnumber',
					'appraiseridnumber',
					'posidnumber',
					'postitle',
					'posstartdate',
					'posenddate');

			} else if (($element == 'ORG') or ($element == 'POS')) {

				// hierarchy fields
				$fieldnames = array(
					'fullname',
					'idnumber',
					'parentidnumber',
					'typeidnumber',
					'deleted');
			}
		
			$mform->addElement('header', 'fieldmapping', get_string('fieldmapping', 'local_androgogic_sync'));
			foreach ($fieldnames as $fieldname) {
				$elementname = 'field_'.$fieldname;
				$mform->addElement('text', $elementname, $fieldname, array('size'=>5));
				$mform->setType($elementname, PARAM_INT);
			}
			$mform->addRule('field_idnumber', get_string('idnumberuid', 'local_androgogic_sync'), 'required', null, 'client');
			$mform->addRule('field_idnumber', get_string('idnumberuid', 'local_androgogic_sync'), 'nonzero', null, 'client'); 
		
			if ($element == 'USER') {
				$mform->addRule('field_firstname', get_string('requiredfield', 'local_androgogic_sync'), 'required', null, 'client');
				$mform->addRule('field_firstname', get_string('requiredfield', 'local_androgogic_sync'), 'nonzero', null, 'client'); 
				$mform->addRule('field_lastname', get_string('requiredfield', 'local_androgogic_sync'), 'required', null, 'client');
				$mform->addRule('field_lastname', get_string('requiredfield', 'local_androgogic_sync'), 'nonzero', null, 'client'); 
			} else if (($element == 'ORG') or ($element == 'POS')) {
				$mform->addRule('field_fullname', get_string('requiredfield', 'local_androgogic_sync'), 'required', null, 'client');
				$mform->addRule('field_fullname', get_string('requiredfield', 'local_androgogic_sync'), 'nonzero', null, 'client');  
			}
		
			// custom field mapping
			if ($element == 'USER') {
				$rows = $DB->get_records('user_info_field');
				if (count($rows) > 0) {
					$mform->addElement('header', 'customfields', get_string('customfields', 'local_androgogic_sync'));
					foreach ($rows as $row) {
						$fieldname = 'profilefield_'.$row->shortname;
						$mform->addElement('text', $fieldname, $row->shortname, array('size'=>5));
						$mform->setType($fieldname, PARAM_INT);
					}
				} 
			}
		}
		
        /// hidden fields
        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);
        
        $mform->addElement('hidden', 'visible');
        $mform->setType('visible', PARAM_INT);
		$mform->setDefault('visible', 1);
		
       	$mform->addElement('hidden', 'source');
       	$mform->setType('source', PARAM_ALPHA);
		$mform->setDefault('source', $source);

       	$mform->addElement('hidden', 'element');
       	$mform->setType('element', PARAM_ALPHA);
		$mform->setDefault('element', $element);
        
        $this->add_action_buttons();
    }

    function validation($data, $files) {
        $errors = array();
        $data = (object)$data;

        if (!empty($data->syncdir)) {
    		if (!is_dir($data->syncdir)) {
                $errors['syncdir'] = 'invalid directory';
    		}
    	}
        if (!empty($data->archivedir)) {
    		if (!is_dir($data->archivedir)) {
                $errors['archivedir'] = 'invalid directory';
    		}
    	}
        return $errors;
    }
}
