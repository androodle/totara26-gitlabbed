<?php
/**
 * Androgogic Sync
 *
 * @author      Keith Buss <kbuss@outlook.com>
 * @version     May 2015
 *
 **/

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/lib/formslib.php');

class addsourceform extends moodleform {

    // Define the form
    function definition() {

        $mform =& $this->_form;
		
		$mform->addElement('header', 'addsource', get_string('addsource', 'local_androgogic_sync'));

		$options = array(
			'CSV'=>'CSV',
			'WEBSERVICE'=>'WEBSERVICE'
		);
		$mform->addElement('select', 'source', get_string('source', 'local_androgogic_sync'), $options);
        $mform->setType('source', PARAM_TEXT);

		$options = array(
			'USER'=>'USER',
			'ORG'=>'ORG',
			'POS'=>'POS'
		);
		$mform->addElement('select', 'element', get_string('element', 'local_androgogic_sync'), $options);
        $mform->setType('element', PARAM_TEXT);
        
        $this->add_action_buttons(false, 'Add source');
    }
}
