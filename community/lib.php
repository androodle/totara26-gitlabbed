<?php

/**
 * Finsia Professional Passport: Community Library
 *  
 * @copyright	2012+ Androgogic Pty Ltd
 * @author		Praj Basnet
 * 
 */

/**
 * Save a suggested LinkedIn group by a member in the suggested LinkedIn groups
 * table.
 * 
 * @param		User ID
 * @param		Suggested group form data
 * @return		Result of saving suggested group to DB
 *				TRUE if successful
 *				FALSE otherwise
 *  
 */

function save_suggested_linkedin_group($userid, $suggested_group_data) {
	
	global $DB;
	
	$suggested_group = new stdClass();
	$suggested_group->userid = $userid;
	$suggested_group->name = $suggested_group_data->groupname;
	$suggested_group->description = $suggested_group_data->groupdescription;
	//$suggested_group->url = $suggested_group_data->linkedinurl;
	$suggested_group->url = '';
	$suggested_group->willingtobemoderator = $suggested_group_data->willingtobemoderator;
	
	send_group_suggestion_email($suggested_group);
	return $DB->insert_record('finsia_linkedin_suggested_groups', $suggested_group);
	
}

/**
 * Send email to Finsia for any LinkedIn group suggestions
 * 
 * 
 * 
 * @return		TRUE if email sent
 *				FALSE if email not sent or could not be sent
 */

function send_group_suggestion_email($suggested_group) {
	
	global $DB, $CFG;
	
	if (isset($CFG->linkedingroupsuggestionstousername)) {
		$email_to = $DB->get_record('user', array('username' => $CFG->linkedingroupsuggestionstousername));
		$email_from = $DB->get_record('user', array('username' => 'finsia'));
	
		$subject = "New LinkedIn group suggestion: " . $suggested_group->name;
	
		$suggestor = $DB->get_record('user', array('id' => $suggested_group->userid));
	
		$message = get_string('linkedingroupsuggestion', 'local_finsia_passport', $suggested_group->description);	
		$message .= '<br/><ul>';
		$message .= '<li>Suggested by user: ' . $suggestor->firstname . ' ' . $suggestor->lastname . ' ' .
					'(username=' . $suggestor->username . ' email=' . $suggestor->email . ')</li>';
		$message .= '<li>Suggested group name: ' . $suggested_group->name . '</li>';
		$message .= '<li>Suggested group description: ' . $suggested_group->description . '</li>';	

		if ($suggested_group->willingtobemoderator == 1) {
			$willingtomoderate = 'Yes';
		}
		else {
			$willingtomoderate = 'No';
		}
		$message .= '<li>Willing to moderate: ' . $willingtomoderate . '</li>';
		$message .= '</ul>';

		email_to_user($email_to, $email_from, $subject, '', $message);
	}
}

// End of community/lib.php