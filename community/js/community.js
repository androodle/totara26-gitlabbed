/**
 * 
 * Finsia Professional Passport: Community Javascript Library
 * 
 * Javascript library for Finsia Professional Passport profile.
 * 
 * @copyright	2012+ Androgogic Pty Ltd
 * @author		Praj Basnet
 * 
 */

YUI(M.yui.loader).use('node', 'io', function(Y) { 
		
	var header = Y.all('h2.communitysection');
	
	// Figured out which of the many h2.communitysection headings was clicked on
	header.on('click', function(e) {
	
		console.log('Clicked on Triangle');
		selected_header = e.target;
		
		// Get the id of the clicked header to match it to the section to
		// be expanded or collapsed
		
		section_id = selected_header.get('id');
				
		var details_section = Y.one('div#' + section_id);
		
		// Section is visible, hide it
		if (details_section.getStyle('display') == 'block') {			
			details_section.setStyle('display', 'none');
			selected_header.removeClass('expanded');
			selected_header.addClass('collapsed');

		}
		// Section is hidden, show it
		else {			
			details_section.setStyle('display', 'block');
			selected_header.removeClass('collapsed');
			selected_header.addClass('expanded');
		}
		
	});
	
});
