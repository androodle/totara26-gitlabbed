<?php

/**
 * Finsia Professional Passport: Suggest a LinkedIn Group
 * 
 * Allow member to suggest a LinkedIn group to Finsia.
 * 
 * @copyright	2012+ Androgogic Pty Ltd
 * @author		Praj Basnet
 * 
 */

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');
}

// ----------------------------------------------------------------------------
// INCLUDES AND PARAMETERS
// ----------------------------------------------------------------------------

require_once($CFG->dirroot.'/lib/formslib.php');

// ----------------------------------------------------------------------------
// FORM 
// ----------------------------------------------------------------------------

class finsia_suggest_linkedin_group extends moodleform {
	
	 // Define the form
    function definition() {
		
        global $USER, $CFG, $COURSE;
				
        $mform =& $this->_form;
				
		// Group Name
		$mform->addElement(
			'text', 
			'groupname', 
			get_string('groupname', 'local_finsia_passport'), 
			array('size'=>'48')
		);		
		
		$mform->addRule(
			'groupname',
			get_string('groupnamerequired', 'local_finsia_passport'),
			'required',
			null,
			'client'
		);	
		
		// Group Description
		$mform->addElement(
			'textarea', 
			'groupdescription', 
			get_string('groupdescription', 'local_finsia_passport'), 
			'wrap="virtual" rows="3" cols="48"'
		);

		$mform->addRule(
			'groupdescription',
			get_string('groupdescriptionrequired', 'local_finsia_passport'),
			'required',
			null,
			'client'
		);			
		
		// Willing to moderate
		$mform->addElement(
			'advcheckbox', 
			'willingtobemoderator',
			get_string('willingtobemoderator', 'local_finsia_passport')
		);		

		/*
		// LinkedIn URL
		$mform->addElement(
			'text', 
			'linkedinurl', 
			get_string('linkedinurl', 'local_finsia_passport'), 
			array('size'=>'60')
		);		
		*/
				
		// Save/Cancel Buttons
        $this->add_action_buttons(FALSE, get_string('suggest', 'local_finsia_passport'));
    }
	
}

// End of community/forms/suggestagroup.php