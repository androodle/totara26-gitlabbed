<?php

/**
 * Finsia Professional Passport: Community Homepage
 * 
 * Display the member's LinkedIn community information comprising of their 
 * groups, suggested groups, and the ability to suggest a Finsia group for
 * LinkedIn.
 * 
 * If the member has not set up their LinkedIn account, instead display a form
 * to set up LinkedIn instead.
 * 
 * @copyright	2012+ Androgogic Pty Ltd
 * @author		Praj Basnet
 * 
 */

// ----------------------------------------------------------------------------
// INCLUDES AND PARAMETERS
// ----------------------------------------------------------------------------

require_once('../config.php'); 
require_once('lib.php');
require_once('../local/finsia_passport/lib.php');
require_once('../local/finsia_passport/linkedin/linkedin_3.2.0.class.php');
require_once('forms/suggestgroup_form.php');
$PAGE->requires->js('/community/js/community.js');

$group_suggested = optional_param('groupsuggested', 0, PARAM_INT);

// ----------------------------------------------------------------------------
// PAGE SET UP
// ----------------------------------------------------------------------------

$page_location = '/community';
$page_name = '';
$parent_page_title = '';
$page_title = get_string('community', 'local_finsia_passport');

setup_finsia_passport_page($page_location, $page_name, $page_title, TRUE);

// ----------------------------------------------------------------------------
// FORM PROCESSING
// ----------------------------------------------------------------------------

// Suggest a group form
$suggest_linkedin_group_form = new finsia_suggest_linkedin_group();

if ($suggest_linkedin_group_form->is_cancelled()) {
	redirect('index.php'); // Go back to community home page
}
else if ($suggested_group = $suggest_linkedin_group_form->get_data()) {
	save_suggested_linkedin_group($USER->id, $suggested_group);
	redirect('index.php?groupsuggested=1');
}

// ----------------------------------------------------------------------------
// CONTENT
// ----------------------------------------------------------------------------

global $OBJ_linkedin;

render_header_and_navigation($parent_page_title, $page_location, $page_name, $page_title);

echo '<div class="community-header" style="float: left;">';
echo '<h1 class="passport">' . get_string('passportcommunity', 'local_finsia_passport') . '</h1>';	
echo '</div>';
echo '<div class="community-linkedin" style="float: right; margin-right: 10%;">';
$linkedin_logo_image = $CFG->wwwroot.'/theme/anomaly_finsia/pix/linkedin.png';
//echo '<img src="' . $linkedin_logo_image . '" alt="LinkedIn"/>';
echo '</div>';
echo '<div style="clear: both;"></div>';
	

// LINKEDIN AUTHORISATION MESSAGE

if (get_linkedin_authorisation($USER->id) == FALSE) {	
	echo '<p>' . get_string('authorisesummary', 'local_finsia_passport') . '</p>';	
	echo '<p>' . get_string('authoriseexplanation', 'local_finsia_passport') . '</p>';	
	echo '<p>' . get_string('authoriseprocess', 'local_finsia_passport') . '</p>';	
}

else if(isset($OBJ_linkedin)) {

	// MY FINSIA APPROVED GROUPS
		
	echo '<h2 class="communitysection expanded" id="approvedgroups">' .  get_string('mygroups', 'local_finsia_passport') . '</h2>';
	
	$my_groups = get_linkedin_group_memberships_data($OBJ_linkedin);
	$my_groups_total = sizeof($my_groups);
	if ($my_groups_total > 0) {
		$my_groups_total_label = " ($my_groups_total)";
	}
	else {
		$my_groups_total_label = "";
	}	

	echo '<div id="approvedgroups">';	
	if($my_groups_total > 0) {
		echo '<table id="community">';
		foreach($my_groups as $group) {
			echo '<tr>';
			// Check if the group is an official Finsia group
			echo '<td width="15%">';
			if (empty($group->logo)) {
				$group->logo = $CFG->wwwroot.'/theme/anomaly_finsia/pix/linkedin-icon.png';
			}
			echo '<img src="' . $group->logo . 
						'" alt="' . $group->name . '" title="' . $group->name . '" height="60px" 
						style="display: block; margin: 0 auto; padding: 0.25em;" />';			
			echo '<td width="85%"><h2>' . get_linkedin_group_url($group->linkedingroupid, TRUE, $group->name) . '</h2>';
			echo '<p>' . $group->description . '</p></td>';
			echo '</tr>';
		}
		echo '</tbody>';
		echo '</table>';
	}	
	echo '</div><!-- #approvedgroups -->';

	// FINSIA SUGGESTED GROUPS
	/*
	$suggested_groups = get_official_finsia_groups($my_groups);
	echo '<h2 class="communitysection expanded" id="suggestedgroups">' . get_string('finsiagroups', 'local_finsia_passport') . '</h2>';	
	echo '<div id="suggestedgroups">';	
	echo '<table id="community">';
	foreach($suggested_groups as $group) {
		echo '<tr><td>';
		echo '<strong>' . get_linkedin_group_url($group->linkedingroupid, TRUE, $group->name) . '</strong>';
		echo '<br/>' . $group->description;
		echo '</td></tr>';
	}
	echo '</table>';
	echo '</div><!-- #suggestedgroups -->';	

	// SUGGEST A FINSIA GROUP

	echo '<h2 class="communitysection collapsed" id="suggestagroup">' .  get_string('suggestagroup', 'local_finsia_passport') . '</h2>';
	
	if ($group_suggested == TRUE) {
		echo '<div id="suggestagroup">';
	}
	else {
		echo '<div id="suggestagroup" style="display: none">';
	}
	echo '<table id="community">';
	echo '<tr><td>';
	if ($group_suggested == TRUE) {
		echo '<p>' . get_string('suggestionreceived', 'local_finsia_passport') . '</p>';
	}
	else {
		$suggest_linkedin_group_form->display();
	}
	echo '</td></tr>';
	echo '</table>';
	echo '</div><!-- #suggestagroup -->';		
	*/
}
else {
	echo 'LinkedIn Connections block is missing, please report this to support@androgogic.com';
	error_log("Finsia Passport: LinkedIn connections block missing from community page for user id=$USER->id");
}

// ----------------------------------------------------------------------------
// FOOTER
// ----------------------------------------------------------------------------

echo $OUTPUT->footer();

// End of community/index.php