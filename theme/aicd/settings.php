<?php

defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {

        // Logo file setting.
        $name = 'theme_aicd/logo';
        $title = get_string('logo','theme_aicd');
        $description = get_string('logodesc', 'theme_aicd');
        //$default = '';
        $setting = new admin_setting_configstoredfile($name, $title, $description, 'logo');
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);
        
        // Favicon file setting.
        $name = 'theme_aicd/favicon';
        $title = new lang_string('favicon', 'theme_aicd');
        $description = new lang_string('favicondesc', 'theme_aicd');
        $setting = new admin_setting_configstoredfile($name, $title, $description, 'favicon', 0, array('accepted_types' => '.ico'));
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);

        //Banner header text setting
        $name = 'theme_aicd/bannertext';
        $title = get_string('bannertext', 'theme_aicd');
        $description = get_string('bannertextdesc', 'theme_aicd');
        $default = '';
        $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);

        //Custom CSS file
        $name = 'theme_aicd/customcss';
        $title = get_string('customcss', 'theme_aicd');
        $description = get_string('customcssdesc', 'theme_aicd');
        $default = '';
        $setting = new admin_setting_configtextarea($name, $title, $description, $default);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);

        //Footnote setting
        $name = 'theme_aicd/footnote';
        $title = get_string('footnote', 'theme_aicd');
        $description = get_string('footnotedesc', 'theme_aicd');
        $default = '';
        $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);
}