<footer id="page-footer" class="row-fluid">
    <div id="footnotewrapper" class="container-fluid">
        <div class="footer-text">
            <div class="year">&copy;<?php echo date('Y'); ?></div>
            <?php echo $html->footnote; ?>
        </div>
        <div class="helplink"><?php echo $OUTPUT->page_doc_link(); ?></div>
        <div class="footer-logo">
            <!--<img src="<?php echo $OUTPUT->pix_url('bg_footer', 'theme')?>" alt="Unleashing Potential" />-->
        </div>
    </div> 
</footer>