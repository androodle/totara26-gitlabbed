<?php
/**
* Theme version info
*
* @package    theme
* @subpackage androtheme
*
* Documentation: http://docs.moodle.org/dev/version.php
*/

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2014120900; // The current module version (Date: YYYYMMDDXX)
$plugin->requires  = 2013050100; // Requires this Moodle version
$plugin->component = 'theme_tennis'; // Type and name of this plugin
$plugin->maturity = MATURITY_STABLE;
$plugin->release = 'v1.0'; //Version of this plugin
$plugin->dependencies = array(
    'theme_androtheme'  => 2014101000,
);