<?php
$custommenu = $OUTPUT->custom_menu();
$hascustommenu = !empty($custommenu);
$haslogininfo = (empty($PAGE->layout_options['nologininfo']));
$showmenu = empty($PAGE->layout_options['nocustommenu']);

if ($showmenu && !$hascustommenu) {
    // load totara menu
    $menudata = totara_build_menu();
    $totara_core_renderer = $PAGE->get_renderer('totara_core');
    $totaramenu = $totara_core_renderer->print_totara_menu($menudata);
}

if (right_to_left()) {
    $regionbsid = 'region-bs-main-and-post';
} else {
    $regionbsid = 'region-bs-main-and-pre';
}

$left = (!right_to_left());  // To know if to add 'pull-right' and 'desktop-first-column' classes in the layout for LTR.

?>

<header role="banner" class="navbar navbar-fixed-top moodle-has-zindex">
    <nav role="navigation" class="navbar-inner">
        <div class="container-fluid">
            <a class="brand" href="<?php echo $CFG->wwwroot;?>"><?php echo $SITE->shortname; ?></a>
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <div class="nav-collapse collapse">
                <?php if ($showmenu) { ?>
                    <?php if ($hascustommenu) { ?>
                    <div id="custommenu"><?php echo $custommenu; ?></div>
                    <?php } else { ?>
                    <div id="totaramenu"><?php echo $totaramenu; ?></div>
                    <?php } ?>
                <?php } ?>
                <ul class="nav pull-right">
                    <li><?php echo $OUTPUT->page_heading_menu(); ?></li>
                    <li class="navbar-text">
                        <?php echo $OUTPUT->login_info() ?> 
                    </li>
                    <li class="user-picture"><?php if (isloggedin()) {echo $OUTPUT->user_picture($USER, array('size'=>20));}?></li>
                </ul>
            </div>
        </div>
    </nav>
    <div id="page-header" class="clearfix">
        <div id="page-header-wrapper" class="container-fluid">
            <div class="logo">
                <a href="<?php echo $CFG->wwwroot; ?>"> <img src="<?php echo $logourl; ?>" alt="<?php echo 'Link to ', $SITE->fullname, ' homepage';?>" title="<?php echo 'Link to ', $SITE->fullname, ' homepage';?>"/></a>
            </div>
            <!--<?php echo $html->bannertext; ?>-->
            <div class="bannertext text-right">
                <img src="<?php echo $OUTPUT->pix_url('logo_learn', 'theme')?>" alt="LEARN" />
            </div>
        </div>
        
        <div id="page-navbar-wrapper" class="clearfix">    
            <div id="page-navbar" class="container-fluid">
                <nav class="breadcrumb-nav"><?php echo $OUTPUT->navbar(); ?></nav>
                <div class="breadcrumb-button"><?php echo $OUTPUT->page_heading_button(); ?></div>
            </div>
            <!--<?php echo $OUTPUT->page_heading(); ?>
            <div id="course-header">
                <?php echo $OUTPUT->course_header(); ?>
            </div>-->
        </div>
    </div>
</header>