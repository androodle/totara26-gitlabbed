<?php

defined('MOODLE_INTERNAL') || die();

function theme_androgogic_process_css($css, $theme) {
    $logo = $theme->setting_file_url('logo', 'logo');
    $css = theme_androgogic_set_logo($css, $logo);
    
    // Set the custom CSS
    if (!empty($theme->settings->customcss)) {
        $customcss = $theme->settings->customcss;
    } else {
        $customcss = null;
    }
    $css = theme_androgogic_set_customcss($css, $customcss);

    return $css;
}

function theme_androgogic_set_customcss($css, $customcss) {
    $tag = '[[setting:customcss]]';
    $replacement = $customcss;
    if (is_null($replacement)) {
        $replacement = '';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}

function theme_androgogic_set_logo($css, $logo) {
    global $OUTPUT;
    $tag = '[[setting:logo]]';
    $replacement = $logo;
    if (is_null($replacement)) {
        $replacement = $OUTPUT->pix_url('logo','theme');
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}

function theme_androgogic_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options = array()) {
    if ($context->contextlevel == CONTEXT_SYSTEM && ($filearea === 'logo' || $filearea === 'favicon')) {
        $theme = theme_config::load('androgogic');
        return $theme->setting_file_serve($filearea, $args, $forcedownload, $options);
    } else {
        send_file_not_found();
    }
}

function theme_androgogic_get_html_for_settings(renderer_base $output, moodle_page $page) {
    global $CFG;
    $return = new stdClass;

    $return->bannertext = '';
    if (!empty($page->theme->settings->bannertext)) {
        $return->bannertext = '<div class="bannertext text-right">'.$page->theme->settings->bannertext.'</div>';
    }

    $return->footnote = '';
    if (!empty($page->theme->settings->footnote)) {
        $return->footnote = '<div class="footnote text-left">'.$page->theme->settings->footnote.'</div>';
    }

    return $return;
}