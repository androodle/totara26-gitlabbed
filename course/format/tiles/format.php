<?php


/**
 * Internal functions for the Androgogic Tile format.
 *
 * May contain some code derived from the Moodle topics format from Moodle 2.2,
 * originally developed by N.D.Freear@open.ac.uk and others and Copyright 2006
 * The Open University.
 *
 * @since 2.0
 * @package    format
 * @subpackage tiles
 * @copyright 2013 Androgogic
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package
 */

require_once "$CFG->dirroot/course/format/tiles/locallib.php";

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/filelib.php');
require_once($CFG->libdir.'/completionlib.php');

$topic = optional_param('topic', -1, PARAM_RAW);

$formatconfig = get_config('format_tiles');

$context = context_course::instance($course->id);

// Ugly kludge used in the standard formats and poor form. Adds fields from the
// course format option options to the course objects and makes it look like a
// regular course object. Practically designed to confuse people maintaining the
// code. grrr...
$course = course_get_format($course)->get_course();
course_create_sections_if_missing($course, range(0, $course->numsections));

// This needs to be done after course_get_format as it appears to be using a
// cahced veersion of the course info and reverts the marker change (in the
// $course variable).
if (($marker >=0) && has_capability('moodle/course:setcurrentsection', $context) && confirm_sesskey()) {
    $course->marker = $marker;
    course_set_marker($course->id, $marker);
}

if ($topic == "all") {
    if ($formatconfig->allow_all_sections_view and $course->allsections) {
        $displaysection = "all";
    } else {
        $displaysection = 0;
    }
} else if ($topic != "-1") {
//    $displaysection = course_set_display($course->id, $topic);
    $displaysection = (int) $topic;
} else {
//    $displaysection = format_tiles_course_get_display($course->id);
    $displaysection = 0;
}

$renderer = $PAGE->get_renderer('format_tiles');

$canviewhidden = has_capability('moodle/course:viewhiddensections', $context);

// Unlike other formats (like topics) tile has very different layouts depending
// on what we are doing.
if ($PAGE->user_is_editing()) {
    // User has editting on
    $renderer->print_course_editing_page($course, $sections, $mods, $modnames, $modnamesused, $canviewhidden);
} elseif ($displaysection === 0) {
    // Summary page
    $renderer->print_course_summary_page($course, $sections, $mods, $modnames, $modnamesused, $canviewhidden);
} elseif ($displaysection === "all") {
    // All sections view
    $renderer->print_all_sections_page($course, $sections, $mods, $modnames, $modnamesused, $canviewhidden);
} else {
    // We are displaying an individual section.
    $renderer->print_section_page($course, $sections, $displaysection, $mods, $modnames, $modnamesused, $canviewhidden);
}

$PAGE->requires->js('/course/format/tiles/format.js');